<?php

namespace Tests\SalarieBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SalarieControllerTest extends WebTestCase
{

    private $mockCreatedSalarie;
    private $mockSalarie;
    private $mockFile;
    private $container;

    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->container = self::$kernel->getContainer();
        $this->mockFile = $this->container->getParameter('mock_file_dir') . '/Salarie';

        $fileSalarieJson = file_get_contents($this->mockFile . '/salarie.json');
        $this->mockSalarie = json_decode($fileSalarieJson, true);
        $this->mockSalarie['idNirDef'] = rand(000000000000001,999999999999999);

        $fileCreatedSalarieJson = file_get_contents($this->mockFile . '/createdSalarie.json');
        $this->mockCreatedSalarie = json_decode($fileCreatedSalarieJson);

    }

    public function testCreateANewEmployeeWithAttachedChildren(): void
    {

        // Try to create a new employee (salarié) from mockSalarie
        $response = $this->container->get('api_nebula')->request(
            'POST',
            '/salaries',
            $this->mockSalarie
        );

        $this->assertIsObject($response);
        $this->assertObjectHasAttribute('idMatricule', $response);

        // After create we save it in salarie.json
        file_put_contents(
            $this->mockFile . '/createdSalarie.json',
            json_encode($response),
            LOCK_EX
        );
    }

    /**
     * @depends testCreateANewEmployeeWithAttachedChildren
     */
    public function testGetListOfEmployees(): void
    {
        $salaries = $this->container->get('api_nebula')->request(
            'GET',
            '/salaries'
        );

        $this->assertIsArray($salaries);
        $this->assertIsObject($salaries[0]);
        $this->assertObjectHasAttribute('idMatricule', $salaries[0]);

    }

    /**
     * @depends testGetListOfEmployees
     */
    public function testUpdatePartOfEmployeeLikeYourName(): void
    {

        $response = $this->container->get('api_nebula')->request(
            'PATCH',
            sprintf('/salaries/%d', $this->mockCreatedSalarie->idMatricule),
            [ 'liNom' => 'UPDATED' ]
        );

        $this->assertIsObject($response);
        $this->assertObjectHasAttribute('liNom', $response);
        $this->assertEquals('UPDATED', $response->liNom);

    }

    /**
     * @depends testUpdatePartOfEmployeeLikeYourName
     */
    public function testDeleteAnEmployee(): void
    {
        // We remove the mock created
        $response = $this->container->get('api_nebula')->request(
            'DELETE',
            sprintf('/salaries/%d', $this->mockCreatedSalarie->idMatricule)
        );

       $this->assertIsObject($response);
       $this->assertObjectHasAttribute('message', $response);
       $this->assertStringContainsString('Suppression', $response->message);

    }

    /**
     * @depends testDeleteAnEmployee
     */
    public function testReturnANotFoundForTheEmployeeWhoHasJustBeenDeleted() : void
    {
        // After remove we must get not found.
        $response = $this->container->get('api_nebula')->request(
            'GET',
            sprintf('/salaries/%d', $this->mockCreatedSalarie->idMatricule)
        );

        $this->assertEquals(404, $response);
    }

}
