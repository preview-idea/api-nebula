<?php

namespace Tests\Core\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthControllerTest extends WebTestCase
{
    protected $container;

    public function setUp(): void
    {
       self::bootKernel();
       $this->container = self::$kernel->getContainer();

    }

    public function testitShouldReturnAccessToken()
    {
        $token = $this->container->get('api_nebula')->getAccessToken();
        $this->assertStringContainsString('Bearer', $token);
    }

    /**
     *
     * @depends testitShouldReturnAccessToken
     * @dataProvider urlProviderForSalarie
     * @param $url
     */
    public function testPageIsSuccessfulForRoutesSalarie($url)
    {
        $apiNebula = $this->container->get('api_nebula');
        $token = $apiNebula->getAccessToken();

        if ($token) :
            $apiNebula->setHeaders([ 'Authorization' => $token ]);
        endif;

        $client = $apiNebula->getClient();
        $response = $client->get($url);

        $this->assertContainsEquals($response->getStatusCode(), [200, 204], $url);

    }

    public function urlProviderForSalarie()
    {
        yield['/salaries'];
        yield['/salaries/all'];
        yield['/salaries/horscee'];
        yield['/salaries/inactifs'];
        yield['/salaries/actifs'];
        yield['/salaries/nomcomplet'];
        yield['/salaries/mandat'];
        yield['/salaries/protection'];
        yield['/salaries/nblogs'];

    }

}
