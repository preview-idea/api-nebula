<?php

namespace Core\Tools\Monolog;

use Core\Entity\Monolog\ApiLogEnt;
use Core\Entity\Monolog\ApiLogLig;
use Core\Tools\MessageDB\MessageDB;
use Monolog\Handler\AbstractProcessingHandler;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MonologDBHandler extends AbstractProcessingHandler
{

    private $em;
    private $initialized;
    private $container;
    private $channel = 'nebula_db';

    /**
     * MonologDBHandler constructor.
     * @param ManagerRegistry $em
     * @param ContainerInterface $container
     */
    public function __construct(ManagerRegistry $em, ContainerInterface $container)
    {
        parent::__construct();
        $this->em = $em->getManager('api_log');
        $this->container = $container;
    }

    /**
     * Called when writing to our database
     * @param array $record
     */
    protected function write(array $record)
    {

        if (!$this->initialized) :
            $this->initialize();
        endif;

        if ($this->channel != $record['channel']) :
            return;
        endif;

        $monolog_ent = new ApiLogEnt();

        $monolog_ent->setLiStatus($record['context']['li_status']);
        $monolog_ent->setLiUserlaunch($record['context']['li_user_launch']);
        $monolog_ent->setLiJob($record['context']['li_job']);
        $monolog_ent->setIdStatus($record['context']['id_status']);

        $this->em->persist($monolog_ent);
        $this->em->flush();

        foreach ($record['context']['api_log_lig'] as $level => $lig) :

            /** @var MessageDB $msg_db */
            $msg_db = $this->container->get('exception_message_db');
            $msg_db->traitMessageDB($record['message']);

            // INSERT DETAIL MSG ERROR
            $monolog_lig = new ApiLogLig();
            $monolog_lig->setIdJob($monolog_ent);
            $monolog_lig->setLiOrigin($lig['li_origin']);
            $monolog_lig->setLiLevel($level);
            $monolog_lig->setLiFunction($lig['li_function']);
            $monolog_lig->setLiMessage($msg_db->getMsgCut());
            $monolog_lig->setLiClasse($lig['li_classe']);

            $this->em->persist($monolog_lig);
            $this->em->flush();

            // INSERT SCRIPT SQL
            $monolog_lig2 = new ApiLogLig();
            $monolog_lig2->setIdJob($monolog_ent);
            $monolog_lig2->setLiOrigin('SQLExcept');
            $monolog_lig2->setLiLevel($level);
            $monolog_lig2->setLiFunction($lig['li_function']);
            $monolog_lig2->setLiMessage($msg_db->getQuerySql());
            $monolog_lig2->setLiClasse($lig['li_classe']);

            $this->em->persist($monolog_lig2);
            $this->em->flush();

        endforeach;

    }

    private function initialize()
    {
        $this->initialized = true;
    }
}