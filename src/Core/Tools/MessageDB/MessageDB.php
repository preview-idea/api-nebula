<?php


namespace Core\Tools\MessageDB;


class MessageDB
{
    const MSG_START = 38;

    private $message,
        $msg_length,
        $msg_cut,
        $msg_error,
        $msg_detail,
        $query_sql;

    public function traitMessageDB($message)
    {
        $this->message = $message;
        $this->msg_length = strrpos($this->getMessage(), 'SQLSTATE') - (self::MSG_START + 3);
        $this->msg_cut = substr($this->getMessage(), strrpos($this->getMessage(), 'ERROR:'));

        // Trait query SQL
        $this->traitQuerySQL();

        // Trait message ERROR
        $this->traitMessageError();

        // Trait message DETAIL
        $this->traitMessageDetail();
    }

    private function traitQuerySQL()
    {
        $query_sql = substr($this->message, self::MSG_START, $this->getMsgLength());
        $this->setQuerySql($query_sql);
    }

    private function traitMessageError()
    {
        $msg_error = ucwords(substr($this->getMsgCut(), 8, strpos($this->getMsgCut(), 'DETAIL: ') - 9), 1);
        $this->setMsgError($msg_error);
    }

    private function traitMessageDetail()
    {
        $msg_detail = str_replace('DETAIL: ', ',', substr($this->getMsgCut(), strrpos($this->getMsgCut(), 'DETAIL:')));
        $this->setMsgDetail($msg_detail);
    }

    /**
     * @return mixed
     */
    public function getMsgError()
    {
        return $this->msg_error;
    }

    /**
     * @param mixed $msg_error
     */
    public function setMsgError($msg_error)
    {
        $this->msg_error = $msg_error;
    }

    /**
     * @return mixed
     */
    public function getMsgDetail()
    {
        return $this->msg_detail;
    }

    /**
     * @param mixed $msg_detail
     */
    public function setMsgDetail($msg_detail)
    {
        $this->msg_detail = $msg_detail;
    }

    /**
     * @return mixed
     */
    public function getQuerySql()
    {
        return $this->query_sql;
    }

    /**
     * @param mixed $query_sql
     */
    public function setQuerySql($query_sql)
    {
        $this->query_sql = $query_sql;
    }

    /**
     * @return bool|int
     */
    public function getMsgLength()
    {
        return $this->msg_length;
    }

    /**
     * @return mixed
     */
    public function getMsgCut()
    {
        return $this->msg_cut;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

}