<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 26/07/2018
 * Time: 17:35
 */

namespace Core\EventListener;

use Core\Entity\Utilisateurs;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

class JWTCreatedListener
{

    /**
     * @param JWTCreatedEvent $event
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        /** @var Utilisateurs $user */
        $user = $event->getUser();

        $payload = $event->getData();
        $payload['liNomUser'] = $user->getLiNomutilisateur();
        $payload['roles'] = $user->getRoles();
        $payload['idMatricule'] = $user->getIdMatricule();
        $payload['idUser'] = $user->getIdUtilisateur();
        $payload['exp'] = strtotime('10 hours');

        $event->setData($payload);

    }


}