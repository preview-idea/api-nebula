<?php

namespace Core\EventListener;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ExceptionListener
{
    /** @var \Psr\Log\LoggerInterface $logger */
    private $logger;

    /** @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage $security */
    private $security;

    public function __construct(LoggerInterface $logger, TokenStorageInterface $security)
    {
        $this->logger = $logger;
        $this->security = $security;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();

        $apiLogLig = [];
        $countTrace = 0;
        foreach ($exception->getTrace() as $trace) :

            if ($countTrace): continue; endif;

            $apiLogLig[] = [
                'li_origin'     => !empty($trace['file']) ? mb_strimwidth(basename($trace['file']),0,10) : mb_strimwidth(basename($exception->getFile()),0,10),
                'li_function'   => !empty($trace['function']) ? mb_strimwidth($trace['function'],0,50) : '',
                'li_classe'     => !empty($trace['class']) ? mb_strimwidth(basename(str_replace('\\','/', $trace['class'])),0,50) : ''
            ];

            $countTrace++;

        endforeach;

        $loggerFunction = 'error';
        if ( ($exception instanceof HttpExceptionInterface) && ($exception->getStatusCode() <= 500)) :
            $loggerFunction = 'warning';
        endif;

        $this->logger->$loggerFunction($exception->getMessage(), [
            'li_job' => sprintf('%s %s', $event->getRequest()->getMethod(), $event->getRequest()->getRequestUri()),
            'id_status' => ($loggerFunction === 'error') ? 3 : 2,
            'li_user_launch' => (!empty($this->security->getToken())) ? ((is_string($this->security->getToken()->getUser())) ? mb_strimwidth($this->security->getToken()->getUser(),0,30) : mb_strimwidth($this->security->getToken()->getUser()->getLiNomutilisateur(),0,30)) : '',
            'li_status' => ($exception instanceof HttpExceptionInterface) ? mb_strimwidth(sprintf('%s - %s', $exception->getStatusCode(), Response::$statusTexts[$exception->getStatusCode()]),0,30) : mb_strimwidth(sprintf('%s - %s', Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR]),0,30),
            'api_log_lig' => $apiLogLig,
        ]);

        // Customize your response object to display the exception details
        $response = new JsonResponse([
            'message' => $exception->getMessage()
        ]);
        $response->headers->set('Content-Type', 'application/problem+json');

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) :
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        else :
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        endif;

        // sends the modified response object to the event
        $event->setResponse($response);
    }
}
