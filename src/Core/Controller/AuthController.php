<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 19/12/2017
 * Time: 11:12
 */

namespace Core\Controller;

use Core\Entity\Utilisateurs;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class AuthController extends Controller
{

    /**
     * @Rest\View()
     * @Rest\Get("/", name="app_home")
     */
    public function homeAction()
    {
        return $this->redirectToRoute('verify_token',[]);
    }

    /**
     * @Rest\View()
     * @Rest\Post("/auth/verify")
     * @param Request $request
     * @return array|View
     */
    public function verifyTokenAction(Request $request)
    {

        // TODO: verify if user has allowed to access to the API
        // Code removed for security reasons

    }

    private function newTokenAccess(Utilisateurs $user)
    {

        $token = $this->get('lexik_jwt_authentication.jwt_manager')->create($user);
        $authHandler = $this->get('lexik_jwt_authentication.handler.authentication_success');
        $authHandler->handleAuthenticationSuccess($user, $token);

        return $token;

    }

    /**
     * @param Utilisateurs $user
     * @return mixed
     */
    private function verifyLifeCycleToken(Utilisateurs $user)
    {
        // TODO: verify cycle life of token
        // Code removed for security reasons

        return $payload;
    }

    /**
     * @return View
     */
    private function userNotFound()
    {
        return View::create([
            'message' => 'Utilisateur introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }

}