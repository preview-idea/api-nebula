<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 19/12/2017
 * Time: 11:17
 */

namespace Core\Controller;

use Core\Entity\Niveaux\NiveauxEnt;
use Core\Entity\Utilisateurs;
use Core\Repository\Niveaux\NiveauxEntRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ApplicationController extends Controller
{
    /** @var NiveauxEntRepository $niveauHierarchieRepo */
    protected $niveauHierarchieRepo;

    /** @var \Doctrine\Common\Persistence\ObjectManager $manager */
    protected $manager;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        if (!$this->manager instanceof EntityManager) :
            $this->manager = $managerRegistry->getManager();
            $this->niveauHierarchieRepo = $this->manager->getRepository(NiveauxEnt::class);

        endif;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/app/niveaux/hierarchie")
     * @return array
     */
    public function accessNiveauxHierarchieListAction()
    {
        return $this->niveauHierarchieRepo->getListNiveauHierarchie();
    }

}
