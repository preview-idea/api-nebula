<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 04/06/2018
 * Time: 12:40
 */

namespace Core\Controller;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SalarieBundle\Repository\Param\ParamAdministrationRepository;
use Symfony\Component\HttpFoundation\Request;

class FilesController extends Controller
{

    /** @var \Doctrine\Common\Persistence\ObjectManager $manager */
    protected $manager;

    /** @var ParamAdministrationRepository $objParamAdminFichiers */
    protected $objParamAdminFichiers;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        if (!$this->manager instanceof EntityManager) :
            $this->manager = $managerRegistry->getManager('salarie');
            $this->objParamAdminFichiers =  $this->manager->getRepository('SalarieBundle:Param\\ParamAdministrationFichiers');
        endif;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/files/upload/forms/{tableRef}")
     * @param Request $request
     * @return object|array
     */
    public function filesFormByTableRefAction(Request $request)
    {
        return $this->createFormatForForms(
            $this->objParamAdminFichiers->getTablesNotEmptyDescriptionAndUiElements(
                $request->get('tableRef')
            )
        );
    }

    /**
     * @param $listElementForm
     * @return array
     */
    protected function createFormatForForms($listElementForm)
    {

        $listElement = [];
        foreach ($listElementForm as $element) :
            $listElement[$element['idTable']]['columns'][] = array_slice($element, 3, 10, true);

        endforeach;

        return count($listElement) ? array_values($listElement)[0] : [];

    }

}