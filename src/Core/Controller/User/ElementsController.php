<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 21/08/2018
 * Time: 17:43
 */

namespace Core\Controller\User;

use Core\Controller\UserController;
use Core\Entity\Combi\CombiNiveauxElementsExceptions;
use Core\Entity\Utilisateurs;
use Core\Form\UtilisateursType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Core\Entity\UtilisateursNiveauxElements;
use Symfony\Component\HttpFoundation\Request;

class ElementsController extends UserController
{

    /**
     * @Rest\View()
     * @Rest\Get("/admin/utilisateurs/{idUtilisateur}/elements", requirements={"idUtilisateur"="\d+"})
     * @param Request $request
     * @return View|object
     */
    public function userElementsListAction(Request $request)
    {
        /** @var Utilisateurs $user */
        $user = $this->userOneAction($request);

        if ($user instanceof View) :
            return $user;
        endif;

        $em = $this->getDoctrine()->getManager();

        // Get all elements in the utilisateursNiveauxElements
        $elements = $em->getRepository(UtilisateursNiveauxElements::class)
            ->getAllByUser($user->getIdUtilisateur());

        // Get all Elements in the CombiNiveauxElementsExceptions
        $exceptions = $em->getRepository(CombiNiveauxElementsExceptions::class)
            ->getAllByUser($user->getIdUtilisateur());

        $allElements = array_merge($elements, $exceptions);

        return $allElements;

    }

    /**
     * @Rest\View()
     * @Rest\Post("/admin/utilisateurs/{idUtilisateur}/elements/default", requirements={"idUtilisateur"="\d+"})
     * @param Request $request
     * @return object
     */
    public function userElementDefaultNewAction(Request $request)
    {
        /** @var Utilisateurs $user */
        $user = $this->userOneAction($request);

        if ($user instanceof View) :
            return $user;
        endif;

        // Clear elements in table utilisateurs_niveaux_eleemnts
        $this->clearUserElements($user->getIdUtilisateur());

        $form = $this->createForm(UtilisateursType::class, $user);
        $form->submit($request->request->all(), false);

        if ($form->isValid()) :
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $user;

        endif;

        return $form;

    }

    /**
     * @Rest\View()
     * @Rest\Post("/admin/utilisateurs/{idUtilisateur}/elements/exception", requirements={"idUtilisateur"="\d+"})
     * @param Request $request
     * @return object
     */
    public function userElementExceptionNewAction(Request $request)
    {
        /** @var Utilisateurs $user */
        $user = $this->userOneAction($request);

        if ($user instanceof View) :
            return $user;
        endif;

        // Clear elements in table utilisateurs_niveaux_eleemnts
        $this->clearUserElementsException($user->getIdUtilisateur());

        $form = $this->createForm(UtilisateursType::class, $user);
        $form->submit($request->request->all(), false);

        if ($form->isValid()) :
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $user;

        endif;

        return $form;

    }

    /**
     * @param $idUtilisateur
     */
    private function clearUserElements($idUtilisateur)
    {

        $em = $this->getDoctrine()->getManager();
        $elements = $em->getRepository(UtilisateursNiveauxElements::class)
            ->findBy(['idUtilisateur' => $idUtilisateur]);

        foreach ($elements as $element) :
            $em->remove($element);
            $em->flush();
        endforeach;

    }

    /**
     * @param $idUtilisateur
     */
    private function clearUserElementsException($idUtilisateur)
    {

        $em = $this->getDoctrine()->getManager();
        $elements = $em->getRepository(CombiNiveauxElementsExceptions::class)
            ->findBy(['idUtilisateur' => $idUtilisateur]);

        foreach ($elements as $element) :
            $em->remove($element);
            $em->flush();
        endforeach;

    }

}