<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 21/08/2018
 * Time: 17:43
 */

namespace Core\Controller\User;

use Core\Controller\UserController;
use Core\Entity\Combi\CombiNiveauxElements;
use Core\Entity\Niveaux\NiveauxEnt;
use Core\Entity\UtilisateursQualifcontrat;
use Core\Entity\Views\Combi\ViewCombiNiveauxElementsWithUnusedNiveauxLig;
use Core\Entity\Views\ViewNiveauxLigParent;
use Core\Form\Combi\CombiNiveauxElementsType;
use Core\Form\UtilisateursQualifcontratType;
use Core\Repository\Combi\CombiNiveauxElementsRepository;
use Core\Repository\Niveaux\NiveauxEntRepository;
use Core\Repository\UtilisateursQualifcontratRepository;
use Core\Repository\Views\Combi\ViewCombiNiveauxElementsWithUnusedNiveauxLigRepository;
use Core\Repository\Views\ViewNiveauxLigParentRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Core\Entity\Views\Param\ViewParamQualifcontratNonUtilise;
use Core\Repository\Views\Param\ViewParamQualifcontratNonUtiliseRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ParamUserController extends UserController
{

    /** @var UtilisateursQualifcontratRepository $objUserQualifContratRepo */
    protected $objUserQualifContratRepo;

    /** @var NiveauxEntRepository $objUserNiveauEntRepo */
    protected $objUserNiveauEntRepo;

    /** @var ViewParamQualifcontratNonUtiliseRepository $viewParamQualifcontratNonUtiliseRepository */
    protected $viewParamQualifcontratNonUtiliseRepository;

    /** @var ViewCombiNiveauxElementsWithUnusedNiveauxLigRepository $combiNiveauxElementsWithUnusedNiveauxLigRepository */
    protected $combiNiveauxElementsWithUnusedNiveauxLigRepository;

    /** @var ViewNiveauxLigParentRepository $viewNiveauxLigParentRepository */
    protected $viewNiveauxLigParentRepository;

    /** @var CombiNiveauxElementsRepository $combiNiveauxElementsRepository */
    protected $combiNiveauxElementsRepository;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry);
        $this->objUserQualifContratRepo = $this->manager->getRepository(UtilisateursQualifcontrat::class);
        $this->objUserNiveauEntRepo = $this->manager->getRepository(NiveauxEnt::class);
        $this->viewParamQualifcontratNonUtiliseRepository = $this->manager->getRepository(ViewParamQualifcontratNonUtilise::class);
        $this->combiNiveauxElementsWithUnusedNiveauxLigRepository = $this->manager->getRepository(ViewCombiNiveauxElementsWithUnusedNiveauxLig::class);
        $this->viewNiveauxLigParentRepository = $this->manager->getRepository(ViewNiveauxLigParent::class);
        $this->combiNiveauxElementsRepository = $this->manager->getRepository(CombiNiveauxElements::class);
    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/utilisateurs/qualifcontrat")
     */
    public function userQualifContratListAction()
    {
        $usersQualifContrat = (method_exists($this->objUserQualifContratRepo, 'getAllWithoutJoin')) ? $this->objUserQualifContratRepo->getAllWithoutJoin() : $this->objUserQualifContratRepo->findAll();
        return $usersQualifContrat;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/utilisateurs/niveauxent")
     */
    public function userNiveauxEntListAction()
    {
        $usersNiveauxEnt = (method_exists($this->objUserNiveauEntRepo, 'getAllWithoutJoin')) ? $this->objUserNiveauEntRepo->getAllWithoutJoin() : $this->objUserNiveauEntRepo->findAll();
        return $usersNiveauxEnt;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/utilisateurs/qualifcontrat/unused")
     */
    public function userUnusedQualifContratListAction()
    {
        return $this->viewParamQualifcontratNonUtiliseRepository->getParams();
    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/utilisateurs/combiniveauxelements")
     */
    public function userCombiNiveauxElementsListAction()
    {
        return $this->combiNiveauxElementsWithUnusedNiveauxLigRepository->getCombi();
    }

    /**
     * @Rest\View()
     * @Rest\Post("/admin/utilisateurs/niveauxligparent")
     * @param Request $request
     * @return object
     */
    public function userNiveauxLigParentListAction(Request $request)
    {
        $viewNiveauxLigParent = $this->viewNiveauxLigParentRepository->getNiveauxLigParent(
            $request->get('idNiveau'),
            $request->get('idElement')
        );
        return $viewNiveauxLigParent;
    }

    /**
     * @Rest\View()
     * @Rest\Post("/admin/utilisateurs/qualifcontrat")
     * @param Request $request
     * @return object|View
     */
    public function userQualifContratNewAction(Request $request)
    {
        $usersQualifContrat = new UtilisateursQualifcontrat();

        $form = $this->createForm(UtilisateursQualifcontratType::class, $usersQualifContrat);
        $form->submit($request->request->all());

        if ($form->isValid()) :
            $this->manager->persist($usersQualifContrat);
            $this->manager->flush();

            return $usersQualifContrat;
        endif;

        return $form;
    }

    /**
     * @Rest\View()
     * @Rest\Post("/admin/utilisateurs/combiniveauxelements")
     * @param Request $request
     * @return object|View
     */
    public function userCombiNiveauxElementsNewAction(Request $request)
    {
        $combiNiveauxElement = new CombiNiveauxElements();

        $form = $this->createForm(CombiNiveauxElementsType::class, $combiNiveauxElement);
        $form->submit($request->request->all());

        if ($form->isValid()) :
            $this->manager->persist($combiNiveauxElement);
            $this->manager->flush();

            return $combiNiveauxElement;
        endif;

        return $form;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/utilisateurs/combiniveauxelements/{idLigneCombiNiveauxElements}", requirements={"idLigneCombiNiveauxElements"="\d+"})
     * @param Request $request
     * @return View|CombiNiveauxElements
     */
    public function userCombiNiveauxElementsOneAction(Request $request)
    {
        $combiNiveauxElements = $this->combiNiveauxElementsRepository->find(
            $request->get('idLigneCombiNiveauxElements')
        );

        /** @var CombiNiveauxElements $combiNiveauxElements */
        if(!$combiNiveauxElements) :
            return $this->userCombiNiveauxElementsNotFound();

        endif;

        return $combiNiveauxElements;
    }

    /**
     * @Rest\View()
     * @Rest\Put("/admin/utilisateurs/combiniveauxelements/{idLigneCombiNiveauxElements}", requirements={"idLigneCombiNiveauxElements"="\d+"})
     * @param Request $request
     * @param bool $clearMissing
     * @return View|object
     */
    public function userCombiNiveauxElementsUpdateAction(Request $request, $clearMissing = true)
    {
        $combiNiveauxElements = $this->userCombiNiveauxElementsOneAction($request);

        if ($combiNiveauxElements instanceof View) :
            return $combiNiveauxElements;
        endif;

        $form = $this->createForm(CombiNiveauxElementsType::class, $combiNiveauxElements);
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) :
            $this->manager->persist($combiNiveauxElements);
            $this->manager->flush();

            return $combiNiveauxElements;
        endif;

        return $form;
    }

    /**
     * @Rest\View()
     * @Rest\Delete("/admin/utilisateurs/combiniveauxelements/{idLigneCombiNiveauxElements}", requirements={"idLigneCombiNiveauxElements"="\d+"})
     * @param Request $request
     * @return View|array
     */
    public function userCombiNiveauxElementsRemoveAction(Request $request)
    {
        $combiNiveauxElements = $this->combiNiveauxElementsRepository->find(
            $request->get('idLigneCombiNiveauxElements')
        );

        /** @var CombiNiveauxElements $combiNiveauxElements */
        if(!$combiNiveauxElements) :
            return $this->userCombiNiveauxElementsNotFound();
        endif;

        $this->manager->remove($combiNiveauxElements);
        $this->manager->flush();

        return View::create(
            ['message' => 'Suppression effectuée'],
            Response::HTTP_OK
        );

    }

    /**
     * @Rest\View()
     * @Rest\Patch("/admin/utilisateurs/combiniveauxelements/{idLigneCombiNiveauxElements}", requirements={"idLigneCombiNiveauxElements"="\d+"})
     * @param Request $request
     * @return View|object
     */
    public function userCombiNiveauxElementsUpdatePatchAction(Request $request)
    {
        return $this->userCombiNiveauxElementsUpdateAction($request, false);
    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/utilisateurs/qualifcontrat/{idQualifcontrat}", requirements={"idQualifcontrat"="\d+"})
     * @param Request $request
     * @return View|UtilisateursQualifcontrat
     */
    public function userQualifContratOneAction(Request $request)
    {
        $usersQualifContrat = $this->objUserQualifContratRepo->find(
            $request->get('idQualifcontrat')
        );

        /** @var UtilisateursQualifcontrat $usersQualifContrat */
        if(!$usersQualifContrat) :
            return $this->userQualifContratNotFound();

        endif;

        return $usersQualifContrat;
    }

    /**
     * @Rest\View()
     * @Rest\Put("/admin/utilisateurs/qualifcontrat/{idQualifcontrat}", requirements={"idQualifcontrat"="\d+"})
     * @param Request $request
     * @param bool $clearMissing
     * @return View|object
     */
    public function userQualifContratUpdateAction(Request $request, $clearMissing = true)
    {
        $usersQualifContrat = $this->userQualifContratOneAction($request);

        if ($usersQualifContrat instanceof View) :
            return $usersQualifContrat;
        endif;

        $form = $this->createForm(UtilisateursQualifcontratType::class, $usersQualifContrat);
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) :
            $this->manager->persist($usersQualifContrat);
            $this->manager->flush();

            return $usersQualifContrat;
        endif;

        return $form;
    }

    /**
     * @Rest\View()
     * @Rest\Patch("/admin/utilisateurs/qualifcontrat/{idQualifcontrat}", requirements={"idQualifcontrat"="\d+"})
     * @param Request $request
     * @return View|object
     */
    public function userQualifContratUpdatePatchAction(Request $request)
    {
        return $this->userQualifContratUpdateAction($request, false);
    }

    /**
     * @Rest\View()
     * @Rest\Delete("/admin/utilisateurs/qualifcontrat/{idQualifcontrat}", requirements={"idQualifcontrat"="\d+"})
     * @param Request $request
     * @return View|array
     */
    public function userQualifContratRemoveAction(Request $request)
    {
        $usersQualifContrat = $this->objUserQualifContratRepo->find(
            $request->get('idQualifcontrat')
        );

        /** @var UtilisateursQualifcontrat $usersQualifContrat */
        if(!$usersQualifContrat) :
            return $this->userQualifContratNotFound();
        endif;

        $this->manager->remove($usersQualifContrat);
        $this->manager->flush();

        return View::create(
            ['message' => 'Suppression effectuée'],
            Response::HTTP_OK
        );

    }

    /**
     * @return View
     */
    protected function userQualifContratNotFound()
    {
        return View::create(
            ['message' => 'Utilisateur Qualif contrat introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }

    /**
     * @return View
     */
    protected function userCombiNiveauxElementsNotFound()
    {
        return View::create(
            ['message' => 'Qualif niveaux élement introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }



}