<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 07/12/2018
 * Time: 16:45
 */

namespace Core\Controller\User;

use Core\Controller\UserController;
use FOS\RestBundle\Controller\Annotations as Rest;

class ApplicationController extends UserController
{
    /**
     * @Rest\View()
     * @Rest\Get("/admin/utilisateurs/apps")
     * @return array
     */
    public function userAppsListAction()
    {

        $em = $this->getDoctrine()->getManager();
        $apps = $em->getRepository('CoreNebula:Application\Applications')
            ->findBy([], ['liApplication' => 'ASC']);

        return $apps;
    }
}