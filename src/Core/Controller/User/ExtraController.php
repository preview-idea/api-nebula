<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 22/08/2018
 * Time: 11:49
 */

namespace Core\Controller\User;

use Core\Controller\UserController;
use Core\Repository\UtilisateursRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;

class ExtraController extends UserController
{

    /**
     * @Rest\View()
     * @Rest\Post("/utilisateurs/sign")
     * @param Request $request
     * @return object|View
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function userListSignataireAction(Request $request)
    {
        $sign = $this->objUserRepo->getListOfSignataires($request->get('filter'));

        if (!$sign) :
            return $this->userNotFound();
        endif;

        return $sign;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/utilisateurs/{idUtilisateur}/perimeter")
     * @param Request $request
     * @return array
     */
    public function userListPerimeterAction(Request $request)
    {
        return $this->objUserRepo->getUserPerimeterByidMatricule(
            $request->get('idUtilisateur')
        );

    }

}