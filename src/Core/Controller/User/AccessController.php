<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 22/08/2018
 * Time: 11:49
 */

namespace Core\Controller\User;

use Core\Controller\UserController;
use Core\Entity\Application\ApplicationsUtilisateursDroits;
use Core\Entity\Niveaux\NiveauxEnt;
use Core\Entity\Niveaux\NiveauxLig;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

class AccessController extends UserController
{

    /**
     * @Rest\View()
     * @Rest\Get("/admin/utilisateurs/niveaux")
     * @return object
     */
    public function getListNiveauxAction()
    {
        $em = $this->getDoctrine()->getManager();
        $niveaux = $em->getRepository(NiveauxEnt::class)
            ->getList();

        return $niveaux;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/utilisateurs/niveaux/{idNiveau}/elements", requirements={"idNiveau"="\d+"})
     * @param Request $request
     * @return object
     */
    public function getElementsNiveauDefaultAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $elements = $em->getRepository(NiveauxLig::class)
            ->getAllByNiveau($request->get('idNiveau'));

        return $elements;

    }


    /**
     * @Rest\View()
     * @Rest\Get("/admin/utilisateurs/{idUtilisateur}/apps", requirements={"idUtilisateur"="\d+"})
     * @param Request $request
     * @return array
     */
    public function getListUserAppsAction(Request $request)
    {
        $idUser = $request->get('idUtilisateur');

        $em = $this->getDoctrine()->getManager();
        $apps = $em->getRepository(ApplicationsUtilisateursDroits::class)
            ->getListAppsDroit($idUser);

        return $this->makeRoleToApp($apps);

    }

    public function makeRoleToApp($apps)
    {
        return array_map(function($alias) {
            if (key_exists('liAlias', $alias)) {
                $alias['liAlias'] = sprintf('ROLE_%s_%s', $alias['liAlias'], ($alias['isEcriture']? 'ADMIN' : 'USER' ));
            }

            return $alias;

        }, $apps);
    }

}