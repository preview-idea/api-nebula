<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 19/12/2017
 * Time: 11:17
 */

namespace Core\Controller;

use Core\Entity\Application\ApplicationsUtilisateursDroits;
use Core\Entity\Niveaux\NiveauxEnt;
use Core\Entity\Utilisateurs;
use Core\Form\UtilisateursType;
use Core\Repository\Niveaux\NiveauxEntRepository;
use Core\Repository\UtilisateursRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    /** @var NiveauxEntRepository $niveauHierarchieRepo */
    protected $niveauHierarchieRepo;

    /** @var UtilisateursRepository $objUserRepo */
    protected $objUserRepo;

    /** @var \Doctrine\Common\Persistence\ObjectManager $manager */
    protected $manager;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        if (!$this->manager instanceof EntityManager) :
            $this->manager = $managerRegistry->getManager();
            $this->objUserRepo = $this->manager->getRepository(Utilisateurs::class);

            $this->niveauHierarchieRepo = $this->manager->getRepository(NiveauxEnt::class);
        endif;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/utilisateurs")
     */
    public function userListAction()
    {
        $users = $this->objUserRepo->getListUser();
        return $users;
    }

    /**
     * @Rest\View()
     * @Rest\Post("/admin/utilisateurs")
     * @param Request $request
     * @return object|View
     */
    public function userNewAction(Request $request)
    {

        $user = new Utilisateurs();

        $form = $this->createForm(UtilisateursType::class, $user);
        $form->submit($request->request->all());

        if ($form->isValid()) :
            $this->manager->persist($user);
            $this->manager->flush();

            return $user;
        endif;

        return $form;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/utilisateurs/{idUtilisateur}", requirements={"idUtilisateur"="\d+"})
     * @param Request $request
     * @return object|View
     */
    public function userOneAction(Request $request)
    {
        $user = $this->objUserRepo->find($request->get('idUtilisateur'));

        if (!$user) :
            return $this->userNotFound();
        endif;

        return $user;

    }

    /**
     * @Rest\View()
     * @Rest\Put("/admin/utilisateurs/{idUtilisateur}", requirements={"idUtilisateur"="\d+"})
     * @param Request $request
     * @param bool $clearMissing
     * @return object|View
     */
    public function userUpdateAction(Request $request, $clearMissing = true)
    {
        $user = $this->userOneAction($request);

        if ($user instanceof View) :
            return $user;
        endif;

        $form = $this->createForm(UtilisateursType::class, $user);
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) :
            $this->manager->persist($user);
            $this->manager->flush();

            return $user;
        endif;

        return $form;

    }

    /**
     * @Rest\View()
     * @Rest\Patch("/admin/utilisateurs/{idUtilisateur}", requirements={"idUtilisateur"="\d+"})
     * @param Request $request
     * @return object|View
     */
    public function userUpdatePatchAction(Request $request)
    {
        return $this->userUpdateAction($request, false);
    }

    /**
     * @Rest\View()
     * @Rest\Delete("/admin/utilisateurs/{idUtilisateur}", requirements={"idUtilisateur"="\d+"})
     * @param Request $request
     * @return View|array
     */
    public function userRemoveAction(Request $request)
    {
        $user = $this->userOneAction($request);

        if ($user instanceof View) :
            return $user;
        endif;

        $this->manager->remove($user);
        $this->manager->flush();

        return View::create(
            ["message" => "Suppression effectuée"],
            Response::HTTP_OK
        );

    }

    /**
     * @return View
     */
    protected function userNotFound()
    {
        return View::create([
            'message' => 'Utilisateur introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }

}
