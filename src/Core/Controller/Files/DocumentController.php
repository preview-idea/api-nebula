<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 04/06/2018
 * Time: 12:40
 */

namespace Core\Controller\Files;

use Core\Controller\FilesController;
use Doctrine\Common\Persistence\ManagerRegistry;
use FOS\RestBundle\Controller\Annotations as Rest;
use SalarieBundle\Repository\Param\ParamTypeDocumentGenereRepository;

class DocumentController extends FilesController
{

    /** @var ParamTypeDocumentGenereRepository $paramTypeDocRepo */
    protected $paramTypeDocRepo;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry);
        $this->paramTypeDocRepo = $this->manager->getRepository('SalarieBundle:Param\\ParamTypeDoc');

    }

    /**
     * @Rest\View()
     * @Rest\Get("/files/doc/types")
     * @return object|array
     */
    public function getListTypeFichiersAction()
    {
        return $this->paramTypeDocRepo->getParams(null);
    }

}