<?php

namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UtilisateursNiveauxElements
 *
 * @ORM\Table(name="utilisateurs_niveaux_elements", uniqueConstraints={@ORM\UniqueConstraint(name="utilisateurs_niveaux_elements_unq", columns={"id_utilisateur", "id_niveau_ref", "id_ligne_niveaux_lig"})}, indexes={@ORM\Index(name="IDX_5F1F6F5850EAE44", columns={"id_utilisateur"}), @ORM\Index(name="IDX_5F1F6F5840855BCD", columns={"id_niveau_ref"}), @ORM\Index(name="IDX_5F1F6F58C771E640", columns={"id_ligne_niveaux_lig"})})
 * @ORM\Entity(repositoryClass="Core\Repository\UtilisateursNiveauxElementsRepository")
 */
class UtilisateursNiveauxElements
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_utilisateurs_elements", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="utilisateurs_niveaux_elements_id_ligne_utilisateurs_element_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneUtilisateursElements;

    /**
     * @var \Core\Entity\Utilisateurs
     *
     * @ORM\ManyToOne(targetEntity="Core\Entity\Utilisateurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;

    /**
     * @var \Core\Entity\Niveaux\NiveauxEnt
     *
     * @ORM\OneToOne(targetEntity="Core\Entity\Niveaux\NiveauxEnt")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_niveau_ref", referencedColumnName="id_niveau")
     * })
     */
    private $idNiveauRef;

    /**
     * @var \Core\Entity\Niveaux\NiveauxLig
     *
     * @ORM\OneToOne(targetEntity="Core\Entity\Niveaux\NiveauxLig")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ligne_niveaux_lig", referencedColumnName="id_ligne_niveaux_lig")
     * })
     */
    private $idLigneNiveauxLig;

    /**
     * Get idLigneUtilisateursElements
     *
     * @return integer
     */
    public function getIdLigneUtilisateursElements()
    {
        return $this->idLigneUtilisateursElements;
    }

    /**
     * Set idUtilisateur
     *
     * @param \Core\Entity\Utilisateurs $idUtilisateur
     *
     * @return UtilisateursNiveauxElements
     */
    public function setIdUtilisateur(\Core\Entity\Utilisateurs $idUtilisateur)
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }

    /**
     * Get idUtilisateur
     *
     * @return \Core\Entity\Utilisateurs
     */
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }

    /**
     * Set idNiveauRef
     *
     * @param \Core\Entity\Niveaux\NiveauxEnt $idNiveauRef
     *
     * @return UtilisateursNiveauxElements
     */
    public function setIdNiveauRef(\Core\Entity\Niveaux\NiveauxEnt $idNiveauRef)
    {
        $this->idNiveauRef = $idNiveauRef;

        return $this;
    }

    /**
     * Get idNiveauRef
     *
     * @return \Core\Entity\Niveaux\NiveauxEnt
     */
    public function getIdNiveauRef()
    {
        return $this->idNiveauRef;
    }

    /**
     * Set idLigneNiveauxLig
     *
     * @param \Core\Entity\Niveaux\NiveauxLig $idLigneNiveauxLig
     *
     * @return UtilisateursNiveauxElements
     */
    public function setIdLigneNiveauxLig(\Core\Entity\Niveaux\NiveauxLig $idLigneNiveauxLig)
    {
        $this->idLigneNiveauxLig = $idLigneNiveauxLig;

        return $this;
    }

    /**
     * Get idLigneNiveauxLig
     *
     * @return \Core\Entity\Niveaux\NiveauxLig
     */
    public function getIdLigneNiveauxLig()
    {
        return $this->idLigneNiveauxLig;
    }

}
