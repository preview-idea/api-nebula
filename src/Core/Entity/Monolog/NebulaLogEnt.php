<?php

namespace Core\Entity\Monolog;

use Doctrine\ORM\Mapping as ORM;

/**
 * NebulaLogEnt
 *
 * @ORM\Table(name="nebula_log_ent")
 * @ORM\Entity
 */
class NebulaLogEnt
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_job", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="nebula_log_ent_id_job_seq", allocationSize=1, initialValue=1)
     */
    private $idJob;

    /**
     * @var string
     *
     * @ORM\Column(name="li_job", type="string", length=50, nullable=false)
     */
    private $liJob;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_job", type="datetime", nullable=false)
     */
    private $dtJob;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_status", type="integer", nullable=false)
     */
    private $idStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="li_userlaunch", type="string", length=30, nullable=false)
     */
    private $liUserlaunch;

    /**
     * @var string
     *
     * @ORM\Column(name="li_status", type="string", length=30, nullable=true)
     */
    private $liStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_end_job", type="datetime", nullable=true)
     */
    private $dtEndJob;

    /**
     * Get idJob
     *
     * @return integer
     */
    public function getIdJob()
    {
        return $this->idJob;
    }

    /**
     * Set liJob
     *
     * @param string $liJob
     *
     * @return NebulaLogEnt
     */
    public function setLiJob($liJob)
    {
        $this->liJob = $liJob;

        return $this;
    }

    /**
     * Get liJob
     *
     * @return string
     */
    public function getLiJob()
    {
        return $this->liJob;
    }

    /**
     * Set dtJob
     *
     * @param \DateTime $dtJob
     *
     * @return NebulaLogEnt
     */
    public function setDtJob($dtJob)
    {
        $this->dtJob = $dtJob;

        return $this;
    }

    /**
     * Get dtJob
     *
     * @return \DateTime
     */
    public function getDtJob()
    {
        return $this->dtJob;
    }

    /**
     * Set idStatus
     *
     * @param integer $idStatus
     *
     * @return NebulaLogEnt
     */
    public function setIdStatus($idStatus)
    {
        $this->idStatus = $idStatus;

        return $this;
    }

    /**
     * Get idStatus
     *
     * @return integer
     */
    public function getIdStatus()
    {
        return $this->idStatus;
    }

    /**
     * Set liUserlaunch
     *
     * @param string $liUserlaunch
     *
     * @return NebulaLogEnt
     */
    public function setLiUserlaunch($liUserlaunch)
    {
        $this->liUserlaunch = $liUserlaunch;

        return $this;
    }

    /**
     * Get liUserlaunch
     *
     * @return string
     */
    public function getLiUserlaunch()
    {
        return $this->liUserlaunch;
    }

    /**
     * Set liStatus
     *
     * @param string $liStatus
     *
     * @return NebulaLogEnt
     */
    public function setLiStatus($liStatus)
    {
        $this->liStatus = $liStatus;

        return $this;
    }

    /**
     * Get liStatus
     *
     * @return string
     */
    public function getLiStatus()
    {
        return $this->liStatus;
    }

    /**
     * Set dtEndJob
     *
     * @param \DateTime $dtEndJob
     *
     * @return NebulaLogEnt
     */
    public function setDtEndJob($dtEndJob)
    {
        $this->dtEndJob = $dtEndJob;

        return $this;
    }

    /**
     * Get dtEndJob
     *
     * @return \DateTime
     */
    public function getDtEndJob()
    {
        return $this->dtEndJob;
    }
}
