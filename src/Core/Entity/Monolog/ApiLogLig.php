<?php

namespace Core\Entity\Monolog;

use Doctrine\ORM\Mapping as ORM;

/**
 * ApiLogLig
 *
 * @ORM\Table(name="api_log_lig", indexes={@ORM\Index(name="IDX_5BBE92FF657C08A2", columns={"id_job"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class ApiLogLig
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="api_log_lig_id_ligne_seq", allocationSize=1, initialValue=1)
     */
    private $idLigne;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_event", type="datetime", nullable=false)
     */
    private $dtEvent;

    /**
     * @var string
     *
     * @ORM\Column(name="li_origin", type="string", length=10, nullable=false)
     */
    private $liOrigin;

    /**
     * @var string
     *
     * @ORM\Column(name="li_level", type="string", length=4, nullable=false)
     */
    private $liLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="li_function", type="string", length=50, nullable=false)
     */
    private $liFunction;

    /**
     * @var string
     *
     * @ORM\Column(name="li_message", type="text", nullable=true)
     */
    private $liMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="li_classe", type="string", length=50, nullable=false)
     */
    private $liClasse;

    /**
     * @var \Core\Entity\Monolog\ApiLogEnt
     *
     * @ORM\ManyToOne(targetEntity="Core\Entity\Monolog\ApiLogEnt")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_job", referencedColumnName="id_job")
     * })
     */
    private $idJob;

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->dtEvent = new \DateTime();
    }

    /**
     * Get idLigne
     *
     * @return integer
     */
    public function getIdLigne()
    {
        return $this->idLigne;
    }

    /**
     * Set dtEvent
     *
     * @param \DateTime $dtEvent
     *
     * @return ApiLogLig
     */
    public function setDtEvent($dtEvent)
    {
        $this->dtEvent = $dtEvent;

        return $this;
    }

    /**
     * Get dtEvent
     *
     * @return \DateTime
     */
    public function getDtEvent()
    {
        return $this->dtEvent;
    }

    /**
     * Set liOrigin
     *
     * @param string $liOrigin
     *
     * @return ApiLogLig
     */
    public function setLiOrigin($liOrigin)
    {
        $this->liOrigin = $liOrigin;

        return $this;
    }

    /**
     * Get liOrigin
     *
     * @return string
     */
    public function getLiOrigin()
    {
        return $this->liOrigin;
    }

    /**
     * Set liLevel
     *
     * @param string $liLevel
     *
     * @return ApiLogLig
     */
    public function setLiLevel($liLevel)
    {
        $this->liLevel = $liLevel;

        return $this;
    }

    /**
     * Get liLevel
     *
     * @return string
     */
    public function getLiLevel()
    {
        return $this->liLevel;
    }

    /**
     * Set liFunction
     *
     * @param string $liFunction
     *
     * @return ApiLogLig
     */
    public function setLiFunction($liFunction)
    {
        $this->liFunction = $liFunction;

        return $this;
    }

    /**
     * Get liFunction
     *
     * @return string
     */
    public function getLiFunction()
    {
        return $this->liFunction;
    }

    /**
     * Set liMessage
     *
     * @param string $liMessage
     *
     * @return ApiLogLig
     */
    public function setLiMessage($liMessage)
    {
        $this->liMessage = $liMessage;

        return $this;
    }

    /**
     * Get liMessage
     *
     * @return string
     */
    public function getLiMessage()
    {
        return $this->liMessage;
    }

    /**
     * Set liClasse
     *
     * @param string $liClasse
     *
     * @return ApiLogLig
     */
    public function setLiClasse($liClasse)
    {
        $this->liClasse = $liClasse;

        return $this;
    }

    /**
     * Get liClasse
     *
     * @return string
     */
    public function getLiClasse()
    {
        return $this->liClasse;
    }

    /**
     * Set idJob
     *
     * @param \Core\Entity\Monolog\ApiLogEnt $idJob
     *
     * @return ApiLogLig
     */
    public function setIdJob(\Core\Entity\Monolog\ApiLogEnt $idJob = null)
    {
        $this->idJob = $idJob;

        return $this;
    }

    /**
     * Get idJob
     *
     * @return \Core\Entity\Monolog\ApiLogEnt
     */
    public function getIdJob()
    {
        return $this->idJob;
    }
}
