<?php

namespace Core\Entity\Views\Combi;

use Doctrine\ORM\Mapping as ORM;

/**
 * ViewCombiNiveauxElementsWithUnusedNiveauxLig
 *
 * @ORM\Table(name="v_combi_niveaux_elements_with_unused_niveaux_lig")
 * @ORM\Entity(repositoryClass="Core\Repository\Views\Combi\ViewCombiNiveauxElementsWithUnusedNiveauxLigRepository")
 */
class ViewCombiNiveauxElementsWithUnusedNiveauxLig
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id_ligne_combi_niveaux_elements", type="integer", nullable=true)
     */
    private $idLigneCombiNiveauxElements;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_niveau", type="integer", nullable=false)
     */
    private $idNiveau;

    /**
     * @var string
     *
     * @ORM\Column(name="li_niveau", type="string", nullable=false)
     */
    private $liNiveau;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_element", type="integer", nullable=false)
     */
    private $idElement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_element", type="string", nullable=false)
     */
    private $liElement;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_niveau_parent", type="integer", nullable=true)
     */
    private $idNiveauParent;

    /**
     * @var string
     *
     * @ORM\Column(name="li_niveau_parent", type="string", nullable=true)
     */
    private $liNiveauParent;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_element_parent", type="integer", nullable=true)
     */
    private $idElementParent;

    /**
     * @var string
     *
     * @ORM\Column(name="li_element_parent", type="string", nullable=true)
     */
    private $liElementParent;

    /**
     * @return int
     */
    public function getIdLigneCombiNiveauxElements()
    {
        return $this->idLigneCombiNiveauxElements;
    }

    /**
     * @param int $idLigneCombiNiveauxElements
     * @return CombiNiveauxElementsWithUnusedNiveauxLig
     */
    public function setIdLigneCombiNiveauxElements($idLigneCombiNiveauxElements)
    {
        $this->idLigneCombiNiveauxElements = $idLigneCombiNiveauxElements;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdNiveau()
    {
        return $this->idNiveau;
    }

    /**
     * @param int $idNiveau
     * @return CombiNiveauxElementsWithUnusedNiveauxLig
     */
    public function setIdNiveau($idNiveau)
    {
        $this->idNiveau = $idNiveau;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiNiveau()
    {
        return $this->liNiveau;
    }

    /**
     * @param string $liNiveau
     * @return CombiNiveauxElementsWithUnusedNiveauxLig
     */
    public function setLiNiveau($liNiveau)
    {
        $this->liNiveau = $liNiveau;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdElement()
    {
        return $this->idElement;
    }

    /**
     * @param int $idElement
     * @return CombiNiveauxElementsWithUnusedNiveauxLig
     */
    public function setIdElement($idElement)
    {
        $this->idElement = $idElement;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiElement()
    {
        return $this->liElement;
    }

    /**
     * @param string $liElement
     * @return CombiNiveauxElementsWithUnusedNiveauxLig
     */
    public function setLiElement($liElement)
    {
        $this->liElement = $liElement;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdNiveauParent()
    {
        return $this->idNiveauParent;
    }

    /**
     * @param int $idNiveauParent
     * @return CombiNiveauxElementsWithUnusedNiveauxLig
     */
    public function setIdNiveauParent($idNiveauParent)
    {
        $this->idNiveauParent = $idNiveauParent;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiNiveauParent()
    {
        return $this->liNiveauParent;
    }

    /**
     * @param string $liNiveauParent
     * @return CombiNiveauxElementsWithUnusedNiveauxLig
     */
    public function setLiNiveauParent($liNiveauParent)
    {
        $this->liNiveauParent = $liNiveauParent;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdElementParent()
    {
        return $this->idElementParent;
    }

    /**
     * @param int $idElementParent
     * @return CombiNiveauxElementsWithUnusedNiveauxLig
     */
    public function setIdElementParent($idElementParent)
    {
        $this->idElementParent = $idElementParent;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiElementParent()
    {
        return $this->liElementParent;
    }

    /**
     * @param string $liElementParent
     * @return CombiNiveauxElementsWithUnusedNiveauxLig
     */
    public function setLiElementParent($liElementParent)
    {
        $this->liElementParent = $liElementParent;
        return $this;
    }

}
