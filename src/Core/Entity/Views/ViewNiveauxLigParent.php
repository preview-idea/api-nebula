<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 22/05/2019
 * Time: 09:35
 */

namespace Core\Entity\Views;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_niveaux_lig_parent")
 * @ORM\Entity(repositoryClass="Core\Repository\Views\ViewNiveauxLigParentRepository")
 */
class ViewNiveauxLigParent
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_element", type="integer", nullable=false)
     */
    private $idElement;

    /**
     * @var string
     *
     * @ORM\Column(name="id_niveau", type="integer", nullable=false)
     */
    private $idNiveau;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_element_parent", type="integer", nullable=false)
     */
    private $idElementParent;

    /**
     * @var string
     *
     * @ORM\Column(name="li_element_parent", type="string", nullable=false)
     */
    private $liElementParent;

    /**
     * @return int
     */
    public function getIdElement()
    {
        return $this->idElement;
    }

    /**
     * @param int $idElement
     * @return ViewNiveauxLigParent
     */
    public function setIdElement($idElement)
    {
        $this->idElement = $idElement;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdNiveau()
    {
        return $this->idNiveau;
    }

    /**
     * @param string $idNiveau
     * @return ViewNiveauxLigParent
     */
    public function setIdNiveau($idNiveau)
    {
        $this->idNiveau = $idNiveau;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdElementParent()
    {
        return $this->idElementParent;
    }

    /**
     * @param int $idElementParent
     * @return ViewNiveauxLigParent
     */
    public function setIdElementParent($idElementParent)
    {
        $this->idElementParent = $idElementParent;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiElementParent()
    {
        return $this->liElementParent;
    }

    /**
     * @param string $liElementParent
     * @return ViewNiveauxLigParent
     */
    public function setLiElementParent($liElementParent)
    {
        $this->liElementParent = $liElementParent;
        return $this;
    }

}
