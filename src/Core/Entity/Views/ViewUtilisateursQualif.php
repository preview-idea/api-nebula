<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 22/05/2019
 * Time: 09:35
 */

namespace Core\Entity\Views;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_utilisateurs_qualif")
 * @ORM\Entity(repositoryClass="Core\Repository\Views\ViewUtilisateursQualifRepository")
 */
class ViewUtilisateursQualif
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_utilisateur", type="integer", nullable=false)
     */
    private $idUtilisateur;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_qualifcontrat", type="integer", nullable=false)
     */
    private $idQualifcontrat;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_qualifcontrat_structure", type="bit", nullable=false)
     */
    private $isQualifcontratStructure;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_droit_voir_structure", type="bit", nullable=false)
     */
    private $isDroitVoirStructure;

    /**
     * Set idUtilisateur
     *
     * @param integer $idUtilisateur
     *
     * @return ViewUtilisateursQualif
     */
    public function setIdUtilisateur($idUtilisateur)
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }

    /**
     * Get idUtilisateur
     *
     * @return integer
     */
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }

    /**
     * Set idMatricule
     *
     * @param integer $idMatricule
     *
     * @return ViewUtilisateursQualif
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return integer
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set idQualifcontrat
     *
     * @param integer $idQualifcontrat
     *
     * @return ViewUtilisateursQualif
     */
    public function setIdQualifcontrat($idQualifcontrat)
    {
        $this->idQualifcontrat = $idQualifcontrat;

        return $this;
    }

    /**
     * Get idQualifcontrat
     *
     * @return integer
     */
    public function getIdQualifcontrat()
    {
        return $this->idQualifcontrat;
    }

    /**
     * Set isQualifcontratStructure
     *
     * @param bit $isQualifcontratStructure
     *
     * @return ViewUtilisateursQualif
     */
    public function setIsQualifcontratStructure($isQualifcontratStructure)
    {
        $this->isQualifcontratStructure = $isQualifcontratStructure;

        return $this;
    }

    /**
     * Get isQualifcontratStructure
     *
     * @return bit
     */
    public function getIsQualifcontratStructure()
    {
        return $this->isQualifcontratStructure;
    }

    /**
     * Set isDroitVoirStructure
     *
     * @param bit $isDroitVoirStructure
     *
     * @return ViewUtilisateursQualif
     */
    public function setIsDroitVoirStructure($isDroitVoirStructure)
    {
        $this->isDroitVoirStructure = $isDroitVoirStructure;

        return $this;
    }

    /**
     * Get isDroitVoirStructure
     *
     * @return bit
     */
    public function getIsDroitVoirStructure()
    {
        return $this->isDroitVoirStructure;
    }
}
