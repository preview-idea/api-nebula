<?php

namespace Core\Entity\Views\Param;

use Doctrine\ORM\Mapping as ORM;

/**
 * ViewParamQualifcontratNonUtilise
 *
 * @ORM\Table(name="v_param_qualifcontrat_non_utilise")
 * @ORM\Entity(repositoryClass="Core\Repository\Views\Param\ViewParamQualifcontratNonUtiliseRepository")
 */
class ViewParamQualifcontratNonUtilise
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_qualifcontrat", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idQualifcontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_qualifcontrat", type="string", length=32, nullable=false)
     */
    private $liQualifcontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="id_qualifcontrat_rhpi", type="string", length=5, nullable=false)
     */
    private $idQualifcontratRhpi;

    /**
     * @var string
     *
     * @ORM\Column(name="li_code_insee", type="string", length=8, nullable=false)
     */
    private $liCodeInsee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_actif", type="date", nullable=false)
     */
    private $dtDebutActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_actif", type="date", nullable=true)
     */
    private $dtFinActif;

    /**
     * Get idQualifcontrat
     *
     * @return integer
     */
    public function getIdQualifcontrat()
    {
        return $this->idQualifcontrat;
    }

    /**
     * Set liQualifcontrat
     *
     * @param string $liQualifcontrat
     *
     * @return ViewParamQualifcontratNonUtilise
     */
    public function setLiQualifcontrat($liQualifcontrat)
    {
        $this->liQualifcontrat = $liQualifcontrat;

        return $this;
    }

    /**
     * Get liQualifcontrat
     *
     * @return string
     */
    public function getLiQualifcontrat()
    {
        return $this->liQualifcontrat;
    }

    /**
     * Set idQualifcontratRhpi
     *
     * @param string $idQualifcontratRhpi
     *
     * @return ViewParamQualifcontratNonUtilise
     */
    public function setIdQualifcontratRhpi($idQualifcontratRhpi)
    {
        $this->idQualifcontratRhpi = $idQualifcontratRhpi;

        return $this;
    }

    /**
     * Get idQualifcontratRhpi
     *
     * @return string
     */
    public function getIdQualifcontratRhpi()
    {
        return $this->idQualifcontratRhpi;
    }

    /**
     * Set liCodeInsee
     *
     * @param string $liCodeInsee
     *
     * @return ViewParamQualifcontratNonUtilise
     */
    public function setLiCodeInsee($liCodeInsee)
    {
        $this->liCodeInsee = $liCodeInsee;

        return $this;
    }

    /**
     * Get liCodeInsee
     *
     * @return string
     */
    public function getLiCodeInsee()
    {
        return $this->liCodeInsee;
    }

    /**
     * Set dtDebutActif
     *
     * @param \DateTime $dtDebutActif
     *
     * @return ViewParamQualifcontratNonUtilise
     */
    public function setDtDebutActif($dtDebutActif)
    {
        $this->dtDebutActif = $dtDebutActif;

        return $this;
    }

    /**
     * Get dtDebutActif
     *
     * @return \DateTime
     */
    public function getDtDebutActif()
    {
        return $this->dtDebutActif;
    }

    /**
     * Set dtFinActif
     *
     * @param \DateTime $dtFinActif
     *
     * @return ViewParamQualifcontratNonUtilise
     */
    public function setDtFinActif($dtFinActif)
    {
        $this->dtFinActif = $dtFinActif;

        return $this;
    }

    /**
     * Get dtFinActif
     *
     * @return \DateTime
     */
    public function getDtFinActif()
    {
        return $this->dtFinActif;
    }
}
