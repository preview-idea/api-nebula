<?php

namespace Core\Entity\Views\DWH;

use Doctrine\ORM\Mapping as ORM;

/**
 * MvContrats
 *
 * @ORM\Table(name="mv_contrats")
 * @ORM\Entity(repositoryClass="Core\Repository\Views\DWH\ViewMvContratsObjSalarieRepository")
 */

class ViewMvContratsObjSalarie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_contrat", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idContrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", length=60, nullable=false)
     */
    private $liAgence;

    /**
     * @var string
     *
     * @ORM\Column(name="li_typecontrat", type="string", length=50, nullable=false)
     */
    private $liTypeContrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_naturecontrat", type="string", length=50, nullable=false)
     */
    private $liNatureContrat;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dt_debutcontrat", type="datetime", nullable=false)
     */
    private $dtDebutContrat;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dt_fincontrat_prevue", type="datetime", nullable=false)
     */
    private $dtFinContratPrevue;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dt_fincontrat", type="datetime", nullable=false)
     */
    private $dtFinContrat;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_agence", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idAgence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_categorieemploye", type="string", length=30, nullable=false)
     */
    private $liCategorieEmploye;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_bu", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idBu;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_region", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idRegion;
    /**
     * @var string
     *
     * @ORM\Column(name="li_bu", type="string", length=30, nullable=false)
     */
    private $liBu;

    /**
     * @var string
     *
     * @ORM\Column(name="li_region", type="string", length=30, nullable=false)
     */
    private $liRegion;

    

    /**
     * Set idContrat
     *
     * @param integer $idContrat
     *
     * @return ViewMvContratsObjSalarie
     */
    public function setIdContrat($idContrat)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return integer
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set liAgence
     *
     * @param string $liAgence
     *
     * @return ViewMvContratsObjSalarie
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;

        return $this;
    }

    /**
     * Get liAgence
     *
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * Set liTypeContrat
     *
     * @param string $liTypeContrat
     *
     * @return ViewMvContratsObjSalarie
     */
    public function setLiTypeContrat($liTypeContrat)
    {
        $this->liTypeContrat = $liTypeContrat;

        return $this;
    }

    /**
     * Get liTypeContrat
     *
     * @return string
     */
    public function getLiTypeContrat()
    {
        return $this->liTypeContrat;
    }

    /**
     * Set liNatureContrat
     *
     * @param string $liNatureContrat
     *
     * @return ViewMvContratsObjSalarie
     */
    public function setLiNatureContrat($liNatureContrat)
    {
        $this->liNatureContrat = $liNatureContrat;

        return $this;
    }

    /**
     * Get liNatureContrat
     *
     * @return string
     */
    public function getLiNatureContrat()
    {
        return $this->liNatureContrat;
    }

    /**
     * Set dtDebutContrat
     *
     * @param \DateTime $dtDebutContrat
     *
     * @return ViewMvContratsObjSalarie
     */
    public function setDtDebutContrat($dtDebutContrat)
    {
        $this->dtDebutContrat = $dtDebutContrat;

        return $this;
    }

    /**
     * Get dtDebutContrat
     *
     * @return \DateTime
     */
    public function getDtDebutContrat()
    {
        return $this->dtDebutContrat;
    }

    /**
     * Set dtFinContratPrevue
     *
     * @param \DateTime $dtFinContratPrevue
     *
     * @return ViewMvContratsObjSalarie
     */
    public function setDtFinContratPrevue($dtFinContratPrevue)
    {
        $this->dtFinContratPrevue = $dtFinContratPrevue;

        return $this;
    }

    /**
     * Get dtFinContratPrevue
     *
     * @return \DateTime
     */
    public function getDtFinContratPrevue()
    {
        return $this->dtFinContratPrevue;
    }

    /**
     * Set dtFinContrat
     *
     * @param \DateTime $dtFinContrat
     *
     * @return ViewMvContratsObjSalarie
     */
    public function setDtFinContrat($dtFinContrat)
    {
        $this->dtFinContrat = $dtFinContrat;

        return $this;
    }

    /**
     * Get dtFinContrat
     *
     * @return \DateTime
     */
    public function getDtFinContrat()
    {
        return $this->dtFinContrat;
    }

    /**
     * Set idAgence
     *
     * @param integer $idAgence
     *
     * @return ViewMvContratsObjSalarie
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;

        return $this;
    }

    /**
     * Get idAgence
     *
     * @return integer
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * Set idMatricule
     *
     * @param integer $idMatricule
     *
     * @return ViewMvContratsObjSalarie
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return integer
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set liCategorieEmploye
     *
     * @param string $liCategorieEmploye
     *
     * @return ViewMvContratsObjSalarie
     */
    public function setLiCategorieEmploye($liCategorieEmploye)
    {
        $this->liCategorieEmploye = $liCategorieEmploye;

        return $this;
    }

    /**
     * Get liCategorieEmploye
     *
     * @return string
     */
    public function getLiCategorieEmploye()
    {
        return $this->liCategorieEmploye;
    }

    /**
     * Set idBu
     *
     * @param integer $idBu
     *
     * @return ViewMvContratsObjSalarie
     */
    public function setIdBu($idBu)
    {
        $this->idBu = $idBu;

        return $this;
    }

    /**
     * Get idBu
     *
     * @return integer
     */
    public function getIdBu()
    {
        return $this->idBu;
    }

    /**
     * Set idRegion
     *
     * @param integer $idRegion
     *
     * @return ViewMvContratsObjSalarie
     */
    public function setIdRegion($idRegion)
    {
        $this->idRegion = $idRegion;

        return $this;
    }

    /**
     * Get idRegion
     *
     * @return integer
     */
    public function getIdRegion()
    {
        return $this->idRegion;
    }

    /**
     * Set liBu
     *
     * @param string $liBu
     *
     * @return ViewMvContratsObjSalarie
     */
    public function setLiBu($liBu)
    {
        $this->liBu = $liBu;

        return $this;
    }

    /**
     * Get liBu
     *
     * @return string
     */
    public function getLiBu()
    {
        return $this->liBu;
    }

    /**
     * Set liRegion
     *
     * @param string $liRegion
     *
     * @return ViewMvContratsObjSalarie
     */
    public function setLiRegion($liRegion)
    {
        $this->liRegion = $liRegion;

        return $this;
    }

    /**
     * Get liRegion
     *
     * @return string
     */
    public function getLiRegion()
    {
        return $this->liRegion;
    }
}
