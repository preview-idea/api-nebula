<?php

namespace Core\Entity\Views\DWH;

use Doctrine\ORM\Mapping as ORM;

/**
 * MvCartePro
 *
 * @ORM\Table(name="mv_cartepro")
 * @ORM\Entity(repositoryClass="Core\Repository\Views\DWH\ViewMvCarteProObjSalarieRepository")
 */

class ViewMvCarteProObjSalarie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cartepro", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCartePro;

    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", length=60, nullable=false)
     */
    private $liAgence;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dt_debutvalidite", type="datetime", nullable=false)
     */
    private $dtDebutValidite;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dt_finvalidite", type="datetime", nullable=false)
     */
    private $dtFinValidite;

    /**
     * @var string
     *
     * @ORM\Column(name="li_etat_cartepro", type="string", length=10, nullable=false)
     */
    private $liEtatCartePro;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_agence", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idAgence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idMatricule;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_bu", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idBu;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_region", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idRegion;
    /**
     * @var string
     *
     * @ORM\Column(name="li_bu", type="string", length=30, nullable=false)
     */
    private $liBu;

    /**
     * @var string
     *
     * @ORM\Column(name="li_region", type="string", length=30, nullable=false)
     */
    private $liRegion;



    /**
     * Set idCartePro
     *
     * @param integer $idCartePro
     *
     * @return ViewMvCarteProObjSalarie
     */
    public function setIdCartePro($idCartePro)
    {
        $this->idCartePro = $idCartePro;

        return $this;
    }

    /**
     * Get idCartePro
     *
     * @return integer
     */
    public function getIdCartePro()
    {
        return $this->idCartePro;
    }

    /**
     * Set liAgence
     *
     * @param string $liAgence
     *
     * @return ViewMvCarteProObjSalarie
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;

        return $this;
    }

    /**
     * Get liAgence
     *
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * Set dtDebutValidite
     *
     * @param \DateTime $dtDebutValidite
     *
     * @return ViewMvCarteProObjSalarie
     */
    public function setDtDebutValidite($dtDebutValidite)
    {
        $this->dtDebutValidite = $dtDebutValidite;

        return $this;
    }

    /**
     * Get dtDebutValidite
     *
     * @return \DateTime
     */
    public function getDtDebutValidite()
    {
        return $this->dtDebutValidite;
    }

    /**
     * Set dtFinValidite
     *
     * @param \DateTime $dtFinValidite
     *
     * @return ViewMvCarteProObjSalarie
     */
    public function setDtFinValidite($dtFinValidite)
    {
        $this->dtFinValidite = $dtFinValidite;

        return $this;
    }

    /**
     * Get dtFinValidite
     *
     * @return \DateTime
     */
    public function getDtFinValidite()
    {
        return $this->dtFinValidite;
    }

    /**
     * Set liEtatCartePro
     *
     * @param string $liEtatCartePro
     *
     * @return ViewMvCarteProObjSalarie
     */
    public function setLiEtatCartePro($liEtatCartePro)
    {
        $this->liEtatCartePro = $liEtatCartePro;

        return $this;
    }

    /**
     * Get liEtatCartePro
     *
     * @return string
     */
    public function getLiEtatCartePro()
    {
        return $this->liEtatCartePro;
    }

    /**
     * Set idAgence
     *
     * @param integer $idAgence
     *
     * @return ViewMvCarteProObjSalarie
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;

        return $this;
    }

    /**
     * Get idAgence
     *
     * @return integer
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * Set idMatricule
     *
     * @param integer $idMatricule
     *
     * @return ViewMvCarteProObjSalarie
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return integer
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set idBu
     *
     * @param integer $idBu
     *
     * @return ViewMvCarteProObjSalarie
     */
    public function setIdBu($idBu)
    {
        $this->idBu = $idBu;

        return $this;
    }

    /**
     * Get idBu
     *
     * @return integer
     */
    public function getIdBu()
    {
        return $this->idBu;
    }

    /**
     * Set idRegion
     *
     * @param integer $idRegion
     *
     * @return ViewMvCarteProObjSalarie
     */
    public function setIdRegion($idRegion)
    {
        $this->idRegion = $idRegion;

        return $this;
    }

    /**
     * Get idRegion
     *
     * @return integer
     */
    public function getIdRegion()
    {
        return $this->idRegion;
    }

    /**
     * Set liBu
     *
     * @param string $liBu
     *
     * @return ViewMvCarteProObjSalarie
     */
    public function setLiBu($liBu)
    {
        $this->liBu = $liBu;

        return $this;
    }

    /**
     * Get liBu
     *
     * @return string
     */
    public function getLiBu()
    {
        return $this->liBu;
    }

    /**
     * Set liRegion
     *
     * @param string $liRegion
     *
     * @return ViewMvCarteProObjSalarie
     */
    public function setLiRegion($liRegion)
    {
        $this->liRegion = $liRegion;

        return $this;
    }

    /**
     * Get liRegion
     *
     * @return string
     */
    public function getLiRegion()
    {
        return $this->liRegion;
    }
}
