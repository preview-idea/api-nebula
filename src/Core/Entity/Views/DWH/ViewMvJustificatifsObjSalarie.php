<?php

namespace Core\Entity\Views\DWH;

use Doctrine\ORM\Mapping as ORM;

/**
 * MvCartePro
 *
 * @ORM\Table(name="mv_justificatifs")
 * @ORM\Entity(repositoryClass="Core\Repository\Views\DWH\ViewMvJustificatifsObjSalarieRepository")
 */

class ViewMvJustificatifsObjSalarie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_usage", type="string", length=100, nullable=false)
     */
    private $liNomUsage;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_piece", type="string", length=50, nullable=false)
     */
    private $liTypePiece;

    /**
     * @var string
     *
     * @ORM\Column(name="li_numero_piece", type="string", length=100, nullable=false)
     */
    private $liNumeroPiece;


    /**
     * @var datetime
     *
     * @ORM\Column(name="dt_debut_validite", type="datetime", nullable=false)
     */
    private $dtDebutValidite;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dt_fin_validite", type="datetime", nullable=false)
     */
    private $dtFinValidite;

    /**
     * @var string
     *
     * @ORM\Column(name="li_etat", type="string", length=20, nullable=false)
     */
    private $liEtat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", length=100, nullable=false)
     */
    private $liAgence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_agence", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idAgence;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dt_debutcontrat", type="datetime", nullable=false)
     */
    private $dtDebutContrat;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dt_fincontrat", type="datetime", nullable=false)
     */
    private $dtFinContrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_typecontrat", type="string", length=50, nullable=false)
     */
    private $liTypeContrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nationalite", type="string", length=100, nullable=false)
     */
    private $liNationalite;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_bu", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idBu;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_region", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idRegion;
    /**
     * @var string
     *
     * @ORM\Column(name="li_bu", type="string", length=30, nullable=false)
     */
    private $liBu;

    /**
     * @var string
     *
     * @ORM\Column(name="li_region", type="string", length=30, nullable=false)
     */
    private $liRegion;




    /**
     * Set idMatricule
     *
     * @param integer $idMatricule
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return integer
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set liNomUsage
     *
     * @param string $liNomUsage
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setLiNomUsage($liNomUsage)
    {
        $this->liNomUsage = $liNomUsage;

        return $this;
    }

    /**
     * Get liNomUsage
     *
     * @return string
     */
    public function getLiNomUsage()
    {
        return $this->liNomUsage;
    }

    /**
     * Set liTypePiece
     *
     * @param string $liTypePiece
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setLiTypePiece($liTypePiece)
    {
        $this->liTypePiece = $liTypePiece;

        return $this;
    }

    /**
     * Get liTypePiece
     *
     * @return string
     */
    public function getLiTypePiece()
    {
        return $this->liTypePiece;
    }

    /**
     * Set liNumeroPiece
     *
     * @param string $liNumeroPiece
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setLiNumeroPiece($liNumeroPiece)
    {
        $this->liNumeroPiece = $liNumeroPiece;

        return $this;
    }

    /**
     * Get liNumeroPiece
     *
     * @return string
     */
    public function getLiNumeroPiece()
    {
        return $this->liNumeroPiece;
    }

    /**
     * Set dtDebutValidite
     *
     * @param \DateTime $dtDebutValidite
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setDtDebutValidite($dtDebutValidite)
    {
        $this->dtDebutValidite = $dtDebutValidite;

        return $this;
    }

    /**
     * Get dtDebutValidite
     *
     * @return \DateTime
     */
    public function getDtDebutValidite()
    {
        return $this->dtDebutValidite;
    }

    /**
     * Set dtFinValidite
     *
     * @param \DateTime $dtFinValidite
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setDtFinValidite($dtFinValidite)
    {
        $this->dtFinValidite = $dtFinValidite;

        return $this;
    }

    /**
     * Get dtFinValidite
     *
     * @return \DateTime
     */
    public function getDtFinValidite()
    {
        return $this->dtFinValidite;
    }

    /**
     * Set liEtat
     *
     * @param string $liEtat
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setLiEtat($liEtat)
    {
        $this->liEtat = $liEtat;

        return $this;
    }

    /**
     * Get liEtat
     *
     * @return string
     */
    public function getLiEtat()
    {
        return $this->liEtat;
    }

    /**
     * Set liAgence
     *
     * @param string $liAgence
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;

        return $this;
    }

    /**
     * Get liAgence
     *
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * Set idAgence
     *
     * @param integer $idAgence
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;

        return $this;
    }

    /**
     * Get idAgence
     *
     * @return integer
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * Set dtDebutContrat
     *
     * @param \DateTime $dtDebutContrat
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setDtDebutContrat($dtDebutContrat)
    {
        $this->dtDebutContrat = $dtDebutContrat;

        return $this;
    }

    /**
     * Get dtDebutContrat
     *
     * @return \DateTime
     */
    public function getDtDebutContrat()
    {
        return $this->dtDebutContrat;
    }

    /**
     * Set dtFinContrat
     *
     * @param \DateTime $dtFinContrat
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setDtFinContrat($dtFinContrat)
    {
        $this->dtFinContrat = $dtFinContrat;

        return $this;
    }

    /**
     * Get dtFinContrat
     *
     * @return \DateTime
     */
    public function getDtFinContrat()
    {
        return $this->dtFinContrat;
    }

    /**
     * Set liTypeContrat
     *
     * @param string $liTypeContrat
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setLiTypeContrat($liTypeContrat)
    {
        $this->liTypeContrat = $liTypeContrat;

        return $this;
    }

    /**
     * Get liTypeContrat
     *
     * @return string
     */
    public function getLiTypeContrat()
    {
        return $this->liTypeContrat;
    }

    /**
     * Set liNationalite
     *
     * @param string $liNationalite
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setLiNationalite($liNationalite)
    {
        $this->liNationalite = $liNationalite;

        return $this;
    }

    /**
     * Get liNationalite
     *
     * @return string
     */
    public function getLiNationalite()
    {
        return $this->liNationalite;
    }

    /**
     * Set idBu
     *
     * @param integer $idBu
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setIdBu($idBu)
    {
        $this->idBu = $idBu;

        return $this;
    }

    /**
     * Get idBu
     *
     * @return integer
     */
    public function getIdBu()
    {
        return $this->idBu;
    }

    /**
     * Set idRegion
     *
     * @param integer $idRegion
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setIdRegion($idRegion)
    {
        $this->idRegion = $idRegion;

        return $this;
    }

    /**
     * Get idRegion
     *
     * @return integer
     */
    public function getIdRegion()
    {
        return $this->idRegion;
    }

    /**
     * Set liBu
     *
     * @param string $liBu
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setLiBu($liBu)
    {
        $this->liBu = $liBu;

        return $this;
    }

    /**
     * Get liBu
     *
     * @return string
     */
    public function getLiBu()
    {
        return $this->liBu;
    }

    /**
     * Set liRegion
     *
     * @param string $liRegion
     *
     * @return ViewMvJustificatifsObjSalarie
     */
    public function setLiRegion($liRegion)
    {
        $this->liRegion = $liRegion;

        return $this;
    }

    /**
     * Get liRegion
     *
     * @return string
     */
    public function getLiRegion()
    {
        return $this->liRegion;
    }
}
