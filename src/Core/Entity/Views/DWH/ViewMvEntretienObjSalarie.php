<?php

namespace Core\Entity\Views\DWH;

use Core\Repository\Views\DWH\ViewMvEntretienObjSalarieRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * MvEntretien
 *
 * @ORM\Table(name="mv_entretien")
 * @ORM\Entity(repositoryClass="Core\Repository\Views\DWH\ViewMvEntretienObjSalarieRepository")
 */

class ViewMvEntretienObjSalarie
{
   /**
     * @var datetime
     *
     * @ORM\Column(name="dt_dernier_entretien", type="datetime", nullable=false)
     */
    private $dtDernierEntretien;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dt_echeance_entretien", type="datetime", nullable=false)
     */
    private $dtEcheanceEntretien;

    /**
     * @var string
     *
     * @ORM\Column(name="statut_entretien", type="string", length=100, nullable=false)
     */
    private $statutEntretien;

    /**
     * @var string
     *
     * @ORM\Column(name="statut_details", type="string", length=100, nullable=false)
     */
    private $statutDetails;

    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", length=100, nullable=false)
     */
    private $liAgence;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dt_planifie", type="datetime", nullable=false)
     */
    private $dtPlanifie;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dt_absence", type="datetime", nullable=false)
     */
    private $dtAbsence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_absence", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idAbsence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_type_absence", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idTypeAbsence;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif_absence", type="bit", nullable=true)
     */
    private $isActifAbsence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_agence", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idAgence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idMatricule;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_bu", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idBu;

    /**
     * @var string
     *
     * @ORM\Column(name="li_bu", type="string", length=100, nullable=false)
     */
    private $liBu;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_region", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idRegion;

    /**
     * @var string
     *
     * @ORM\Column(name="li_region", type="string", length=100, nullable=false)
     */
    private $liRegion;

    /**
     * @var string
     *
     * @ORM\Column(name="is_retour_absence", type="string", length=3, nullable=false)
     */
    private $isRetourAbsence;


    /**
 * Set dtDernierEntretien
 *
 * @param \DateTime dtDernierEntretien
 *
 * @return ViewMvEntretienObjSalarieRepository
 */
    public function setDtDernierEntretien($dtDernierEntretien)
    {
        $this->dtDernierEntretien = $dtDernierEntretien;

        return $this;
    }

    /**
     * Get dtDernierEntretien
     *
     * @return \DateTime
     */
    public function getDtDernierEntretien()
    {
        return $this->dtDernierEntretien;
    }

    /**
     * Set dtEcheanceEntretien
     *
     * @param \DateTime dtEcheanceEntretien
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setDtEcheanceEntretien($dtEcheanceEntretien)
    {
        $this->dtEcheanceEntretien = $dtEcheanceEntretien;

        return $this;
    }

    /**
     * Get dtEcheanceEntretien
     *
     * @return \DateTime
     */
    public function getDtEcheanceEntretien()
    {
        return $this->dtEcheanceEntretien;
    }

    /**
     * Set dtPlanifie
     *
     * @param \DateTime dtPlanifie
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setDtPlanifie($dtPlanifie)
    {
        $this->dtPlanifie = $dtPlanifie;

        return $this;
    }

    /**
     * Get dtPlanifie
     *
     * @return \DateTime
     */
    public function getDtPlanifie()
    {
        return $this->dtPlanifie;
    }

    /**
     * Set dtAbsence
     *
     * @param \DateTime dtAbsence
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setDtAbsence($dtAbsence)
    {
        $this->dtAbsence = $dtAbsence;

        return $this;
    }

    /**
     * Get dtAbsence
     *
     * @return \DateTime
     */
    public function getDtAbsence()
    {
        return $this->dtAbsence;
    }

    /**
     * Set idAbsence
     *
     * @param integer idAbsence
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setIdAbsence($idAbsence)
    {
        $this->idAbsence = $idAbsence;

        return $this;
    }

    /**
     * Get idAbsence
     *
     * @return integer
     */
    public function getIdAbsence()
    {
        return $this->idAbsence;
    }

    /**
     * Set idTypeAbsence
     *
     * @param integer idTypeAbsence
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setIdTypeAbsence($idTypeAbsence)
    {
        $this->idTypeAbsence = $idTypeAbsence;

        return $this;
    }

    /**
     * Get idTypeAbsence
     *
     * @return integer
     */
    public function getIdTypeAbsence()
    {
        return $this->idTypeAbsence;
    }

    /**
     * Set isActifAbsence
     *
     * @param bit isActifAbsence
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setIsActifAbsence($isActifAbsence)
    {
        $this->isActifAbsence = $isActifAbsence;

        return $this;
    }

    /**
     * Get isActifAbsence
     *
     * @return bit
     */
    public function getIsActifAbsence()
    {
        return $this->isActifAbsence;
    }

    /**
     * Set idAgence
     *
     * @param integer $idAgence
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;

        return $this;
    }

    /**
     * Get idAgence
     *
     * @return integer
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * Set idMatricule
     *
     * @param integer $idMatricule
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return integer
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set idBu
     *
     * @param integer $idBu
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setIdBu($idBu)
    {
        $this->idBu = $idBu;

        return $this;
    }

    /**
     * Get idBu
     *
     * @return integer
     */
    public function getIdBu()
    {
        return $this->idBu;
    }

    /**
     * Set idRegion
     *
     * @param integer $idRegion
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setIdRegion($idRegion)
    {
        $this->idRegion = $idRegion;

        return $this;
    }

    /**
     * Get idRegion
     *
     * @return integer
     */
    public function getIdRegion()
    {
        return $this->idRegion;
    }

    /**
     * Set liBu
     *
     * @param string $liBu
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setLiBu($liBu)
    {
        $this->liBu = $liBu;

        return $this;
    }

    /**
     * Get liBu
     *
     * @return string
     */
    public function getLiBu()
    {
        return $this->liBu;
    }

    /**
     * Set liRegion
     *
     * @param string $liRegion
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setLiRegion($liRegion)
    {
        $this->liRegion = $liRegion;

        return $this;
    }

    /**
     * Get liRegion
     *
     * @return string
     */
    public function getLiRegion()
    {
        return $this->liRegion;
    }

    /**
     * Set liAgence
     *
     * @param string $liAgence
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;

        return $this;
    }

    /**
     * Get liAgence
     *
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * Set statutEntretien
     *
     * @param string statutEntretien
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setStatutEntretien($statutEntretien)
    {
        $this->statutEntretien = $statutEntretien;

        return $this;
    }

    /**
     * Get statutEntretien
     *
     * @return string
     */
    public function getStatutEntretien()
    {
        return $this->statutEntretien;
    }

    /**
     * Set statutDetails
     *
     * @param string statutDetails
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setStatutDetails($statutDetails)
    {
        $this->statutDetails = $statutDetails;

        return $this;
    }

    /**
     * Get statutDetails
     *
     * @return string
     */
    public function getStatutDetails()
    {
        return $this->statutDetails;
    }

    /**
     * Set isRetourAbsence
     *
     * @param string isRetourAbsence
     *
     * @return ViewMvEntretienObjSalarieRepository
     */
    public function setIsRetourAbsence($isRetourAbsence)
    {
        $this->isRetourAbsence = $isRetourAbsence;

        return $this;
    }

    /**
     * Get isRetourAbsence
     *
     * @return string
     */
    public function getIsRetourAbsence()
    {
        return $this->isRetourAbsence;
    }

}
