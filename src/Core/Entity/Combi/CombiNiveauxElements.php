<?php

namespace Core\Entity\Combi;

use Doctrine\ORM\Mapping as ORM;

/**
 * CombiNiveauxElements
 *
 * @ORM\Table(name="combi_niveaux_elements", uniqueConstraints={@ORM\UniqueConstraint(name="combi_niveaux_elements_ukey", columns={"id_niveau", "id_element", "id_element_parent"})})
 * @ORM\Entity(repositoryClass="Core\Repository\Combi\CombiNiveauxElementsRepository")
 */
class CombiNiveauxElements
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_combi_niveaux_elements", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="combi_niveaux_elements_id_ligne_combi_niveaux_elements_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneCombiNiveauxElements;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_niveau", type="smallint", nullable=false)
     */
    private $idNiveau;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_element", type="integer", nullable=false)
     */
    private $idElement;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_element_parent", type="integer", nullable=false)
     */
    private $idElementParent;

    /**
     * @return int
     */
    public function getIdLigneCombiNiveauxElements()
    {
        return $this->idLigneCombiNiveauxElements;
    }

    /**
     * @param int $idLigneCombiNiveauxElements
     * @return CombiNiveauxElements
     */
    public function setIdLigneCombiNiveauxElements($idLigneCombiNiveauxElements)
    {
        $this->idLigneCombiNiveauxElements = $idLigneCombiNiveauxElements;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdNiveau()
    {
        return $this->idNiveau;
    }

    /**
     * @param int $idNiveau
     * @return CombiNiveauxElements
     */
    public function setIdNiveau($idNiveau)
    {
        $this->idNiveau = $idNiveau;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdElement()
    {
        return $this->idElement;
    }

    /**
     * @param int $idElement
     * @return CombiNiveauxElements
     */
    public function setIdElement($idElement)
    {
        $this->idElement = $idElement;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdElementParent()
    {
        return $this->idElementParent;
    }

    /**
     * @param int $idElementParent
     * @return CombiNiveauxElements
     */
    public function setIdElementParent($idElementParent)
    {
        $this->idElementParent = $idElementParent;
        return $this;
    }

}
