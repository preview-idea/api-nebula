<?php

namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UtilisateursQualifcontrat
 *
 * @ORM\Table(name="utilisateurs_qualifcontrat", indexes={@ORM\Index(name="IDX_F490884640855BCD", columns={"id_niveau_ref"})})
 * @ORM\Entity(repositoryClass="Core\Repository\UtilisateursQualifcontratRepository")
 */
class UtilisateursQualifcontrat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_qualifcontrat", type="integer", nullable=false)
     * @ORM\Id
     */
    private $idQualifcontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_qualifcontrat", type="string", nullable=false)
     */
    private $liQualifcontrat;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_element_ref", type="integer", nullable=true)
     */
    private $idElementRef;

    /**
     * @var \Core\Entity\Niveaux\NiveauxEnt
     *
     * @ORM\ManyToOne(targetEntity="Core\Entity\Niveaux\NiveauxEnt")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_niveau_ref", referencedColumnName="id_niveau")
     * })
     */
    private $idNiveauRef;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_qualifcontrat_structure", type="bit", nullable=false)
     */
    private $isQualifcontratStructure;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_droit_voir_structure", type="bit", nullable=false)
     */
    private $isDroitVoirStructure;

    /**
     * @var string
     *
     * @ORM\Column(name="js_role", type="string", nullable=true)
     */
    private $jsRole;

    /**
     * @return bit
     */
    public function getisQualifcontratStructure()
    {
        return $this->isQualifcontratStructure;
    }

    /**
     * @param bit $isQualifcontratStructure
     * @return UtilisateursQualifcontrat
     */
    public function setIsQualifcontratStructure($isQualifcontratStructure)
    {
        $this->isQualifcontratStructure = $isQualifcontratStructure;
        return $this;
    }

    /**
     * @return bit
     */
    public function getisDroitVoirStructure()
    {
        return $this->isDroitVoirStructure;
    }

    /**
     * @param bit $isDroitVoirStructure
     * @return UtilisateursQualifcontrat
     */
    public function setIsDroitVoirStructure($isDroitVoirStructure)
    {
        $this->isDroitVoirStructure = $isDroitVoirStructure;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdQualifcontrat()
    {
        return $this->idQualifcontrat;
    }

    /**
     * @param int $idQualifcontrat
     * @return UtilisateursQualifcontrat
     */
    public function setIdQualifcontrat($idQualifcontrat)
    {
        $this->idQualifcontrat = $idQualifcontrat;
        return $this;
    }


    /**
     * Set liQualifcontrat
     *
     * @param string $liQualifcontrat
     *
     * @return UtilisateursQualifcontrat
     */
    public function setLiQualifcontrat($liQualifcontrat)
    {
        $this->liQualifcontrat = $liQualifcontrat;

        return $this;
    }

    /**
     * Get liQualifcontrat
     *
     * @return string
     */
    public function getLiQualifcontrat()
    {
        return $this->liQualifcontrat;
    }

    /**
     * Set idElementRef
     *
     * @param integer $idElementRef
     *
     * @return UtilisateursQualifcontrat
     */
    public function setIdElementRef($idElementRef)
    {
        $this->idElementRef = $idElementRef;

        return $this;
    }

    /**
     * Get idElementRef
     *
     * @return integer
     */
    public function getIdElementRef()
    {
        return $this->idElementRef;
    }

    /**
     * Set idNiveauRef
     *
     * @param \Core\Entity\Niveaux\NiveauxEnt $idNiveauRef
     *
     * @return UtilisateursQualifcontrat
     */
    public function setIdNiveauRef(\Core\Entity\Niveaux\NiveauxEnt $idNiveauRef = null)
    {
        $this->idNiveauRef = $idNiveauRef;

        return $this;
    }

    /**
     * Get idNiveauRef
     *
     * @return \Core\Entity\Niveaux\NiveauxEnt
     */
    public function getIdNiveauRef()
    {
        return $this->idNiveauRef;
    }

    /**
     * @return string
     */
    public function getJsRole()
    {
        return $this->jsRole;
    }

    /**
     * @param array $jsRole
     * @return UtilisateursQualifcontrat
     */
    public function setJsRole($jsRole)
    {
        $this->jsRole = $jsRole;
        return $this;
    }
}
