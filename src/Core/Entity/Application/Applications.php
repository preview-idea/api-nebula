<?php

namespace Core\Entity\Application;

use Doctrine\ORM\Mapping as ORM;

/**
 * Applications
 *
 * @ORM\Table(name="applications")
 * @ORM\Entity
 */
class Applications
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_application", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="applications_id_application_seq", allocationSize=1, initialValue=1)
     */
    private $idApplication;

    /**
     * @var string
     *
     * @ORM\Column(name="li_application", type="string", length=30, nullable=false)
     */
    private $liApplication;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_base_application", type="bit", nullable=false)
     */
    private $isBaseApplication = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_ordre_hierarchique", type="smallint", nullable=false)
     */
    private $nbOrdreHierarchique;

    /**
     * @var string
     *
     * @ORM\Column(name="li_alias", type="string", length=10, nullable=false)
     */
    private $liAlias;

    /**
     * Get idApplication
     *
     * @return integer
     */
    public function getIdApplication()
    {
        return $this->idApplication;
    }

    /**
     * Set liApplication
     *
     * @param string $liApplication
     *
     * @return Applications
     */
    public function setLiApplication($liApplication)
    {
        $this->liApplication = $liApplication;

        return $this;
    }

    /**
     * Get liApplication
     *
     * @return string
     */
    public function getLiApplication()
    {
        return $this->liApplication;
    }

    /**
     * Set isBaseApplication
     *
     * @param bit $isBaseApplication
     *
     * @return Applications
     */
    public function setIsBaseApplication($isBaseApplication)
    {
        $this->isBaseApplication = $isBaseApplication;

        return $this;
    }

    /**
     * Get isBaseApplication
     *
     * @return bit
     */
    public function getIsBaseApplication()
    {
        return $this->isBaseApplication;
    }

    /**
     * Set nbOrdreHierarchique
     *
     * @param integer $nbOrdreHierarchique
     *
     * @return Applications
     */
    public function setNbOrdreHierarchique($nbOrdreHierarchique)
    {
        $this->nbOrdreHierarchique = $nbOrdreHierarchique;

        return $this;
    }

    /**
     * Get nbOrdreHierarchique
     *
     * @return integer
     */
    public function getNbOrdreHierarchique()
    {
        return $this->nbOrdreHierarchique;
    }

    /**
     * Set liAlias
     *
     * @param string $liAlias
     *
     * @return Applications
     */
    public function setLiAlias($liAlias)
    {
        $this->liAlias = $liAlias;

        return $this;
    }

    /**
     * Get liAlias
     *
     * @return string
     */
    public function getLiAlias()
    {
        return $this->liAlias;
    }
}
