<?php

namespace Core\Entity;

use Core\Entity\Combi\CombiNiveauxElementsExceptions;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUser;

/**
 * Utilisateurs
 *
 * @ORM\Table(name="utilisateurs", uniqueConstraints={@ORM\UniqueConstraint(name="users_li_email_key", columns={"li_email"})}, indexes={@ORM\Index(name="IDX_497B315E2B4AB12A", columns={"id_qualifcontrat"})})
 * @ORM\Entity(repositoryClass="Core\Repository\UtilisateursRepository")
 */
class Utilisateurs extends JWTUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_utilisateur", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="utilisateurs_id_utilisateur_seq", allocationSize=1, initialValue=1)
     */
    private $idUtilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="li_email", type="string", nullable=true)
     */
    private $liEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="li_password", type="string", length=80, nullable=true)
     */
    private $liPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nomutilisateur", type="string", length=50, nullable=true)
     */
    private $liNomutilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="li_token", type="string", length=1000, nullable=false)
     */
    private $liToken;

    /**
     * @var string
     *
     * @ORM\Column(name="li_login_ldap", type="string", nullable=false)
     */
    private $liLoginLdap;

    /**
     * @var array
     *
     * @ORM\Column(name="js_role", type="json", nullable=true)
     */
    private $jsRole;

    /**
     * @var string
     *
     * @ORM\Column(name="li_chemin_photo", type="string", length=100, nullable=true)
     */
    private $liCheminPhoto;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_matricule", type="integer", nullable=true)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="id_matricule_saphir", type="string", length=9, nullable=true)
     */
    private $idMatriculeSaphir;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_niveau_ref", type="smallint", nullable=true)
     */
    private $idNiveauRef;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif = '1';

    /**
     * @var \Core\Entity\UtilisateursQualifcontrat
     *
     * @ORM\ManyToOne(targetEntity="Core\Entity\UtilisateursQualifcontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_qualifcontrat", referencedColumnName="id_qualifcontrat")
     * })
     * @Serializer\Exclude()
     */
    private $idQualifcontrat;

    /**
     * @var \Core\Entity\Application\ApplicationsUtilisateursDroits
     * @ORM\OneToMany(targetEntity="Core\Entity\Application\ApplicationsUtilisateursDroits", mappedBy="idUtilisateur", cascade={"persist", "remove"})
     * @Serializer\MaxDepth(5)
     */
    private $appsDroit;

    /**
     * @var UtilisateursNiveauxElements
     * @ORM\OneToMany(targetEntity="Core\Entity\UtilisateursNiveauxElements", mappedBy="idUtilisateur", cascade={"persist", "remove"})
     * @Serializer\MaxDepth(5)
     */
    private $elements;

    /**
     * @var \Core\Entity\Combi\CombiNiveauxElementsExceptions
     * @ORM\OneToMany(targetEntity="Core\Entity\Combi\CombiNiveauxElementsExceptions", mappedBy="idUtilisateur", cascade={"persist", "remove"})
     * @Serializer\MaxDepth(5)
     */
    private $exceptions;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct($this->liLoginLdap, $this->getRoles());
        $this->appsDroit = new ArrayCollection();
        $this->elements = new ArrayCollection();
        $this->exceptions = new ArrayCollection();
    }

    /**
     * Get jsRole
     *
     * @return array
     */
    public function getRoles()
    {
        $roles = $this->jsRole;
        if (!$roles) :
            $roles = ['ROLE_SALARIE_USER'];
        endif;

        return $roles;
    }

    /**
     * Get idUtilisateur
     *
     * @return integer
     */
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }

    /**
     * Set liEmail
     *
     * @param string $liEmail
     *
     * @return Utilisateurs
     */
    public function setLiEmail($liEmail)
    {
        $this->liEmail = $liEmail;

        return $this;
    }

    /**
     * Get liEmail
     *
     * @return string
     */
    public function getLiEmail()
    {
        return $this->liEmail;
    }

    /**
     * Set liPassword
     *
     * @param string $liPassword
     *
     * @return Utilisateurs
     */
    public function setLiPassword($liPassword)
    {
        $this->liPassword = $liPassword;

        return $this;
    }

    /**
     * Get liPassword
     *
     * @return string
     */
    public function getLiPassword()
    {
        return $this->liPassword;
    }

    /**
     * Set liNomutilisateur
     *
     * @param string $liNomutilisateur
     *
     * @return Utilisateurs
     */
    public function setLiNomutilisateur($liNomutilisateur)
    {
        $this->liNomutilisateur = $liNomutilisateur;

        return $this;
    }

    /**
     * Get liNomutilisateur
     *
     * @return string
     */
    public function getLiNomutilisateur()
    {
        return $this->liNomutilisateur;
    }

    /**
     * Set liToken
     *
     * @param string $liToken
     *
     * @return Utilisateurs
     */
    public function setLiToken($liToken)
    {
        $this->liToken = $liToken;

        return $this;
    }

    /**
     * Get liToken
     *
     * @return string
     */
    public function getLiToken()
    {
        return $this->liToken;
    }

    /**
     * Set liLoginLdap
     *
     * @param string $liLoginLdap
     *
     * @return Utilisateurs
     */
    public function setLiLoginLdap($liLoginLdap)
    {
        $this->liLoginLdap = $liLoginLdap;

        return $this;
    }

    /**
     * Get liLoginLdap
     *
     * @return string
     */
    public function getLiLoginLdap()
    {
        return $this->liLoginLdap;
    }

    /**
     * Set jsRole
     *
     * @param array $jsRole
     *
     * @return Utilisateurs
     */
    public function setJsRole($jsRole)
    {
        $this->jsRole = $jsRole;

        return $this;
    }

    /**
     * Get jsRole
     *
     * @return array
     */
    public function getJsRole()
    {
        return $this->jsRole;
    }

    /**
     * Set liCheminPhoto
     *
     * @param string $liCheminPhoto
     *
     * @return Utilisateurs
     */
    public function setLiCheminPhoto($liCheminPhoto)
    {
        $this->liCheminPhoto = $liCheminPhoto;

        return $this;
    }

    /**
     * Get liCheminPhoto
     *
     * @return string
     */
    public function getLiCheminPhoto()
    {
        return $this->liCheminPhoto;
    }

    /**
     * Set idMatricule
     *
     * @param integer $idMatricule
     *
     * @return Utilisateurs
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return integer
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set idMatriculeSaphir
     *
     * @param string $idMatriculeSaphir
     *
     * @return Utilisateurs
     */
    public function setIdMatriculeSaphir($idMatriculeSaphir)
    {
        $this->idMatriculeSaphir = $idMatriculeSaphir;

        return $this;
    }

    /**
     * Get idMatriculeSaphir
     *
     * @return string
     */
    public function getIdMatriculeSaphir()
    {
        return $this->idMatriculeSaphir;
    }

    /**
     * Set idNiveauRef
     *
     * @param integer $idNiveauRef
     *
     * @return Utilisateurs
     */
    public function setIdNiveauRef($idNiveauRef)
    {
        $this->idNiveauRef = $idNiveauRef;

        return $this;
    }

    /**
     * Get idNiveauRef
     *
     * @return integer
     */
    public function getIdNiveauRef()
    {
        return $this->idNiveauRef;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return Utilisateurs
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set idQualifcontrat
     *
     * @param \Core\Entity\UtilisateursQualifcontrat $idQualifcontrat
     *
     * @return Utilisateurs
     */
    public function setIdQualifcontrat(\Core\Entity\UtilisateursQualifcontrat $idQualifcontrat = null)
    {
        $this->idQualifcontrat = $idQualifcontrat;

        return $this;
    }

    /**
     * Get idQualifcontrat
     *
     * @return \Core\Entity\UtilisateursQualifcontrat
     */
    public function getIdQualifcontrat()
    {
        return $this->idQualifcontrat;
    }

    /**
     * Add appsDroit
     *
     * @param \Core\Entity\Application\ApplicationsUtilisateursDroits $appsDroit
     *
     * @return Utilisateurs
     */
    public function addAppsDroit(\Core\Entity\Application\ApplicationsUtilisateursDroits $appsDroit)
    {
        $this->appsDroit[] = $appsDroit;
        $appsDroit->setIdUtilisateur($this);

        return $this;
    }

    /**
     * Remove appsDroit
     *
     * @param \Core\Entity\Application\ApplicationsUtilisateursDroits $appsDroit
     */
    public function removeAppsDroit(\Core\Entity\Application\ApplicationsUtilisateursDroits $appsDroit)
    {
        $this->appsDroit->removeElement($appsDroit);
    }

    /**
     * Get appsDroit
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppsDroit()
    {
        return $this->appsDroit;
    }

    /**
     * Add element
     *
     * @param \Core\Entity\UtilisateursNiveauxElements $element
     *
     * @return Utilisateurs
     */
    public function addElement(\Core\Entity\UtilisateursNiveauxElements $element)
    {
        $this->elements[] = $element;
        $element->setIdUtilisateur($this);

        return $this;
    }

    /**
     * Remove element
     *
     * @param \Core\Entity\UtilisateursNiveauxElements $element
     */
    public function removeElement(\Core\Entity\UtilisateursNiveauxElements $element)
    {
        $this->elements->removeElement($element);
    }

    /**
     * Get elements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * Add element
     *
     * @param \Core\Entity\Combi\CombiNiveauxElementsExceptions $exception
     *
     * @return Utilisateurs
     */
    public function addException(\Core\Entity\Combi\CombiNiveauxElementsExceptions $exception)
    {
        $this->exceptions[] = $exception;
        $exception->setIdUtilisateur($this);

        return $this;
    }

    /**
     * Remove element
     *
     * @param \Core\Entity\Combi\CombiNiveauxElementsExceptions $exception
     */
    public function removeException(\Core\Entity\Combi\CombiNiveauxElementsExceptions $exception)
    {
        $this->exceptions->removeElement($exception);
    }

    /**
     * Get elements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExceptions()
    {
        return $this->exceptions;
    }
}
