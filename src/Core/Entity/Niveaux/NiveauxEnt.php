<?php

namespace Core\Entity\Niveaux;

use Doctrine\ORM\Mapping as ORM;

/**
 * NiveauxEnt
 *
 * @ORM\Table(name="niveaux_ent")
 * @ORM\Entity(repositoryClass="Core\Repository\Niveaux\NiveauxEntRepository")
 */
class NiveauxEnt
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_niveau", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="niveaux_ent_id_niveau_seq", allocationSize=1, initialValue=1)
     */
    private $idNiveau;

    /**
     * @var string
     *
     * @ORM\Column(name="li_niveau", type="string", nullable=false)
     */
    private $liNiveau;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_niveau_enfant", type="integer", nullable=true)
     */
    private $idNiveauEnfant;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_champ", type="string", nullable=true)
     */
    private $nomChamp;

    /**
     * Get idNiveau
     *
     * @return integer
     */
    public function getIdNiveau()
    {
        return $this->idNiveau;
    }

    /**
     * Set liNiveau
     *
     * @param string $liNiveau
     *
     * @return NiveauxEnt
     */
    public function setLiNiveau($liNiveau)
    {
        $this->liNiveau = $liNiveau;

        return $this;
    }

    /**
     * Get liNiveau
     *
     * @return string
     */
    public function getLiNiveau()
    {
        return $this->liNiveau;
    }

    /**
     * Set idNiveauEnfant
     *
     * @param integer $idNiveauEnfant
     *
     * @return NiveauxEnt
     */
    public function setIdNiveauEnfant($idNiveauEnfant)
    {
        $this->idNiveauEnfant = $idNiveauEnfant;

        return $this;
    }

    /**
     * Get idNiveauEnfant
     *
     * @return integer
     */
    public function getIdNiveauEnfant()
    {
        return $this->idNiveauEnfant;
    }

    /**
     * Set nomChamp
     *
     * @param string $nomChamp
     *
     * @return NiveauxEnt
     */
    public function setNomChamp($nomChamp)
    {
        $this->nomChamp = $nomChamp;

        return $this;
    }

    /**
     * Get nomChamp
     *
     * @return string
     */
    public function getNomChamp()
    {
        return $this->nomChamp;
    }
}
