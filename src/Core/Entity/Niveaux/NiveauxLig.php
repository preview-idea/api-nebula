<?php

namespace Core\Entity\Niveaux;

use Doctrine\ORM\Mapping as ORM;

/**
 * NiveauxLig
 *
 * @ORM\Table(name="niveaux_lig", indexes={@ORM\Index(name="fki_niveaux_lig_fkey", columns={"id_element", "id_niveau"}), @ORM\Index(name="IDX_55A34E606DE84686", columns={"id_niveau"})})
 * @ORM\Entity(repositoryClass="Core\Repository\Niveaux\NiveauxLigRepository")
 */
class NiveauxLig
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_niveaux_lig", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="niveaux_lig_id_ligne_niveaux_lig_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneNiveauxLig;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_element", type="integer", nullable=false)
     */
    private $idElement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_element", type="string", length=60, nullable=false)
     */
    private $liElement;

    /**
     * @var \Core\Entity\Niveaux\NiveauxEnt
     *
     * @ORM\ManyToOne(targetEntity="Core\Entity\Niveaux\NiveauxEnt")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_niveau", referencedColumnName="id_niveau")
     * })
     */
    private $idNiveau;

    /**
     * Get idLigneNiveauxLig
     *
     * @return integer
     */
    public function getIdLigne()
    {
        return $this->idLigneNiveauxLig;
    }

    /**
     * Set idElement
     *
     * @param integer $idElement
     *
     * @return NiveauxLig
     */
    public function setIdElement($idElement)
    {
        $this->idElement = $idElement;

        return $this;
    }

    /**
     * Get idElement
     *
     * @return integer
     */
    public function getIdElement()
    {
        return $this->idElement;
    }

    /**
     * Set liElement
     *
     * @param string $liElement
     *
     * @return NiveauxLig
     */
    public function setLiElement($liElement)
    {
        $this->liElement = $liElement;

        return $this;
    }

    /**
     * Get liElement
     *
     * @return string
     */
    public function getLiElement()
    {
        return $this->liElement;
    }

    /**
     * Set idNiveau
     *
     * @param \Core\Entity\Niveaux\NiveauxEnt $idNiveau
     *
     * @return NiveauxLig
     */
    public function setIdNiveau(\Core\Entity\Niveaux\NiveauxEnt $idNiveau = null)
    {
        $this->idNiveau = $idNiveau;

        return $this;
    }

    /**
     * Get idNiveau
     *
     * @return \Core\Entity\Niveaux\NiveauxEnt
     */
    public function getIdNiveau()
    {
        return $this->idNiveau;
    }
}
