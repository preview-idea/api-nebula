<?php

namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Objets
 *
 * @ORM\Table(name="objets")
 * @ORM\Entity
 */
class Objets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_objet", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="objets_id_objet_seq", allocationSize=1, initialValue=1)
     */
    private $idObjet;

    /**
     * @var string
     *
     * @ORM\Column(name="li_objet", type="string", length=80, nullable=false)
     */
    private $liObjet;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_utile", type="bit", nullable=false)
     */
    private $isUtile;

    /**
     * Get idObjet
     *
     * @return integer
     */
    public function getIdObjet()
    {
        return $this->idObjet;
    }

    /**
     * Set liObjet
     *
     * @param string $liObjet
     *
     * @return Objets
     */
    public function setLiObjet($liObjet)
    {
        $this->liObjet = $liObjet;

        return $this;
    }

    /**
     * Get liObjet
     *
     * @return string
     */
    public function getLiObjet()
    {
        return $this->liObjet;
    }

    /**
     * Set isUtile
     *
     * @param bit $isUtile
     *
     * @return Objets
     */
    public function setIsUtile($isUtile)
    {
        $this->isUtile = $isUtile;

        return $this;
    }

    /**
     * Get isUtile
     *
     * @return bit
     */
    public function getIsUtile()
    {
        return $this->isUtile;
    }
}
