<?php

namespace Core\Entity\Group\Param;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParamGroup
 *
 * @ORM\Table(name="param_group")
 * @ORM\Entity
 */
class ParamGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_group", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="param_group_id_group_seq", allocationSize=1, initialValue=1)
     */
    private $idGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="li_group", type="string", length=100, nullable=false)
     */
    private $liGroup;



    /**
     * Get idGroup.
     *
     * @return int
     */
    public function getIdGroup()
    {
        return $this->idGroup;
    }

    /**
     * Set liGroup.
     *
     * @param string $liGroup
     *
     * @return ParamGroup
     */
    public function setLiGroup($liGroup)
    {
        $this->liGroup = $liGroup;

        return $this;
    }

    /**
     * Get liGroup.
     *
     * @return string
     */
    public function getLiGroup()
    {
        return $this->liGroup;
    }
}
