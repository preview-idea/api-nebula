<?php

namespace Core\Entity\Group;

use Doctrine\ORM\Mapping as ORM;

/**
 * CombiGroupUser
 *
 * @ORM\Table(name="combi_group_user", indexes={@ORM\Index(name="IDX_A6F2FE0D834505F5", columns={"id_group"}), @ORM\Index(name="IDX_A6F2FE0D50EAE44", columns={"id_utilisateur"})})
 * @ORM\Entity(repositoryClass="Core\Repository\Group\CombiGroupUserRepository")
 */
class CombiGroupUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_group_user", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="combi_group_user_id_group_user_seq", allocationSize=1, initialValue=1)
     */
    private $idGroupUser;

    /**
     * @var \Core\Entity\Group\Param\ParamGroup
     *
     * @ORM\ManyToOne(targetEntity="Core\Entity\Group\Param\ParamGroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_group", referencedColumnName="id_group")
     * })
     */
    private $idGroup;

    /**
     * @var \Core\Entity\Utilisateurs
     *
     * @ORM\ManyToOne(targetEntity="Core\Entity\Utilisateurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;



    /**
     * Get idGroupUser.
     *
     * @return int
     */
    public function getIdGroupUser()
    {
        return $this->idGroupUser;
    }

    /**
     * Set idGroup.
     *
     * @param \Core\Entity\Group\Param\ParamGroup|null $idGroup
     *
     * @return CombiGroupUser
     */
    public function setIdGroup(\Core\Entity\Group\Param\ParamGroup $idGroup = null)
    {
        $this->idGroup = $idGroup;

        return $this;
    }

    /**
     * Get idGroup.
     *
     * @return \Core\Entity\Group\Param\ParamGroup|null
     */
    public function getIdGroup()
    {
        return $this->idGroup;
    }

    /**
     * Set idUtilisateur.
     *
     * @param \Core\Entity\Utilisateurs|null $idUtilisateur
     *
     * @return CombiGroupUser
     */
    public function setIdUtilisateur(\Core\Entity\Utilisateurs $idUtilisateur = null)
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }

    /**
     * Get idUtilisateur.
     *
     * @return \Core\Entity\Utilisateurs|null
     */
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }
}
