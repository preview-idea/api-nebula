<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 21/01/2018
 * Time: 19:14
 */

namespace Core\Custom\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class BitType extends Type
{

    const BIT = 'bit';

    /**
     * @param array $fieldDeclaration
     * @param AbstractPlatform $platform
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getDoctrineTypeMapping('BIT');
    }

    /**
     * Gets the name of this type.
     * @return string
     *
     */
    public function getName()
    {
        return self::BIT;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return ($value == B'1') ? 1 : 0;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return ($value == 1) ? B'1' : B'0';
    }

    public function canRequireSQLConversion()
    {
        return true;
    }

    /**
     * @param AbstractPlatform $platform
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }

}