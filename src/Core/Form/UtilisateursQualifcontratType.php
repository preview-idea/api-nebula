<?php

namespace Core\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UtilisateursQualifcontratType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idQualifcontrat')
            ->add('liQualifcontrat')
            ->add('idElementRef')
            ->add('isQualifcontratStructure')
            ->add('isDroitVoirStructure')
            ->add('idNiveauRef')
            ->add('jsRole');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Core\Entity\UtilisateursQualifcontrat'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'core_utilisateursqualifcontrat';
    }


}
