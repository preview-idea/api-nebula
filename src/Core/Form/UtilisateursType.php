<?php

namespace Core\Form;

use Core\Form\Application\ApplicationsUtilisateursDroitsType;
use Core\Form\Combi\CombiNiveauxElementsExceptionsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UtilisateursType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('liEmail', EmailType::class)
            ->add('liPassword')
            ->add('liNomutilisateur')
            ->add('liToken')
            ->add('liLoginLdap')
            ->add('jsRole', CollectionType::class, [
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'by_reference' => false
            ])
            ->add('liCheminPhoto')
            ->add('idMatricule')
            ->add('idMatriculeSaphir')
            ->add('idQualifcontrat')
            ->add('appsDroit', CollectionType::class, ["entry_type" => ApplicationsUtilisateursDroitsType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'by_reference' => false
            ])
            ->add('elements', CollectionType::class, ["entry_type" => UtilisateursNiveauxElementsType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'by_reference' => false
            ])
            ->add('exceptions', CollectionType::class, ["entry_type" => CombiNiveauxElementsExceptionsType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'by_reference' => false
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Core\Entity\Utilisateurs'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'core_utilisateurs';
    }


}
