<?php


namespace Core\Security\Traits;


use Core\Entity\Utilisateurs;
use Core\Repository\UtilisateursRepository;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Types\Types;
use SensioLabs\Security\Exception\HttpException;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserInterface;

trait TraitSQLFilter
{
    private $manager;
    private $storage;
    private $filter;

    public function onKernelRequest(KernelEvent $event)
    {
        // TODO: Get the request event to apply the filter
        // Code removed for security reasons
    }

    public function getUser()
    {
        // TODO: verify if user has allowed to access to the API
        // Code removed for security reasons

        return $user;
    }

    public function getUserPerimeter( $idUtilisateur )
    {
        /** @var UtilisateursRepository $userRepo */
        $userRepo = $this->manager->getRepository(Utilisateurs::class);
        return $userRepo->getUserPerimeterByidMatricule($idUtilisateur);
    }


}