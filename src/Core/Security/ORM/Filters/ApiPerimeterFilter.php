<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 30/07/2018
 * Time: 09:21
 */

namespace Core\Security\ORM\Filters;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class ApiPerimeterFilter extends SQLFilter
{

    /**
     * Gets the SQL query part to add to a query.
     *
     * @param ClassMetaData $targetEntity
     * @param string $targetTableAlias
     *
     * @return string The constraint SQL if there is available, empty string otherwise.
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        
        // TODO: add filter data from user's perimeter to query
        // Code removed for security reasons

        return $query;
    }


}