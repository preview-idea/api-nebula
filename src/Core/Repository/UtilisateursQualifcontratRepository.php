<?php

namespace Core\Repository;

use Doctrine\ORM\EntityRepository;

class UtilisateursQualifcontratRepository extends EntityRepository
{
    public function getAllWithoutJoin()
    {
        $query = $this->createQueryBuilder("q")
            ->select("
                q.idQualifcontrat,
                q.liQualifcontrat,
                n.liNiveau,
                q.isQualifcontratStructure,
                q.isDroitVoirStructure,
                q.jsRole
            ")
            ->leftJoin("q.idNiveauRef","n")
            ->orderBy('q.idQualifcontrat', 'ASC')
            ->getQuery();

        return $query->getResult();
    }
}

