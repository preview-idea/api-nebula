<?php

namespace Core\Repository\Application;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class ApplicationsUtilisateursDroitsRepository extends EntityRepository
{
    /**
     * @param $idUser
     * @return array
     */
    public function getListAppsDroit($idUser)
    {

        $query = $this->createQueryBuilder("app")
            ->select("
                apli.idApplication,
                apli.liAlias,
                app.isEcriture,
                apli.isBaseApplication,
                apli.nbOrdreHierarchique
            ")
            ->leftJoin('app.idApplication', 'apli')
            ->where('app.idUtilisateur = :idUser')
            ->setParameter('idUser', $idUser, Types::INTEGER)
            ->getQuery();

        return $query->getResult();


    }
}