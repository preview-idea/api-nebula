<?php

namespace Core\Repository\Views;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class ViewNiveauxLigParentRepository extends EntityRepository
{
    function getNiveauxLigParent($idNiveau, $idElement) {

        $query = $this->createQueryBuilder("v")
            ->select("
                v.idElementParent,
                v.liElementParent
            ")
            ->where('v.idNiveau = :idNiveau')
            ->andWhere('v.idElement = :idElement')
            ->setParameter('idNiveau', $idNiveau, Types::INTEGER)
            ->setParameter('idElement', $idElement, Types::INTEGER)
            ->orderBy('v.liElementParent', 'ASC')
            ->getQuery();

        return $query->getResult();

    }
}
