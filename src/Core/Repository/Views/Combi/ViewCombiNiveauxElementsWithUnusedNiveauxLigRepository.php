<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 21/08/2018
 * Time: 17:47
 */

namespace Core\Repository\Views\Combi;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class ViewCombiNiveauxElementsWithUnusedNiveauxLigRepository extends EntityRepository
{

    public function getCombi()
    {

        $query = $this->createQueryBuilder("c")
            ->select("
                c.idLigneCombiNiveauxElements,
                c.idNiveau,
                c.liNiveau,
                c.idElement,
                c.liElement,
                c.idNiveauParent,
                c.liNiveauParent,
                c.idElementParent,
                c.liElementParent
            ")
            ->orderBy('c.idLigneCombiNiveauxElements', 'DESC')
            ->orderBy('c.idNiveau', 'DESC')
            ->getQuery();

        return $query->getResult();
    }
}