<?php

namespace Core\Repository\Views\Param;

use Doctrine\ORM\EntityRepository;

class ViewParamQualifcontratNonUtiliseRepository extends EntityRepository
{
    public function getParams($filter = null)
    {
        $query = $this->createQueryBuilder("q")
            ->select("
                q.idQualifcontrat,
                q.liQualifcontrat
            ")
            ->where("q.liQualifcontrat LIKE :filter")
            ->andWhere("q.dtDebutActif IS NULL  OR q.dtDebutActif <= CURRENT_DATE()")
            ->andWhere("q.dtFinActif IS NULL OR q.dtFinActif >= CURRENT_DATE()")
            ->orderBy('q.liQualifcontrat', 'ASC')
            ->setParameter('filter', '%'.$filter.'%')
            ->getQuery();

        return $query->getResult();

    }

}
