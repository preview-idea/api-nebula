<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 21/08/2018
 * Time: 17:47
 */

namespace Core\Repository;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class UtilisateursNiveauxElementsRepository extends EntityRepository
{

    public function getAllByUser($user)
    {

        $query = $this->createQueryBuilder("e")
            ->select("
                ent.idNiveau,
                lig.idLigneNiveauxLig,
                lig.liElement
            ")
            ->leftJoin('e.idLigneNiveauxLig', 'lig')
            ->leftJoin('e.idNiveauRef', 'ent')
            ->where('e.idUtilisateur = :user')
            ->setParameter('user', $user, Types::INTEGER)
            ->orderBy('lig.idElement', 'ASC')
            ->getQuery();

        return $query->getResult();
    }
}