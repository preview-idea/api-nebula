<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 21/08/2018
 * Time: 17:47
 */

namespace Core\Repository\Group;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class CombiGroupUserRepository extends EntityRepository
{

    public function getIdGroupByUser($idUser)
    {

        $query = $this->createQueryBuilder("s")
            ->select("g.idGroup")
            ->join("s.idGroup", "g")
            ->where('s.idUtilisateur = :idUser')
            ->setParameter('idUser', $idUser, Types::INTEGER)
            ->getQuery();

        return $query->getResult();
    }
}