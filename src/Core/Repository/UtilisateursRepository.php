<?php

namespace Core\Repository;

use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class UtilisateursRepository extends EntityRepository
{

    public function getListUser()
    {
        $query = $this->createQueryBuilder("u")
            ->select("
                u.idUtilisateur,
                u.liNomutilisateur,
                u.liLoginLdap,
                u.liToken,
                u.liEmail,
                u.jsRole,
                u.liCheminPhoto,
                u.idNiveauRef,
                u.idMatricule                
            ")
            ->where('u.isActif = :bit')
            ->setParameter('bit', 1, Types::INTEGER)
            ->orderBy('u.liLoginLdap', 'ASC')
            ->getQuery();

        return $query->getResult();

    }

    public function getDroitUtilisateurStructure($idUtilisateur) {

        $connect = $this->_em->getConnection();
        $sql = $connect->createQueryBuilder()
            ->select('
                v_u.id_utilisateur as "idUtilisateur",
                v_u.id_matricule as "idMatricule",
                v_u.id_qualifcontrat as "idQualifcontrat",
                v_u.is_qualifcontrat_structure as "isQualifcontratStructure",
                v_u.is_droit_voir_structure as "isDroitVoirStructure"                
            ')
            ->from('v_utilisateurs_qualif', 'v_u')
            ->where('v_u.id_utilisateur = :idUtilisateur')
            ->orderBy('v_u.id_utilisateur', 'ASC')
            ->getSQL();

        try {
            $query = $connect->prepare($sql);
            $query->bindValue(':idUtilisateur', $idUtilisateur, Types::INTEGER);
            $query->execute();

            return $query->fetchAllAssociative();

        } catch (\Doctrine\DBAL\Exception $e) {
            return $e->getMessage();

        } catch (Exception $e) {
            return $e->getMessage();

        }

    }

    public function getUserPerimeterByidMatricule( $idUtilisateur )
    {
        $connect = $this->_em->getConnection();
        $sql = $connect->createQueryBuilder()
            ->select('
                v_n.li_objet as "objet",
                v_n.li_liste_filtrage as "filtrage",
                v_n.li_liste_filtrage_matricules_interdits as "matricule_exception"
            ')
            ->from('v_obj_niveaux_objets', 'v_n')
            ->where('id_utilisateur = :user')
            ->andWhere('li_liste_filtrage IS NOT NULL')
            ->andWhere('li_objet IS NOT NULL')
            ->setMaxResults(1)
            ->getSQL();

        try {
            $query = $connect->prepare($sql);
            $query->bindValue(':user', $idUtilisateur, Types::INTEGER);
            $query->execute();

            return $query->fetchAllAssociative();

        } catch (\Doctrine\DBAL\Exception $e) {
            return ['message' => $e->getMessage()];

        } catch (Exception $e) {
            return ['message' => $e->getMessage()];
        }

    }
}