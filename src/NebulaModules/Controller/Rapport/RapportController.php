<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 13/06/2018
 * Time: 13:08
 */

namespace NebulaModules\Controller\Rapport;

use Core\Entity\Group\CombiGroupUser;
use FOS\RestBundle\View\View;
use NebulaModules\Form\Rapport\ObjRapportType;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use NebulaModules\Entity\Rapport\ObjRapport;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class RapportController extends Controller
{
    /** @var EntityManager $manager */
    protected $manager;

    /** @var EntityManager $managerDefault */
    protected $managerDefault;

    /** @var ObjRapport $rapportRepo */
    protected $rapportRepo;

    /** @var CombiGroupUser $combiGroupUser */
    protected $combiGroupUser;

    /**
     * RapportController constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        if (!$this->manager instanceof EntityManager) :
            $this->manager = $managerRegistry->getManager('nebula_modules');
            $this->managerDefault = $managerRegistry->getManager();

            $this->rapportRepo = $this->manager->getRepository(ObjRapport::class);
            $this->combiGroupUser = $this->managerDefault->getRepository(CombiGroupUser::class);

        endif;

    }

    /**
     * @Rest\View()
     * @Rest\Put("/rapport/{idRapport}", requirements={"idRapport"="\d+"})
     * @param Request $request
     * @param bool $clearMissing
     * @return View|object
     */
    public function rapportUpdateAction(Request $request, $clearMissing = true)
    {
        $rapport = $this->rapportOneAction($request);

        if ($rapport instanceof View):
            return $rapport;
        endif;

        $form = $this->createForm(ObjRapportType::class, $rapport);
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) :
            $this->manager->persist($rapport);
            $this->manager->flush();

            return $rapport;
        endif;

        return $form;

    }

    /**
     * @Rest\View()
     * @Rest\Patch("/rapport/{idRapport}", requirements={"idRapport"="\d+"})
     * @param Request $request
     * @return View|object
     */
    public function rapportUpdatePatchAction(Request $request)
    {
        return $this->rapportUpdateAction($request, false);
    }

    /**
     * @Rest\View()
     * @Rest\Get("/rapport/{idRapport}", requirements={"idRapport"="\d+"})
     * @param Request $request
     * @return object
     */
    public function rapportOneAction(Request $request)
    {
        $idRapport = $this->rapportRepo->find($request->get('idRapport'));

        if (!$idRapport) :
            return $this->rapportNotFound();
        endif;

        return $idRapport;
    }

    /**
     * @return View
     */
    protected function rapportNotFound()
    {
        return View::create(
            ['message' => 'Rapport introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }


}