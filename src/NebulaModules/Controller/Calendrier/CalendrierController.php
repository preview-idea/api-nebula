<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 13/06/2018
 * Time: 13:08
 */

namespace NebulaModules\Controller\Calendrier;

use FOS\RestBundle\View\View;
use NebulaModules\Form\Calendrier\ObjCalendrierEtapeDateType;
use NebulaModules\Repository\Calendrier\ObjCalendrierEtapeDateRepository;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use NebulaModules\Entity\Calendrier\ObjCalendrierEtapeDate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class CalendrierController extends Controller
{
    /** @var EntityManager $manager */
    protected $manager;

    /** @var ObjCalendrierEtapeDateRepository $etapeRepo */
    protected $etapeRepo;

    /**
     * CalendrierController constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        if (!$this->manager instanceof EntityManager) :
            $this->manager = $managerRegistry->getManager('nebula_modules');
            $this->etapeRepo = $this->manager->getRepository(ObjCalendrierEtapeDate::class);

        endif;

    }

    /**
     * @Rest\View()
     * @Rest\Post("/calendrier/etapes")
     * @param Request $request
     * @return array
     */
    public function calendrierListAction(Request $request)
    {
        $params = $request->get('params');
        return $this->etapeRepo->getAllEtapes($params);
    }


    /**
     * @Rest\View()
     * @Rest\Get("/calendrier/etapes/{idEtapeDate}", requirements={"idEtapeDate"="\d+"})
     * @param Request $request
     * @return object
     */
    public function etapeDateOneEachAction(Request $request)
    {
        $etapeDate = $this->etapeRepo->find($request->get('idEtapeDate'));

        if (!$etapeDate) :
            return $this->etapeDateNotFound();
        endif;

        return $etapeDate;

    }

    /**
     * @Rest\View()
     * @Rest\Patch("/calendrier/etapes/{idEtapeDate}", requirements={"idEtapeDate"="\d+"})
     * @param Request $request
     * @return View|object
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function etapeDateUpdateAction(Request $request)
    {
        $etapeDate = $this->etapeDateOneEachAction($request);

        $form = $this->createForm(ObjCalendrierEtapeDateType::class, $etapeDate);
        $form->submit($request->request->all(), false);

        if ($form->isValid()) :
            $this->manager->persist($etapeDate);
            $this->manager->flush();

            return $etapeDate;
        endif;

        return $form;
    }

    /**
     * @Rest\View()
     * @Rest\Delete("/calendrier/etapes/{idEtapeDate}", requirements={"idEtapeDate"="\d+"})
     * @param Request $request
     * @return View|array
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function etapeDateRemoveAction(Request $request)
    {
        $etapeDate = $this->etapeDateOneEachAction($request);

        /** @var ObjCalendrierEtapeDate $etapeDate */
        if(!$etapeDate) :
            return $this->etapeDateNotFound();

        endif;

        $this->manager->remove($etapeDate);
        $this->manager->flush();

        return View::create(
            ['message' => 'Suppression effectuée'],
            Response::HTTP_OK
        );

    }

    /**
     * @Rest\View()
     * @Rest\Post("/calendrier/etapes/alert")
     * @param Request $request
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function calendrierEtapeAlertAction(Request $request)
    {
        $filter = $request->get('params');
        return $this->etapeRepo->getLastEtapesForAlert($filter);

    }

    /**
     * @return View
     */
    protected function etapeDateNotFound()
    {
        return View::create(
            ['message' => 'Etape introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }

    /**
     * @Rest\View()
     * @Rest\Post("/calendrier/etapes/periode")
     * @param Request $request
     * @return array
     */
    public function etapeDatePeriodeAction(Request $request)
    {
        $etapeDates = $this->etapeRepo->getEtapesByPeriod($request->get("param"));

        if (!$etapeDates) :
            return $this->etapeDateNotFound();
        endif;

       return $etapeDates;

    }
}