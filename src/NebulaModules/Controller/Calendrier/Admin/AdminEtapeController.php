<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 13/06/2018
 * Time: 13:08
 */

namespace NebulaModules\Controller\Calendrier\Admin;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use NebulaModules\Controller\Calendrier\CalendrierController;
use NebulaModules\Entity\Calendrier\Param\ParamCalendrierEtape;
use NebulaModules\Form\Calendrier\Param\ParamCalendrierEtapeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminEtapeController extends CalendrierController
{

    /** @var ParamCalendrierEtape $paramEtapesRepo */
    protected $paramEtapesRepo;

    /**
     * AdminEtapeController constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry);
        $this->paramEtapesRepo = $this->manager->getRepository(ParamCalendrierEtape::class);

    }

    /**
     * @Rest\View()
     * @Rest\Get("calendrier/admin/etapes")
     * @return object
     */
    public function adminEtapesListAction()
    {
       return $this->paramEtapesRepo->findAll();
    }

    /**
     * @Rest\View()
     * @Rest\Patch("/calendrier/admin/etapes/{idEtape}", requirements={"idEtape"="\d+"})
     * @param Request $request
     * @return View|object
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function adminEtapeUpdateAction(Request $request)
    {
        $etape = $this->etapeOneEachAction($request);

        $form = $this->createForm(ParamCalendrierEtapeType::class, $etape);
        $form->submit($request->request->all(), false);

        if ($form->isValid()) :
            $this->manager->persist($etape);
            $this->manager->flush();

            return $etape;
        endif;

        return $form;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/calendrier/admin/etapes")
     * @param Request $request
     * @return object
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function etapeNewAction(Request $request)
    {
        $etape = new ParamCalendrierEtape();

        $form = $this->createForm(ParamCalendrierEtapeType::class, $etape);
        $form->submit($request->request->all());

        if ($form->isValid()) :
            $this->manager->persist($etape);
            $this->manager->flush();

            return $etape;
        endif;

        return $form;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/calendrier/admin/etapes/{idEtape}", requirements={"idEtape"="\d+"})
     * @param Request $request
     * @return object
     */
    public function etapeOneEachAction(Request $request)
    {
        $etape = $this->paramEtapesRepo->find($request->get('idEtape'));

        if (!$etape) :
            return $this->etapeDateNotFound();
        endif;

        return $etape;

    }

    /**
     * @Rest\View()
     * @Rest\Delete("/calendrier/admin/etapes/{idEtape}", requirements={"idEtape"="\d+"})
     * @param Request $request
     * @return View|array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function adminEtapeRemoveAction(Request $request)
    {
        $etape = $this->etapeOneEachAction($request);

        /** @var ParamCalendrierEtape $etapeDate */
        if (!$etape) :
            return $this->etapeDateNotFound();
        endif;

        $this->manager->remove($etape);
        $this->manager->flush();

        return View::create(
            ['message' => 'Suppression effectuée'],
            Response::HTTP_OK
        );

    }

}