<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 13/06/2018
 * Time: 13:08
 */

namespace NebulaModules\Controller\Calendrier\Admin;

use Doctrine\Common\Persistence\ManagerRegistry;
use NebulaModules\Controller\Calendrier\CalendrierController;
use FOS\RestBundle\View\View;
use NebulaModules\Entity\Calendrier\ObjCalendrierEtapeDate;
use NebulaModules\Entity\Calendrier\Param\ParamCalendrierTypeMensuel;
use NebulaModules\Form\Calendrier\ObjCalendrierEtapeDateType;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

class AdminEtapeDateController extends CalendrierController
{

    /** @var ParamCalendrierTypeMensuel $paramTypeRepo */
    protected $paramTypeRepo;

    /**
     * AdminEtapeDateController constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry);
        $this->paramTypeRepo = $this->manager->getRepository(ParamCalendrierTypeMensuel::class);
    }

    /**
     * @Rest\View()
     * @Rest\Patch("/calendrier/admin/etapedate/{idEtapeDate}", requirements={"idEtapeDate"="\d+"})
     * @param Request $request
     * @return View|object
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function adminEtapeDateUpdateAction(Request $request)
    {
        $etapeDate = $this->etapeDateOneEachAction($request);

        $form = $this->createForm(ObjCalendrierEtapeDateType::class, $etapeDate);
        $form->submit($request->request->all(), false);

        if ($form->isValid()) :
            $this->manager->persist($etapeDate);
            $this->manager->flush();

            return $etapeDate;
        endif;

        return $form;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/calendrier/admin/etapedate")
     * @param Request $request
     * @return object
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function adminEtapeDateAddAction(Request $request)
    {
        $etapeDate = new ObjCalendrierEtapeDate();

        $form = $this->createForm(ObjCalendrierEtapeDateType::class, $etapeDate);
        $form->submit($request->request->all());

        if ($form->isValid()) :
            $this->manager->persist($etapeDate);
            $this->manager->flush();

            return $etapeDate;
        endif;

        return $form;

    }

    /**
     * @Rest\View()
     * @Rest\Delete("/calendrier/admin/etapedate/{idEtapeDate}", requirements={"idEtapeDate"="\d+"})
     * @param Request $request
     * @return View|array
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function adminEtapeDateRemoveAction(Request $request)
    {
        $etapeDate = $this->etapeDateOneEachAction($request);

        /** @var ObjCalendrierEtapeDate $etapeDate */
        if (!$etapeDate) :
            return $this->etapeDateNotFound();
        endif;

        $this->manager->remove($etapeDate);
        $this->manager->flush();

        return View::create(
            ['message' => 'Suppression effectuée'],
            Response::HTTP_OK
        );

    }

    /**
     * @Rest\View()
     * @Rest\Get("/tt/{idEtapeDate}", requirements={"idEtapeDate"="\d+"})
     * @param Request $request
     * @return ObjCalendrierEtapeDate|object
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function ttAction(Request $request)
    {
        $etapeDate = $this->etapeDateOneEachAction($request);

        /** @var ObjCalendrierEtapeDate $etapeDate */
        if(!$etapeDate) :
            return $this->etapeDateNotFound();

        endif;

        return $etapeDate;

    }

    /**
     * @Rest\View()
     * @Rest\Post("/calendrier/admin/etapedate/generate")
     * @param Request $request
     * @return View|array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function adminEtapeDateGenerateTypeMensuelAction(Request $request)    {

        //Suppression des étapes date sur la période sélectionnée afin d'éviter les doublons
        $etapeDates = $this->etapeDatePeriodeAction($request);
        foreach ($etapeDates as $etapeDate) {
            $this->manager->remove($etapeDate);
            $this->manager->flush();
        }

        //récupération du type mensuel calendrier
        $types = $this->paramTypeRepo->getTypes($request->get("param")["idProcess"]);

        $dateDebut = explode("/", $request->get("param")["idDateDebutPeriodeTypeCalendrier"]);
        $dateFin = explode("/", $request->get("param")["idDateFinPeriodeTypeCalendrier"]);

        //Pour chaque mois de la période
        for ($mois = intval($dateDebut[0]); $mois <= intval($dateFin[0]); $mois++)
        {
            //Pour chaque etape date type enregistrée
            foreach ($types as $type) {
                $etapeDate = new ObjCalendrierEtapeDate();

                $etapeDate->setIdEtape($type->getIdEtape());

                //verification de la validité de la date
                //on met automatiquement au dernier jour du mois
                if(!checkdate($mois,$type->getJourEtapeDate(),$dateDebut[1])) {
                    $date = new \DateTime(date("Y-m-d H:i:s", mktime($type->getHeureEtapeDate(), 0, 0, $mois+1,0,$dateDebut[1])));
                } else {
                    $date = new \DateTime(date("Y-m-d H:i:s", mktime($type->getHeureEtapeDate(), 0, 0, $mois,$type->getJourEtapeDate(),$dateDebut[1])));
                }
                $etapeDate->setDtEtapeDate($date);

                if ($type->getJourAlerteDate() && $type->getHeureAlerteDate()) {
                    $dateAlerte = new \DateTime(date("Y-m-d H:i:s", mktime($type->getHeureAlerteDate(), 0, 0, $mois,$type->getJourAlerteDate(),$dateDebut[1])));
                    $etapeDate->setDtEtapeAlerte($dateAlerte);
                }


                $this->manager->persist($etapeDate);
                $this->manager->flush();
            }
        }

        return View::create(
            ['message' => 'Génération effectuée avec succès.'],
            Response::HTTP_OK
        );

    }

}