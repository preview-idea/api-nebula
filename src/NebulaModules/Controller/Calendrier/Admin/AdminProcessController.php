<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 13/06/2018
 * Time: 13:08
 */

namespace NebulaModules\Controller\Calendrier\Admin;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use NebulaModules\Controller\Calendrier\CalendrierController;
use NebulaModules\Entity\Calendrier\Param\ParamCalendrierProcess;
use NebulaModules\Form\Calendrier\Param\ParamCalendrierProcessType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminProcessController extends CalendrierController
{

    /** @var ParamCalendrierProcess $paramProcessRepo */
    protected $paramProcessRepo;

    /**
     * CalendrierController constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry);
        $this->paramProcessRepo = $this->manager->getRepository(ParamCalendrierProcess::class);

    }

    /**
     * @Rest\View()
     * @Rest\Get("calendrier/admin/process")
     * @return object
     */
    public function adminProcessListAction()
    {
        return $this->paramProcessRepo->findAll();
    }

    /**
     * @Rest\View()
     * @Rest\Patch("/calendrier/admin/process/{idProcess}", requirements={"idProcess"="\d+"})
     * @param Request $request
     * @return View|object
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function adminProcessUpdateAction(Request $request)
    {
        $process = $this->processOneEachAction($request);

        $form = $this->createForm(ParamCalendrierProcessType::class, $process);
        $form->submit($request->request->all(), false);

        if ($form->isValid()) :
            $this->manager->persist($process);
            $this->manager->flush();

            return $process;
        endif;

        return $form;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/calendrier/admin/process")
     * @param Request $request
     * @return object
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function processNewAction(Request $request)
    {
        $process = new ParamCalendrierProcess();

        $form = $this->createForm(ParamCalendrierProcessType::class, $process);
        $form->submit($request->request->all());

        if ($form->isValid()) :
            $this->manager->persist($process);
            $this->manager->flush();

            return $process;
        endif;

        return $form;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/calendrier/admin/process/{idProcess}", requirements={"idProcess"="\d+"})
     * @param Request $request
     * @return object
     */
    public function processOneEachAction(Request $request)
    {
        $process = $this->paramProcessRepo->find($request->get('idProcess'));

        if (!$process) :
            return $this->etapeDateNotFound();
        endif;

        return $process;

    }

    /**
     * @Rest\View()
     * @Rest\Delete("/calendrier/admin/process/{idProcess}", requirements={"idProcess"="\d+"})
     * @param Request $request
     * @return View|array
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function adminProcessRemoveAction(Request $request)
    {
        $process = $this->processOneEachAction($request);

        /** @var ParamCalendrierProcess $etapeDate */
        if (!$process) :
            return $this->etapeDateNotFound();
        endif;

        $this->manager->remove($process);
        $this->manager->flush();

        return View::create(
            ['message' => 'Suppression effectuée'],
            Response::HTTP_OK
        );

    }

}