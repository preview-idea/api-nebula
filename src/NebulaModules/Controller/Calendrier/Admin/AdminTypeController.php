<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 13/06/2018
 * Time: 13:08
 */

namespace NebulaModules\Controller\Calendrier\Admin;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use NebulaModules\Entity\Calendrier\Param\ParamCalendrierTypeMensuel;
use NebulaModules\Form\Calendrier\Param\ParamCalendrierTypeMensuelType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminTypeController extends Controller
{
    /** @var EntityManager $manager */
    protected $manager;

    /** @var ParamCalendrierTypeMensuel $paramTypeRepo */
    protected $paramTypeRepo;

    /**
     * AdminTypeController constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        if (!$this->manager instanceof EntityManager) :
            $this->manager = $managerRegistry->getManager('nebula_modules');
            $this->paramTypeRepo = $this->manager->getRepository(ParamCalendrierTypeMensuel::class);

        endif;

    }

    /**
     * @Rest\View()
     * @Rest\Get("calendrier/admin/type")
     * @return object
     */
    public function adminTypeListAction()
    {
       return $this->paramTypeRepo->findAll();
    }

    /**
     * @Rest\View()
     * @Rest\Patch("/calendrier/admin/type/{idType}", requirements={"idType"="\d+"})
     * @param Request $request
     * @return View|object
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function adminTypeUpdateAction(Request $request)
    {
        $type = $this->typeOneEachAction($request);

        $form = $this->createForm(ParamCalendrierTypeMensuelType::class, $type);
        $form->submit($request->request->all(), false);

        if ($form->isValid()) :
            $this->manager->merge($type);
            $this->manager->flush();

            return $type;
        endif;

        return $form;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/calendrier/admin/type")
     * @param Request $request
     * @return object
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function typeNewAction(Request $request)
    {
        $type = new ParamCalendrierTypeMensuel();

        $form = $this->createForm(ParamCalendrierTypeMensuelType::class, $type);
        $form->submit($request->request->all());

        if ($form->isValid()) :
            $this->manager->persist($type);
            $this->manager->flush();

            return $type;
        endif;

        return $form;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/calendrier/admin/type/{idType}", requirements={"idType"="\d+"})
     * @return object
     */
    public function typeOneEachAction(Request $request)
    {
        $type = $this->paramTypeRepo->find($request->get('idType'));

        return $type;

    }

    /**
     * @Rest\View()
     * @Rest\Delete("/calendrier/admin/type/{idType}", requirements={"idType"="\d+"})
     * @param Request $request
     * @return View|array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function adminTypeRemoveAction(Request $request)
    {
        $type = $this->typeOneEachAction($request);

        $this->manager->remove($type);
        $this->manager->flush();

        return View::create(
            ['message' => 'Suppression effectuée'],
            Response::HTTP_OK
        );

    }

}