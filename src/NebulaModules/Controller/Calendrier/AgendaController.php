<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 13/06/2018
 * Time: 13:08
 */

namespace NebulaModules\Controller\Calendrier;

use FOS\RestBundle\Controller\Annotations as Rest;

class AgendaController extends CalendrierController
{
    /**
     * @Rest\View()
     * @Rest\Get("calendrier/agenda")
     * @return array|int|string
     */
    public function agendaListAction()
    {
       return $this->etapeRepo->getLastEtapes();
    }

}