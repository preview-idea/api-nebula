<?php

namespace NebulaModules\Form\Calendrier;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ObjCalendrierEtapeDateType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dtEtapeDate', DateType::class, ["widget" => "single_text", "format" => "dd/MM/yyyy HH:mm:ss"])
            ->add('isRealise')
            ->add('isKo')
            ->add('liCommentaire')
            ->add('dtEtapeAlerte', DateType::class, ["widget" => "single_text", "format" => "dd/MM/yyyy HH:mm:ss"])
            ->add('idEtape')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NebulaModules\Entity\Calendrier\ObjCalendrierEtapeDate'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'nebulamodules_calendrier_objcalendrieretapedate';
    }


}
