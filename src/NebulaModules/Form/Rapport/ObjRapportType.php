<?php

namespace NebulaModules\Form\Rapport;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ObjRapportType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('liRapport')
            ->add('liMotCle')
            ->add('nbVersion')
            ->add('jsAlerte')
            ->add('jsPeriode')
            ->add('liChemin')
            ->add('idTheme')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NebulaModules\Entity\Rapport\ObjRapport'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'nebulamodules_rapport_objrapport';
    }


}
