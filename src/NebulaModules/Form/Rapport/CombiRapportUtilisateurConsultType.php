<?php

namespace NebulaModules\Form\Rapport;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CombiRapportUtilisateurConsultType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idRapport')
            ->add('idUtilisateur')
            ->add('idPeriode')
            ->add('nbConsultation')
            ->add('dtDerniereConsult', DateType::class, ["widget" => "single_text", "format" => "dd/MM/yyyy H:mm:ss"])
            ->add('isArchive')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NebulaModules\Entity\Rapport\CombiRapportUtilisateurConsult'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'nebulamodules_rapport_combirapportutilisateurconsult';
    }


}
