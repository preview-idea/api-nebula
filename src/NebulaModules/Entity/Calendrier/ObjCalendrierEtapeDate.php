<?php

namespace NebulaModules\Entity\Calendrier;

use Doctrine\ORM\Mapping as ORM;

/**
 * ObjCalendrierEtapeDate
 *
 * @ORM\Table(name="obj_calendrier_etape_date", indexes={@ORM\Index(name="IDX_EEA6A3D7C6DA34ED", columns={"id_etape"})})
 * @ORM\Entity(repositoryClass="NebulaModules\Repository\Calendrier\ObjCalendrierEtapeDateRepository")
 */
class ObjCalendrierEtapeDate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_etape_date", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="etape_date_id_etape_date_seq", allocationSize=1, initialValue=1)
     */
    private $idEtapeDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_etape_date", type="datetime", nullable=true)
     */
    private $dtEtapeDate;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_realise", type="bit", nullable=true)
     */
    private $isRealise;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_ko", type="bit", nullable=true)
     */
    private $isKo;

    /**
     * @var string
     *
     * @ORM\Column(name="li_commentaire", type="string", length=60, nullable=true)
     */
    private $liCommentaire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_etape_alerte", type="datetime", nullable=true)
     */
    private $dtEtapeAlerte;

    /**
     * @var \NebulaModules\Entity\Calendrier\Param\ParamCalendrierEtape
     *
     * @ORM\ManyToOne(targetEntity="NebulaModules\Entity\Calendrier\Param\ParamCalendrierEtape")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_etape", referencedColumnName="id_etape")
     * })
     */
    private $idEtape;



    /**
     * Get idEtapeDate
     *
     * @return integer
     */
    public function getIdEtapeDate()
    {
        return $this->idEtapeDate;
    }

    /**
     * Set dtEtapeDate
     *
     * @param \DateTime $dtEtapeDate
     *
     * @return ObjCalendrierEtapeDate
     */
    public function setDtEtapeDate($dtEtapeDate)
    {
        $this->dtEtapeDate = $dtEtapeDate;

        return $this;
    }

    /**
     * Get dtEtapeDate
     *
     * @return \DateTime
     */
    public function getDtEtapeDate()
    {
        return $this->dtEtapeDate;
    }

    /**
     * Set isRealise
     *
     * @param bit $isRealise
     *
     * @return ObjCalendrierEtapeDate
     */
    public function setIsRealise($isRealise)
    {
        $this->isRealise = $isRealise;

        return $this;
    }

    /**
     * Get isRealise
     *
     * @return bit
     */
    public function getIsRealise()
    {
        return $this->isRealise;
    }

    /**
     * Set isKo
     *
     * @param bit $isKo
     *
     * @return ObjCalendrierEtapeDate
     */
    public function setIsKo($isKo)
    {
        $this->isKo = $isKo;

        return $this;
    }

    /**
     * Get isKo
     *
     * @return bit
     */
    public function getIsKo()
    {
        return $this->isKo;
    }

    /**
     * Set liCommentaire
     *
     * @param string $liCommentaire
     *
     * @return ObjCalendrierEtapeDate
     */
    public function setLiCommentaire($liCommentaire)
    {
        $this->liCommentaire = $liCommentaire;

        return $this;
    }

    /**
     * Get liCommentaire
     *
     * @return string
     */
    public function getLiCommentaire()
    {
        return $this->liCommentaire;
    }

    /**
     * Set dtEtapeAlerte
     *
     * @param \DateTime $dtEtapeAlerte
     *
     * @return ObjCalendrierEtapeDate
     */
    public function setDtEtapeAlerte($dtEtapeAlerte)
    {
        $this->dtEtapeAlerte = $dtEtapeAlerte;

        return $this;
    }

    /**
     * Get dtEtapeAlerte
     *
     * @return \DateTime
     */
    public function getDtEtapeAlerte()
    {
        return $this->dtEtapeAlerte;
    }

    /**
     * Set idEtape
     *
     * @param \NebulaModules\Entity\Calendrier\Param\ParamCalendrierEtape $idEtape
     *
     * @return ObjCalendrierEtapeDate
     */
    public function setIdEtape(\NebulaModules\Entity\Calendrier\Param\ParamCalendrierEtape $idEtape = null)
    {
        $this->idEtape = $idEtape;

        return $this;
    }

    /**
     * Get idEtape
     *
     * @return \NebulaModules\Entity\Calendrier\Param\ParamCalendrierEtape
     */
    public function getIdEtape()
    {
        return $this->idEtape;
    }
}
