<?php

namespace NebulaModules\Entity\Rapport;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ObjRapport
 *
 * @ORM\Table(name="obj_rapport", indexes={@ORM\Index(name="IDX_1BAA92ED79F0A638", columns={"id_theme"})})
 * @ORM\Entity(repositoryClass="NebulaModules\Repository\Rapport\ObjRapportRepository")
 */
class ObjRapport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_rapport", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="obj_rapport_id_rapport_seq", allocationSize=1, initialValue=1)
     */
    private $idRapport;

    /**
     * @var string
     *
     * @ORM\Column(name="li_rapport", type="string", length=60, nullable=true)
     */
    private $liRapport;

    /**
     * @var string
     *
     * @ORM\Column(name="li_mot_cle", type="string", length=100, nullable=true)
     */
    private $liMotCle;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_version", type="integer", nullable=true)
     */
    private $nbVersion;

    /**
     * @var json|null
     *
     * @ORM\Column(name="js_alerte", type="json", nullable=true)
     */
    private $jsAlerte;

    /**
     * @var json|null
     *
     * @ORM\Column(name="js_periode", type="json", nullable=true)
     */
    private $jsPeriode;

    /**
     * @var string
     *
     * @ORM\Column(name="li_chemin", type="string", length=150, nullable=true)
     */
    private $liChemin;

    /**
     * @var \NebulaModules\Entity\Rapport\Param\ParamTheme
     *
     * @ORM\ManyToOne(targetEntity="NebulaModules\Entity\Rapport\Param\ParamTheme")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_theme", referencedColumnName="id_theme")
     * })
     */
    private $idTheme;

    /**
     * @var \NebulaModules\Entity\Rapport\CombiRapportModele
     *
     * @ORM\OneToMany(targetEntity="NebulaModules\Entity\Rapport\CombiRapportModele", mappedBy="idRapport", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rapport", referencedColumnName="id_rapport")
     * })
     */
    private $sousRapports;

    /**
     * @var \NebulaModules\Entity\Rapport\CombiRapportChamp
     *
     * @ORM\OneToMany(targetEntity="NebulaModules\Entity\Rapport\CombiRapportChamp", mappedBy="idRapport", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rapport", referencedColumnName="id_rapport")
     * })
     */
    private $champsRecherche;

    /**
     * @var \NebulaModules\Entity\Rapport\CombiRapportGroup
     *
     * @ORM\OneToMany(targetEntity="NebulaModules\Entity\Rapport\CombiRapportGroup", mappedBy="idRapport", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rapport", referencedColumnName="id_rapport")
     * })
     */
    private $groups;

    /**
     * @var \NebulaModules\Entity\Rapport\CombiRapportAlerte
     *
     * @ORM\OneToMany(targetEntity="NebulaModules\Entity\Rapport\CombiRapportAlerte", mappedBy="idRapport", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rapport", referencedColumnName="id_rapport")
     * })
     */
    private $alerts;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sousRapports = new ArrayCollection();
        $this->champsRecherche = new ArrayCollection();
        $this->groups = new ArrayCollection();
        $this->alerts = new ArrayCollection();
    }

    /**
     * Get idRapport.
     *
     * @return int
     */
    public function getIdRapport()
    {
        return $this->idRapport;
    }

    /**
     * Set liRapport.
     *
     * @param string|null $liRapport
     *
     * @return ObjRapport
     */
    public function setLiRapport($liRapport = null)
    {
        $this->liRapport = $liRapport;

        return $this;
    }

    /**
     * Get liRapport.
     *
     * @return string|null
     */
    public function getLiRapport()
    {
        return $this->liRapport;
    }

    /**
     * Set liMotCle.
     *
     * @param string|null $liMotCle
     *
     * @return ObjRapport
     */
    public function setLiMotCle($liMotCle = null)
    {
        $this->liMotCle = $liMotCle;

        return $this;
    }

    /**
     * Get liMotCle.
     *
     * @return string|null
     */
    public function getLiMotCle()
    {
        return $this->liMotCle;
    }

    /**
     * Set nbVersion.
     *
     * @param int|null $nbVersion
     *
     * @return ObjRapport
     */
    public function setNbVersion($nbVersion = null)
    {
        $this->nbVersion = $nbVersion;

        return $this;
    }

    /**
     * Get nbVersion.
     *
     * @return int|null
     */
    public function getNbVersion()
    {
        return $this->nbVersion;
    }

    /**
     * Set jsAlerte.
     *
     * @param json|null $jsAlerte
     *
     * @return ObjRapport
     */
    public function setJsAlerte($jsAlerte = null)
    {
        $this->jsAlerte = $jsAlerte;

        return $this;
    }

    /**
     * Get jsAlerte.
     *
     * @return json|null
     */
    public function getJsAlerte()
    {
        return $this->jsAlerte;
    }

    /**
     * Set jsPeriode.
     *
     * @param json|null $jsPeriode
     *
     * @return ObjRapport
     */
    public function setJsPeriode($jsPeriode = null)
    {
        $this->jsPeriode = $jsPeriode;

        return $this;
    }

    /**
     * Get jsPeriode.
     *
     * @return json|null
     */
    public function getJsPeriode()
    {
        return $this->jsPeriode;
    }

    /**
     * Set liChemin.
     *
     * @param string|null $liChemin
     *
     * @return ObjRapport
     */
    public function setLiChemin($liChemin = null)
    {
        $this->liChemin = $liChemin;

        return $this;
    }

    /**
     * Get liChemin.
     *
     * @return string|null
     */
    public function getLiChemin()
    {
        return $this->liChemin;
    }

    /**
     * Set idTheme.
     *
     * @param \NebulaModules\Entity\Rapport\Param\ParamTheme|null $idTheme
     *
     * @return ObjRapport
     */
    public function setIdTheme(\NebulaModules\Entity\Rapport\Param\ParamTheme $idTheme = null)
    {
        $this->idTheme = $idTheme;

        return $this;
    }

    /**
     * Get idTheme.
     *
     * @return \NebulaModules\Entity\Rapport\Param\ParamTheme|null
     */
    public function getIdTheme()
    {
        return $this->idTheme;
    }

    /**
     * Add sousRapport.
     *
     * @param \NebulaModules\Entity\Rapport\CombiRapportModele $sousRapport
     *
     * @return ObjRapport
     */
    public function addSousRapport(\NebulaModules\Entity\Rapport\CombiRapportModele $sousRapport)
    {
        $this->sousRapports[] = $sousRapport;
        $sousRapport->setIdRapport($this);

        return $this;
    }

    /**
     * Remove sousRapport.
     *
     * @param \NebulaModules\Entity\Rapport\CombiRapportModele $sousRapport
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSousRapport(\NebulaModules\Entity\Rapport\CombiRapportModele $sousRapport)
    {
        return $this->sousRapports->removeElement($sousRapport);
    }

    /**
     * Get sousRapports.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSousRapports()
    {
        return $this->sousRapports;
    }

    /**
     * Add champsRecherche.
     *
     * @param \NebulaModules\Entity\Rapport\CombiRapportChamp $champsRecherche
     *
     * @return ObjRapport
     */
    public function addChampsRecherche(\NebulaModules\Entity\Rapport\CombiRapportChamp $champsRecherche)
    {
        $this->champsRecherche[] = $champsRecherche;
        $champsRecherche->setIdRapport($this);

        return $this;
    }

    /**
     * Remove champsRecherche.
     *
     * @param \NebulaModules\Entity\Rapport\CombiRapportChamp $champsRecherche
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChampsRecherche(\NebulaModules\Entity\Rapport\CombiRapportChamp $champsRecherche)
    {
        return $this->champsRecherche->removeElement($champsRecherche);
    }

    /**
     * Get champsRecherche.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChampsRecherche()
    {
        return $this->champsRecherche;
    }

    /**
     * Add group.
     *
     * @param \NebulaModules\Entity\Rapport\CombiRapportGroup $group
     *
     * @return ObjRapport
     */
    public function addGroup(\NebulaModules\Entity\Rapport\CombiRapportGroup $group)
    {
        $this->groups[] = $group;
        $group->setIdRapport($this);

        return $this;
    }

    /**
     * Remove group.
     *
     * @param \NebulaModules\Entity\Rapport\CombiRapportGroup $group
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGroup(\NebulaModules\Entity\Rapport\CombiRapportGroup $group)
    {
        return $this->groups->removeElement($group);
    }

    /**
     * Get groups.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Add alert.
     *
     * @param \NebulaModules\Entity\Rapport\CombiRapportAlerte $alert
     *
     * @return ObjRapport
     */
    public function addAlert(\NebulaModules\Entity\Rapport\CombiRapportAlerte $alert)
    {
        $this->alerts[] = $alert;
        $alert->setIdRapport($this);

        return $this;
    }

    /**
     * Remove alert.
     *
     * @param \NebulaModules\Entity\Rapport\CombiRapportAlerte $alert
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAlert(\NebulaModules\Entity\Rapport\CombiRapportAlerte $alert)
    {
        return $this->alerts->removeElement($alert);
    }

    /**
     * Get alerts.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlerts()
    {
        return $this->alerts;
    }
}
