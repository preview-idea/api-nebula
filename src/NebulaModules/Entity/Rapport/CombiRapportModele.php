<?php

namespace NebulaModules\Entity\Rapport;

use Doctrine\ORM\Mapping as ORM;

/**
 * CombiRapportModele
 *
 * @ORM\Table(name="combi_rapport_modele", indexes={@ORM\Index(name="IDX_8A0B7F7960A909EC", columns={"id_rapport"}), @ORM\Index(name="IDX_8A0B7F79363530B5", columns={"id_modele"})})
 * @ORM\Entity(repositoryClass="NebulaModules\Repository\Rapport\CombiRapportModeleRepository")
 */
class CombiRapportModele
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_rapport_modele", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="combi_rapport_modele_id_ligne_seq", allocationSize=1, initialValue=1)
     */
    private $idRapportModele;


    /**
     * @var string|null
     *
     * @ORM\Column(name="li_vue_ref", type="string", length=100, nullable=true)
     */
    private $liVueRef;


    /**
     * @var json|null
     *
     * @ORM\Column(name="js_structure", type="json", nullable=true)
     */
    private $jsStructure;

    /**
     * @var json|null
     *
     * @ORM\Column(name="js_params", type="json", nullable=true)
     */
    private $jsParams;

    /**
     * @var \NebulaModules\Entity\Rapport\ObjRapport
     *
     * @ORM\ManyToOne(targetEntity="NebulaModules\Entity\Rapport\ObjRapport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rapport", referencedColumnName="id_rapport")
     * })
     */
    private $idRapport;

    /**
     * @var \NebulaModules\Entity\Rapport\Param\ParamModele
     *
     * @ORM\ManyToOne(targetEntity="NebulaModules\Entity\Rapport\Param\ParamModele")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_modele", referencedColumnName="id_modele")
     * })
     */
    private $idModele;



    /**
     * Get idRapportModele.
     *
     * @return int
     */
    public function getIdRapportModele()
    {
        return $this->idRapportModele;
    }


    /**
     * Set liVueRef.
     *
     * @param string|null $liVueRef
     *
     * @return CombiRapportModele
     */
    public function setLiVueRef($liVueRef = null)
    {
        $this->liVueRef = $liVueRef;

        return $this;
    }

    /**
     * Get liVueRef.
     *
     * @return string|null
     */
    public function getLiVueRef()
    {
        return $this->liVueRef;
    }


    /**
     * Set jsStructure.
     *
     * @param json|null $jsStructure
     *
     * @return CombiRapportModele
     */
    public function setJsStructure($jsStructure = null)
    {
        $this->jsStructure = $jsStructure;

        return $this;
    }

    /**
     * Get jsStructure.
     *
     * @return json|null
     */
    public function getJsStructure()
    {
        return $this->jsStructure;
    }


    /**
     * Set jsParams.
     *
     * @param json|null $jsParams
     *
     * @return CombiRapportModele
     */
    public function setJsParams($jsParams = null)
    {
        $this->jsParams = $jsParams;

        return $this;
    }

    /**
     * Get jsParams.
     *
     * @return json|null
     */
    public function getJsParams()
    {
        return $this->jsParams;
    }


    /**
     * Set idRapport.
     *
     * @param \NebulaModules\Entity\Rapport\ObjRapport|null $idRapport
     *
     * @return CombiRapportModele
     */
    public function setIdRapport(\NebulaModules\Entity\Rapport\ObjRapport $idRapport = null)
    {
        $this->idRapport = $idRapport;

        return $this;
    }

    /**
     * Get idRapport.
     *
     * @return \NebulaModules\Entity\Rapport\ObjRapport|null
     */
    public function getIdRapport()
    {
        return $this->idRapport;
    }

    /**
     * Set idModele.
     *
     * @param \NebulaModules\Entity\Rapport\Param\ParamModele|null $idModele
     *
     * @return CombiRapportModele
     */
    public function setIdModele(\NebulaModules\Entity\Rapport\Param\ParamModele $idModele = null)
    {
        $this->idModele = $idModele;

        return $this;
    }

    /**
     * Get idModele.
     *
     * @return \NebulaModules\Entity\Rapport\ObjModele|null
     */
    public function getIdModele()
    {
        return $this->idModele;
    }

}
