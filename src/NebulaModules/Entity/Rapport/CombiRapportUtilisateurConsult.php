<?php

namespace NebulaModules\Entity\Rapport;

use Doctrine\ORM\Mapping as ORM;

/**
 * CombiRapportUtilisateurConsult
 *
 * @ORM\Table(name="combi_rapport_utilisateur_consult", indexes={@ORM\Index(name="IDX_5A9F8CD060A909EC", columns={"id_rapport"}), @ORM\Index(name="IDX_5A9F8CD050EAE44", columns={"id_utilisateur"})})
 * @ORM\Entity(repositoryClass="NebulaModules\Repository\Rapport\CombiRapportUtilisateurConsultRepository")
 */
class CombiRapportUtilisateurConsult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_rapport_utilisateur_consult", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="combi_rapport_utilisateur_consult_id_rapport_utilisateur_consult_seq", allocationSize=1, initialValue=1)
     */
    private $idRapportUtilisateurConsult;

    /**
     * @var int
     *
     * @ORM\Column(name="id_periode", type="integer", nullable=false)
     */
    private $idPeriode;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_consultation", type="integer", nullable=false)
     */
    private $nbConsultation;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dt_derniere_consult", type="datetime", nullable=true)
     */
    private $dtDerniereConsult;

    /**
     * @var bit|null
     *
     * @ORM\Column(name="is_archive", type="bit", nullable=true)
     */
    private $isArchive = '0';

    /**
     * @var \NebulaModules\Entity\Rapport\ObjRapport
     *
     * @ORM\ManyToOne(targetEntity="NebulaModules\Entity\Rapport\ObjRapport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rapport", referencedColumnName="id_rapport")
     * })
     */
    private $idRapport;

    /**
     * @var \NebulaModules\Entity\Rapport\Param\ParamUsers
     *
     * @ORM\ManyToOne(targetEntity="NebulaModules\Entity\Rapport\Param\ParamUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;



    /**
     * Get idRapportUtilisateurConsult.
     *
     * @return int
     */
    public function getIdRapportUtilisateurConsult()
    {
        return $this->idRapportUtilisateurConsult;
    }

    /**
     * Set idPeriode.
     *
     * @param int $idPeriode
     *
     * @return CombiRapportUtilisateurConsult
     */
    public function setIdPeriode($idPeriode)
    {
        $this->idPeriode = $idPeriode;

        return $this;
    }

    /**
     * Get idPeriode.
     *
     * @return int
     */
    public function getIdPeriode()
    {
        return $this->idPeriode;
    }

    /**
     * Set nbConsultation.
     *
     * @param int $nbConsultation
     *
     * @return CombiRapportUtilisateurConsult
     */
    public function setNbConsultation($nbConsultation)
    {
        $this->nbConsultation = $nbConsultation;

        return $this;
    }

    /**
     * Get nbConsultation.
     *
     * @return int
     */
    public function getNbConsultation()
    {
        return $this->nbConsultation;
    }

    /**
     * Set dtDerniereConsult.
     *
     * @param \DateTime|null $dtDerniereConsult
     *
     * @return CombiRapportUtilisateurConsult
     */
    public function setDtDerniereConsult($dtDerniereConsult = null)
    {
        $this->dtDerniereConsult = $dtDerniereConsult;

        return $this;
    }

    /**
     * Get dtDerniereConsult.
     *
     * @return \DateTime|null
     */
    public function getDtDerniereConsult()
    {
        return $this->dtDerniereConsult;
    }

    /**
     * Set isArchive.
     *
     * @param bit|null $isArchive
     *
     * @return CombiRapportUtilisateurConsult
     */
    public function setIsArchive($isArchive = null)
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * Get isArchive.
     *
     * @return bit|null
     */
    public function getIsArchive()
    {
        return $this->isArchive;
    }

    /**
     * Set idRapport.
     *
     * @param \NebulaModules\Entity\Rapport\ObjRapport|null $idRapport
     *
     * @return CombiRapportUtilisateurConsult
     */
    public function setIdRapport(\NebulaModules\Entity\Rapport\ObjRapport $idRapport = null)
    {
        $this->idRapport = $idRapport;

        return $this;
    }

    /**
     * Get idRapport.
     *
     * @return \NebulaModules\Entity\Rapport\ObjRapport|null
     */
    public function getIdRapport()
    {
        return $this->idRapport;
    }

    /**
     * Set idUtilisateur.
     *
     * @param \NebulaModules\Entity\Rapport\Param\ParamUsers|null $idUtilisateur
     *
     * @return CombiRapportUtilisateurConsult
     */
    public function setIdUtilisateur(\NebulaModules\Entity\Rapport\Param\ParamUsers $idUtilisateur = null)
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }

    /**
     * Get idUtilisateur.
     *
     * @return \NebulaModules\Entity\Rapport\Param\ParamUsers|null
     */
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }
}
