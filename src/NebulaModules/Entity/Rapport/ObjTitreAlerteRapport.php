<?php

namespace NebulaModules\Entity\Rapport;

use Doctrine\ORM\Mapping as ORM;

/**
 * ObjTitreAlerteRapport
 *
 * @ORM\Table(name="obj_titre_alerte_rapport", indexes={@ORM\Index(name="IDX_5E38D2C514BE4EC9", columns={"id_type_alerte_rapport"})})
 * @ORM\Entity
 */
class ObjTitreAlerteRapport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_titre_alerte_rapport", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="obj_titre_alerte_rapport_id_titre_alerte_rapport_seq", allocationSize=1, initialValue=1)
     */
    private $idTitreAlerteRapport;

    /**
     * @var string|null
     *
     * @ORM\Column(name="li_titre_alerte_rapport", type="string", length=150, nullable=true)
     */
    private $liTitreAlerteRapport;

    /**
     * @var \NebulaModules\Entity\Rapport\Param\ParamTypeAlerteRapport
     *
     * @ORM\ManyToOne(targetEntity="NebulaModules\Entity\Rapport\Param\ParamTypeAlerteRapport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_alerte_rapport", referencedColumnName="id_type_alerte_rapport")
     * })
     */
    private $idTypeAlerteRapport;

    /**
     * Get idTitreAlerteRapport.
     *
     * @return int
     */
    public function getIdTitreAlerteRapport()
    {
        return $this->idTitreAlerteRapport;
    }

    /**
     * Set liTitreAlerteRapport.
     *
     * @param string|null $liTitreAlerteRapport
     *
     * @return ObjTitreAlerteRapport
     */
    public function setLiTitreAlerteRapport($liTitreAlerteRapport = null)
    {
        $this->liTitreAlerteRapport = $liTitreAlerteRapport;

        return $this;
    }

    /**
     * Get liTitreAlerteRapport.
     *
     * @return string|null
     */
    public function getLiTitreAlerteRapport()
    {
        return $this->liTitreAlerteRapport;
    }

    /**
     * Set idTypeAlerteRapport.
     *
     * @param \NebulaModules\Entity\Rapport\Param\ParamTypeAlerteRapport|null $idTypeAlerteRapport
     *
     * @return ObjTitreAlerteRapport
     */
    public function setIdTypeAlerteRapport(\NebulaModules\Entity\Rapport\Param\ParamTypeAlerteRapport $idTypeAlerteRapport = null)
    {
        $this->idTypeAlerteRapport = $idTypeAlerteRapport;

        return $this;
    }

    /**
     * Get idTypeAlerteRapport.
     *
     * @return \NebulaModules\Entity\Rapport\Param\ParamTypeAlerteRapport|null
     */
    public function getIdTypeAlerteRapport()
    {
        return $this->idTypeAlerteRapport;
    }
}
