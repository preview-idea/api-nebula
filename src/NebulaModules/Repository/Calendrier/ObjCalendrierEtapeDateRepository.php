<?php


namespace NebulaModules\Repository\Calendrier;


use Core\Custom\Types\BitType;
use Doctrine\ORM\EntityRepository;

class ObjCalendrierEtapeDateRepository extends EntityRepository
{
    public function getLastEtapes()
    {

        $query = $this->createQueryBuilder("s")
            ->select("s")
            ->where("s.dtEtapeDate >= CURRENT_DATE()")
            ->orderBy('s.dtEtapeDate', 'ASC')
            ->setMaxResults(5)
            ->getQuery();

        return $query->getResult();
    }

    public function getAllEtapes($params)
    {

        $query = $this->createQueryBuilder("s")
            ->select("
                s.idEtapeDate,
                s.dtEtapeDate,
                s.isRealise,
                s.isKo,
                s.liCommentaire,
                s.dtEtapeAlerte,
                pe.idEtape,
                pe.liEtape,
                pe.isPrive
            ")
            ->leftJoin("s.idEtape", "pe");

        if (isset($params['is_prive'])) :
            $query = $query->andWhere("pe.isPrive = :isPrive")
                ->setParameter('isPrive', $params['is_prive'], BitType::BIT);
        endif;

        if (isset($params['id_process'])) :
            $query = $query->andWhere("pe.idProcess = :idProcess")
                ->setParameter('idProcess', $params['id_process']);

        endif;

        $query = $query->getQuery();

        return $query->getResult();
    }

    /**
     * @param $filter
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLastEtapesForAlert($filter)
    {

        $typeSTC = implode(',', [31,35]);

        $query = $this->createQueryBuilder("ed")
            ->select("
                ed.idEtapeDate,
                ed.dtEtapeDate,
                ed.dtEtapeAlerte,
                pe.liCommentaire,
                pe.idEtape,
                pe.liEtape,
                pp.idProcess,
                pp.liProcess
            ")
            ->join('ed.idEtape', 'pe')
            ->leftJoin('pe.idProcess', 'pp')
            ->where("ed.dtEtapeAlerte IS NOT NULL");

        // Filter for type STC
        if (isset($filter['type']) && (strtoupper($filter['type']) === 'STC')) :

            // If next event we take it
            $query = isset($filter['next_event']) && $filter['next_event']
                ? $query->andWhere("ed.dtEtapeDate IS NOT NULL AND CURRENT_TIMESTAMP() <= ed.dtEtapeDate")
                : $query->andWhere("DATE_DIFF(CURRENT_TIMESTAMP(), ed.dtEtapeAlerte) = 0");

            $query = $query->andWhere(sprintf(' pe.idEtape IN (%s)', $typeSTC));

        else :
            $query = $query->andWhere("CURRENT_TIMESTAMP() >= ed.dtEtapeAlerte")
                ->andWhere("ed.dtEtapeDate IS NOT NULL AND CURRENT_TIMESTAMP() <= ed.dtEtapeDate")
                ->andWhere( sprintf(' pe.idEtape NOT IN (%s)', $typeSTC));

        endif;

        $query = $query->andWhere("pe.isPrive = :Non")
            ->orderBy('ed.dtEtapeDate', 'ASC')
            ->setParameter('Non', 0, BitType::BIT)
            ->setMaxResults(1)
            ->getQuery();

        return $query->getOneOrNullResult();

    }

    public function getEtapesByPeriod($param)
    {
        $dateDebut = implode("-", array_reverse(explode("/", $param["idDateDebutPeriodeTypeCalendrier"])))."-01 00:00:00";
        $dateFin = implode("-", array_reverse(explode("/", $param["idDateFinPeriodeTypeCalendrier"])))."-31 24:00:00";

        $query = $this->createQueryBuilder("s")
            ->select("s")
            ->leftJoin('s.idEtape', 'p')
            ->where("s.dtEtapeDate >= :debut")
            ->andWhere("s.dtEtapeDate <= :fin")
            ->setParameter('debut', new \DateTime($dateDebut))
            ->setParameter('fin', new \DateTime($dateFin));

        if (!empty($param['idProcess'])) :
            $query = $query->andWhere("p.idProcess = :idProcess")
                ->setParameter('idProcess',$param["idProcess"]);
        endif;

        $query = $query->getQuery();

        return $query->getResult();
    }
}