<?php


namespace NebulaModules\Repository\Rapport;


use Doctrine\ORM\EntityRepository;

class CombiRapportModeleRepository extends EntityRepository
{
    public function getCombiRapportModeleByIdRapport($idRapport)
    {

        $query = $this->createQueryBuilder("c")
            ->select("c")
            ->join("c.idRapport", "r")
            ->join("c.idModele", "m")
            ->where("c.idRapport = :idRapport")
            ->setParameter('idRapport', $idRapport)
            ->getQuery();

        return $query->getResult();
    }
}