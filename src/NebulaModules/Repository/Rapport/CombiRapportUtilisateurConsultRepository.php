<?php


namespace NebulaModules\Repository\Rapport;

use Doctrine\ORM\EntityRepository;

class CombiRapportUtilisateurConsultRepository extends EntityRepository
{
    /**
     * @param $idRapport
     * @param $idUtilisateur
     * @param $idPeriode
     * @return mixed|string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getConsultationByRapportUserPeriod($idRapport, $idUtilisateur, $idPeriode)
    {
        $query = $this->createQueryBuilder("c")
            ->select("c")
            ->where("c.idRapport = :idRapport")
            ->andWhere("c.idUtilisateur = :idUtilisateur")
            ->andWhere("c.idPeriode = :idPeriode")
            ->setParameter('idRapport', $idRapport)
            ->setParameter('idUtilisateur', $idUtilisateur)
            ->setParameter('idPeriode', $idPeriode)
            ->getQuery();

        return $query->getOneOrNullResult();
    }
}