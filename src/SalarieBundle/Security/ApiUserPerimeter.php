<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 30/07/2018
 * Time: 09:38
 */

namespace SalarieBundle\Security;

use Core\Security\Traits\TraitSQLFilter;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ApiUserPerimeter
{

    use TraitSQLFilter;

    public function __construct(RegistryInterface $registry, TokenStorageInterface $storage)
    {
        $this->storage = $storage;
        $this->manager = $registry;
        $this->filter = $this->manager
            ->getManager('salarie')
            ->getFilters()
            ->enable('api_perimeter_filter');
    }

}