<?php

namespace SalarieBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ObjSalarieType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dtNaissance', DateType::class, ["widget" => "single_text", "format" => "dd/MM/yyyy"])
            ->add('liVilleNaissance')
            ->add('liDepNaissance')
            ->add('idNirDef')
            ->add('liNom')
            ->add('liPrenom')
            ->add('liNomVoie')
            ->add('liAdresse2')
            ->add('liPrefTelephone1')
            ->add('liTelephone1')
            ->add('liPrefTelephone2')
            ->add('liTelephone2')
            ->add('liMail', EmailType::class)
            ->add('liNumeroPiece')
            ->add('dtDebutValidite', DateType::class, ["widget" => "single_text", "format" => "dd/MM/yyyy"])
            ->add('dtFinValidite', DateType::class, ["widget" => "single_text", "format" => "dd/MM/yyyy"])
            ->add('liCheminPhoto')
            ->add('isNirProv')
            ->add('isEnvoiMail')
            ->add('liNomUsage')
            ->add('isPaiementVirement')
            ->add('liTitulaireCompte')
            ->add('liBic')
            ->add('liIban')
            ->add('liDomiciliationBancaire')
            ->add('liVilleEtranger')
            ->add('liCodepostalEtranger')
            ->add('idAgence')
            ->add('idCivilite')
            ->add('idCodepostal')
            ->add('idMatriculeMaj')
            ->add('idPays')
            ->add('idSituationFamille')
            ->add('idTypePiece')
            ->add('idVille')
            ->add('idPaysNaissance')
            ->add('idPaysNationalite')
            ->add('idBddPlannet')
            ->add('enfants', CollectionType::class, ["entry_type" => ObjSalarieEnfantsType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'by_reference' => false
            ])
            ->add('personnes', CollectionType::class, ["entry_type" => ObjSalariePersonnesacontacterType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'by_reference' => false
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SalarieBundle\Entity\ObjSalarie'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'salariebundle_objsalarie';
    }


}
