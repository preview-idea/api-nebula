<?php

namespace SalarieBundle\Form\Param;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParamActiviteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('liActivite')
            ->add('idActiviteAx')
            ->add('dtDebutActif',DateType::class, ["widget" => "single_text", "format" => "dd/MM/yyyy"])
            ->add('dtFinActif', DateType::class, ["widget" => "single_text", "format" => "dd/MM/yyyy"])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SalarieBundle\Entity\Param\ParamActivite'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'salariebundle_param_paramactivite';
    }


}
