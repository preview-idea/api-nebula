<?php

namespace SalarieBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ObjSalarieProtectionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dtDateDebut', DateType::class, ["widget" => "single_text", "format" => "dd/MM/yyyy"])
            ->add('dtDateFinTheorique', DateType::class, ["widget" => "single_text", "format" => "dd/MM/yyyy"])
            ->add('dtDateFinReelle', DateType::class, ["widget" => "single_text", "format" => "dd/MM/yyyy"])
            ->add('idOrgSyndicale')
            ->add('idPerimetreElectoral')
            ->add('idTypeProtection')
            ->add('idMatriculeMaj')
            ->add('idMatricule')
            ->add('liCommentaire')
            ->add('mandats', CollectionType::class, ["entry_type" => ObjSalarieMandatType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'by_reference' => false
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SalarieBundle\Entity\ObjSalarieProtection'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'salariebundle_objsalarieprotection';
    }


}
