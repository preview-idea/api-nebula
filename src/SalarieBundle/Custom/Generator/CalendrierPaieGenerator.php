<?php

namespace SalarieBundle\Custom\Generator;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

class CalendrierPaieGenerator extends AbstractIdGenerator
{

    public function generate(EntityManager $em, $entity)
    {
        return $entity->getDtPremierJourMois()->format('Ym');
    }
}