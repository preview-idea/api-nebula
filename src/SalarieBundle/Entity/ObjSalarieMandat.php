<?php

namespace SalarieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * ObjSalarieMandat
 *
 * @ORM\Table(name="obj_salarie_mandat", indexes={@ORM\Index(name="IDX_DF87922112BEFBD3", columns={"id_cse"}), @ORM\Index(name="IDX_DF87922153B78B7C", columns={"id_org_syndicale"}), @ORM\Index(name="IDX_DF879221189FBDFD", columns={"id_perimetre_electoral"}), @ORM\Index(name="IDX_DF8792212596491A", columns={"id_type_mandat"}), @ORM\Index(name="IDX_DF879221126FAF62", columns={"id_protection"}), @ORM\Index(name="IDX_DF879221CBC3B464", columns={"id_matricule_maj"}), @ORM\Index(name="IDX_DF879221928760BB", columns={"id_matricule"})})
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\ObjSalarieMandatRepository")
 */
class ObjSalarieMandat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_mandat", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="obj_salarie_mandat_id_mandat_seq", allocationSize=1, initialValue=1)
     */
    private $idMandat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_telephone", type="string", length=16, nullable=true)
     */
    private $liTelephone;

    /**
     * @var string
     *
     * @ORM\Column(name="li_mail", type="string", length=50, nullable=true)
     */
    private $liMail;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_debut", type="date", nullable=false)
     */
    private $dtDateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_fin_theorique", type="date", nullable=false)
     */
    private $dtDateFinTheorique;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_fin_reelle", type="date", nullable=true)
     */
    private $dtDateFinReelle;

    /**
     * @var string
     *
     * @ORM\Column(name="li_commentaire", type="string", length=100, nullable=true)
     */
    private $liCommentaire;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_heure_delegation", type="smallint", nullable=true)
     */
    private $nbHeureDelegation;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCse
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCse")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cse", referencedColumnName="id_cse")
     * })
     */
    private $idCse;

    /**
     * @var \SalarieBundle\Entity\Param\ParamOrgSyndicale
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamOrgSyndicale")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_org_syndicale", referencedColumnName="id_org_syndicale")
     * })
     */
    private $idOrgSyndicale;

    /**
     * @var \SalarieBundle\Entity\Param\ParamPerimetreElectoral
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamPerimetreElectoral")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_perimetre_electoral", referencedColumnName="id_perimetre_electoral")
     * })
     */
    private $idPerimetreElectoral;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeMandat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeMandat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_mandat", referencedColumnName="id_type_mandat")
     * })
     */
    private $idTypeMandat;

    /**
     * @var \SalarieBundle\Entity\ObjSalarieProtection
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\ObjSalarieProtection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_protection", referencedColumnName="id_protection")
     * })
     * @Serializer\Exclude()
     */
    private $idProtection;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\ObjSalarie
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\ObjSalarie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule", referencedColumnName="id_matricule")
     * })
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_commission", type="string", length=200, nullable=true)
     */
    private $liCommission;

    /**
     * Get idMandat
     *
     * @return integer
     */
    public function getIdMandat()
    {
        return $this->idMandat;
    }

    /**
     * Set liTelephone
     *
     * @param string $liTelephone
     *
     * @return ObjSalarieMandat
     */
    public function setLiTelephone($liTelephone)
    {
        $this->liTelephone = $liTelephone;

        return $this;
    }

    /**
     * Get liTelephone
     *
     * @return string
     */
    public function getLiTelephone()
    {
        return $this->liTelephone;
    }

    /**
     * Set liMail
     *
     * @param string $liMail
     *
     * @return ObjSalarieMandat
     */
    public function setLiMail($liMail)
    {
        $this->liMail = $liMail;

        return $this;
    }

    /**
     * Get liMail
     *
     * @return string
     */
    public function getLiMail()
    {
        return $this->liMail;
    }

    /**
     * Set dtDateDebut
     *
     * @param \DateTime $dtDateDebut
     *
     * @return ObjSalarieMandat
     */
    public function setDtDateDebut($dtDateDebut)
    {
        $this->dtDateDebut = $dtDateDebut;

        return $this;
    }

    /**
     * Get dtDateDebut
     *
     * @return \DateTime
     */
    public function getDtDateDebut()
    {
        return $this->dtDateDebut;
    }

    /**
     * Set dtDateFinTheorique
     *
     * @param \DateTime $dtDateFinTheorique
     *
     * @return ObjSalarieMandat
     */
    public function setDtDateFinTheorique($dtDateFinTheorique)
    {
        $this->dtDateFinTheorique = $dtDateFinTheorique;

        return $this;
    }

    /**
     * Get dtDateFinTheorique
     *
     * @return \DateTime
     */
    public function getDtDateFinTheorique()
    {
        return $this->dtDateFinTheorique;
    }

    /**
     * Set dtDateFinReelle
     *
     * @param \DateTime $dtDateFinReelle
     *
     * @return ObjSalarieMandat
     */
    public function setDtDateFinReelle($dtDateFinReelle)
    {
        $this->dtDateFinReelle = $dtDateFinReelle;

        return $this;
    }

    /**
     * Get dtDateFinReelle
     *
     * @return \DateTime
     */
    public function getDtDateFinReelle()
    {
        return $this->dtDateFinReelle;
    }

    /**
     * Set liCommentaire
     *
     * @param string $liCommentaire
     *
     * @return ObjSalarieMandat
     */
    public function setLiCommentaire($liCommentaire)
    {
        $this->liCommentaire = $liCommentaire;

        return $this;
    }

    /**
     * Get liCommentaire
     *
     * @return string
     */
    public function getLiCommentaire()
    {
        return $this->liCommentaire;
    }

    /**
     * Set nbHeureDelegation
     *
     * @param integer $nbHeureDelegation
     *
     * @return ObjSalarieMandat
     */
    public function setNbHeureDelegation($nbHeureDelegation)
    {
        $this->nbHeureDelegation = $nbHeureDelegation;

        return $this;
    }

    /**
     * Get nbHeureDelegation
     *
     * @return integer
     */
    public function getNbHeureDelegation()
    {
        return $this->nbHeureDelegation;
    }

    /**
     * Set liCommission
     *
     * @param string $liCommission
     *
     * @return ObjSalarieMandat
     */
    public function setLiCommission($liCommission)
    {
        $this->liCommission = $liCommission;

        return $this;
    }

    /**
     * Get liCommission
     *
     * @return string
     */
    public function getLiCommission()
    {
        return $this->liCommission;
    }

    /**
     * Set idCse
     *
     * @param \SalarieBundle\Entity\Param\ParamCse $idCse
     *
     * @return ObjSalarieMandat
     */
    public function setIdCse(\SalarieBundle\Entity\Param\ParamCse $idCse = null)
    {
        $this->idCse = $idCse;

        return $this;
    }

    /**
     * Get idCse
     *
     * @return \SalarieBundle\Entity\Param\ParamCse
     */
    public function getIdCse()
    {
        return $this->idCse;
    }

    /**
     * Set idOrgSyndicale
     *
     * @param \SalarieBundle\Entity\Param\ParamOrgSyndicale $idOrgSyndicale
     *
     * @return ObjSalarieMandat
     */
    public function setIdOrgSyndicale(\SalarieBundle\Entity\Param\ParamOrgSyndicale $idOrgSyndicale = null)
    {
        $this->idOrgSyndicale = $idOrgSyndicale;

        return $this;
    }

    /**
     * Get idOrgSyndicale
     *
     * @return \SalarieBundle\Entity\Param\ParamOrgSyndicale
     */
    public function getIdOrgSyndicale()
    {
        return $this->idOrgSyndicale;
    }

    /**
     * Set idPerimetreElectoral
     *
     * @param \SalarieBundle\Entity\Param\ParamPerimetreElectoral $idPerimetreElectoral
     *
     * @return ObjSalarieMandat
     */
    public function setIdPerimetreElectoral(\SalarieBundle\Entity\Param\ParamPerimetreElectoral $idPerimetreElectoral = null)
    {
        $this->idPerimetreElectoral = $idPerimetreElectoral;

        return $this;
    }

    /**
     * Get idPerimetreElectoral
     *
     * @return \SalarieBundle\Entity\Param\ParamPerimetreElectoral
     */
    public function getIdPerimetreElectoral()
    {
        return $this->idPerimetreElectoral;
    }

    /**
     * Set idTypeMandat
     *
     * @param \SalarieBundle\Entity\Param\ParamTypeMandat $idTypeMandat
     *
     * @return ObjSalarieMandat
     */
    public function setIdTypeMandat(\SalarieBundle\Entity\Param\ParamTypeMandat $idTypeMandat = null)
    {
        $this->idTypeMandat = $idTypeMandat;

        return $this;
    }

    /**
     * Get idTypeMandat
     *
     * @return \SalarieBundle\Entity\Param\ParamTypeMandat
     */
    public function getIdTypeMandat()
    {
        return $this->idTypeMandat;
    }

    /**
     * Set idProtection
     *
     * @param \SalarieBundle\Entity\ObjSalarieProtection $idProtection
     *
     * @return ObjSalarieMandat
     */
    public function setIdProtection(\SalarieBundle\Entity\ObjSalarieProtection $idProtection = null)
    {
        $this->idProtection = $idProtection;

        return $this;
    }

    /**
     * Get idProtection
     *
     * @return \SalarieBundle\Entity\ObjSalarieProtection
     */
    public function getIdProtection()
    {
        return $this->idProtection;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ObjSalarieMandat
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idMatricule
     *
     * @param \SalarieBundle\Entity\ObjSalarie $idMatricule
     *
     * @return ObjSalarieMandat
     */
    public function setIdMatricule(\SalarieBundle\Entity\ObjSalarie $idMatricule = null)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return \SalarieBundle\Entity\ObjSalarie
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }
}
