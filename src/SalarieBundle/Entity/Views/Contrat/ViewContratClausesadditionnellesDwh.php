<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/10/2018
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_contrat_clausesadditionnelles_dwh")
 * @ORM\Entity()
 */
class ViewContratClausesadditionnellesDwh
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_ligne_clausesadditionnelles", type="integer", nullable=false)
     */
    private $idLigneClausesadditionnelles;

    /**
     * @var \SalarieBundle\Entity\Views\Contrat\ViewContratDwh
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Views\Contrat\ViewContratDwh", inversedBy="clause_add")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_societe_ancien_employeur", type="string", nullable=true)
     */
    private $liSocieteAncienEmployeur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_embauche_ancien_employeur", type="datetime", nullable=true)
     */
    private $dtEmbaucheAncienEmployeur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_signature_contrat_ancien_employeur", type="datetime", nullable=true)
     */
    private $dtSignatureContratAncienEmployeur;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_site_repris", type="string", nullable=true)
     */
    private $liNomSiteRepris;

    /**
     * @var string
     *
     * @ORM\Column(name="li_categorieemploye", type="string", nullable=true)
     */
    private $liCategorieemploye;

    /**
     * @var string
     *
     * @ORM\Column(name="li_coeffcontrat_ancien_employeur", type="string", nullable=true)
     */
    private $liCoeffcontratAncienEmployeur;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_salarie_protege", type="bit", nullable=false)
     */
    private $isSalarieProtege;

    /**
     * @var string
     *
     * @ORM\Column(name="li_cycle_formation", type="string", nullable=true)
     */
    private $liCycleFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_heures", type="decimal", precision=5, scale=1, nullable=true)
     */
    private $nbHeures;

    /**
     * @var string
     *
     * @ORM\Column(name="li_intitule_diplome", type="string", nullable=true)
     */
    private $liIntituleDiplome;

    /**
     * @var string
     *
     * @ORM\Column(name="li_prenom_remplacement", type="string", nullable=true)
     */
    private $liPrenomRemplacement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_remplacement", type="string", nullable=true)
     */
    private $liNomRemplacement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_motif_personne_remplacement", type="string", nullable=true)
     */
    private $liMotifPersonneRemplacement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_motif_surcroit", type="string", nullable=true)
     */
    private $liMotifSurcroit;

    /**
     * @var string
     *
     * @ORM\Column(name="li_motif_contrat_saisonnier", type="string", nullable=true)
     */
    private $liMotifContratSaisonnier;

    /**
     * @var string
     *
     * @ORM\Column(name="li_coeffcontrat_apres_pe", type="string", nullable=true)
     */
    private $liCoeffcontratApresPe;

    /**
     * @var string
     *
     * @ORM\Column(name="li_prenom_ancien_titulaire", type="string", nullable=true)
     */
    private $liPrenomAncienTitulaire;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_ancien_titulaire", type="string", nullable=true)
     */
    private $liNomAncienTitulaire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_embauche_remplacant_cdi", type="datetime", nullable=true)
     */
    private $dtEmbaucheRemplacantCdi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_courrier_demande_tp", type="datetime", nullable=true)
     */
    private $dtCourrierDemandeTp;

    /**
     * @var string
     *
     * @ORM\Column(name="li_motif_tempspartiel", type="string", nullable=true)
     */
    private $liMotifTempspartiel;

    /**
     * @var string
     *
     * @ORM\Column(name="li_qualifcontrat", type="string", nullable=true)
     */
    private $liQualifcontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_diplome", type="string", nullable=true)
     */
    private $liTypeDiplome;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nivechpos_ancien_employeur", type="string", nullable=true)
     */
    private $liNivechposAncienEmployeur;

    /**
     * @return int
     */
    public function getIdLigneClausesadditionnelles()
    {
        return $this->idLigneClausesadditionnelles;
    }

    /**
     * @return ViewContratDwh
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * @return string
     */
    public function getLiSocieteAncienEmployeur()
    {
        return $this->liSocieteAncienEmployeur;
    }

    /**
     * @return \DateTime
     */
    public function getDtEmbaucheAncienEmployeur()
    {
        return $this->dtEmbaucheAncienEmployeur;
    }

    /**
     * @return \DateTime
     */
    public function getDtSignatureContratAncienEmployeur()
    {
        return $this->dtSignatureContratAncienEmployeur;
    }

    /**
     * @return string
     */
    public function getLiNomSiteRepris()
    {
        return $this->liNomSiteRepris;
    }

    /**
     * @return string
     */
    public function getLiCategorieemploye()
    {
        return $this->liCategorieemploye;
    }

    /**
     * @return string
     */
    public function getLiCoeffcontratAncienEmployeur()
    {
        return $this->liCoeffcontratAncienEmployeur;
    }

    /**
     * @return string
     */
    public function getLiCycleFormation()
    {
        return $this->liCycleFormation;
    }

    /**
     * @return string
     */
    public function getNbHeures()
    {
        return $this->nbHeures;
    }

    /**
     * @return string
     */
    public function getLiIntituleDiplome()
    {
        return $this->liIntituleDiplome;
    }

    /**
     * @return string
     */
    public function getLiPrenomRemplacement()
    {
        return $this->liPrenomRemplacement;
    }

    /**
     * @return string
     */
    public function getLiNomRemplacement()
    {
        return $this->liNomRemplacement;
    }

    /**
     * @return string
     */
    public function getLiMotifPersonneRemplacement()
    {
        return $this->liMotifPersonneRemplacement;
    }

    /**
     * @return string
     */
    public function getLiMotifSurcroit()
    {
        return $this->liMotifSurcroit;
    }

    /**
     * @return string
     */
    public function getLiMotifContratSaisonnier()
    {
        return $this->liMotifContratSaisonnier;
    }

    /**
     * @return string
     */
    public function getLiCoeffcontratApresPe()
    {
        return $this->liCoeffcontratApresPe;
    }

    /**
     * @return string
     */
    public function getLiPrenomAncienTitulaire()
    {
        return $this->liPrenomAncienTitulaire;
    }

    /**
     * @return string
     */
    public function getLiNomAncienTitulaire()
    {
        return $this->liNomAncienTitulaire;
    }

    /**
     * @return \DateTime
     */
    public function getDtEmbaucheRemplacantCdi()
    {
        return $this->dtEmbaucheRemplacantCdi;
    }

    /**
     * @return \DateTime
     */
    public function getDtCourrierDemandeTp()
    {
        return $this->dtCourrierDemandeTp;
    }

    /**
     * @return string
     */
    public function getLiMotifTempspartiel()
    {
        return $this->liMotifTempspartiel;
    }

    /**
     * @return string
     */
    public function getLiQualifcontrat()
    {
        return $this->liQualifcontrat;
    }

    /**
     * @return string
     */
    public function getLiTypeDiplome()
    {
        return $this->liTypeDiplome;
    }

    /**
     * @return string
     */
    public function getLiNivechposAncienEmployeur()
    {
        return $this->liNivechposAncienEmployeur;
    }

    /**
     * @return bit
     */
    public function getisSalarieProtege()
    {
        return $this->isSalarieProtege;
    }

}
