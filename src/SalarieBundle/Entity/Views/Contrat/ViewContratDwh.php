<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/10/2018
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Contrat;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="v_contrat_dwh")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Contrat\ViewContratDwhRepository")
 */
class ViewContratDwh
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_contrat", type="integer", nullable=false)
     */
    private $idContrat;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_contrat_saphir", type="integer", nullable=false)
     */
    private $idContratSaphir;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     * @Serializer\Exclude()
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom", type="string", nullable=true)
     */
    private $liNom;

    /**
     * @var string
     *
     * @ORM\Column(name="li_prenom", type="string", nullable=true)
     */
    private $liPrenom;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_annexe8", type="bit", nullable=false)
     */
    private $isAnnexe8;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_manager", type="bit", nullable=false)
     */
    private $isManager;

    /**
     * @var string
     *
     * @ORM\Column(name="li_positionnementposte", type="string", nullable=true)
     */
    private $liPositionnementposte;

    /**
     * @var string
     *
     * @ORM\Column(name="li_societe", type="string", nullable=true)
     */
    private $liSociete;

    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", nullable=true)
     */
    private $liAgence;

    /**
     * @var string
     *
     * @ORM\Column(name="li_etablissement", type="string", nullable=true)
     */
    private $liEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_conventioncollective", type="string", nullable=true)
     */
    private $liConventioncollective;

    /**
     * @var string
     *
     * @ORM\Column(name="li_typecontrat", type="string", nullable=true)
     */
    private $liTypecontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_naturecontrat", type="string", nullable=true)
     */
    private $liNaturecontrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debutcontrat", type="datetime", nullable=false)
     */
    private $dtDebutcontrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fincontrat_prevue", type="datetime", nullable=true)
     */
    private $dtFincontratPrevue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fincontrat", type="datetime", nullable=true)
     */
    private $dtFincontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_motiffincontrat", type="string", nullable=true)
     */
    private $liMotiffincontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_qualifcontrat", type="string", nullable=true)
     */
    private $liQualifcontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_categorieemploye", type="string", nullable=true)
     */
    private $liCategorieemploye;

    /**
     * @var string
     *
     * @ORM\Column(name="li_coeffcontrat", type="string", nullable=true)
     */
    private $liCoeffcontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_horaire_mensuel", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $nbHoraireMensuel;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_horaire_annuel", type="decimal", precision=7, scale=2, nullable=true)
     */
    private $nbHoraireAnnuel;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_ticket_resto", type="bit", nullable=false)
     */
    private $isTicketResto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_entree_groupe", type="datetime", nullable=true)
     */
    private $dtEntreeGroupe;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_entree_societe", type="datetime", nullable=true)
     */
    private $dtEntreeSociete;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_anciennete_branche", type="datetime", nullable=true)
     */
    private $dtAncienneteBranche;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_anciennete_csp", type="datetime", nullable=true)
     */
    private $dtAncienneteCsp;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_periodeessai", type="string", nullable=true)
     */
    private $liTypePeriodeessai;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_duree_initiale", type="integer", nullable=true)
     */
    private $nbDureeInitiale;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_mois_renouvellement", type="smallint", nullable=true)
     */
    private $nbMoisRenouvellement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_initiale", type="datetime", nullable=true)
     */
    private $dtFinInitiale;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_renouvellement", type="datetime", nullable=true)
     */
    private $dtFinRenouvellement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_temps_travail", type="string", nullable=true)
     */
    private $liTempsTravail;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_jour_forfait", type="smallint", nullable=true)
     */
    private $nbJourForfait;

    /**
     * @var string
     *
     * @ORM\Column(name="li_salaire_base", type="string", nullable=true)
     */
    private $liSalaireBase;

    /**
     * @var string
     *
     * @ORM\Column(name="li_cycle_temps_partiel", type="string", nullable=true)
     */
    private $liCycleTempsPartiel;

    /**
     * @var string
     *
     * @ORM\Column(name="li_fraisdeplacement", type="string", nullable=true)
     */
    private $liFraisdeplacement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_affectation", type="datetime", nullable=true)
     */
    private $dtAffectation;

    /**
     * @var string
     *
     * @ORM\Column(name="li_bu", type="string", nullable=true)
     */
    private $liBu;

    /**
     * @var string
     *
     * @ORM\Column(name="li_activite", type="string", nullable=true)
     */
    private $liActivite;

    /**
     * @var string
     *
     * @ORM\Column(name="li_chantier", type="string", nullable=true)
     */
    private $liChantier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_signature", type="datetime", nullable=true)
     */
    private $dtSignature;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_valide", type="bit", nullable=false)
     */
    private $isValide;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_salarie_sorti", type="bit", nullable=false)
     */
    private $isSalarieSorti;

    /**
     * @var string
     *
     * @ORM\Column(name="li_etat", type="string", nullable=true)
     */
    private $liEtat;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_salaire_base", type="decimal", precision=9, scale=2, nullable=true)
     */
    private $nbSalaireBase;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_taux_horaire", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $nbTauxHoraire;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_signataire", type="string", nullable=true)
     */
    private $liNomSignataire;

    /**
     * @var string
     *
     * @ORM\Column(name="li_prenom_signataire", type="string", nullable=true)
     */
    private $liPrenomSignataire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_avenant", type="datetime", nullable=true)
     */
    private $dtDebutAvenant;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_avenant", type="datetime", nullable=true)
     */
    private $dtFinAvenant;

    /**
     * @var string
     *
     * @ORM\Column(name="li_typeavenant", type="string", nullable=true)
     */
    private $liTypeavenant;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="SalarieBundle\Entity\Views\Contrat\ViewContratPrimesDwh", mappedBy="idContrat")
     */
    public $primes;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="SalarieBundle\Entity\Views\Contrat\ViewContratClausesadditionnellesDwh", mappedBy="idContrat")
     */
    public $clause_add;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="SalarieBundle\Entity\Views\Contrat\ViewContratClausesoptionnellesDwh", mappedBy="idContrat")
     */
    public $clause_opt;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="SalarieBundle\Entity\Views\Contrat\ViewContratRepartitionhoraireDwh", mappedBy="idContrat")
     * @ORM\OrderBy({"nbSemaine" = "ASC"})
     */
    public $repartition;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->primes = new ArrayCollection();
        $this->clause_add = new ArrayCollection();
        $this->clause_opt = new ArrayCollection();
        $this->repartition = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * @param int $idContrat
     * @return ViewContratDwh
     */
    public function setIdContrat($idContrat)
    {
        $this->idContrat = $idContrat;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdContratSaphir()
    {
        return $this->idContratSaphir;
    }

    /**
     * @param int $idContratSaphir
     * @return ViewContratDwh
     */
    public function setIdContratSaphir($idContratSaphir)
    {
        $this->idContratSaphir = $idContratSaphir;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param \SalarieBundle\Entity\ObjSalarie $idMatricule
     * @return ViewContratDwh
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiNom()
    {
        return $this->liNom;
    }

    /**
     * @param string $liNom
     * @return ViewContratDwh
     */
    public function setLiNom($liNom)
    {
        $this->liNom = $liNom;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiPrenom()
    {
        return $this->liPrenom;
    }

    /**
     * @param string $liPrenom
     * @return ViewContratDwh
     */
    public function setLiPrenom($liPrenom)
    {
        $this->liPrenom = $liPrenom;
        return $this;
    }

    /**
     * @return bit
     */
    public function getisAnnexe8()
    {
        return $this->isAnnexe8;
    }

    /**
     * @param bit $isAnnexe8
     * @return ViewContratDwh
     */
    public function setIsAnnexe8($isAnnexe8)
    {
        $this->isAnnexe8 = $isAnnexe8;
        return $this;
    }

    /**
     * @return bit
     */
    public function getisManager()
    {
        return $this->isManager;
    }

    /**
     * @param bit $isManager
     * @return ViewContratDwh
     */
    public function setIsManager($isManager)
    {
        $this->isManager = $isManager;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiPositionnementposte()
    {
        return $this->liPositionnementposte;
    }

    /**
     * @param string $liPositionnementposte
     * @return ViewContratDwh
     */
    public function setLiPositionnementposte($liPositionnementposte)
    {
        $this->liPositionnementposte = $liPositionnementposte;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiSociete()
    {
        return $this->liSociete;
    }

    /**
     * @param string $liSociete
     * @return ViewContratDwh
     */
    public function setLiSociete($liSociete)
    {
        $this->liSociete = $liSociete;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * @param string $liAgence
     * @return ViewContratDwh
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiEtablissement()
    {
        return $this->liEtablissement;
    }

    /**
     * @param string $liEtablissement
     * @return ViewContratDwh
     */
    public function setLiEtablissement($liEtablissement)
    {
        $this->liEtablissement = $liEtablissement;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiConventioncollective()
    {
        return $this->liConventioncollective;
    }

    /**
     * @param string $liConventioncollective
     * @return ViewContratDwh
     */
    public function setLiConventioncollective($liConventioncollective)
    {
        $this->liConventioncollective = $liConventioncollective;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiNaturecontrat()
    {
        return $this->liNaturecontrat;
    }

    /**
     * @param string $liNaturecontrat
     * @return ViewContratDwh
     */
    public function setLiNaturecontrat($liNaturecontrat)
    {
        $this->liNaturecontrat = $liNaturecontrat;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtDebutcontrat()
    {
        return $this->dtDebutcontrat;
    }

    /**
     * @param \DateTime $dtDebutcontrat
     * @return ViewContratDwh
     */
    public function setDtDebutcontrat($dtDebutcontrat)
    {
        $this->dtDebutcontrat = $dtDebutcontrat;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtFincontratPrevue()
    {
        return $this->dtFincontratPrevue;
    }

    /**
     * @param \DateTime $dtFincontratPrevue
     * @return ViewContratDwh
     */
    public function setDtFincontratPrevue($dtFincontratPrevue)
    {
        $this->dtFincontratPrevue = $dtFincontratPrevue;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtFincontrat()
    {
        return $this->dtFincontrat;
    }

    /**
     * @param \DateTime $dtFincontrat
     * @return ViewContratDwh
     */
    public function setDtFincontrat($dtFincontrat)
    {
        $this->dtFincontrat = $dtFincontrat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiMotiffincontrat()
    {
        return $this->liMotiffincontrat;
    }

    /**
     * @param string $liMotiffincontrat
     * @return ViewContratDwh
     */
    public function setLiMotiffincontrat($liMotiffincontrat)
    {
        $this->liMotiffincontrat = $liMotiffincontrat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiQualifcontrat()
    {
        return $this->liQualifcontrat;
    }

    /**
     * @param string $liQualifcontrat
     * @return ViewContratDwh
     */
    public function setLiQualifcontrat($liQualifcontrat)
    {
        $this->liQualifcontrat = $liQualifcontrat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiCategorieemploye()
    {
        return $this->liCategorieemploye;
    }

    /**
     * @param string $liCategorieemploye
     * @return ViewContratDwh
     */
    public function setLiCategorieemploye($liCategorieemploye)
    {
        $this->liCategorieemploye = $liCategorieemploye;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiCoeffcontrat()
    {
        return $this->liCoeffcontrat;
    }

    /**
     * @param string $liCoeffcontrat
     * @return ViewContratDwh
     */
    public function setLiCoeffcontrat($liCoeffcontrat)
    {
        $this->liCoeffcontrat = $liCoeffcontrat;
        return $this;
    }

    /**
     * @return string
     */
    public function getNbHoraireMensuel()
    {
        return $this->nbHoraireMensuel;
    }

    /**
     * @param string $nbHoraireMensuel
     * @return ViewContratDwh
     */
    public function setNbHoraireMensuel($nbHoraireMensuel)
    {
        $this->nbHoraireMensuel = $nbHoraireMensuel;
        return $this;
    }

    /**
     * @return string
     */
    public function getNbHoraireAnnuel()
    {
        return $this->nbHoraireAnnuel;
    }

    /**
     * @param string $nbHoraireAnnuel
     * @return ViewContratDwh
     */
    public function setNbHoraireAnnuel($nbHoraireAnnuel)
    {
        $this->nbHoraireAnnuel = $nbHoraireAnnuel;
        return $this;
    }

    /**
     * @return bit
     */
    public function getisTicketResto()
    {
        return $this->isTicketResto;
    }

    /**
     * @param bit $isTicketResto
     * @return ViewContratDwh
     */
    public function setIsTicketResto($isTicketResto)
    {
        $this->isTicketResto = $isTicketResto;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtEntreeGroupe()
    {
        return $this->dtEntreeGroupe;
    }

    /**
     * @param \DateTime $dtEntreeGroupe
     * @return ViewContratDwh
     */
    public function setDtEntreeGroupe($dtEntreeGroupe)
    {
        $this->dtEntreeGroupe = $dtEntreeGroupe;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtEntreeSociete()
    {
        return $this->dtEntreeSociete;
    }

    /**
     * @param \DateTime $dtEntreeSociete
     * @return ViewContratDwh
     */
    public function setDtEntreeSociete($dtEntreeSociete)
    {
        $this->dtEntreeSociete = $dtEntreeSociete;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtAncienneteBranche()
    {
        return $this->dtAncienneteBranche;
    }

    /**
     * @param \DateTime $dtAncienneteBranche
     * @return ViewContratDwh
     */
    public function setDtAncienneteBranche($dtAncienneteBranche)
    {
        $this->dtAncienneteBranche = $dtAncienneteBranche;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtAncienneteCsp()
    {
        return $this->dtAncienneteCsp;
    }

    /**
     * @param \DateTime $dtAncienneteCsp
     * @return ViewContratDwh
     */
    public function setDtAncienneteCsp($dtAncienneteCsp)
    {
        $this->dtAncienneteCsp = $dtAncienneteCsp;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiTypePeriodeessai()
    {
        return $this->liTypePeriodeessai;
    }

    /**
     * @param string $liTypePeriodeessai
     * @return ViewContratDwh
     */
    public function setLiTypePeriodeessai($liTypePeriodeessai)
    {
        $this->liTypePeriodeessai = $liTypePeriodeessai;
        return $this;
    }

    /**
     * @return int
     */
    public function getNbDureeInitiale()
    {
        return $this->nbDureeInitiale;
    }

    /**
     * @param int $nbDureeInitiale
     * @return ViewContratDwh
     */
    public function setNbDureeInitiale($nbDureeInitiale)
    {
        $this->nbDureeInitiale = $nbDureeInitiale;
        return $this;
    }

    /**
     * @return int
     */
    public function getNbMoisRenouvellement()
    {
        return $this->nbMoisRenouvellement;
    }

    /**
     * @param int $nbMoisRenouvellement
     * @return ViewContratDwh
     */
    public function setNbMoisRenouvellement($nbMoisRenouvellement)
    {
        $this->nbMoisRenouvellement = $nbMoisRenouvellement;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtFinInitiale()
    {
        return $this->dtFinInitiale;
    }

    /**
     * @param \DateTime $dtFinInitiale
     * @return ViewContratDwh
     */
    public function setDtFinInitiale($dtFinInitiale)
    {
        $this->dtFinInitiale = $dtFinInitiale;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtFinRenouvellement()
    {
        return $this->dtFinRenouvellement;
    }

    /**
     * @param \DateTime $dtFinRenouvellement
     * @return ViewContratDwh
     */
    public function setDtFinRenouvellement($dtFinRenouvellement)
    {
        $this->dtFinRenouvellement = $dtFinRenouvellement;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiTempsTravail()
    {
        return $this->liTempsTravail;
    }

    /**
     * @param string $liTempsTravail
     * @return ViewContratDwh
     */
    public function setLiTempsTravail($liTempsTravail)
    {
        $this->liTempsTravail = $liTempsTravail;
        return $this;
    }

    /**
     * @return int
     */
    public function getNbJourForfait()
    {
        return $this->nbJourForfait;
    }

    /**
     * @param int $nbJourForfait
     * @return ViewContratDwh
     */
    public function setNbJourForfait($nbJourForfait)
    {
        $this->nbJourForfait = $nbJourForfait;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiSalaireBase()
    {
        return $this->liSalaireBase;
    }

    /**
     * @param string $liSalaireBase
     * @return ViewContratDwh
     */
    public function setLiSalaireBase($liSalaireBase)
    {
        $this->liSalaireBase = $liSalaireBase;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiCycleTempsPartiel()
    {
        return $this->liCycleTempsPartiel;
    }

    /**
     * @param string $liCycleTempsPartiel
     * @return ViewContratDwh
     */
    public function setLiCycleTempsPartiel($liCycleTempsPartiel)
    {
        $this->liCycleTempsPartiel = $liCycleTempsPartiel;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiFraisdeplacement()
    {
        return $this->liFraisdeplacement;
    }

    /**
     * @param string $liFraisdeplacement
     * @return ViewContratDwh
     */
    public function setLiFraisdeplacement($liFraisdeplacement)
    {
        $this->liFraisdeplacement = $liFraisdeplacement;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtAffectation()
    {
        return $this->dtAffectation;
    }

    /**
     * @param \DateTime $dtAffectation
     * @return ViewContratDwh
     */
    public function setDtAffectation($dtAffectation)
    {
        $this->dtAffectation = $dtAffectation;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiBu()
    {
        return $this->liBu;
    }

    /**
     * @param string $liBu
     * @return ViewContratDwh
     */
    public function setLiBu($liBu)
    {
        $this->liBu = $liBu;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiActivite()
    {
        return $this->liActivite;
    }

    /**
     * @param string $liActivite
     * @return ViewContratDwh
     */
    public function setLiActivite($liActivite)
    {
        $this->liActivite = $liActivite;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiChantier()
    {
        return $this->liChantier;
    }

    /**
     * @param string $liChantier
     * @return ViewContratDwh
     */
    public function setLiChantier($liChantier)
    {
        $this->liChantier = $liChantier;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtSignature()
    {
        return $this->dtSignature;
    }

    /**
     * @param \DateTime $dtSignature
     * @return ViewContratDwh
     */
    public function setDtSignature($dtSignature)
    {
        $this->dtSignature = $dtSignature;
        return $this;
    }

    /**
     * @return bit
     */
    public function getisValide()
    {
        return $this->isValide;
    }

    /**
     * @param bit $isValide
     * @return ViewContratDwh
     */
    public function setIsValide($isValide)
    {
        $this->isValide = $isValide;
        return $this;
    }

    /**
     * @return bit
     */
    public function getisSalarieSorti()
    {
        return $this->isSalarieSorti;
    }

    /**
     * @param bit $isSalarieSorti
     * @return ViewContratDwh
     */
    public function setIsSalarieSorti($isSalarieSorti)
    {
        $this->isSalarieSorti = $isSalarieSorti;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiEtat()
    {
        return $this->liEtat;
    }

    /**
     * @param string $liEtat
     * @return ViewContratDwh
     */
    public function setLiEtat($liEtat)
    {
        $this->liEtat = $liEtat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiTypecontrat()
    {
        return $this->liTypecontrat;
    }

    /**
     * @param string $liTypecontrat
     * @return ViewContratDwh
     */
    public function setLiTypecontrat($liTypecontrat)
    {
        $this->liTypecontrat = $liTypecontrat;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPrimes()
    {
        return $this->primes;
    }

    /**
     * @return ArrayCollection
     */
    public function getClauseAdd()
    {
        return $this->clause_add;
    }

    /**
     * @return ArrayCollection
     */
    public function getClauseOpt()
    {
        return $this->clause_opt;
    }

    /**
     * @return ArrayCollection
     */
    public function getRepartition()
    {
        return $this->repartition;
    }

    /**
     * @return string
     */
    public function getNbSalaireBase()
    {
        return $this->nbSalaireBase;
    }

    /**
     * @return string
     */
    public function getNbTauxHoraire()
    {
        return $this->nbTauxHoraire;
    }

    /**
     * @return string
     */
    public function getLiNomSignataire()
    {
        return $this->liNomSignataire;
    }

    /**
     * @return string
     */
    public function getLiPrenomSignataire()
    {
        return $this->liPrenomSignataire;
    }

    /**
     * @return \DateTime
     */
    public function getDtDebutAvenant()
    {
        return $this->dtDebutAvenant;
    }

    /**
     * @return \DateTime
     */
    public function getDtFinAvenant()
    {
        return $this->dtFinAvenant;
    }

    /**
     * @return string
     */
    public function getLiTypeavenant()
    {
        return $this->liTypeavenant;
    }

}
