<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/10/2018
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_contrat_cloture")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Contrat\ViewClotureObjContratRepository")
 */
class ViewClotureObjContrat
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_contrat", type="integer", nullable=false)
     */
    private $idContrat;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_complet", type="string", nullable=false)
     */
    private $liNomComplet;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debutcontrat", type="datetime", nullable=false)
     */
    private $dtDebutcontrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fincontrat", type="datetime", nullable=false)
     */
    private $dtFincontrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamAgence
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamAgence")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_agence", referencedColumnName="id_agence")
     * })
     */
    private $idAgence;

    /**
     * @var \SalarieBundle\Entity\Param\ParamMotiffincontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamMotiffincontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_motiffincontrat", referencedColumnName="id_motiffincontrat")
     * })
     */
    private $idMotiffincontrat;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_planning_cloture", type="smallint", nullable=true)
     */
    private $isPlanningCloture;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @return int
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * @param int $idContrat
     * @return ViewClotureObjContrat
     */
    public function setIdContrat($idContrat)
    {
        $this->idContrat = $idContrat;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param int $idMatricule
     * @return ViewClotureObjContrat
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiNomComplet()
    {
        return $this->liNomComplet;
    }

    /**
     * @param string $liNomComplet
     * @return ViewClotureObjContrat
     */
    public function setLiNomComplet($liNomComplet)
    {
        $this->liNomComplet = $liNomComplet;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtDebutcontrat()
    {
        return $this->dtDebutcontrat;
    }

    /**
     * @param \DateTime $dtDebutcontrat
     * @return ViewClotureObjContrat
     */
    public function setDtDebutcontrat($dtDebutcontrat)
    {
        $this->dtDebutcontrat = $dtDebutcontrat;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtFincontrat()
    {
        return $this->dtFincontrat;
    }

    /**
     * @param \DateTime $dtFincontrat
     * @return ViewClotureObjContrat
     */
    public function setDtFincontrat($dtFincontrat)
    {
        $this->dtFincontrat = $dtFincontrat;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamAgence
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamAgence $idAgence
     * @return ViewClotureObjContrat
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamMotiffincontrat
     */
    public function getIdMotiffincontrat()
    {
        return $this->idMotiffincontrat;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamMotiffincontrat $idMotiffincontrat
     * @return ViewClotureObjContrat
     */
    public function setIdMotiffincontrat($idMotiffincontrat)
    {
        $this->idMotiffincontrat = $idMotiffincontrat;
        return $this;
    }

    /**
     * @return int
     */
    public function getisPlanningCloture()
    {
        return $this->isPlanningCloture;
    }

    /**
     * @param int $isPlanningCloture
     * @return ViewClotureObjContrat
     */
    public function setIsPlanningCloture($isPlanningCloture)
    {
        $this->isPlanningCloture = $isPlanningCloture;
        return $this;
    }

    /**
     * @return bit
     */
    public function getisActif()
    {
        return $this->isActif;
    }

    /**
     * @param bit $isActif
     * @return ViewClotureObjContrat
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;
        return $this;
    }
}
