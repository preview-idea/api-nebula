<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/10/2018
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_contrat_clausesoptionnelles_dwh")
 * @ORM\Entity()
 */
class ViewContratClausesoptionnellesDwh
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_ligne_clausesoptionnelles", type="integer", nullable=false)
     */
    private $idLigneClausesoptionnelles;

    /**
     * @var \SalarieBundle\Entity\Views\Contrat\ViewContratDwh
     *
     * @ORM\ManyToOne(targetEntity="ViewContratDwh", inversedBy="clause_opt")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_duree_clause_non_concurrence", type="integer", nullable=true)
     */
    private $nbDureeClauseNonConcurrence;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_pourcent_clause_non_concurrence", type="decimal", precision=4, scale=1, nullable=true)
     */
    private $nbPourcentClauseNonConcurrence;

    /**
     * @var string
     *
     * @ORM\Column(name="li_clausescontrat", type="string", nullable=true)
     */
    private $liClausescontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_qualifcontrat", type="string", nullable=true)
     */
    private $liQualifcontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_lieu_dedit_formation", type="string", nullable=true)
     */
    private $liLieuDeditFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="li_organisme_dedit_formation", type="string", nullable=true)
     */
    private $liOrganismeDeditFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_cout_dedit_formation", type="decimal", precision=9, scale=2, nullable=true)
     */
    private $nbCoutDeditFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="li_cout_dedit_formation", type="string", nullable=true)
     */
    private $liCoutDeditFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="li_programme_dedit_formation", type="string", nullable=true)
     */
    private $liProgrammeDeditFormation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_dedit_formation", type="datetime", nullable=true)
     */
    private $dtDebutDeditFormation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_dedit_formation", type="datetime", nullable=true)
     */
    private $dtFinDeditFormation;


    /**
     * @var string
     *
     * @ORM\Column(name="li_diplome_dedit_formation", type="string", nullable=true)
     */
    private $liDiplomeDeditFormation;

    /**
     * @return int
     */
    public function getIdLigneClausesoptionnelles()
    {
        return $this->idLigneClausesoptionnelles;
    }

    /**
     * @return ViewContratDwh
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * @return int
     */
    public function getNbDureeClauseNonConcurrence()
    {
        return $this->nbDureeClauseNonConcurrence;
    }

    /**
     * @return string
     */
    public function getNbPourcentClauseNonConcurrence()
    {
        return $this->nbPourcentClauseNonConcurrence;
    }

    /**
     * @return string
     */
    public function getLiClausescontrat()
    {
        return $this->liClausescontrat;
    }

    /**
     * @return string
     */
    public function getLiQualifcontrat()
    {
        return $this->liQualifcontrat;
    }

    /**
     * @return string
     */
    public function getLiLieuDeditFormation()
    {
        return $this->liLieuDeditFormation;
    }

    /**
     * @return string
     */
    public function getLiOrganismeDeditFormation()
    {
        return $this->liOrganismeDeditFormation;
    }

    /**
     * @return string
     */
    public function getNbCoutDeditFormation()
    {
        return $this->nbCoutDeditFormation;
    }

    /**
     * @return string
     */
    public function getLiCoutDeditFormation()
    {
        return $this->liCoutDeditFormation;
    }

    /**
     * @return string
     */
    public function getLiProgrammeDeditFormation()
    {
        return $this->liProgrammeDeditFormation;
    }

    /**
     * @return \DateTime
     */
    public function getDtDebutDeditFormation()
    {
        return $this->dtDebutDeditFormation;
    }

    /**
     * @return \DateTime
     */
    public function getDtFinDeditFormation()
    {
        return $this->dtFinDeditFormation;
    }

    /**
     * @return string
     */
    public function getLiDiplomeDeditFormation()
    {
        return $this->liDiplomeDeditFormation;
    }

}
