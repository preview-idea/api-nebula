<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/10/2018
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_contrat_repartitionhoraire_dwh")
 * @ORM\Entity()
 */
class ViewContratRepartitionhoraireDwh
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_ligne_repartitionhoraire", type="integer", nullable=false)
     */
    private $idLigneRepartitionhoraire;

    /**
     * @var \SalarieBundle\Entity\Views\Contrat\ViewContratDwh
     *
     * @ORM\ManyToOne(targetEntity="ViewContratDwh", inversedBy="repartition")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_semaine", type="smallint", nullable=true)
     */
    private $nbSemaine;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_lundi", type="smallint", nullable=true)
     */
    private $nbLundi;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_mardi", type="smallint", nullable=true)
     */
    private $nbMardi;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_mercredi", type="smallint", nullable=true)
     */
    private $nbMercredi;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_jeudi", type="smallint", nullable=true)
     */
    private $nbJeudi;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_vendredi", type="smallint", nullable=true)
     */
    private $nbVendredi;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_samedi", type="smallint", nullable=true)
     */
    private $nbSamedi;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_dimanche", type="smallint", nullable=true)
     */
    private $nbDimanche;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_total_heures", type="decimal", precision=7, scale=2, nullable=true)
     */
    private $nbTotalHeures;

    /**
     * @return int
     */
    public function getIdLigneRepartitionhoraire()
    {
        return $this->idLigneRepartitionhoraire;
    }

    /**
     * @return ViewContratDwh
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * @return int
     */
    public function getNbSemaine()
    {
        return $this->nbSemaine;
    }

    /**
     * @return int
     */
    public function getNbLundi()
    {
        return $this->nbLundi;
    }

    /**
     * @return int
     */
    public function getNbMardi()
    {
        return $this->nbMardi;
    }

    /**
     * @return int
     */
    public function getNbMercredi()
    {
        return $this->nbMercredi;
    }

    /**
     * @return int
     */
    public function getNbJeudi()
    {
        return $this->nbJeudi;
    }

    /**
     * @return int
     */
    public function getNbVendredi()
    {
        return $this->nbVendredi;
    }

    /**
     * @return int
     */
    public function getNbSamedi()
    {
        return $this->nbSamedi;
    }

    /**
     * @return int
     */
    public function getNbDimanche()
    {
        return $this->nbDimanche;
    }

    /**
     * @return string
     */
    public function getNbTotalHeures()
    {
        return $this->nbTotalHeures;
    }

}
