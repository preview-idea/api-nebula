<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/10/2018
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_contrat_non_cloture_mosaic_comete")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Contrat\ViewNonClotureMosaicCometeObjContratRepository")
 */
class ViewNonClotureMosaicCometeObjContrat
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_contrat", type="integer", nullable=false)
     */
    private $idContrat;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var \SalarieBundle\Entity\Param\ParamAgence
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamAgence")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_agence", referencedColumnName="id_agence")
     * })
     */
    private $idAgence;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_complet", type="string", length=80, nullable=true)
     */
    private $liNomComplet;

    /**
     * @var string
     *
     * @ORM\Column(name="li_typecontrat", type="string", length=15, nullable=true)
     */
    private $liTypecontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_qualifcontrat", type="string", length=32, nullable=true)
     */
    private $liQualifcontrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fincontrat", type="datetime", nullable=true)
     */
    private $dtFincontrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debutcontrat", type="datetime", nullable=true)
     */
    private $dtDebutcontrat;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_cloture_comete", type="integer", nullable=true)
     */
    private $isClotureComete;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_cloture_mosaic", type="integer", nullable=false)
     */
    private $isClotureMosaic;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @return int
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * @return \DateTime
     */
    public function getDtDebutcontrat()
    {
        return $this->dtDebutcontrat;
    }

    /**
     * @param int $idContrat
     * @return ViewNonClotureMosaicCometeObjContrat
     */
    public function setIdContrat($idContrat)
    {
        $this->idContrat = $idContrat;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param int $idMatricule
     * @return ViewNonClotureMosaicCometeObjContrat
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamAgence
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamAgence $idAgence
     * @return ViewNonClotureMosaicCometeObjContrat
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiNomComplet()
    {
        return $this->liNomComplet;
    }

    /**
     * @param string $liNomComplet
     * @return ViewNonClotureMosaicCometeObjContrat
     */
    public function setLiNomComplet($liNomComplet)
    {
        $this->liNomComplet = $liNomComplet;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiTypecontrat()
    {
        return $this->liTypecontrat;
    }

    /**
     * @param string $liTypecontrat
     * @return ViewNonClotureMosaicCometeObjContrat
     */
    public function setLiTypecontrat($liTypecontrat)
    {
        $this->liTypecontrat = $liTypecontrat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiQualifcontrat()
    {
        return $this->liQualifcontrat;
    }

    /**
     * @param string $liQualifcontrat
     * @return ViewNonClotureMosaicCometeObjContrat
     */
    public function setLiQualifcontrat($liQualifcontrat)
    {
        $this->liQualifcontrat = $liQualifcontrat;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtFincontrat()
    {
        return $this->dtFincontrat;
    }

    /**
     * @param \DateTime $dtFincontrat
     * @return ViewNonClotureMosaicCometeObjContrat
     */
    public function setDtFincontrat($dtFincontrat)
    {
        $this->dtFincontrat = $dtFincontrat;
        return $this;
    }

    /**
     * @return int
     */
    public function getisClotureComete()
    {
        return $this->isClotureComete;
    }

    /**
     * @param int $isClotureComete
     * @return ViewNonClotureMosaicCometeObjContrat
     */
    public function setIsClotureComete($isClotureComete)
    {
        $this->isClotureComete = $isClotureComete;
        return $this;
    }

    /**
     * @return int
     */
    public function getisClotureMosaic()
    {
        return $this->isClotureMosaic;
    }

    /**
     * @param int $isClotureMosaic
     * @return ViewNonClotureMosaicCometeObjContrat
     */
    public function setIsClotureMosaic($isClotureMosaic)
    {
        $this->isClotureMosaic = $isClotureMosaic;
        return $this;
    }

    /**
     * @return bit
     */
    public function getisActif()
    {
        return $this->isActif;
    }

    /**
     * @param bit $isActif
     * @return ViewNonClotureMosaicCometeObjContrat
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;
        return $this;
    }

}
