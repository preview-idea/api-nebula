<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/10/2018
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_contrat_primes_dwh")
 * @ORM\Entity()
 */
class ViewContratPrimesDwh
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_ligne_primes", type="integer", nullable=false)
     */
    private $idLignePrimes;

    /**
     * @var \SalarieBundle\Entity\Views\Contrat\ViewContratDwh
     *
     * @ORM\ManyToOne(targetEntity="ViewContratDwh", inversedBy="primes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_primecontrat", type="string", nullable=true)
     */
    private $liPrimecontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_montant_prime", type="decimal", precision=8, scale=4, nullable=true)
     */
    private $nbMontantPrime;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_tauxprime", type="string", nullable=true)
     */
    private $liTypeTauxPrime;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_avecprime", type="bit", nullable=false)
     */
    private $isAvecPrime;

    /**
     * @return int
     */
    public function getIdLignePrimes()
    {
        return $this->idLignePrimes;
    }

    /**
     * @return ViewContratDwh
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * @return string
     */
    public function getLiPrimecontrat()
    {
        return $this->liPrimecontrat;
    }

    /**
     * @return string
     */
    public function getNbMontantPrime()
    {
        return $this->nbMontantPrime;
    }

    /**
     * @return string
     */
    public function getLiTypeTauxPrime()
    {
        return $this->liTypeTauxPrime;
    }

    /**
     * @return bit
     */
    public function getisAvecPrime()
    {
        return $this->isAvecPrime;
    }

}
