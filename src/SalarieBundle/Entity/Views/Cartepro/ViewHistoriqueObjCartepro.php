<?php

namespace SalarieBundle\Entity\Views\Cartepro;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_cartepro_historique")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Cartepro\ViewHistoriqueObjCarteproRepository")
 */
class ViewHistoriqueObjCartepro
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_cartepro", type="integer", nullable=false)
     */
    private $idCartepro;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeCartepro
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeCartepro")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_cartepro", referencedColumnName="id_type_cartepro")
     * })
     */
    private $idTypeCartepro;

    /**
     * @var string
     *
     * @ORM\Column(name="li_num_cartepro", type="string", nullable=false)
     */
    private $liNumCartepro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debutvalidite", type="datetime", nullable=false)
     */
    private $dtDebutvalidite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_finvalidite", type="datetime", nullable=false)
     */
    private $dtFinvalidite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_derniercontrole", type="datetime", nullable=true)
     */
    private $dtDerniercontrole;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_cynophile", type="bit", nullable=false)
     */
    private $isCynophile;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_videoprotection", type="bit", nullable=false)
     */
    private $isVideoprotection;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_aeroport", type="bit", nullable=false)
     */
    private $isAeroport;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_surveillance", type="bit", nullable=false)
     */
    private $isSurveillance;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_teleprotection", type="bit", nullable=false)
     */
    private $isTeleprotection;

    /**
     * @var \SalarieBundle\Entity\ObjSalarie
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\ObjSalarie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule", referencedColumnName="id_matricule")
     * })
     */
    private $idMatricule;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_suppression_objet", type="datetime", nullable=false)
     */

    private $dtSuppressionObjet;

    /**
     * @var string
     *
     * @ORM\Column(name="li_etat", type="string", nullable=false)
     */
    private $liEtat;

    /**
     * @return int
     */
    public function getIdCartepro()
    {
        return $this->idCartepro;
    }

    /**
     * @param int $idCartepro
     */
    public function setIdCartepro($idCartepro)
    {
        $this->idCartepro = $idCartepro;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamTypeCartepro
     */
    public function getIdTypeCartepro()
    {
        return $this->idTypeCartepro;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamTypeCartepro $idTypeCartepro
     */
    public function setIdTypeCartepro($idTypeCartepro)
    {
        $this->idTypeCartepro = $idTypeCartepro;
    }

    /**
     * @return string
     */
    public function getLiNumCartepro()
    {
        return $this->liNumCartepro;
    }

    /**
     * @param string $liNumCartepro
     */
    public function setLiNumCartepro($liNumCartepro)
    {
        $this->liNumCartepro = $liNumCartepro;
    }

    /**
     * @return \DateTime
     */
    public function getDtDebutvalidite()
    {
        return $this->dtDebutvalidite;
    }

    /**
     * @param \DateTime $dtDebutvalidite
     */
    public function setDtDebutvalidite($dtDebutvalidite)
    {
        $this->dtDebutvalidite = $dtDebutvalidite;
    }

    /**
     * @return \DateTime
     */
    public function getDtFinvalidite()
    {
        return $this->dtFinvalidite;
    }

    /**
     * @param \DateTime $dtFinvalidite
     */
    public function setDtFinvalidite($dtFinvalidite)
    {
        $this->dtFinvalidite = $dtFinvalidite;
    }

    /**
     * @return \DateTime
     */
    public function getDtDerniercontrole()
    {
        return $this->dtDerniercontrole;
    }

    /**
     * @param \DateTime $dtDerniercontrole
     */
    public function setDtDerniercontrole($dtDerniercontrole)
    {
        $this->dtDerniercontrole = $dtDerniercontrole;
    }

    /**
     * @return bit
     */
    public function getisCynophile()
    {
        return $this->isCynophile;
    }

    /**
     * @param bit $isCynophile
     */
    public function setIsCynophile($isCynophile)
    {
        $this->isCynophile = $isCynophile;
    }

    /**
     * @return bit
     */
    public function getisVideoprotection()
    {
        return $this->isVideoprotection;
    }

    /**
     * @param bit $isVideoprotection
     */
    public function setIsVideoprotection($isVideoprotection)
    {
        $this->isVideoprotection = $isVideoprotection;
    }

    /**
     * @return bit
     */
    public function getisAeroport()
    {
        return $this->isAeroport;
    }

    /**
     * @param bit $isAeroport
     */
    public function setIsAeroport($isAeroport)
    {
        $this->isAeroport = $isAeroport;
    }

    /**
     * @return bit
     */
    public function getisSurveillance()
    {
        return $this->isSurveillance;
    }

    /**
     * @param bit $isSurveillance
     */
    public function setIsSurveillance($isSurveillance)
    {
        $this->isSurveillance = $isSurveillance;
    }

    /**
     * @return \SalarieBundle\Entity\ObjSalarie
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param \SalarieBundle\Entity\ObjSalarie $idMatricule
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
    }

    /**
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     */
    public function setIdMatriculeMaj($idMatriculeMaj)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;
    }

    /**
     * @return \DateTime
     */
    public function getDtSuppressionObjet()
    {
        return $this->dtSuppressionObjet;
    }

    /**
     * @param \DateTime $dtSuppressionObjet
     */
    public function setDtSuppressionObjet($dtSuppressionObjet)
    {
        $this->dtSuppressionObjet = $dtSuppressionObjet;
    }

    /**
     * @return string
     */
    public function getLiEtat()
    {
        return $this->liEtat;
    }

    /**
     * @param string $liEtat
     */
    public function setLiEtat($liEtat)
    {
        $this->liEtat = $liEtat;
    }

    /**
     * @return bit
     */
    public function getisTeleprotection()
    {
        return $this->isTeleprotection;
    }

    /**
     * @param bit $isTeleprotection
     * @return ViewHistoriqueObjCartepro
     */
    public function setIsTeleprotection($isTeleprotection)
    {
        $this->isTeleprotection = $isTeleprotection;
        return $this;
    }
}
