<?php

namespace SalarieBundle\Entity\Views\Cartepro;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_cartepro_salarie_contrat")
 * @ORM\Entity(readOnly=true)
 */
class ViewCarteproSalarieContrat
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_usage", type="string", nullable=false)
     */
    private $liNomUsage;

    /**
     * @var string
     *
     * @ORM\Column(name="li_prenom", type="string", nullable=false)
     */
    private $liPrenom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_naissance", type="datetime", nullable=true)
     */
    private $dtNaissance;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="li_chemin_photo", type="string", nullable=false)
     */
    private $liCheminPhoto;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id_cartepro", type="integer", nullable=false)
     */
    private $idCartepro;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="li_num_cartepro", type="string", nullable=false)
     */
    private $liNumCartepro;


    /**
     * @var bit
     *
     * @ORM\Column(name="is_cynophile", type="bit", nullable=false)
     */
    private $isCynophile;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_videoprotection", type="bit", nullable=false)
     */
    private $isVideoprotection;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_aeroport", type="bit", nullable=false)
     */
    private $isAeroport;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_surveillance", type="bit", nullable=false)
     */
    private $isSurveillance;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_teleprotection", type="bit", nullable=false)
     */
    private $isTeleprotection;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id_contrat", type="integer", nullable=false)
     */
    private $idContrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debutcontrat", type="datetime", nullable=true)
     */
    private $dtDebutcontrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fincontrat_prevue", type="datetime", nullable=true)
     */
    private $dtFincontratPrevue;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="li_agence", type="string", nullable=false)
     */
    private $liAgence;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="li_societe", type="string", nullable=false)
     */
    private $liSociete;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="li_type_cartepro_pref", type="string", nullable=false)
     */
    private $liTypeCarteproPref;


    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="li_etablissement", type="string", nullable=false)
     */
    private $liEtablissement;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="li_nom_voie", type="string", nullable=false)
     */
    private $liNomVoie;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="li_adresse_2", type="string", nullable=false)
     */
    private $liAdresse2;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="li_codepostal", type="string", nullable=false)
     */
    private $liCodepostal;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="li_ville", type="string", nullable=false)
     */
    private $liVille;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="li_telephone", type="string", nullable=false)
     */
    private $liTelephone;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="nb_autorisation", type="string", nullable=false)
     */
    private $nbAutorisation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_delivrance_autorisation", type="datetime", nullable=true)
     */
    private $dtDelivranceAutorisation;

    /**
     * Set idMatricule
     *
     * @param integer $idMatricule
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return integer
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set liNomUsage
     *
     * @param string $liNomUsage
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setLiNomUsage($liNomUsage)
    {
        $this->liNomUsage = $liNomUsage;

        return $this;
    }

    /**
     * Get liNomUsage
     *
     * @return string
     */
    public function getLiNomUsage()
    {
        return $this->liNomUsage;
    }

    /**
     * Set liPrenom
     *
     * @param string $liPrenom
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setLiPrenom($liPrenom)
    {
        $this->liPrenom = $liPrenom;

        return $this;
    }

    /**
     * Get liPrenom
     *
     * @return string
     */
    public function getLiPrenom()
    {
        return $this->liPrenom;
    }

    /**
     * Set dtNaissance
     *
     * @param \DateTime $dtNaissance
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setDtNaissance($dtNaissance)
    {
        $this->dtNaissance = $dtNaissance;

        return $this;
    }

    /**
     * Get dtNaissance
     *
     * @return \DateTime
     */
    public function getDtNaissance()
    {
        return $this->dtNaissance;
    }

    /**
     * Set liCheminPhoto
     *
     * @param string $liCheminPhoto
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setLiCheminPhoto($liCheminPhoto)
    {
        $this->liCheminPhoto = $liCheminPhoto;

        return $this;
    }

    /**
     * Get liCheminPhoto
     *
     * @return string
     */
    public function getLiCheminPhoto()
    {
        return $this->liCheminPhoto;
    }

    /**
     * Set idCartepro
     *
     * @param integer $idCartepro
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setIdCartepro($idCartepro)
    {
        $this->idCartepro = $idCartepro;

        return $this;
    }

    /**
     * Get idCartepro
     *
     * @return integer
     */
    public function getIdCartepro()
    {
        return $this->idCartepro;
    }

    /**
     * Set liNumCartepro
     *
     * @param string $liNumCartepro
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setLiNumCartepro($liNumCartepro)
    {
        $this->liNumCartepro = $liNumCartepro;

        return $this;
    }

    /**
     * Get liNumCartepro
     *
     * @return string
     */
    public function getLiNumCartepro()
    {
        return $this->liNumCartepro;
    }

    /**
     * Set isCynophile
     *
     * @param bit $isCynophile
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setIsCynophile($isCynophile)
    {
        $this->isCynophile = $isCynophile;

        return $this;
    }

    /**
     * Get isCynophile
     *
     * @return bit
     */
    public function getIsCynophile()
    {
        return $this->isCynophile;
    }

    /**
     * Set isVideoprotection
     *
     * @param bit $isVideoprotection
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setIsVideoprotection($isVideoprotection)
    {
        $this->isVideoprotection = $isVideoprotection;

        return $this;
    }

    /**
     * Get isVideoprotection
     *
     * @return bit
     */
    public function getIsVideoprotection()
    {
        return $this->isVideoprotection;
    }

    /**
     * Set isAeroport
     *
     * @param bit $isAeroport
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setIsAeroport($isAeroport)
    {
        $this->isAeroport = $isAeroport;

        return $this;
    }

    /**
     * Get isAeroport
     *
     * @return bit
     */
    public function getIsAeroport()
    {
        return $this->isAeroport;
    }

    /**
     * Set isSurveillance
     *
     * @param bit $isSurveillance
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setIsSurveillance($isSurveillance)
    {
        $this->isSurveillance = $isSurveillance;

        return $this;
    }

    /**
     * Get isSurveillance
     *
     * @return bit
     */
    public function getIsSurveillance()
    {
        return $this->isSurveillance;
    }

    /**
     * Set idContrat
     *
     * @param integer $idContrat
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setIdContrat($idContrat)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return integer
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set dtDebutcontrat
     *
     * @param \DateTime $dtDebutcontrat
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setDtDebutcontrat($dtDebutcontrat)
    {
        $this->dtDebutcontrat = $dtDebutcontrat;

        return $this;
    }

    /**
     * Get dtDebutcontrat
     *
     * @return \DateTime
     */
    public function getDtDebutcontrat()
    {
        return $this->dtDebutcontrat;
    }

    /**
     * Set dtFincontratPrevue
     *
     * @param \DateTime $dtFincontratPrevue
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setDtFincontratPrevue($dtFincontratPrevue)
    {
        $this->dtFincontratPrevue = $dtFincontratPrevue;

        return $this;
    }

    /**
     * Get dtFincontratPrevue
     *
     * @return \DateTime
     */
    public function getDtFincontratPrevue()
    {
        return $this->dtFincontratPrevue;
    }

    /**
     * Set liAgence
     *
     * @param string $liAgence
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;

        return $this;
    }

    /**
     * Get liAgence
     *
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * Set liSociete
     *
     * @param string $liSociete
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setLiSociete($liSociete)
    {
        $this->liSociete = $liSociete;

        return $this;
    }

    /**
     * Get liSociete
     *
     * @return string
     */
    public function getLiSociete()
    {
        return $this->liSociete;
    }

    /**
     * Set liTypeCarteproPref
     *
     * @param string $liTypeCarteproPref
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setLiTypeCarteproPref($liTypeCarteproPref)
    {
        $this->liTypeCarteproPref = $liTypeCarteproPref;

        return $this;
    }

    /**
     * Get liTypeCarteproPref
     *
     * @return string
     */
    public function getLiTypeCarteproPref()
    {
        return $this->liTypeCarteproPref;
    }

    /**
     * Set liEtablissement
     *
     * @param string $liEtablissement
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setLiEtablissement($liEtablissement)
    {
        $this->liEtablissement = $liEtablissement;

        return $this;
    }

    /**
     * Get liEtablissement
     *
     * @return string
     */
    public function getLiEtablissement()
    {
        return $this->liEtablissement;
    }

    /**
     * Set liNomVoie
     *
     * @param string $liNomVoie
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setLiNomVoie($liNomVoie)
    {
        $this->liNomVoie = $liNomVoie;

        return $this;
    }

    /**
     * Get liNomVoie
     *
     * @return string
     */
    public function getLiNomVoie()
    {
        return $this->liNomVoie;
    }

    /**
     * Set liAdresse2
     *
     * @param string $liAdresse2
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setLiAdresse2($liAdresse2)
    {
        $this->liAdresse2 = $liAdresse2;

        return $this;
    }

    /**
     * Get liAdresse2
     *
     * @return string
     */
    public function getLiAdresse2()
    {
        return $this->liAdresse2;
    }

    /**
     * Set liCodepostal
     *
     * @param string $liCodepostal
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setLiCodepostal($liCodepostal)
    {
        $this->liCodepostal = $liCodepostal;

        return $this;
    }

    /**
     * Get liCodepostal
     *
     * @return string
     */
    public function getLiCodepostal()
    {
        return $this->liCodepostal;
    }

    /**
     * Set liVille
     *
     * @param string $liVille
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setLiVille($liVille)
    {
        $this->liVille = $liVille;

        return $this;
    }

    /**
     * Get liVille
     *
     * @return string
     */
    public function getLiVille()
    {
        return $this->liVille;
    }

    /**
     * Set liTelephone
     *
     * @param string $liTelephone
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setLiTelephone($liTelephone)
    {
        $this->liTelephone = $liTelephone;

        return $this;
    }

    /**
     * Get liTelephone
     *
     * @return string
     */
    public function getLiTelephone()
    {
        return $this->liTelephone;
    }

    /**
     * Set nbAutorisation
     *
     * @param string $nbAutorisation
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setNbAutorisation($nbAutorisation)
    {
        $this->nbAutorisation = $nbAutorisation;

        return $this;
    }

    /**
     * Get nbAutorisation
     *
     * @return string
     */
    public function getNbAutorisation()
    {
        return $this->nbAutorisation;
    }

    /**
     * Set dtDelivranceAutorisation
     *
     * @param \DateTime $dtDelivranceAutorisation
     *
     * @return ViewCarteproSalarieContrat
     */
    public function setDtDelivranceAutorisation($dtDelivranceAutorisation)
    {
        $this->dtDelivranceAutorisation = $dtDelivranceAutorisation;

        return $this;
    }

    /**
     * Get dtDelivranceAutorisation
     *
     * @return \DateTime
     */
    public function getDtDelivranceAutorisation()
    {
        return $this->dtDelivranceAutorisation;
    }

    /**
     * @return bit
     */
    public function getisTeleprotection()
    {
        return $this->isTeleprotection;
    }

    /**
     * @param bit $isTeleprotection
     * @return ViewCarteproSalarieContrat
     */
    public function setIsTeleprotection($isTeleprotection)
    {
        $this->isTeleprotection = $isTeleprotection;
        return $this;
    }
}
