<?php

/**
 * @author      Kevin Gicquiaud
 * @Date:       26/12/2018
 */

namespace SalarieBundle\Entity\Views;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_salarie_acompte_historique")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\ViewHistoriqueObjSalarieAcompteRepository")
 */
class ViewHistoriqueObjSalarieAcompte
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_acompte", type="integer", nullable=false)
     */
    private $idAcompte;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeAcompte
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeAcompte")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_acompte", referencedColumnName="id_type_acompte")
     * })
     */
    private $idTypeAcompte;

    /**
     * @var \SalarieBundle\Entity\ObjSalarie
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\ObjSalarie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule", referencedColumnName="id_matricule")
     * })
     */
    private $idMatricule;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat_initial", referencedColumnName="id_contrat")
     * })
     */
    private $idContratInitial;

    /**
     * @var \SalarieBundle\Entity\Param\ParamEtablissement
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamEtablissement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_etablissement_initial", referencedColumnName="id_etablissement")
     * })
     */
    private $idEtablissementInitial;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_montant_demande", type="decimal", precision=6, scale=2, nullable=false)
     */
    private $nbMontantDemande;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_montant_accorde", type="decimal", precision=6, scale=2, nullable=true)
     */
    private $nbMontantAccorde;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCalendrierPaie
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCalendrierPaie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_calendrier_paie_debut", referencedColumnName="id_calendrier_paie")
     * })
     */
    private $idCalendrierPaieDebut;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCalendrierPaie
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCalendrierPaie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_calendrier_paie_fin", referencedColumnName="id_calendrier_paie")
     * })
     */
    private $idCalendrierPaieFin;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_valideur", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeValideur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_suppression_objet", type="datetime", nullable=false)
     */
    private $dtSuppressionObjet;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fincontrat", type="datetime", nullable=false)
     */
    private $dtFincontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_etat", type="string", nullable=false)
     */
    private $liEtat;

    /**
     * @return int
     */
    public function getIdAcompte()
    {
        return $this->idAcompte;
    }

    /**
     * @param int $idAcompte
     */
    public function setIdAcompte($idAcompte)
    {
        $this->idAcompte = $idAcompte;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamTypeAcompte
     */
    public function getIdTypeAcompte()
    {
        return $this->idTypeAcompte;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamTypeAcompte $idTypeAcompte
     */
    public function setIdTypeAcompte($idTypeAcompte)
    {
        $this->idTypeAcompte = $idTypeAcompte;
    }

    /**
     * @return \SalarieBundle\Entity\ObjSalarie
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param \SalarieBundle\Entity\ObjSalarie $idMatricule
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
    }

    /**
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContratInitial()
    {
        return $this->idContratInitial;
    }

    /**
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContratInitial
     */
    public function setIdContratInitial($idContratInitial)
    {
        $this->idContratInitial = $idContratInitial;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamEtablissement
     */
    public function getIdEtablissementInitial()
    {
        return $this->idEtablissementInitial;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamEtablissement $idEtablissementInitial
     */
    public function setIdEtablissementInitial($idEtablissementInitial)
    {
        $this->idEtablissementInitial = $idEtablissementInitial;
    }

    /**
     * @return string
     */
    public function getNbMontantDemande()
    {
        return $this->nbMontantDemande;
    }

    /**
     * @param string $nbMontantDemande
     */
    public function setNbMontantDemande($nbMontantDemande)
    {
        $this->nbMontantDemande = $nbMontantDemande;
    }

    /**
     * @return string
     */
    public function getNbMontantAccorde()
    {
        return $this->nbMontantAccorde;
    }

    /**
     * @param string $nbMontantAccorde
     */
    public function setNbMontantAccorde($nbMontantAccorde)
    {
        $this->nbMontantAccorde = $nbMontantAccorde;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamCalendrierPaie
     */
    public function getIdCalendrierPaieDebut()
    {
        return $this->idCalendrierPaieDebut;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamCalendrierPaie $idCalendrierPaieDebut
     */
    public function setIdCalendrierPaieDebut($idCalendrierPaieDebut)
    {
        $this->idCalendrierPaieDebut = $idCalendrierPaieDebut;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamCalendrierPaie
     */
    public function getIdCalendrierPaieFin()
    {
        return $this->idCalendrierPaieFin;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamCalendrierPaie $idCalendrierPaieFin
     */
    public function setIdCalendrierPaieFin($idCalendrierPaieFin)
    {
        $this->idCalendrierPaieFin = $idCalendrierPaieFin;
    }

    /**
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     */
    public function setIdMatriculeMaj($idMatriculeMaj)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;
    }

    /**
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeValideur()
    {
        return $this->idMatriculeValideur;
    }

    /**
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeValideur
     */
    public function setIdMatriculeValideur($idMatriculeValideur)
    {
        $this->idMatriculeValideur = $idMatriculeValideur;
    }

    /**
     * @return \DateTime
     */
    public function getDtSuppressionObjet()
    {
        return $this->dtSuppressionObjet;
    }

    /**
     * @param \DateTime $dtSuppressionObjet
     */
    public function setDtSuppressionObjet($dtSuppressionObjet)
    {
        $this->dtSuppressionObjet = $dtSuppressionObjet;
    }

    /**
     * @return \DateTime
     */
    public function getDtFincontrat()
    {
        return $this->dtFincontrat;
    }

    /**
     * @param \DateTime $dtFincontrat
     */
    public function setDtFincontrat($dtFincontrat)
    {
        $this->dtSuppressionObjet = $dtFincontrat;
    }

    /**
     * @return string
     */
    public function getLiEtat()
    {
        return $this->liEtat;
    }

    /**
     * @param string $liEtat
     */
    public function setLiEtat($liEtat)
    {
        $this->liEtat = $liEtat;
    }
}
