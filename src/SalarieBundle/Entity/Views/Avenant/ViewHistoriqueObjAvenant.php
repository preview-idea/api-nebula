<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/10/2018
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Avenant;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_avenant_historique")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Avenant\ViewHistoriqueObjAvenantRepository")
 */
class ViewHistoriqueObjAvenant
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_ligne_avenant", type="integer", nullable=false)
     */
    private $idLigneAvenant;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_contrat", type="integer", nullable=true)
     */
    private $idContrat;

    /**
     * @var \SalarieBundle\Entity\ObjSalarie
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\ObjSalarie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule", referencedColumnName="id_matricule")
     * })
     */
    private $idMatricule;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_annexe8", type="bit", nullable=false)
     */
    private $isAnnexe8;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_manager", type="bit", nullable=false)
     */
    private $isManager;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debutcontrat", type="datetime", nullable=false)
     */
    private $dtDebutcontrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fincontrat_prevue", type="datetime", nullable=true)
     */
    private $dtFincontratPrevue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fincontrat", type="datetime", nullable=true)
     */
    private $dtFincontrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCategorieemploye
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCategorieemploye")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_categorieemploye", referencedColumnName="id_categorieemploye")
     * })
     */
    private $idCategorieemploye;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_horaire_mensuel", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $nbHoraireMensuel;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_horaire_annuel", type="decimal", precision=6, scale=2, nullable=false)
     */
    private $nbHoraireAnnuel;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_salaire_base", type="decimal", precision=9, scale=2, nullable=false)
     */
    private $nbSalaireBase;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_taux_horaire", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $nbTauxHoraire;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_ticket_resto", type="bit", nullable=false)
     */
    private $isTicketResto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_entree_groupe", type="datetime", nullable=false)
     */
    private $dtEntreeGroupe;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_entree_societe", type="datetime", nullable=false)
     */
    private $dtEntreeSociete;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_anciennete_branche", type="datetime", nullable=false)
     */
    private $dtAncienneteBranche;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_anciennete_csp", type="datetime", nullable=false)
     */
    private $dtAncienneteCsp;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_duree_initiale", type="integer", nullable=true)
     */
    private $nbDureeInitiale;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_mois_renouvellement", type="smallint", nullable=true)
     */
    private $nbMoisRenouvellement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_initiale", type="datetime", nullable=true)
     */
    private $dtFinInitiale;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_renouvellement", type="datetime", nullable=true)
     */
    private $dtFinRenouvellement;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_jour_forfait", type="smallint", nullable=true)
     */
    private $nbJourForfait;

    /**
     * @var string
     *
     * @ORM\Column(name="li_salaire_base", type="string", length=80, nullable=true)
     */
    private $liSalaireBase;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_affectation", type="datetime", nullable=true)
     */
    private $dtDebutAffectation;

    /**
     * @var string
     *
     * @ORM\Column(name="li_bu", type="string", length=3, nullable=true)
     */
    private $liBu;

    /**
     * @var string
     *
     * @ORM\Column(name="li_activite", type="string", length=3, nullable=true)
     */
    private $liActivite;

    /**
     * @var string
     *
     * @ORM\Column(name="li_chantier", type="string", length=9, nullable=true)
     */
    private $liChantier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_signature", type="datetime", nullable=false)
     */
    private $dtSignature;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_duree_periode_probatoire", type="integer", nullable=true)
     */
    private $nbDureePeriodeProbatoire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_periode_probatoire", type="date", nullable=true)
     */
    private $dtFinPeriodeProbatoire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_avenant", type="date", nullable=true)
     */
    private $dtDebutAvenant;

    /**
     * @var string
     *
     * @ORM\Column(name="li_etat", type="string", nullable=true)
     */
    private $liEtat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_avenant", type="date", nullable=true)
     */
    private $dtFinAvenant;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_valid", type="bit", nullable=false)
     */
    private $isValid = '1';

    /**
     * @var \SalarieBundle\Entity\Param\ParamAgence
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamAgence")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_agence", referencedColumnName="id_agence")
     * })
     */
    private $idAgence;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCoeffcontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCoeffcontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_coeffcontrat", referencedColumnName="id_coeffcontrat")
     * })
     */
    private $idCoeffcontrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamConventioncollective
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamConventioncollective")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_conventioncollective", referencedColumnName="id_conventioncollective")
     * })
     */
    private $idConventioncollective;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCycleTempsPartiel
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCycleTempsPartiel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cycle_temps_partiel", referencedColumnName="id_cycle_temps_partiel")
     * })
     */
    private $idCycleTempsPartiel;

    /**
     * @var \SalarieBundle\Entity\Param\ParamDecompteTempsTravail
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamDecompteTempsTravail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_decompte_temps_travail", referencedColumnName="id_decompte_temps_travail")
     * })
     */
    private $idDecompteTempsTravail;

    /**
     * @var \SalarieBundle\Entity\Param\ParamEtablissement
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamEtablissement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_etablissement", referencedColumnName="id_etablissement")
     * })
     */
    private $idEtablissement;

    /**
     * @var \SalarieBundle\Entity\Param\ParamFraisdeplacement
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamFraisdeplacement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_fraisdeplacement", referencedColumnName="id_fraisdeplacement")
     * })
     */
    private $idFraisdeplacement;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\Param\ParamMotiffincontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamMotiffincontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_motiffincontrat", referencedColumnName="id_motiffincontrat")
     * })
     */
    private $idMotiffincontrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamNaturecontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamNaturecontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_naturecontrat", referencedColumnName="id_naturecontrat")
     * })
     */
    private $idNaturecontrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamPositionnementposte
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamPositionnementposte")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_positionnementposte", referencedColumnName="id_positionnementposte")
     * })
     */
    private $idPositionnementposte;

    /**
     * @var \SalarieBundle\Entity\Param\ParamQualifcontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamQualifcontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_qualifcontrat", referencedColumnName="id_qualifcontrat")
     * })
     */
    private $idQualifcontrat;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_signataire", referencedColumnName="id_matricule")
     * })
     */
    private $idSignataire;

    /**
     * @var \SalarieBundle\Entity\Param\ParamSociete
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamSociete")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_societe", referencedColumnName="id_societe")
     * })
     */
    private $idSociete;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTempsTravail
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTempsTravail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_temps_travail", referencedColumnName="id_temps_travail")
     * })
     */
    private $idTempsTravail;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypePeriodeessai
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypePeriodeessai")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_periodeessai", referencedColumnName="id_type_periodeessai")
     * })
     */
    private $idTypePeriodeessai;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypecontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypecontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_typecontrat", referencedColumnName="id_typecontrat")
     * })
     */
    private $idTypecontrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeavenant
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeavenant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_typeavenant", referencedColumnName="id_typeavenant")
     * })
     */
    private $idTypeavenant;

    /**
     * @var \SalarieBundle\Entity\ObjContrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\ObjContrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat_origine", referencedColumnName="id_contrat")
     * })
     */
    private $idContratOrigine;

    /**
     * Set idLigneAvenant
     *
     * @param integer $idLigneAvenant
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdLigneAvenant($idLigneAvenant)
    {
        $this->idLigneAvenant = $idLigneAvenant;

        return $this;
    }

    /**
     * Get idLigneAvenant
     *
     * @return integer
     */
    public function getIdLigneAvenant()
    {
        return $this->idLigneAvenant;
    }

    /**
     * Set idContrat
     *
     * @param integer $idContrat
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdContrat($idContrat)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return integer
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set isAnnexe8
     *
     * @param bit $isAnnexe8
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIsAnnexe8($isAnnexe8)
    {
        $this->isAnnexe8 = $isAnnexe8;

        return $this;
    }

    /**
     * Get isAnnexe8
     *
     * @return bit
     */
    public function getIsAnnexe8()
    {
        return $this->isAnnexe8;
    }

    /**
     * Set isManager
     *
     * @param bit $isManager
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIsManager($isManager)
    {
        $this->isManager = $isManager;

        return $this;
    }

    /**
     * Get isManager
     *
     * @return bit
     */
    public function getIsManager()
    {
        return $this->isManager;
    }

    /**
     * Set dtDebutcontrat
     *
     * @param \DateTime $dtDebutcontrat
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setDtDebutcontrat($dtDebutcontrat)
    {
        $this->dtDebutcontrat = $dtDebutcontrat;

        return $this;
    }

    /**
     * Get dtDebutcontrat
     *
     * @return \DateTime
     */
    public function getDtDebutcontrat()
    {
        return $this->dtDebutcontrat;
    }

    /**
     * Set dtFincontratPrevue
     *
     * @param \DateTime $dtFincontratPrevue
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setDtFincontratPrevue($dtFincontratPrevue)
    {
        $this->dtFincontratPrevue = $dtFincontratPrevue;

        return $this;
    }

    /**
     * Get dtFincontratPrevue
     *
     * @return \DateTime
     */
    public function getDtFincontratPrevue()
    {
        return $this->dtFincontratPrevue;
    }

    /**
     * Set dtFincontrat
     *
     * @param \DateTime $dtFincontrat
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setDtFincontrat($dtFincontrat)
    {
        $this->dtFincontrat = $dtFincontrat;

        return $this;
    }

    /**
     * Get dtFincontrat
     *
     * @return \DateTime
     */
    public function getDtFincontrat()
    {
        return $this->dtFincontrat;
    }

    /**
     * Set nbHoraireMensuel
     *
     * @param string $nbHoraireMensuel
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setNbHoraireMensuel($nbHoraireMensuel)
    {
        $this->nbHoraireMensuel = $nbHoraireMensuel;

        return $this;
    }

    /**
     * Get nbHoraireMensuel
     *
     * @return string
     */
    public function getNbHoraireMensuel()
    {
        return $this->nbHoraireMensuel;
    }

    /**
     * Set nbHoraireAnnuel
     *
     * @param string $nbHoraireAnnuel
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setNbHoraireAnnuel($nbHoraireAnnuel)
    {
        $this->nbHoraireAnnuel = $nbHoraireAnnuel;

        return $this;
    }

    /**
     * Get nbHoraireAnnuel
     *
     * @return string
     */
    public function getNbHoraireAnnuel()
    {
        return $this->nbHoraireAnnuel;
    }

    /**
     * Set nbSalaireBase
     *
     * @param string $nbSalaireBase
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setNbSalaireBase($nbSalaireBase)
    {
        $this->nbSalaireBase = $nbSalaireBase;

        return $this;
    }

    /**
     * Get nbSalaireBase
     *
     * @return string
     */
    public function getNbSalaireBase()
    {
        return $this->nbSalaireBase;
    }

    /**
     * Set nbTauxHoraire
     *
     * @param string $nbTauxHoraire
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setNbTauxHoraire($nbTauxHoraire)
    {
        $this->nbTauxHoraire = $nbTauxHoraire;

        return $this;
    }

    /**
     * Get nbTauxHoraire
     *
     * @return string
     */
    public function getNbTauxHoraire()
    {
        return $this->nbTauxHoraire;
    }

    /**
     * Set isTicketResto
     *
     * @param bit $isTicketResto
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIsTicketResto($isTicketResto)
    {
        $this->isTicketResto = $isTicketResto;

        return $this;
    }

    /**
     * Get isTicketResto
     *
     * @return bit
     */
    public function getIsTicketResto()
    {
        return $this->isTicketResto;
    }

    /**
     * Set dtEntreeGroupe
     *
     * @param \DateTime $dtEntreeGroupe
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setDtEntreeGroupe($dtEntreeGroupe)
    {
        $this->dtEntreeGroupe = $dtEntreeGroupe;

        return $this;
    }

    /**
     * Get dtEntreeGroupe
     *
     * @return \DateTime
     */
    public function getDtEntreeGroupe()
    {
        return $this->dtEntreeGroupe;
    }

    /**
     * Set dtEntreeSociete
     *
     * @param \DateTime $dtEntreeSociete
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setDtEntreeSociete($dtEntreeSociete)
    {
        $this->dtEntreeSociete = $dtEntreeSociete;

        return $this;
    }

    /**
     * Get dtEntreeSociete
     *
     * @return \DateTime
     */
    public function getDtEntreeSociete()
    {
        return $this->dtEntreeSociete;
    }

    /**
     * Set dtAncienneteBranche
     *
     * @param \DateTime $dtAncienneteBranche
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setDtAncienneteBranche($dtAncienneteBranche)
    {
        $this->dtAncienneteBranche = $dtAncienneteBranche;

        return $this;
    }

    /**
     * Get dtAncienneteBranche
     *
     * @return \DateTime
     */
    public function getDtAncienneteBranche()
    {
        return $this->dtAncienneteBranche;
    }

    /**
     * Set dtAncienneteCsp
     *
     * @param \DateTime $dtAncienneteCsp
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setDtAncienneteCsp($dtAncienneteCsp)
    {
        $this->dtAncienneteCsp = $dtAncienneteCsp;

        return $this;
    }

    /**
     * Get dtAncienneteCsp
     *
     * @return \DateTime
     */
    public function getDtAncienneteCsp()
    {
        return $this->dtAncienneteCsp;
    }

    /**
     * Set nbDureeInitiale
     *
     * @param integer $nbDureeInitiale
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setNbDureeInitiale($nbDureeInitiale)
    {
        $this->nbDureeInitiale = $nbDureeInitiale;

        return $this;
    }

    /**
     * Get nbDureeInitiale
     *
     * @return integer
     */
    public function getNbDureeInitiale()
    {
        return $this->nbDureeInitiale;
    }

    /**
     * Set nbMoisRenouvellement
     *
     * @param integer $nbMoisRenouvellement
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setNbMoisRenouvellement($nbMoisRenouvellement)
    {
        $this->nbMoisRenouvellement = $nbMoisRenouvellement;

        return $this;
    }

    /**
     * Get nbMoisRenouvellement
     *
     * @return integer
     */
    public function getNbMoisRenouvellement()
    {
        return $this->nbMoisRenouvellement;
    }

    /**
     * Set dtFinInitiale
     *
     * @param \DateTime $dtFinInitiale
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setDtFinInitiale($dtFinInitiale)
    {
        $this->dtFinInitiale = $dtFinInitiale;

        return $this;
    }

    /**
     * Get dtFinInitiale
     *
     * @return \DateTime
     */
    public function getDtFinInitiale()
    {
        return $this->dtFinInitiale;
    }

    /**
     * Set dtFinRenouvellement
     *
     * @param \DateTime $dtFinRenouvellement
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setDtFinRenouvellement($dtFinRenouvellement)
    {
        $this->dtFinRenouvellement = $dtFinRenouvellement;

        return $this;
    }

    /**
     * Get dtFinRenouvellement
     *
     * @return \DateTime
     */
    public function getDtFinRenouvellement()
    {
        return $this->dtFinRenouvellement;
    }

    /**
     * Set nbJourForfait
     *
     * @param integer $nbJourForfait
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setNbJourForfait($nbJourForfait)
    {
        $this->nbJourForfait = $nbJourForfait;

        return $this;
    }

    /**
     * Get nbJourForfait
     *
     * @return integer
     */
    public function getNbJourForfait()
    {
        return $this->nbJourForfait;
    }

    /**
     * Set liSalaireBase
     *
     * @param string $liSalaireBase
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setLiSalaireBase($liSalaireBase)
    {
        $this->liSalaireBase = $liSalaireBase;

        return $this;
    }

    /**
     * Get liSalaireBase
     *
     * @return string
     */
    public function getLiSalaireBase()
    {
        return $this->liSalaireBase;
    }

    /**
     * Set dtDebutAffectation
     *
     * @param \DateTime $dtDebutAffectation
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setDtDebutAffectation($dtDebutAffectation)
    {
        $this->dtDebutAffectation = $dtDebutAffectation;

        return $this;
    }

    /**
     * Get dtDebutAffectation
     *
     * @return \DateTime
     */
    public function getDtDebutAffectation()
    {
        return $this->dtDebutAffectation;
    }

    /**
     * Set liBu
     *
     * @param string $liBu
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setLiBu($liBu)
    {
        $this->liBu = $liBu;

        return $this;
    }

    /**
     * Get liBu
     *
     * @return string
     */
    public function getLiBu()
    {
        return $this->liBu;
    }

    /**
     * Set liActivite
     *
     * @param string $liActivite
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setLiActivite($liActivite)
    {
        $this->liActivite = $liActivite;

        return $this;
    }

    /**
     * Get liActivite
     *
     * @return string
     */
    public function getLiActivite()
    {
        return $this->liActivite;
    }

    /**
     * Set liChantier
     *
     * @param string $liChantier
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setLiChantier($liChantier)
    {
        $this->liChantier = $liChantier;

        return $this;
    }

    /**
     * Get liChantier
     *
     * @return string
     */
    public function getLiChantier()
    {
        return $this->liChantier;
    }

    /**
     * Set dtSignature
     *
     * @param \DateTime $dtSignature
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setDtSignature($dtSignature)
    {
        $this->dtSignature = $dtSignature;

        return $this;
    }

    /**
     * Get dtSignature
     *
     * @return \DateTime
     */
    public function getDtSignature()
    {
        return $this->dtSignature;
    }

    /**
     * Set nbDureePeriodeProbatoire
     *
     * @param integer $nbDureePeriodeProbatoire
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setNbDureePeriodeProbatoire($nbDureePeriodeProbatoire)
    {
        $this->nbDureePeriodeProbatoire = $nbDureePeriodeProbatoire;

        return $this;
    }

    /**
     * Get nbDureePeriodeProbatoire
     *
     * @return integer
     */
    public function getNbDureePeriodeProbatoire()
    {
        return $this->nbDureePeriodeProbatoire;
    }

    /**
     * Set dtFinPeriodeProbatoire
     *
     * @param \DateTime $dtFinPeriodeProbatoire
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setDtFinPeriodeProbatoire($dtFinPeriodeProbatoire)
    {
        $this->dtFinPeriodeProbatoire = $dtFinPeriodeProbatoire;

        return $this;
    }

    /**
     * Get dtFinPeriodeProbatoire
     *
     * @return \DateTime
     */
    public function getDtFinPeriodeProbatoire()
    {
        return $this->dtFinPeriodeProbatoire;
    }

    /**
     * Set dtDebutAvenant
     *
     * @param \DateTime $dtDebutAvenant
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setDtDebutAvenant($dtDebutAvenant)
    {
        $this->dtDebutAvenant = $dtDebutAvenant;

        return $this;
    }

    /**
     * Get dtDebutAvenant
     *
     * @return \DateTime
     */
    public function getDtDebutAvenant()
    {
        return $this->dtDebutAvenant;
    }

    /**
     * Set liEtat
     *
     * @param string $liEtat
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setLiEtat($liEtat)
    {
        $this->liEtat = $liEtat;

        return $this;
    }

    /**
     * Get liEtat
     *
     * @return string
     */
    public function getLiEtat()
    {
        return $this->liEtat;
    }

    /**
     * Set dtFinAvenant
     *
     * @param \DateTime $dtFinAvenant
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setDtFinAvenant($dtFinAvenant)
    {
        $this->dtFinAvenant = $dtFinAvenant;

        return $this;
    }

    /**
     * Get dtFinAvenant
     *
     * @return \DateTime
     */
    public function getDtFinAvenant()
    {
        return $this->dtFinAvenant;
    }

    /**
     * Set isValid
     *
     * @param bit $isValid
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;

        return $this;
    }

    /**
     * Get isValid
     *
     * @return bit
     */
    public function getIsValid()
    {
        return $this->isValid;
    }

    /**
     * Set idMatricule
     *
     * @param \SalarieBundle\Entity\ObjSalarie $idMatricule
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdMatricule(\SalarieBundle\Entity\ObjSalarie $idMatricule = null)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return \SalarieBundle\Entity\ObjSalarie
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set idCategorieemploye
     *
     * @param \SalarieBundle\Entity\Param\ParamCategorieemploye $idCategorieemploye
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdCategorieemploye(\SalarieBundle\Entity\Param\ParamCategorieemploye $idCategorieemploye = null)
    {
        $this->idCategorieemploye = $idCategorieemploye;

        return $this;
    }

    /**
     * Get idCategorieemploye
     *
     * @return \SalarieBundle\Entity\Param\ParamCategorieemploye
     */
    public function getIdCategorieemploye()
    {
        return $this->idCategorieemploye;
    }

    /**
     * Set idAgence
     *
     * @param \SalarieBundle\Entity\Param\ParamAgence $idAgence
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdAgence(\SalarieBundle\Entity\Param\ParamAgence $idAgence = null)
    {
        $this->idAgence = $idAgence;

        return $this;
    }

    /**
     * Get idAgence
     *
     * @return \SalarieBundle\Entity\Param\ParamAgence
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * Set idCoeffcontrat
     *
     * @param \SalarieBundle\Entity\Param\ParamCoeffcontrat $idCoeffcontrat
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdCoeffcontrat(\SalarieBundle\Entity\Param\ParamCoeffcontrat $idCoeffcontrat = null)
    {
        $this->idCoeffcontrat = $idCoeffcontrat;

        return $this;
    }

    /**
     * Get idCoeffcontrat
     *
     * @return \SalarieBundle\Entity\Param\ParamCoeffcontrat
     */
    public function getIdCoeffcontrat()
    {
        return $this->idCoeffcontrat;
    }

    /**
     * Set idConventioncollective
     *
     * @param \SalarieBundle\Entity\Param\ParamConventioncollective $idConventioncollective
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdConventioncollective(\SalarieBundle\Entity\Param\ParamConventioncollective $idConventioncollective = null)
    {
        $this->idConventioncollective = $idConventioncollective;

        return $this;
    }

    /**
     * Get idConventioncollective
     *
     * @return \SalarieBundle\Entity\Param\ParamConventioncollective
     */
    public function getIdConventioncollective()
    {
        return $this->idConventioncollective;
    }

    /**
     * Set idCycleTempsPartiel
     *
     * @param \SalarieBundle\Entity\Param\ParamCycleTempsPartiel $idCycleTempsPartiel
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdCycleTempsPartiel(\SalarieBundle\Entity\Param\ParamCycleTempsPartiel $idCycleTempsPartiel = null)
    {
        $this->idCycleTempsPartiel = $idCycleTempsPartiel;

        return $this;
    }

    /**
     * Get idCycleTempsPartiel
     *
     * @return \SalarieBundle\Entity\Param\ParamCycleTempsPartiel
     */
    public function getIdCycleTempsPartiel()
    {
        return $this->idCycleTempsPartiel;
    }

    /**
     * Set idDecompteTempsTravail
     *
     * @param \SalarieBundle\Entity\Param\ParamDecompteTempsTravail $idDecompteTempsTravail
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdDecompteTempsTravail(\SalarieBundle\Entity\Param\ParamDecompteTempsTravail $idDecompteTempsTravail = null)
    {
        $this->idDecompteTempsTravail = $idDecompteTempsTravail;

        return $this;
    }

    /**
     * Get idDecompteTempsTravail
     *
     * @return \SalarieBundle\Entity\Param\ParamDecompteTempsTravail
     */
    public function getIdDecompteTempsTravail()
    {
        return $this->idDecompteTempsTravail;
    }

    /**
     * Set idEtablissement
     *
     * @param \SalarieBundle\Entity\Param\ParamEtablissement $idEtablissement
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdEtablissement(\SalarieBundle\Entity\Param\ParamEtablissement $idEtablissement = null)
    {
        $this->idEtablissement = $idEtablissement;

        return $this;
    }

    /**
     * Get idEtablissement
     *
     * @return \SalarieBundle\Entity\Param\ParamEtablissement
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }

    /**
     * Set idFraisdeplacement
     *
     * @param \SalarieBundle\Entity\Param\ParamFraisdeplacement $idFraisdeplacement
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdFraisdeplacement(\SalarieBundle\Entity\Param\ParamFraisdeplacement $idFraisdeplacement = null)
    {
        $this->idFraisdeplacement = $idFraisdeplacement;

        return $this;
    }

    /**
     * Get idFraisdeplacement
     *
     * @return \SalarieBundle\Entity\Param\ParamFraisdeplacement
     */
    public function getIdFraisdeplacement()
    {
        return $this->idFraisdeplacement;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idMotiffincontrat
     *
     * @param \SalarieBundle\Entity\Param\ParamMotiffincontrat $idMotiffincontrat
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdMotiffincontrat(\SalarieBundle\Entity\Param\ParamMotiffincontrat $idMotiffincontrat = null)
    {
        $this->idMotiffincontrat = $idMotiffincontrat;

        return $this;
    }

    /**
     * Get idMotiffincontrat
     *
     * @return \SalarieBundle\Entity\Param\ParamMotiffincontrat
     */
    public function getIdMotiffincontrat()
    {
        return $this->idMotiffincontrat;
    }

    /**
     * Set idNaturecontrat
     *
     * @param \SalarieBundle\Entity\Param\ParamNaturecontrat $idNaturecontrat
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdNaturecontrat(\SalarieBundle\Entity\Param\ParamNaturecontrat $idNaturecontrat = null)
    {
        $this->idNaturecontrat = $idNaturecontrat;

        return $this;
    }

    /**
     * Get idNaturecontrat
     *
     * @return \SalarieBundle\Entity\Param\ParamNaturecontrat
     */
    public function getIdNaturecontrat()
    {
        return $this->idNaturecontrat;
    }

    /**
     * Set idPositionnementposte
     *
     * @param \SalarieBundle\Entity\Param\ParamPositionnementposte $idPositionnementposte
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdPositionnementposte(\SalarieBundle\Entity\Param\ParamPositionnementposte $idPositionnementposte = null)
    {
        $this->idPositionnementposte = $idPositionnementposte;

        return $this;
    }

    /**
     * Get idPositionnementposte
     *
     * @return \SalarieBundle\Entity\Param\ParamPositionnementposte
     */
    public function getIdPositionnementposte()
    {
        return $this->idPositionnementposte;
    }

    /**
     * Set idQualifcontrat
     *
     * @param \SalarieBundle\Entity\Param\ParamQualifcontrat $idQualifcontrat
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdQualifcontrat(\SalarieBundle\Entity\Param\ParamQualifcontrat $idQualifcontrat = null)
    {
        $this->idQualifcontrat = $idQualifcontrat;

        return $this;
    }

    /**
     * Get idQualifcontrat
     *
     * @return \SalarieBundle\Entity\Param\ParamQualifcontrat
     */
    public function getIdQualifcontrat()
    {
        return $this->idQualifcontrat;
    }

    /**
     * Set idSignataire
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idSignataire
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdSignataire(\SalarieBundle\Entity\Salarie\SalarieInfobase $idSignataire = null)
    {
        $this->idSignataire = $idSignataire;

        return $this;
    }

    /**
     * Get idSignataire
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdSignataire()
    {
        return $this->idSignataire;
    }

    /**
     * Set idSociete
     *
     * @param \SalarieBundle\Entity\Param\ParamSociete $idSociete
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdSociete(\SalarieBundle\Entity\Param\ParamSociete $idSociete = null)
    {
        $this->idSociete = $idSociete;

        return $this;
    }

    /**
     * Get idSociete
     *
     * @return \SalarieBundle\Entity\Param\ParamSociete
     */
    public function getIdSociete()
    {
        return $this->idSociete;
    }

    /**
     * Set idTempsTravail
     *
     * @param \SalarieBundle\Entity\Param\ParamTempsTravail $idTempsTravail
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdTempsTravail(\SalarieBundle\Entity\Param\ParamTempsTravail $idTempsTravail = null)
    {
        $this->idTempsTravail = $idTempsTravail;

        return $this;
    }

    /**
     * Get idTempsTravail
     *
     * @return \SalarieBundle\Entity\Param\ParamTempsTravail
     */
    public function getIdTempsTravail()
    {
        return $this->idTempsTravail;
    }

    /**
     * Set idTypePeriodeessai
     *
     * @param \SalarieBundle\Entity\Param\ParamTypePeriodeessai $idTypePeriodeessai
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdTypePeriodeessai(\SalarieBundle\Entity\Param\ParamTypePeriodeessai $idTypePeriodeessai = null)
    {
        $this->idTypePeriodeessai = $idTypePeriodeessai;

        return $this;
    }

    /**
     * Get idTypePeriodeessai
     *
     * @return \SalarieBundle\Entity\Param\ParamTypePeriodeessai
     */
    public function getIdTypePeriodeessai()
    {
        return $this->idTypePeriodeessai;
    }

    /**
     * Set idTypecontrat
     *
     * @param \SalarieBundle\Entity\Param\ParamTypecontrat $idTypecontrat
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdTypecontrat(\SalarieBundle\Entity\Param\ParamTypecontrat $idTypecontrat = null)
    {
        $this->idTypecontrat = $idTypecontrat;

        return $this;
    }

    /**
     * Get idTypecontrat
     *
     * @return \SalarieBundle\Entity\Param\ParamTypecontrat
     */
    public function getIdTypecontrat()
    {
        return $this->idTypecontrat;
    }

    /**
     * Set idTypeavenant
     *
     * @param \SalarieBundle\Entity\Param\ParamTypeavenant $idTypeavenant
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdTypeavenant(\SalarieBundle\Entity\Param\ParamTypeavenant $idTypeavenant = null)
    {
        $this->idTypeavenant = $idTypeavenant;

        return $this;
    }

    /**
     * Get idTypeavenant
     *
     * @return \SalarieBundle\Entity\Param\ParamTypeavenant
     */
    public function getIdTypeavenant()
    {
        return $this->idTypeavenant;
    }

    /**
     * Set idContratOrigine
     *
     * @param \SalarieBundle\Entity\ObjContrat $idContratOrigine
     *
     * @return ViewHistoriqueObjAvenant
     */
    public function setIdContratOrigine(\SalarieBundle\Entity\ObjContrat $idContratOrigine = null)
    {
        $this->idContratOrigine = $idContratOrigine;

        return $this;
    }

    /**
     * Get idContratOrigine
     *
     * @return \SalarieBundle\Entity\ObjContrat
     */
    public function getIdContratOrigine()
    {
        return $this->idContratOrigine;
    }
}
