<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/10/2018
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Entretien;

use Core\Repository\Views\DWH\ViewMvEntretienObjSalarieRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_entretien_pro")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Entretien\ViewEntretienProRepository")
 */
class ViewEntretienProObjSalarie
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_societe", type="string", nullable=true)
     */
    private $liSociete;

    /**
     * @var string
     *
     * @ORM\Column(name="li_region", type="string", nullable=true)
     */
    private $liRegion;

    /**
     * @var string
     *
     * @ORM\Column(name="id_agence", type="integer", nullable=true)
     */
    private $idAgence;

    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", nullable=true)
     */
    private $liAgence;

    /**
     * @var string
     *
     * @ORM\Column(name="li_prenom", type="string", nullable=true)
     */
    private $liPrenom;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_usage", type="string", nullable=true)
     */
    private $liNomUsage;

    /**
     * @var string
     *
     * @ORM\Column(name="li_qualifcontrat", type="string", nullable=true)
     */
    private $liQualifcontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_typecontrat", type="string", nullable=true)
     */
    private $liTypecontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_positionnementposte", type="string", nullable=true)
     */
    private $lPpositionnementposte;

    /**
     * @var string
     *
     * @ORM\Column(name="li_categorieemploye", type="string", nullable=true)
     */
    private $liCategorieemploye;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debutcontrat", type="datetime", nullable=true)
     */
    private $dtDebutcontrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_anciennete_branche", type="datetime", nullable=true)
     */

    private $dtAncienneteBranche;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_dernier_entretien", type="datetime", nullable=true)
     */
    private $dtDernierEntretien;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_echeance_entretien", type="datetime", nullable=true)
     */
    private $dtEcheanceEntretien;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_dernier_recap", type="datetime", nullable=true)
     */
    private $dtDernierRecap;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_echeance_recap", type="datetime", nullable=true)
     */
    private $dtEcheanceRecap;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_planifie", type="datetime", nullable=true)
     */
    private $dtPlanifie;

    /**
     * @var string
     *
     * @ORM\Column(name="statut_entretien", type="string", nullable=true)
     */
    private $statutEntretien;

    /**
     * @var string
     *
     * @ORM\Column(name="statut_details", type="string", nullable=true)
     */
    private $statutDetails;

    /**
     * @var string
     *
     * @ORM\Column(name="dt_absence", type="datetime", nullable=true)
     */
    private $dtAbsence;

    /**
     * @var string
     *
     * @ORM\Column(name="entretien_en_defaut", type="string", nullable=true)
     */
    private $entretienEnDefaut;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_type_absence", type="integer", nullable=true)
     */
    private $idTypeAbsence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_absence", type="integer", nullable=true)
     */
    private $idAbsence;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_retour_absence", type="string", nullable=true)
     */
    private $isRetourAbsence;

    /**
     * Set idMatricule
     *
     * @param integer $idMatricule
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return integer
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set liSociete
     *
     * @param string liSociete
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setLiSociete($liSociete)
    {
        $this->liSociete = $liSociete;

        return $this;
    }

    /**
     * Get liSociete
     *
     * @return string
     */
    public function getLiSociete()
    {
        return $this->liSociete;
    }

    /**
     * Set liRegion
     *
     * @param string $liRegion
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setLiRegion($liRegion)
    {
        $this->liRegion = $liRegion;

        return $this;
    }

    /**
     * Get liRegion
     *
     * @return string
     */
    public function getLiRegion()
    {
        return $this->liRegion;
    }

    /**
     * Set idAgence
     *
     * @param integer $idAgence
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;

        return $this;
    }

    /**
     * Get idAgence
     *
     * @return integer
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * Set liAgence
     *
     * @param string $liAgence
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;

        return $this;
    }

    /**
     * Get liAgence
     *
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * Set liPrenom
     *
     * @param string $liPrenom
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setLiPrenom($liPrenom)
    {
        $this->liPrenom = $liPrenom;

        return $this;
    }

    /**
     * Get liPrenom
     *
     * @return string
     */
    public function getLiPrenom()
    {
        return $this->liPrenom;
    }

    /**
     * Set liNomUsage
     *
     * @param string $liNomUsage
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setLiNomUsage($liNomUsage)
    {
        $this->liNomUsage = $liNomUsage;

        return $this;
    }

    /**
     * Get liNomUsage
     *
     * @return string
     */
    public function getLiNomUsage()
    {
        return $this->liNomUsage;
    }

    /**
     * Set liQualifcontrat
     *
     * @param string $liQualifcontrat
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setLiQualifcontrat($liQualifcontrat)
    {
        $this->liQualifcontrat = $liQualifcontrat;

        return $this;
    }

    /**
     * Get liQualifcontrat
     *
     * @return string
     */
    public function getLiQualifcontrat()
    {
        return $this->liQualifcontrat;
    }

    /**
     * Set liTypecontrat
     *
     * @param string $liTypecontrat
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setLiTypecontrat($liTypecontrat)
    {
        $this->liTypecontrat = $liTypecontrat;

        return $this;
    }

    /**
     * Get liTypecontrat
     *
     * @return string
     */
    public function getLiTypecontrat()
    {
        return $this->liTypecontrat;
    }

    /**
     * Set lPpositionnementposte
     *
     * @param string $lPpositionnementposte
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setLPpositionnementposte($lPpositionnementposte)
    {
        $this->lPpositionnementposte = $lPpositionnementposte;

        return $this;
    }

    /**
     * Get lPpositionnementposte
     *
     * @return string
     */
    public function getLPpositionnementposte()
    {
        return $this->lPpositionnementposte;
    }

    /**
     * Set liCategorieemploye
     *
     * @param string $liCategorieemploye
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setLiCategorieemploye($liCategorieemploye)
    {
        $this->liCategorieemploye = $liCategorieemploye;

        return $this;
    }

    /**
     * Get liCategorieemploye
     *
     * @return string
     */
    public function getLiCategorieemploye()
    {
        return $this->liCategorieemploye;
    }

    /**
     * Set dtDebutcontrat
     *
     * @param \DateTime $dtDebutcontrat
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setDtDebutcontrat($dtDebutcontrat)
    {
        $this->dtDebutcontrat = $dtDebutcontrat;

        return $this;
    }

    /**
     * Get dtDebutcontrat
     *
     * @return \DateTime
     */
    public function getDtDebutcontrat()
    {
        return $this->dtDebutcontrat;
    }

    /**
     * Set dtAncienneteBranche
     *
     * @param \DateTime $dtAncienneteBranche
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setDtAncienneteBranche($dtAncienneteBranche)
    {
        $this->dtAncienneteBranche = $dtAncienneteBranche;

        return $this;
    }

    /**
     * Get dtAncienneteBranche
     *
     * @return \DateTime
     */
    public function getDtAncienneteBranche()
    {
        return $this->dtAncienneteBranche;
    }

    /**
     * Set dtDernierEntretien
     *
     * @param \DateTime $dtDernierEntretien
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setDtDernierEntretien($dtDernierEntretien)
    {
        $this->dtDernierEntretien = $dtDernierEntretien;

        return $this;
    }

    /**
     * Get dtDernierEntretien
     *
     * @return \DateTime
     */
    public function getDtDernierEntretien()
    {
        return $this->dtDernierEntretien;
    }

    /**
     * Set dtEcheanceEntretien
     *
     * @param \DateTime $dtEcheanceEntretien
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setDtEcheanceEntretien($dtEcheanceEntretien)
    {
        $this->dtEcheanceEntretien = $dtEcheanceEntretien;

        return $this;
    }

    /**
     * Get dtEcheanceEntretien
     *
     * @return \DateTime
     */
    public function getDtEcheanceEntretien()
    {
        return $this->dtEcheanceEntretien;
    }

    /**
     * Set dtDernierRecap
     *
     * @param \DateTime $dtDernierRecap
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setDtDernierRecap($dtDernierRecap)
    {
        $this->dtDernierRecap = $dtDernierRecap;

        return $this;
    }

    /**
     * Get dtDernierRecap
     *
     * @return \DateTime
     */
    public function getDtDernierRecap()
    {
        return $this->dtDernierRecap;
    }

    /**
     * Set dtEcheanceRecap
     *
     * @param \DateTime $dtEcheanceRecap
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setDtEcheanceRecap($dtEcheanceRecap)
    {
        $this->dtEcheanceRecap = $dtEcheanceRecap;

        return $this;
    }

    /**
     * Get dtEcheanceRecap
     *
     * @return \DateTime
     */
    public function getDtEcheanceRecap()
    {
        return $this->dtEcheanceRecap;
    }

    /**
     * Set dtPlanifie
     *
     * @param \DateTime $dtPlanifie
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setDtPlanifie($dtPlanifie)
    {
        $this->dtPlanifie = $dtPlanifie;

        return $this;
    }

    /**
     * Get dtPlanifie
     *
     * @return \DateTime
     */
    public function getDtPlanifie()
    {
        return $this->dtPlanifie;
    }

    /**
     * Set statutEntretien
     *
     * @param string $statutEntretien
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setStatutEntretien($statutEntretien)
    {
        $this->statutEntretien = $statutEntretien;

        return $this;
    }

    /**
     * Get statutEntretien
     *
     * @return string
     */
    public function getStatutEntretien()
    {
        return $this->statutEntretien;
    }

    /**
     * Set statutPlanification
     *
     * @param string $statutDetails
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setStatutDetails($statutDetails)
    {
        $this->statutDetails = $statutDetails;

        return $this;
    }

    /**
     * Get statutDetails
     *
     * @return string
     */
    public function getStatutDetails()
    {
        return $this->statutDetails;
    }

    /**
     * Set dtAbsence
     *
     * @param \DateTime $dtAbsence
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setDtAbsence($dtAbsence)
    {
        $this->dtAbsence = $dtAbsence;

        return $this;
    }

    /**
     * Get dtAbsence
     *
     * @return \DateTime
     */
    public function getDtAbsence()
    {
        return $this->dtAbsence;
    }

    /**
     * Set entretienEnDefaut
     *
     * @param string $entretienEnDefaut
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setEntretienEnDefaut($entretienEnDefaut)
    {
        $this->entretienEnDefaut = $entretienEnDefaut;

        return $this;
    }

    /**
     * Get entretienEnDefaut
     *
     * @return string
     */
    public function getEntretienEnDefaut()
    {
        return $this->entretienEnDefaut;
    }

    /**
     * Set idTypeAbsence
     *
     * @param integer $idTypeAbsence
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setIdTypeAbsence($idTypeAbsence)
    {
        $this->idTypeAbsence = $idTypeAbsence;

        return $this;
    }

    /**
     * Get idTypeAbsence
     *
     * @return integer
     */
    public function getIdTypeAbsence()
    {
        return $this->idTypeAbsence;
    }

    /**
     * Set idAbsence
     *
     * @param integer $idAbsence
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setIdAbsence($idAbsence)
    {
        $this->idAbsence = $idAbsence;

        return $this;
    }

    /**
     * Get idAbsence
     *
     * @return integer
     */
    public function getIdAbsence()
    {
        return $this->idAbsence;
    }

    /**
     * Set isRetourAbsence
     *
     * @param string isRetourAbsence
     *
     * @return ViewEntretienProObjSalarie
     */
    public function setIsRetourAbsence($isRetourAbsence)
    {
        $this->isRetourAbsence = $isRetourAbsence;

        return $this;
    }

    /**
     * Get isRetourAbsence
     *
     * @return string
     */
    public function getIsRetourAbsence()
    {
        return $this->isRetourAbsence;
    }
}
