<?php
namespace SalarieBundle\Entity\Views\Entretien;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_planification_ep")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Entretien\ViewPlanificationEpRepository")
 */
class ViewPlanificationEp
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_planification_ep", type="integer", nullable=false)
     */
    private $idPlanificationEp;

    /**
     * @var string
     *
     * @ORM\Column(name="li_civilite", type="string", nullable=true)
     */
    private $liCivilite;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_usage", type="string", nullable=true)
     */
    private $liNomUsage;

    /**
     * @var string
     *
     * @ORM\Column(name="li_prenom", type="string", nullable=true)
     */
    private $liPrenom;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_voie", type="string", nullable=true)
     */
    private $liNomVoie;

    /**
     * @var string
     *
     * @ORM\Column(name="li_adresse_2", type="string", nullable=true)
     */
    private $liAdressse2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_codepostal", type="integer", nullable=true)
     */
    private $idCodePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="li_ville", type="string", nullable=true)
     */
    private $liVille;

    /**
     * @var string
     *
     * @ORM\Column(name="li_pays", type="string", nullable=true)
     */
    private $liPays;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_planification", type="datetime", nullable=true)
     */
    private $dtPlanification;

    /**
     * @var string
     *
     * @ORM\Column(name="li_lieu", type="string", nullable=true)
     */
    private $liLieu;

    /**
     * @var string
     *
     * @ORM\Column(name="li_responsable", type="string", nullable=true)
     */
    private $liResponsable;

    /**
     * @var string
     *
     * @ORM\Column(name="id_agence", type="integer", nullable=true)
     */
    private $idAgence;


    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", nullable=true)
     */
    private $liAgence;
}