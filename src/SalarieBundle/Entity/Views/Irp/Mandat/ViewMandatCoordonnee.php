<?php
/**
 * Created by PhpStorm.
 * User: tgautreau
 * Date: 31/07/2019
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Irp\Mandat;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_mandat_coordonnee")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Irp\Mandat\ViewMandatCoordonneeRepository")
 */
class ViewMandatCoordonnee
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_telephone", type="string", length=10, nullable=true)
     */
    private $liTelephone;

    /**
     * @var string
     *
     * @ORM\Column(name="li_mail", type="string", length=40, nullable=true)
     */
    private $liMail;

    /**
     * @return int
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param int $idMatricule
     * @return ViewMandatCoordonnee
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiTelephone()
    {
        return $this->liTelephone;
    }

    /**
     * @param string $liTelephone
     * @return ViewMandatCoordonnee
     */
    public function setLiTelephone($liTelephone)
    {
        $this->liTelephone = $liTelephone;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiMail()
    {
        return $this->liMail;
    }

    /**
     * @param string $liMail
     * @return ViewMandatCoordonnee
     */
    public function setLiMail($liMail)
    {
        $this->liMail = $liMail;
        return $this;
    }

}
