<?php
/**
 * Created by PhpStorm.
 * User: tgautreau
 * Date: 31/07/2019
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Irp\Mandat;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_mandat_classeur_social")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Irp\Mandat\ViewMandatClasseurSocialRepository")
 */
class ViewMandatClasseurSocial
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_mandat", type="integer", nullable=false)
     */
    private $idMandat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_usage", type="string", length=100, nullable=true)
     */
    private $liNomUsage;

    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", length=60, nullable=true)
     */
    private $liAgence;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_mandat", type="string", length=30, nullable=true)
     */
    private $liTypeMandat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_org_syndicale", type="string", length=30, nullable=true)
     */
    private $liOrgSyndicale;

    /**
     * @var string
     *
     * @ORM\Column(name="li_telephone", type="string", length=10, nullable=true)
     */
    private $liTelephone;

    /**
     * @var string
     *
     * @ORM\Column(name="li_mail", type="string", length=40, nullable=true)
     */
    private $liMail;

    /**
     * @var string
     *
     * @ORM\Column(name="li_commission", type="string", length=200, nullable=true)
     */
    private $liCommission;

    /**
     * @var string
     *
     * @ORM\Column(name="li_cse", type="string", length=30, nullable=true)
     */
    private $liCse;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cse", type="integer", nullable=true)
     */
    private $idCse;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_debut", type="datetime", nullable=false)
     */
    private $dtDateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_fin_theorique", type="datetime", nullable=false)
     */
    private $dtDateFinTheorique;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_fin_reelle", type="datetime", nullable=false)
     */
    private $dtDateFinReelle;



    /**
     * @return int
     */
    public function getIdMandat()
    {
        return $this->idMandat;
    }

    /**
     * @param int $idMandat
     * @return ViewMandatClasseurSocial
     */
    public function setIdMandat($idMandat)
    {
        $this->idMandat = $idMandat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiPrenom()
    {
        return $this->liPrenom;
    }

    /**
     * @return string
     */
    public function getLiNomUsage()
    {
        return $this->liNomUsage;
    }

    /**
     * @param string $liNomUsage
     * @return ViewMandatClasseurSocial
     */
    public function setLiNomUsage($liNomUsage)
    {
        $this->liNomUsage = $liNomUsage;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * @param string $liAgence
     * @return ViewMandatClasseurSocial
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiTypeMandat()
    {
        return $this->liTypeMandat;
    }

    /**
     * @param string $liTypeMandat
     * @return ViewMandatClasseurSocial
     */
    public function setLiTypeMandat($liTypeMandat)
    {
        $this->liTypeMandat = $liTypeMandat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiOrgSyndicale()
    {
        return $this->liOrgSyndicale;
    }

    /**
     * @param string $liOrgSyndicale
     * @return ViewMandatClasseurSocial
     */
    public function setLiOrgSyndicale($liOrgSyndicale)
    {
        $this->liOrgSyndicale = $liOrgSyndicale;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiTelephone()
    {
        return $this->liTelephone;
    }

    /**
     * @param string $liTelephone
     * @return ViewMandatClasseurSocial
     */
    public function setLiTelephone($liTelephone)
    {
        $this->liTelephone = $liTelephone;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiMail()
    {
        return $this->liMail;
    }

    /**
     * @param string $liMail
     * @return ViewMandatClasseurSocial
     */
    public function setLiMail($liMail)
    {
        $this->liMail = $liMail;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiCommission()
    {
        return $this->liCommission;
    }

    /**
     * @param string $liCommission
     * @return ViewMandatClasseurSocial
     */
    public function setLiCommission($liCommission)
    {
        $this->liCommission = $liCommission;
        return $this;
    }


    /**
     * @return string
     */
    public function getLiCse()
    {
        return $this->liCse;
    }

    /**
     * @param string $liCse
     * @return ViewMandatClasseurSocial
     */
    public function setLiCse($liCse)
    {
        $this->liCse = $liCse;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdCse()
    {
        return $this->idCse;
    }

    /**
     * @param int $idCse
     * @return ViewMandatClasseurSocial
     */
    public function setIdCse($idCse)
    {
        $this->idCse = $idCse;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtDateDebut()
    {
        return $this->dtDateDebut;
    }

    /**
     * @param \DateTime $dtDateDebut
     * @return ViewMandatClasseurSocial
     */
    public function setDtDateDebut($dtDateDebut)
    {
        $this->dtDateDebut = $dtDateDebut;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtDateFinTheorique()
    {
        return $this->dtDateFinTheorique;
    }

    /**
     * @param \DateTime $dtDateFinTheorique
     * @return ViewMandatClasseurSocial
     */
    public function setDtDateFinTheorique($dtDateFinTheorique)
    {
        $this->dtDateFinTheorique = $dtDateFinTheorique;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtDateFinReelle()
    {
        return $this->dtDateFinReelle;
    }

    /**
     * @param \DateTime $dtDateFinReelle
     * @return ViewMandatClasseurSocial
     */
    public function setDtDateFinReelle($dtDateFinReelle)
    {
        $this->dtDateFinReelle = $dtDateFinReelle;
        return $this;
    }

}
