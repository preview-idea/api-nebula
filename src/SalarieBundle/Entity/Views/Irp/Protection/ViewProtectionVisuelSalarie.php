<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/10/2018
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Irp\Protection;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_protection_visuel_salarie")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Irp\Protection\ViewProtectionVisuelSalarieRepository")
 */
class ViewProtectionVisuelSalarie
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_salarie_protection", type="string")
     */
    private $liSalarieProtection;

    /**
     * @return int
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param int $idMatricule
     * @return ViewProtectionVisuelSalarie
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiSalarieProtection()
    {
        return $this->liSalarieProtection;
    }

    /**
     * @param string $liSalarieProtection
     * @return ViewProtectionVisuelSalarie
     */
    public function setLiSalarieProtection($liSalarieProtection)
    {
        $this->liSalarieProtection = $liSalarieProtection;
        return $this;
    }

}
