<?php

/**
 * @author      Théo Gautreau
 * @Date:       05/07/2019
 */

namespace SalarieBundle\Entity\Views;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_protection_historique")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\ViewHistoriqueObjProtectionRepository")
 */
class ViewHistoriqueObjProtection
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_protection", type="integer", nullable=false)
     */
    private $idProtection;

    /**
     * @var \SalarieBundle\Entity\ObjSalarie
     *
     * @ORM\OneToOne(targetEntity="SalarieBundle\Entity\ObjSalarie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule", referencedColumnName="id_matricule")
     * })
     */
    private $idMatricule;

    /**
     * @var \SalarieBundle\Entity\Views\ViewHistoriqueObjMandat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Views\ViewHistoriqueObjMandat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_mandat", referencedColumnName="id_mandat")
     * })
     */
    private $idMandat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamOrgSyndicale
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamOrgSyndicale")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_org_syndicale", referencedColumnName="id_org_syndicale")
     * })
     */
    private $idOrgSyndicale;

    /**
     * @var \SalarieBundle\Entity\Param\ParamPerimetreElectoral
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamPerimetreElectoral")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_perimetre_electoral", referencedColumnName="id_perimetre_electoral")
     * })
     */
    private $idPerimetreElectoral;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeProtection
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeProtection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_protection", referencedColumnName="id_type_protection")
     * })
     */
    private $idTypeProtection;

    /**
     * @var string
     *
     * @ORM\Column(name="li_commentaire", type="string", nullable=true)
     */
    private $liCommentaire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_debut", type="datetime", nullable=false)
     */
    private $dtDateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_fin_theorique", type="datetime", nullable=false)
     */
    private $dtDateFinTheorique;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_fin_reelle", type="datetime", nullable=true)
     */
    private $dtDateFinReelle;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_suppression_objet", type="datetime", nullable=false)
     */
    private $dtSuppressionObjet;

    /**
     * @var string
     *
     * @ORM\Column(name="li_etat", type="string", nullable=false)
     */
    private $liEtat;

    /**
     * @return int
     */
    public function getIdProtection()
    {
        return $this->idProtection;
    }

    /**
     * @param int $idProtection
     * @return ViewHistoriqueObjProtection
     */
    public function setIdProtection($idProtection)
    {
        $this->idProtection = $idProtection;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\ObjSalarie
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param \SalarieBundle\Entity\ObjSalarie $idMatricule
     * @return ViewHistoriqueObjProtection
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\ObjSalarieMandat
     */
    public function getIdMandat()
    {
        return $this->idMandat;
    }

    /**
     * @param \SalarieBundle\Entity\ObjSalarieMandat $idMandat
     * @return ViewHistoriqueObjProtection
     */
    public function setIdMandat($idMandat)
    {
        $this->idMandat = $idMandat;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamOrgSyndicale
     */
    public function getIdOrgSyndicale()
    {
        return $this->idOrgSyndicale;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamOrgSyndicale $idOrgSyndicale
     * @return ViewHistoriqueObjProtection
     */
    public function setIdOrgSyndicale($idOrgSyndicale)
    {
        $this->idOrgSyndicale = $idOrgSyndicale;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamPerimetreElectoral
     */
    public function getIdPerimetreElectoral()
    {
        return $this->idPerimetreElectoral;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamPerimetreElectoral $idPerimetreElectoral
     * @return ViewHistoriqueObjProtection
     */
    public function setIdPerimetreElectoral($idPerimetreElectoral)
    {
        $this->idPerimetreElectoral = $idPerimetreElectoral;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamTypeProtection
     */
    public function getIdTypeProtection()
    {
        return $this->idTypeProtection;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamTypeProtection $idTypeProtection
     * @return ViewHistoriqueObjProtection
     */
    public function setIdTypeProtection($idTypeProtection)
    {
        $this->idTypeProtection = $idTypeProtection;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiCommentaire()
    {
        return $this->liCommentaire;
    }

    /**
     * @param string $liCommentaire
     * @return ViewHistoriqueObjProtection
     */
    public function setLiCommentaire($liCommentaire)
    {
        $this->liCommentaire = $liCommentaire;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtDateDebut()
    {
        return $this->dtDateDebut;
    }

    /**
     * @param \DateTime $dtDateDebut
     * @return ViewHistoriqueObjProtection
     */
    public function setDtDateDebut($dtDateDebut)
    {
        $this->dtDateDebut = $dtDateDebut;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtDateFinTheorique()
    {
        return $this->dtDateFinTheorique;
    }

    /**
     * @param \DateTime $dtDateFinTheorique
     * @return ViewHistoriqueObjProtection
     */
    public function setDtDateFinTheorique($dtDateFinTheorique)
    {
        $this->dtDateFinTheorique = $dtDateFinTheorique;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtDateFinReelle()
    {
        return $this->dtDateFinReelle;
    }

    /**
     * @param \DateTime $dtDateFinReelle
     * @return ViewHistoriqueObjProtection
     */
    public function setDtDateFinReelle($dtDateFinReelle)
    {
        $this->dtDateFinReelle = $dtDateFinReelle;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     * @return ViewHistoriqueObjProtection
     */
    public function setIdMatriculeMaj($idMatriculeMaj)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtSuppressionObjet()
    {
        return $this->dtSuppressionObjet;
    }

    /**
     * @param \DateTime $dtSuppressionObjet
     * @return ViewHistoriqueObjProtection
     */
    public function setDtSuppressionObjet($dtSuppressionObjet)
    {
        $this->dtSuppressionObjet = $dtSuppressionObjet;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiEtat()
    {
        return $this->liEtat;
    }

    /**
     * @param string $liEtat
     * @return ViewHistoriqueObjProtection
     */
    public function setLiEtat($liEtat)
    {
        $this->liEtat = $liEtat;
        return $this;
    }

}
