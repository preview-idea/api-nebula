<?php

/**
 * @author      Théo Gautreau
 * @Date:       05/07/2019
 */

namespace SalarieBundle\Entity\Views;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_mandat_historique")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\ViewHistoriqueObjMandatRepository")
 */
class ViewHistoriqueObjMandat
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_mandat", type="integer", nullable=false)
     */
    private $idMandat;

    /**
     * @var \SalarieBundle\Entity\ObjSalarie
     *
     * @ORM\OneToOne(targetEntity="SalarieBundle\Entity\ObjSalarie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule", referencedColumnName="id_matricule")
     * })
     */
    private $idMatricule;

    /**
     * @var \SalarieBundle\Entity\Views\ViewHistoriqueObjProtection
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Views\ViewHistoriqueObjProtection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_protection", referencedColumnName="id_protection")
     * })
     */
    private $idProtection;

    /**
     * @var \SalarieBundle\Entity\Param\ParamOrgSyndicale
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamOrgSyndicale")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_org_syndicale", referencedColumnName="id_org_syndicale")
     * })
     */
    private $idOrgSyndicale;

    /**
     * @var \SalarieBundle\Entity\Param\ParamPerimetreElectoral
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamPerimetreElectoral")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_perimetre_electoral", referencedColumnName="id_perimetre_electoral")
     * })
     */
    private $idPerimetreElectoral;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCse
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCse")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cse", referencedColumnName="id_cse")
     * })
     */
    private $idCse;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeMandat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeMandat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_mandat", referencedColumnName="id_type_mandat")
     * })
     */
    private $idTypeMandat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_commentaire", type="string", length=100, nullable=true)
     */
    private $liCommentaire;

    /**
     * @var string
     *
     * @ORM\Column(name="li_commission", type="string", length=200, nullable=true)
     */
    private $liCommission;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_heure_delegation", type="smallint", nullable=true)
     */
    private $nbHeureDelegation;

    /**
     * @var string
     *
     * @ORM\Column(name="li_telephone", type="string", length=16, nullable=true)
     */
    private $liTelephone;

    /**
     * @var string
     *
     * @ORM\Column(name="li_mail", type="string", length=40, nullable=true)
     */
    private $liMail;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_debut", type="datetime", nullable=false)
     */
    private $dtDateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_fin_theorique", type="datetime", nullable=false)
     */
    private $dtDateFinTheorique;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_fin_reelle", type="datetime", nullable=true)
     */
    private $dtDateFinReelle;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_suppression_objet", type="datetime", nullable=false)
     */
    private $dtSuppressionObjet;

    /**
     * @var string
     *
     * @ORM\Column(name="li_etat", type="string", nullable=false)
     */
    private $liEtat;

    /**
     * @return int
     */
    public function getIdMandat()
    {
        return $this->idMandat;
    }

    /**
     * @param int $idMandat
     * @return ViewHistoriqueObjMandat
     */
    public function setIdMandat($idMandat)
    {
        $this->idMandat = $idMandat;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\ObjSalarie
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param \SalarieBundle\Entity\ObjSalarie $idMatricule
     * @return ViewHistoriqueObjMandat
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\ObjSalarieProtection
     */
    public function getIdProtection()
    {
        return $this->idProtection;
    }

    /**
     * @param \SalarieBundle\Entity\ObjSalarieProtection $idProtection
     * @return ViewHistoriqueObjMandat
     */
    public function setIdProtection($idProtection)
    {
        $this->idProtection = $idProtection;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamOrgSyndicale
     */
    public function getIdOrgSyndicale()
    {
        return $this->idOrgSyndicale;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamOrgSyndicale $idOrgSyndicale
     * @return ViewHistoriqueObjMandat
     */
    public function setIdOrgSyndicale($idOrgSyndicale)
    {
        $this->idOrgSyndicale = $idOrgSyndicale;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamPerimetreElectoral
     */
    public function getIdPerimetreElectoral()
    {
        return $this->idPerimetreElectoral;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamPerimetreElectoral $idPerimetreElectoral
     * @return ViewHistoriqueObjMandat
     */
    public function setIdPerimetreElectoral($idPerimetreElectoral)
    {
        $this->idPerimetreElectoral = $idPerimetreElectoral;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamCse
     */
    public function getIdCse()
    {
        return $this->idCse;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamCse $idCse
     * @return ViewHistoriqueObjMandat
     */
    public function setIdCse($idCse)
    {
        $this->idCse = $idCse;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamTypeMandat
     */
    public function getIdTypeMandat()
    {
        return $this->idTypeMandat;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamTypeMandat $idTypeMandat
     * @return ViewHistoriqueObjMandat
     */
    public function setIdTypeMandat($idTypeMandat)
    {
        $this->idTypeMandat = $idTypeMandat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiCommentaire()
    {
        return $this->liCommentaire;
    }

    /**
     * @param string $liCommentaire
     * @return ViewHistoriqueObjMandat
     */
    public function setLiCommentaire($liCommentaire)
    {
        $this->liCommentaire = $liCommentaire;
        return $this;
    }

    /**
     * @return int
     */
    public function getNbHeureDelegation()
    {
        return $this->nbHeureDelegation;
    }

    /**
     * @param int $nbHeureDelegation
     * @return ViewHistoriqueObjMandat
     */
    public function setNbHeureDelegation($nbHeureDelegation)
    {
        $this->nbHeureDelegation = $nbHeureDelegation;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiTelephone()
    {
        return $this->liTelephone;
    }

    /**
     * @param string $liTelephone
     * @return ViewHistoriqueObjMandat
     */
    public function setLiTelephone($liTelephone)
    {
        $this->liTelephone = $liTelephone;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiMail()
    {
        return $this->liMail;
    }

    /**
     * @param string $liMail
     * @return ViewHistoriqueObjMandat
     */
    public function setLiMail($liMail)
    {
        $this->liMail = $liMail;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtDateDebut()
    {
        return $this->dtDateDebut;
    }

    /**
     * @param \DateTime $dtDateDebut
     * @return ViewHistoriqueObjMandat
     */
    public function setDtDateDebut($dtDateDebut)
    {
        $this->dtDateDebut = $dtDateDebut;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtDateFinTheorique()
    {
        return $this->dtDateFinTheorique;
    }

    /**
     * @param \DateTime $dtDateFinTheorique
     * @return ViewHistoriqueObjMandat
     */
    public function setDtDateFinTheorique($dtDateFinTheorique)
    {
        $this->dtDateFinTheorique = $dtDateFinTheorique;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtDateFinReelle()
    {
        return $this->dtDateFinReelle;
    }

    /**
     * @param \DateTime $dtDateFinReelle
     * @return ViewHistoriqueObjMandat
     */
    public function setDtDateFinReelle($dtDateFinReelle)
    {
        $this->dtDateFinReelle = $dtDateFinReelle;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     * @return ViewHistoriqueObjMandat
     */
    public function setIdMatriculeMaj($idMatriculeMaj)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtSuppressionObjet()
    {
        return $this->dtSuppressionObjet;
    }

    /**
     * @param \DateTime $dtSuppressionObjet
     * @return ViewHistoriqueObjMandat
     */
    public function setDtSuppressionObjet($dtSuppressionObjet)
    {
        $this->dtSuppressionObjet = $dtSuppressionObjet;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiEtat()
    {
        return $this->liEtat;
    }

    /**
     * @param string $liEtat
     * @return ViewHistoriqueObjMandat
     */
    public function setLiEtat($liEtat)
    {
        $this->liEtat = $liEtat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiCommission()
    {
        return $this->liCommission;
    }

    /**
     * @param string $liCommission
     * @return ViewHistoriqueObjMandat
     */
    public function setLiCommission($liCommission)
    {
        $this->liCommission = $liCommission;
        return $this;
    }

}
