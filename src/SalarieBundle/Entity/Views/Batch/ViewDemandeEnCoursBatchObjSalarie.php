<?php

namespace SalarieBundle\Entity\Views\Batch;

use Doctrine\ORM\Mapping as ORM;

/**
 * ObjBatches
 *
 * @ORM\Table(name="v_demande_en_cours_batch")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Batch\ViewDemandeEnCoursBatchObjSalarieRepository")
 */
class ViewDemandeEnCoursBatchObjSalarie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_traitement", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idTraitement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_traitement", type="string", length=30, nullable=true)
     */
    private $liTypeTraitement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_demande", type="datetime", nullable=false)
     */
    private $dtDemande;

    /**
     * @var \SalarieBundle\Entity\ObjSalarie
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\ObjSalarie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_demande", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeDemande;

    /**
     * @var string
     *
     * @ORM\Column(name="li_message", type="string", length=100, nullable=true)
     */
    private $liMessage;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_etat_traitement", type="integer", nullable=true)
     */
    private $idEtatTraitement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_etat_traitement", type="string", length=30, nullable=true)
     */
    private $liEtatTraitement;

    /**
     * @var \SalarieBundle\Entity\ObjSalarie
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\ObjSalarie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_valideur", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeValideur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debutcontrat", type="datetime", nullable=false)
     */
    private $dtDebutcontrat;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_matricule", type="integer", nullable=true)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_typecontrat", type="string", length=15, nullable=true)
     */
    private $liTypecontrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fincontrat_prevue", type="datetime", nullable=false)
     */
    private $dtFincontratPrevue;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_agence", type="integer", nullable=true)
     */
    private $idAgence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_contrat", type="integer", nullable=true)
     */
    private $idContrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", length=60, nullable=true)
     */
    private $liAgence;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_complet", type="string", length=100, nullable=true)
     */
    private $liNomComplet;

    /**
     * @return int
     */
    public function getIdTraitement()
    {
        return $this->idTraitement;
    }

    /**
     * @param int $idTraitement
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setIdTraitement($idTraitement)
    {
        $this->idTraitement = $idTraitement;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiTypeTraitement()
    {
        return $this->liTypeTraitement;
    }

    /**
     * @param string $liTypeTraitement
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setLiTypeTraitement($liTypeTraitement)
    {
        $this->liTypeTraitement = $liTypeTraitement;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtDemande()
    {
        return $this->dtDemande;
    }

    /**
     * @param \DateTime $dtDemande
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setDtDemande($dtDemande)
    {
        $this->dtDemande = $dtDemande;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\ObjSalarie
     */
    public function getIdMatriculeDemande()
    {
        return $this->idMatriculeDemande;
    }

    /**
     * @param \SalarieBundle\Entity\ObjSalarie $idMatriculeDemande
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setIdMatriculeDemande($idMatriculeDemande)
    {
        $this->idMatriculeDemande = $idMatriculeDemande;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiMessage()
    {
        return $this->liMessage;
    }

    /**
     * @param string $liMessage
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setLiMessage($liMessage)
    {
        $this->liMessage = $liMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiEtatTraitement()
    {
        return $this->liEtatTraitement;
    }

    /**
     * @param string $liEtatTraitement
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setLiEtatTraitement($liEtatTraitement)
    {
        $this->liEtatTraitement = $liEtatTraitement;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\ObjSalarie
     */
    public function getIdMatriculeValideur()
    {
        return $this->idMatriculeValideur;
    }

    /**
     * @param \SalarieBundle\Entity\ObjSalarie $idMatriculeValideur
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setIdMatriculeValideur($idMatriculeValideur)
    {
        $this->idMatriculeValideur = $idMatriculeValideur;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtDebutcontrat()
    {
        return $this->dtDebutcontrat;
    }

    /**
     * @param \DateTime $dtDebutcontrat
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setDtDebutcontrat($dtDebutcontrat)
    {
        $this->dtDebutcontrat = $dtDebutcontrat;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param int $idMatricule
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiTypecontrat()
    {
        return $this->liTypecontrat;
    }

    /**
     * @param string $liTypecontrat
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setLiTypecontrat($liTypecontrat)
    {
        $this->liTypecontrat = $liTypecontrat;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtFincontratPrevue()
    {
        return $this->dtFincontratPrevue;
    }

    /**
     * @param \DateTime $dtFincontratPrevue
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setDtFincontratPrevue($dtFincontratPrevue)
    {
        $this->dtFincontratPrevue = $dtFincontratPrevue;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * @param int $idAgence
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * @param string $liAgence
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiNomComplet()
    {
        return $this->liNomComplet;
    }

    /**
     * @param string $liNomComplet
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setLiNomComplet($liNomComplet)
    {
        $this->liNomComplet = $liNomComplet;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * @param int $idContrat
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setIdContrat($idContrat)
    {
        $this->idContrat = $idContrat;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdEtatTraitement()
    {
        return $this->idEtatTraitement;
    }

    /**
     * @param int $idEtatTraitement
     * @return ViewDemandeEnCoursBatchObjSalarie
     */
    public function setIdEtatTraitement($idEtatTraitement)
    {
        $this->idEtatTraitement = $idEtatTraitement;
        return $this;
    }

}
