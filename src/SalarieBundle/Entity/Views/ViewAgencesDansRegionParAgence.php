<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/10/2018
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_agences_dans_region_par_agence")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\ViewAgencesDansRegionParAgenceRepository")
 */
class ViewAgencesDansRegionParAgence
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_agence", type="integer", nullable=false)
     */
    private $idAgence;

    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", nullable=false)
     */
    private $liAgence;

    /**
     * @var integer
     * @ORM\Column(name="id_agence_origine", type="integer", nullable=false)
     */
    private $idAgenceOrigine;

    /**
     * Set idAgence
     *
     * @param integer $idAgence
     *
     * @return ViewAgencesDansRegionParAgence
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;

        return $this;
    }

    /**
     * Get idAgence
     *
     * @return integer
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * Set liAgence
     *
     * @param string $liAgence
     *
     * @return ViewAgencesDansRegionParAgence
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;

        return $this;
    }

    /**
     * Get liAgence
     *
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * Set idAgenceOrigine
     *
     * @param integer $idAgenceOrigine
     *
     * @return ViewAgencesDansRegionParAgence
     */
    public function setIdAgenceOrigine($idAgenceOrigine)
    {
        $this->idAgenceOrigine = $idAgenceOrigine;

        return $this;
    }

    /**
     * Get idAgenceOrigine
     *
     * @return integer
     */
    public function getIdAgenceOrigine()
    {
        return $this->idAgenceOrigine;
    }
}
