<?php

namespace SalarieBundle\Entity\Views\Salarie;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_salarie_derniere_piece_identite")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Salarie\ViewDernierIdentiteObjSalarieRepository")
 */
class ViewDerniereIdentiteObjSalarie
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_usage", type="string", nullable=false)
     */
    private $liNomUsage;

    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", nullable=false)
     */
    private $liAgence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_agence", type="integer", nullable=false)
     */
    private $idAgence;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_piece", type="string", length=20, nullable=false)
     */
    private $liTypePiece;

    /**
     * @var string
     *
     * @ORM\Column(name="li_numero_piece", type="string", length=20, nullable=true)
     */
    private $liNumeroPiece;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_validite", type="date", nullable=true)
     */
    private $dtDebutValidite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_validite", type="date", nullable=true)
     */
    private $dtFinValidite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debutcontrat", type="datetime", nullable=false)
     */
    private $dtDebutcontrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fincontrat", type="datetime", nullable=true)
     */
    private $dtFincontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_typecontrat", type="string", nullable=true)
     */
    private $liTypecontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nationalite", type="string", length=50, nullable=true)
     */
    private $liNationalite;

    /**
     * @var string
     *
     * @ORM\Column(name="li_etat", type="string", nullable=false)
     */
    private $liEtat;

    /**
     * Set idMatricule
     *
     * @param integer $idMatricule
     *
     * @return ViewDerniereIdentiteObjSalarie
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return integer
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set liNomUsage
     *
     * @param string $liNomUsage
     *
     * @return ViewDerniereIdentiteObjSalarie
     */
    public function setLiNomUsage($liNomUsage)
    {
        $this->liNomUsage = $liNomUsage;

        return $this;
    }

    /**
     * Get liNomUsage
     *
     * @return string
     */
    public function getLiNomUsage()
    {
        return $this->liNomUsage;
    }

    /**
     * Set liAgence
     *
     * @param string $liAgence
     *
     * @return ViewDerniereIdentiteObjSalarie
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;

        return $this;
    }

    /**
     * Get liAgence
     *
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * @return int
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * @param int $idAgence
     * @return ViewDerniereIdentiteObjSalarie
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;
        return $this;
    }

    /**
     * Set liTypePiece
     *
     * @param string $liTypePiece
     *
     * @return ViewDerniereIdentiteObjSalarie
     */
    public function setLiTypePiece($liTypePiece)
    {
        $this->liTypePiece = $liTypePiece;

        return $this;
    }

    /**
     * Get liTypePiece
     *
     * @return string
     */
    public function getLiTypePiece()
    {
        return $this->liTypePiece;
    }

    /**
     * Set liNumeroPiece
     *
     * @param string $liNumeroPiece
     *
     * @return ViewDerniereIdentiteObjSalarie
     */
    public function setLiNumeroPiece($liNumeroPiece)
    {
        $this->liNumeroPiece = $liNumeroPiece;

        return $this;
    }

    /**
     * Get liNumeroPiece
     *
     * @return string
     */
    public function getLiNumeroPiece()
    {
        return $this->liNumeroPiece;
    }

    /**
     * Set dtDebutValidite
     *
     * @param \DateTime $dtDebutValidite
     *
     * @return ViewDerniereIdentiteObjSalarie
     */
    public function setDtDebutValidite($dtDebutValidite)
    {
        $this->dtDebutValidite = $dtDebutValidite;

        return $this;
    }

    /**
     * Get dtDebutValidite
     *
     * @return \DateTime
     */
    public function getDtDebutValidite()
    {
        return $this->dtDebutValidite;
    }

    /**
     * Set dtFinValidite
     *
     * @param \DateTime $dtFinValidite
     *
     * @return ViewDerniereIdentiteObjSalarie
     */
    public function setDtFinValidite($dtFinValidite)
    {
        $this->dtFinValidite = $dtFinValidite;

        return $this;
    }

    /**
     * Get dtFinValidite
     *
     * @return \DateTime
     */
    public function getDtFinValidite()
    {
        return $this->dtFinValidite;
    }

    /**
     * Set dtDebutcontrat
     *
     * @param \DateTime $dtDebutcontrat
     *
     * @return ViewDerniereIdentiteObjSalarie
     */
    public function setDtDebutcontrat($dtDebutcontrat)
    {
        $this->dtDebutcontrat = $dtDebutcontrat;

        return $this;
    }

    /**
     * Get dtDebutcontrat
     *
     * @return \DateTime
     */
    public function getDtDebutcontrat()
    {
        return $this->dtDebutcontrat;
    }

    /**
     * Set dtFincontrat
     *
     * @param \DateTime $dtFincontrat
     *
     * @return ViewDerniereIdentiteObjSalarie
     */
    public function setDtFincontrat($dtFincontrat)
    {
        $this->dtFincontrat = $dtFincontrat;

        return $this;
    }

    /**
     * Get dtFincontrat
     *
     * @return \DateTime
     */
    public function getDtFincontrat()
    {
        return $this->dtFincontrat;
    }

    /**
     * Set liTypecontrat
     *
     * @param string $liTypecontrat
     *
     * @return ViewDerniereIdentiteObjSalarie
     */
    public function setLiTypecontrat($liTypecontrat)
    {
        $this->liTypecontrat = $liTypecontrat;

        return $this;
    }

    /**
     * Get liTypecontrat
     *
     * @return string
     */
    public function getLiTypecontrat()
    {
        return $this->liTypecontrat;
    }

    /**
     * Set liNationalite
     *
     * @param string $liNationalite
     *
     * @return ViewDerniereIdentiteObjSalarie
     */
    public function setLiNationalite($liNationalite)
    {
        $this->liNationalite = $liNationalite;

        return $this;
    }

    /**
     * Get liNationalite
     *
     * @return string
     */
    public function getLiNationalite()
    {
        return $this->liNationalite;
    }

    /**
     * Set liEtat
     *
     * @param string $liEtat
     *
     * @return ViewDerniereIdentiteObjSalarie
     */
    public function setLiEtat($liEtat)
    {
        $this->liEtat = $liEtat;

        return $this;
    }

    /**
     * Get liEtat
     *
     * @return string
     */
    public function getLiEtat()
    {
        return $this->liEtat;
    }
}
