<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/10/2018
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Salarie;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_salarie_actif_inactif")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Salarie\ViewActifInactifObjSalarieRepository")
 */
class ViewActifInactifObjSalarie
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom", type="string", nullable=false)
     */
    private $liNom;

    /**
     * @var string
     *
     * @ORM\Column(name="li_prenom", type="string", nullable=false)
     */
    private $liPrenom;

    /**
     * @var string
     *
     * @ORM\Column(name="li_ville_naissance", type="string", nullable=false)
     */
    private $liVilleNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="li_dep_naissance", type="string", length=2, nullable=false)
     */
    private $liDepNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="id_nir_def", type="string", length=15, nullable=false)
     */
    private $idNirDef;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_voie", type="string", length=50, nullable=false)
     */
    private $liNomVoie;

    /**
     * @var string
     *
     * @ORM\Column(name="li_adresse_2", type="string", length=50, nullable=true)
     */
    private $liAdresse2;

    /**
     * @var string
     *
     * @ORM\Column(name="li_pref_telephone_1", type="string", length=5, nullable=true)
     */
    private $liPrefTelephone1;

    /**
     * @var string
     *
     * @ORM\Column(name="li_telephone_1", type="string", length=15, nullable=true)
     */
    private $liTelephone1;

    /**
     * @var string
     *
     * @ORM\Column(name="li_pref_telephone_2", type="string", length=5, nullable=true)
     */
    private $liPrefTelephone2;

    /**
     * @var string
     *
     * @ORM\Column(name="li_telephone_2", type="string", length=15, nullable=true)
     */
    private $liTelephone2;

    /**
     * @var string
     *
     * @ORM\Column(name="li_mail", type="string", length=40, nullable=true)
     */
    private $liMail;

    /**
     * @var string
     *
     * @ORM\Column(name="li_numero_piece", type="string", length=20, nullable=true)
     */
    private $liNumeroPiece;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_validite", type="date", nullable=true)
     */
    private $dtDebutValidite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_validite", type="date", nullable=true)
     */
    private $dtFinValidite;

    /**
     * @var string
     *
     * @ORM\Column(name="li_chemin_photo", type="string", length=60, nullable=true)
     */
    private $liCheminPhoto;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_nir_prov", type="bit", nullable=true)
     */
    private $isNirProv;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_envoi_mail", type="bit", nullable=true)
     */
    private $isEnvoiMail;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_usage", type="string", nullable=false)
     */
    private $liNomUsage;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_paiement_virement", type="bit", nullable=true)
     */
    private $isPaiementVirement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_titulaire_compte", type="string", length=30, nullable=true)
     */
    private $liTitulaireCompte;

    /**
     * @var string
     *
     * @ORM\Column(name="li_bic", type="string", length=11, nullable=true)
     */
    private $liBic;

    /**
     * @var string
     *
     * @ORM\Column(name="li_iban", type="string", length=27, nullable=true)
     */
    private $liIban;

    /**
     * @var string
     *
     * @ORM\Column(name="li_domiciliation_bancaire", type="string", length=30, nullable=true)
     */
    private $liDomiciliationBancaire;

    /**
     * @var string
     *
     * @ORM\Column(name="li_ville_etranger", type="string", length=40, nullable=true)
     */
    private $liVilleEtranger;

    /**
     * @var string
     *
     * @ORM\Column(name="li_codepostal_etranger", type="string", length=6, nullable=true)
     */
    private $liCodepostalEtranger;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_codepostal", type="integer", nullable=false)
     */
    private $idCodepostal;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_agence", type="integer", nullable=false)
     */
    private $idAgence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_matricule_maj", type="integer", nullable=false)
     */
    private $idMatriculeMaj;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_civilite", type="integer", nullable=false)
     */
    private $idCivilite;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pays_naissance", type="integer", nullable=false)
     */
    private $idPaysNaissance;


    /**
     * @var integer
     *
     * @ORM\Column(name="id_pays_nationalite", type="integer", nullable=false)
     */
    private $idPaysNationalite;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_situation_famille", type="integer", nullable=false)
     */
    private $idSituationFamille;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_type_piece", type="integer", nullable=false)
     */
    private $idTypePiece;

    /**
     * @var string
     *
     * @ORM\Column(name="li_etat", type="string", nullable=false)
     */
    private $liEtat;

    /**
     * Set idMatricule
     *
     * @param integer $idMatricule
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return integer
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set liNom
     *
     * @param string $liNom
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiNom($liNom)
    {
        $this->liNom = $liNom;

        return $this;
    }

    /**
     * Get liNom
     *
     * @return string
     */
    public function getLiNom()
    {
        return $this->liNom;
    }

    /**
     * Set liPrenom
     *
     * @param string $liPrenom
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiPrenom($liPrenom)
    {
        $this->liPrenom = $liPrenom;

        return $this;
    }

    /**
     * Get liPrenom
     *
     * @return string
     */
    public function getLiPrenom()
    {
        return $this->liPrenom;
    }

    /**
     * Set liVilleNaissance
     *
     * @param string $liVilleNaissance
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiVilleNaissance($liVilleNaissance)
    {
        $this->liVilleNaissance = $liVilleNaissance;

        return $this;
    }

    /**
     * Get liVilleNaissance
     *
     * @return string
     */
    public function getLiVilleNaissance()
    {
        return $this->liVilleNaissance;
    }

    /**
     * Set liDepNaissance
     *
     * @param string $liDepNaissance
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiDepNaissance($liDepNaissance)
    {
        $this->liDepNaissance = $liDepNaissance;

        return $this;
    }

    /**
     * Get liDepNaissance
     *
     * @return string
     */
    public function getLiDepNaissance()
    {
        return $this->liDepNaissance;
    }

    /**
     * Set idNirDef
     *
     * @param string $idNirDef
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setIdNirDef($idNirDef)
    {
        $this->idNirDef = $idNirDef;

        return $this;
    }

    /**
     * Get idNirDef
     *
     * @return string
     */
    public function getIdNirDef()
    {
        return $this->idNirDef;
    }

    /**
     * Set liNomVoie
     *
     * @param string $liNomVoie
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiNomVoie($liNomVoie)
    {
        $this->liNomVoie = $liNomVoie;

        return $this;
    }

    /**
     * Get liNomVoie
     *
     * @return string
     */
    public function getLiNomVoie()
    {
        return $this->liNomVoie;
    }

    /**
     * Set liAdresse2
     *
     * @param string $liAdresse2
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiAdresse2($liAdresse2)
    {
        $this->liAdresse2 = $liAdresse2;

        return $this;
    }

    /**
     * Get liAdresse2
     *
     * @return string
     */
    public function getLiAdresse2()
    {
        return $this->liAdresse2;
    }

    /**
     * Set liPrefTelephone1
     *
     * @param string $liPrefTelephone1
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiPrefTelephone1($liPrefTelephone1)
    {
        $this->liPrefTelephone1 = $liPrefTelephone1;

        return $this;
    }

    /**
     * Get liPrefTelephone1
     *
     * @return string
     */
    public function getLiPrefTelephone1()
    {
        return $this->liPrefTelephone1;
    }

    /**
     * Set liTelephone1
     *
     * @param string $liTelephone1
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiTelephone1($liTelephone1)
    {
        $this->liTelephone1 = $liTelephone1;

        return $this;
    }

    /**
     * Get liTelephone1
     *
     * @return string
     */
    public function getLiTelephone1()
    {
        return $this->liTelephone1;
    }

    /**
     * Set liPrefTelephone2
     *
     * @param string $liPrefTelephone2
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiPrefTelephone2($liPrefTelephone2)
    {
        $this->liPrefTelephone2 = $liPrefTelephone2;

        return $this;
    }

    /**
     * Get liPrefTelephone2
     *
     * @return string
     */
    public function getLiPrefTelephone2()
    {
        return $this->liPrefTelephone2;
    }

    /**
     * Set liTelephone2
     *
     * @param string $liTelephone2
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiTelephone2($liTelephone2)
    {
        $this->liTelephone2 = $liTelephone2;

        return $this;
    }

    /**
     * Get liTelephone2
     *
     * @return string
     */
    public function getLiTelephone2()
    {
        return $this->liTelephone2;
    }

    /**
     * Set liMail
     *
     * @param string $liMail
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiMail($liMail)
    {
        $this->liMail = $liMail;

        return $this;
    }

    /**
     * Get liMail
     *
     * @return string
     */
    public function getLiMail()
    {
        return $this->liMail;
    }

    /**
     * Set liNumeroPiece
     *
     * @param string $liNumeroPiece
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiNumeroPiece($liNumeroPiece)
    {
        $this->liNumeroPiece = $liNumeroPiece;

        return $this;
    }

    /**
     * Get liNumeroPiece
     *
     * @return string
     */
    public function getLiNumeroPiece()
    {
        return $this->liNumeroPiece;
    }

    /**
     * Set dtDebutValidite
     *
     * @param \DateTime $dtDebutValidite
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setDtDebutValidite($dtDebutValidite)
    {
        $this->dtDebutValidite = $dtDebutValidite;

        return $this;
    }

    /**
     * Get dtDebutValidite
     *
     * @return \DateTime
     */
    public function getDtDebutValidite()
    {
        return $this->dtDebutValidite;
    }

    /**
     * Set dtFinValidite
     *
     * @param \DateTime $dtFinValidite
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setDtFinValidite($dtFinValidite)
    {
        $this->dtFinValidite = $dtFinValidite;

        return $this;
    }

    /**
     * Get dtFinValidite
     *
     * @return \DateTime
     */
    public function getDtFinValidite()
    {
        return $this->dtFinValidite;
    }

    /**
     * Set liCheminPhoto
     *
     * @param string $liCheminPhoto
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiCheminPhoto($liCheminPhoto)
    {
        $this->liCheminPhoto = $liCheminPhoto;

        return $this;
    }

    /**
     * Get liCheminPhoto
     *
     * @return string
     */
    public function getLiCheminPhoto()
    {
        return $this->liCheminPhoto;
    }

    /**
     * Set isNirProv
     *
     * @param bit $isNirProv
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setIsNirProv($isNirProv)
    {
        $this->isNirProv = $isNirProv;

        return $this;
    }

    /**
     * Get isNirProv
     *
     * @return bit
     */
    public function getIsNirProv()
    {
        return $this->isNirProv;
    }

    /**
     * Set isEnvoiMail
     *
     * @param bit $isEnvoiMail
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setIsEnvoiMail($isEnvoiMail)
    {
        $this->isEnvoiMail = $isEnvoiMail;

        return $this;
    }

    /**
     * Get isEnvoiMail
     *
     * @return bit
     */
    public function getIsEnvoiMail()
    {
        return $this->isEnvoiMail;
    }

    /**
     * Set liNomUsage
     *
     * @param string $liNomUsage
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiNomUsage($liNomUsage)
    {
        $this->liNomUsage = $liNomUsage;

        return $this;
    }

    /**
     * Get liNomUsage
     *
     * @return string
     */
    public function getLiNomUsage()
    {
        return $this->liNomUsage;
    }

    /**
     * Set isPaiementVirement
     *
     * @param bit $isPaiementVirement
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setIsPaiementVirement($isPaiementVirement)
    {
        $this->isPaiementVirement = $isPaiementVirement;

        return $this;
    }

    /**
     * Get isPaiementVirement
     *
     * @return bit
     */
    public function getIsPaiementVirement()
    {
        return $this->isPaiementVirement;
    }

    /**
     * Set liTitulaireCompte
     *
     * @param string $liTitulaireCompte
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiTitulaireCompte($liTitulaireCompte)
    {
        $this->liTitulaireCompte = $liTitulaireCompte;

        return $this;
    }

    /**
     * Get liTitulaireCompte
     *
     * @return string
     */
    public function getLiTitulaireCompte()
    {
        return $this->liTitulaireCompte;
    }

    /**
     * Set liBic
     *
     * @param string $liBic
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiBic($liBic)
    {
        $this->liBic = $liBic;

        return $this;
    }

    /**
     * Get liBic
     *
     * @return string
     */
    public function getLiBic()
    {
        return $this->liBic;
    }

    /**
     * Set liIban
     *
     * @param string $liIban
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiIban($liIban)
    {
        $this->liIban = $liIban;

        return $this;
    }

    /**
     * Get liIban
     *
     * @return string
     */
    public function getLiIban()
    {
        return $this->liIban;
    }

    /**
     * Set liDomiciliationBancaire
     *
     * @param string $liDomiciliationBancaire
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiDomiciliationBancaire($liDomiciliationBancaire)
    {
        $this->liDomiciliationBancaire = $liDomiciliationBancaire;

        return $this;
    }

    /**
     * Get liDomiciliationBancaire
     *
     * @return string
     */
    public function getLiDomiciliationBancaire()
    {
        return $this->liDomiciliationBancaire;
    }

    /**
     * Set liVilleEtranger
     *
     * @param string $liVilleEtranger
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiVilleEtranger($liVilleEtranger)
    {
        $this->liVilleEtranger = $liVilleEtranger;

        return $this;
    }

    /**
     * Get liVilleEtranger
     *
     * @return string
     */
    public function getLiVilleEtranger()
    {
        return $this->liVilleEtranger;
    }

    /**
     * Set liCodepostalEtranger
     *
     * @param string $liCodepostalEtranger
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setLiCodepostalEtranger($liCodepostalEtranger)
    {
        $this->liCodepostalEtranger = $liCodepostalEtranger;

        return $this;
    }

    /**
     * Get liCodepostalEtranger
     *
     * @return string
     */
    public function getLiCodepostalEtranger()
    {
        return $this->liCodepostalEtranger;
    }

    /**
     * Set idCodepostal
     *
     * @param integer $idCodepostal
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setIdCodepostal($idCodepostal)
    {
        $this->idCodepostal = $idCodepostal;

        return $this;
    }

    /**
     * Get idCodepostal
     *
     * @return integer
     */
    public function getIdCodepostal()
    {
        return $this->idCodepostal;
    }

    /**
     * Set idAgence
     *
     * @param integer $idAgence
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;

        return $this;
    }

    /**
     * Get idAgence
     *
     * @return integer
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param integer $idMatriculeMaj
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setIdMatriculeMaj($idMatriculeMaj)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return integer
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idCivilite
     *
     * @param integer $idCivilite
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setIdCivilite($idCivilite)
    {
        $this->idCivilite = $idCivilite;

        return $this;
    }

    /**
     * Get idCivilite
     *
     * @return integer
     */
    public function getIdCivilite()
    {
        return $this->idCivilite;
    }

    /**
     * Set idPaysNaissance
     *
     * @param integer $idPaysNaissance
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setIdPaysNaissance($idPaysNaissance)
    {
        $this->idPaysNaissance = $idPaysNaissance;

        return $this;
    }

    /**
     * Get idPaysNaissance
     *
     * @return integer
     */
    public function getIdPaysNaissance()
    {
        return $this->idPaysNaissance;
    }

    /**
     * Set idPaysNationalite
     *
     * @param integer $idPaysNationalite
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setIdPaysNationalite($idPaysNationalite)
    {
        $this->idPaysNationalite = $idPaysNationalite;

        return $this;
    }

    /**
     * Get idPaysNationalite
     *
     * @return integer
     */
    public function getIdPaysNationalite()
    {
        return $this->idPaysNationalite;
    }

    /**
     * Set idSituationFamille
     *
     * @param integer $idSituationFamille
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setIdSituationFamille($idSituationFamille)
    {
        $this->idSituationFamille = $idSituationFamille;

        return $this;
    }

    /**
     * Get idSituationFamille
     *
     * @return integer
     */
    public function getIdSituationFamille()
    {
        return $this->idSituationFamille;
    }

    /**
     * Set idTypePiece
     *
     * @param integer $idTypePiece
     *
     * @return ViewActifInactifObjSalarie
     */
    public function setIdTypePiece($idTypePiece)
    {
        $this->idTypePiece = $idTypePiece;

        return $this;
    }

    /**
     * Get idTypePiece
     *
     * @return integer
     */
    public function getIdTypePiece()
    {
        return $this->idTypePiece;
    }

    /**
     * @return string
     */
    public function getLiEtat()
    {
        return $this->liEtat;
    }

    /**
     * @param string $liEtat
     * @return ViewActifInactifObjSalarie
     */
    public function setLiEtat($liEtat)
    {
        $this->liEtat = $liEtat;
        return $this;
    }
}
