<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/10/2018
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Salarie;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_salarie_inactif")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Salarie\ViewSalarieInactifRepository")
 */
class ViewSalarieInactif
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom", type="string", nullable=false)
     */
    private $liNom;

    /**
     * @var string
     *
     * @ORM\Column(name="li_prenom", type="string", nullable=false)
     */
    private $liPrenom;

    /**
     * @var string
     *
     * @ORM\Column(name="li_ville_naissance", type="string", nullable=false)
     */
    private $liVilleNaissance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_naissance", type="date", nullable=false)
     */
    private $dtNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="li_dep_naissance", type="string", length=2, nullable=false)
     */
    private $liDepNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="id_nir_def", type="string", length=15, nullable=false)
     */
    private $idNirDef;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_voie", type="string", length=50, nullable=false)
     */
    private $liNomVoie;

    /**
     * @var string
     *
     * @ORM\Column(name="li_adresse_2", type="string", length=50, nullable=true)
     */
    private $liAdresse2;

    /**
     * @var string
     *
     * @ORM\Column(name="li_pref_telephone_1", type="string", length=5, nullable=true)
     */
    private $liPrefTelephone1;

    /**
     * @var string
     *
     * @ORM\Column(name="li_telephone_1", type="string", length=15, nullable=true)
     */
    private $liTelephone1;

    /**
     * @var string
     *
     * @ORM\Column(name="li_pref_telephone_2", type="string", length=5, nullable=true)
     */
    private $liPrefTelephone2;

    /**
     * @var string
     *
     * @ORM\Column(name="li_telephone_2", type="string", length=15, nullable=true)
     */
    private $liTelephone2;

    /**
     * @var string
     *
     * @ORM\Column(name="li_mail", type="string", length=40, nullable=true)
     */
    private $liMail;

    /**
     * @var string
     *
     * @ORM\Column(name="li_numero_piece", type="string", length=20, nullable=true)
     */
    private $liNumeroPiece;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_validite", type="date", nullable=true)
     */
    private $dtDebutValidite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_validite", type="date", nullable=true)
     */
    private $dtFinValidite;

    /**
     * @var string
     *
     * @ORM\Column(name="li_chemin_photo", type="string", length=60, nullable=true)
     */
    private $liCheminPhoto;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_nir_prov", type="bit", nullable=true)
     */
    private $isNirProv;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_envoi_mail", type="bit", nullable=true)
     */
    private $isEnvoiMail;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_usage", type="string", nullable=false)
     */
    private $liNomUsage;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_paiement_virement", type="bit", nullable=true)
     */
    private $isPaiementVirement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_titulaire_compte", type="string", length=30, nullable=true)
     */
    private $liTitulaireCompte;

    /**
     * @var string
     *
     * @ORM\Column(name="li_bic", type="string", length=11, nullable=true)
     */
    private $liBic;

    /**
     * @var string
     *
     * @ORM\Column(name="li_iban", type="string", length=27, nullable=true)
     */
    private $liIban;

    /**
     * @var string
     *
     * @ORM\Column(name="li_domiciliation_bancaire", type="string", length=30, nullable=true)
     */
    private $liDomiciliationBancaire;

    /**
     * @var string
     *
     * @ORM\Column(name="li_ville_etranger", type="string", length=40, nullable=true)
     */
    private $liVilleEtranger;

    /**
     * @var string
     *
     * @ORM\Column(name="li_codepostal_etranger", type="string", length=6, nullable=true)
     */
    private $liCodepostalEtranger;

    /**
     * @var \SalarieBundle\Entity\Param\ParamAgence
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamAgence")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_agence", referencedColumnName="id_agence")
     * })
     */
    private $idAgence;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCivilite
     *
     * @ORM\OneToOne(targetEntity="SalarieBundle\Entity\Param\ParamCivilite")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_civilite", referencedColumnName="id_civilite")
     * })
     */
    private $idCivilite;

    /**
     * @var \SalarieBundle\Entity\Param\ParamAdresseCodepostal
     *
     * @ORM\OneToOne(targetEntity="SalarieBundle\Entity\Param\ParamAdresseCodepostal")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_codepostal", referencedColumnName="id_codepostal")
     * })
     */
    private $idCodepostal;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\Param\ParamAdressePays
     *
     * @ORM\OneToOne(targetEntity="SalarieBundle\Entity\Param\ParamAdressePays")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pays", referencedColumnName="id_pays")
     * })
     */
    private $idPays;

    /**
     * @var \SalarieBundle\Entity\Param\ParamSituationFamille
     *
     * @ORM\OneToOne(targetEntity="SalarieBundle\Entity\Param\ParamSituationFamille")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_situation_famille", referencedColumnName="id_situation_famille")
     * })
     */
    private $idSituationFamille;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypePieceIdentite
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypePieceIdentite")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_piece", referencedColumnName="id_type_piece")
     * })
     */
    private $idTypePiece;

    /**
     * @var \SalarieBundle\Entity\Param\ParamAdresseVille
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamAdresseVille")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ville", referencedColumnName="id_ville")
     * })
     */
    private $idVille;

    /**
     * @var \SalarieBundle\Entity\Param\ParamAdressePays
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamAdressePays")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pays_naissance", referencedColumnName="id_pays")
     * })
     */
    private $idPaysNaissance;

    /**
     * @var \SalarieBundle\Entity\Param\ParamAdressePays
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamAdressePays")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pays_nationalite", referencedColumnName="id_pays")
     * })
     */
    private $idPaysNationalite;

    /**
     * @var \SalarieBundle\Entity\Param\ParamBddPlannet
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamBddPlannet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_bdd_plannet", referencedColumnName="id_bdd_plannet")
     * })
     */
    private $idBddPlannet;

    /**
     * Set idMatricule
     *
     * @param integer $idMatricule
     *
     * @return ViewSalarieInactif
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return integer
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set liNom
     *
     * @param string $liNom
     *
     * @return ViewSalarieInactif
     */
    public function setLiNom($liNom)
    {
        $this->liNom = $liNom;

        return $this;
    }


    /**
     * Get liNom
     *
     * @return string
     */
    public function getLiNom()
    {
        return $this->liNom;
    }

    /**
     * Set liPrenom
     *
     * @param string $liPrenom
     *
     * @return ViewSalarieInactif
     */
    public function setLiPrenom($liPrenom)
    {
        $this->liPrenom = $liPrenom;

        return $this;
    }

    /**
     * Get liPrenom
     *
     * @return string
     */
    public function getLiPrenom()
    {
        return $this->liPrenom;
    }

    /**
     * Set liVilleNaissance
     *
     * @param string $liVilleNaissance
     *
     * @return ViewSalarieInactif
     */
    public function setLiVilleNaissance($liVilleNaissance)
    {
        $this->liVilleNaissance = $liVilleNaissance;

        return $this;
    }

    /**
     * Get liVilleNaissance
     *
     * @return string
     */
    public function getLiVilleNaissance()
    {
        return $this->liVilleNaissance;
    }

    /**
     * Set dtNaissance
     *
     * @param \DateTime $dtNaissance
     *
     * @return ViewSalarieInactif
     */
    public function setDtNaissance($dtNaissance)
    {
        $this->dtNaissance = $dtNaissance;

        return $this;
    }

    /**
     * Get $dtNaissance
     *
     * @return \DateTime
     */
    public function getDtNaissance()
    {
        return $this->dtNaissance;
    }

    /**
     * Set liDepNaissance
     *
     * @param string $liDepNaissance
     *
     * @return ViewSalarieInactif
     */
    public function setLiDepNaissance($liDepNaissance)
    {
        $this->liDepNaissance = $liDepNaissance;

        return $this;
    }

    /**
     * Get liDepNaissance
     *
     * @return string
     */
    public function getLiDepNaissance()
    {
        return $this->liDepNaissance;
    }

    /**
     * Set idNirDef
     *
     * @param string $idNirDef
     *
     * @return ViewSalarieInactif
     */
    public function setIdNirDef($idNirDef)
    {
        $this->idNirDef = $idNirDef;

        return $this;
    }

    /**
     * Get idNirDef
     *
     * @return string
     */
    public function getIdNirDef()
    {
        return $this->idNirDef;
    }

    /**
     * Set liNomVoie
     *
     * @param string $liNomVoie
     *
     * @return ViewSalarieInactif
     */
    public function setLiNomVoie($liNomVoie)
    {
        $this->liNomVoie = $liNomVoie;

        return $this;
    }

    /**
     * Get liNomVoie
     *
     * @return string
     */
    public function getLiNomVoie()
    {
        return $this->liNomVoie;
    }

    /**
     * Set liAdresse2
     *
     * @param string $liAdresse2
     *
     * @return ViewSalarieInactif
     */
    public function setLiAdresse2($liAdresse2)
    {
        $this->liAdresse2 = $liAdresse2;

        return $this;
    }

    /**
     * Get liAdresse2
     *
     * @return string
     */
    public function getLiAdresse2()
    {
        return $this->liAdresse2;
    }

    /**
     * Set liPrefTelephone1
     *
     * @param string $liPrefTelephone1
     *
     * @return ViewSalarieInactif
     */
    public function setLiPrefTelephone1($liPrefTelephone1)
    {
        $this->liPrefTelephone1 = $liPrefTelephone1;

        return $this;
    }

    /**
     * Get liPrefTelephone1
     *
     * @return string
     */
    public function getLiPrefTelephone1()
    {
        return $this->liPrefTelephone1;
    }

    /**
     * Set liTelephone1
     *
     * @param string $liTelephone1
     *
     * @return ViewSalarieInactif
     */
    public function setLiTelephone1($liTelephone1)
    {
        $this->liTelephone1 = $liTelephone1;

        return $this;
    }

    /**
     * Get liTelephone1
     *
     * @return string
     */
    public function getLiTelephone1()
    {
        return $this->liTelephone1;
    }

    /**
     * Set liPrefTelephone2
     *
     * @param string $liPrefTelephone2
     *
     * @return ViewSalarieInactif
     */
    public function setLiPrefTelephone2($liPrefTelephone2)
    {
        $this->liPrefTelephone2 = $liPrefTelephone2;

        return $this;
    }

    /**
     * Get liPrefTelephone2
     *
     * @return string
     */
    public function getLiPrefTelephone2()
    {
        return $this->liPrefTelephone2;
    }

    /**
     * Set liTelephone2
     *
     * @param string $liTelephone2
     *
     * @return ViewSalarieInactif
     */
    public function setLiTelephone2($liTelephone2)
    {
        $this->liTelephone2 = $liTelephone2;

        return $this;
    }

    /**
     * Get liTelephone2
     *
     * @return string
     */
    public function getLiTelephone2()
    {
        return $this->liTelephone2;
    }

    /**
     * Set liMail
     *
     * @param string $liMail
     *
     * @return ViewSalarieInactif
     */
    public function setLiMail($liMail)
    {
        $this->liMail = $liMail;

        return $this;
    }

    /**
     * Get liMail
     *
     * @return string
     */
    public function getLiMail()
    {
        return $this->liMail;
    }

    /**
     * Set liNumeroPiece
     *
     * @param string $liNumeroPiece
     *
     * @return ViewSalarieInactif
     */
    public function setLiNumeroPiece($liNumeroPiece)
    {
        $this->liNumeroPiece = $liNumeroPiece;

        return $this;
    }

    /**
     * Get liNumeroPiece
     *
     * @return string
     */
    public function getLiNumeroPiece()
    {
        return $this->liNumeroPiece;
    }

    /**
     * Set dtDebutValidite
     *
     * @param \DateTime $dtDebutValidite
     *
     * @return ViewSalarieInactif
     */
    public function setDtDebutValidite($dtDebutValidite)
    {
        $this->dtDebutValidite = $dtDebutValidite;

        return $this;
    }

    /**
     * Get dtDebutValidite
     *
     * @return \DateTime
     */
    public function getDtDebutValidite()
    {
        return $this->dtDebutValidite;
    }

    /**
     * Set dtFinValidite
     *
     * @param \DateTime $dtFinValidite
     *
     * @return ViewSalarieInactif
     */
    public function setDtFinValidite($dtFinValidite)
    {
        $this->dtFinValidite = $dtFinValidite;

        return $this;
    }

    /**
     * Get dtFinValidite
     *
     * @return \DateTime
     */
    public function getDtFinValidite()
    {
        return $this->dtFinValidite;
    }

    /**
     * Set liCheminPhoto
     *
     * @param string $liCheminPhoto
     *
     * @return ViewSalarieInactif
     */
    public function setLiCheminPhoto($liCheminPhoto)
    {
        $this->liCheminPhoto = $liCheminPhoto;

        return $this;
    }

    /**
     * Get liCheminPhoto
     *
     * @return string
     */
    public function getLiCheminPhoto()
    {
        return $this->liCheminPhoto;
    }

    /**
     * Set isNirProv
     *
     * @param bit $isNirProv
     *
     * @return ViewSalarieInactif
     */
    public function setIsNirProv($isNirProv)
    {
        $this->isNirProv = $isNirProv;

        return $this;
    }

    /**
     * Get isNirProv
     *
     * @return bit
     */
    public function getIsNirProv()
    {
        return $this->isNirProv;
    }

    /**
     * Set isEnvoiMail
     *
     * @param bit $isEnvoiMail
     *
     * @return ViewSalarieInactif
     */
    public function setIsEnvoiMail($isEnvoiMail)
    {
        $this->isEnvoiMail = $isEnvoiMail;

        return $this;
    }

    /**
     * Get isEnvoiMail
     *
     * @return bit
     */
    public function getIsEnvoiMail()
    {
        return $this->isEnvoiMail;
    }

    /**
     * Set liNomUsage
     *
     * @param string $liNomUsage
     *
     * @return ViewSalarieInactif
     */
    public function setLiNomUsage($liNomUsage)
    {
        $this->liNomUsage = $liNomUsage;

        return $this;
    }

    /**
     * Get liNomUsage
     *
     * @return string
     */
    public function getLiNomUsage()
    {
        return $this->liNomUsage;
    }

    /**
     * Set isPaiementVirement
     *
     * @param bit $isPaiementVirement
     *
     * @return ViewSalarieInactif
     */
    public function setIsPaiementVirement($isPaiementVirement)
    {
        $this->isPaiementVirement = $isPaiementVirement;

        return $this;
    }

    /**
     * Get isPaiementVirement
     *
     * @return bit
     */
    public function getIsPaiementVirement()
    {
        return $this->isPaiementVirement;
    }

    /**
     * Set liTitulaireCompte
     *
     * @param string $liTitulaireCompte
     *
     * @return ViewSalarieInactif
     */
    public function setLiTitulaireCompte($liTitulaireCompte)
    {
        $this->liTitulaireCompte = $liTitulaireCompte;

        return $this;
    }

    /**
     * Get liTitulaireCompte
     *
     * @return string
     */
    public function getLiTitulaireCompte()
    {
        return $this->liTitulaireCompte;
    }

    /**
     * Set liBic
     *
     * @param string $liBic
     *
     * @return ViewSalarieInactif
     */
    public function setLiBic($liBic)
    {
        $this->liBic = $liBic;

        return $this;
    }

    /**
     * Get liBic
     *
     * @return string
     */
    public function getLiBic()
    {
        return $this->liBic;
    }

    /**
     * Set liIban
     *
     * @param string $liIban
     *
     * @return ViewSalarieInactif
     */
    public function setLiIban($liIban)
    {
        $this->liIban = $liIban;

        return $this;
    }

    /**
     * Get liIban
     *
     * @return string
     */
    public function getLiIban()
    {
        return $this->liIban;
    }

    /**
     * Set liDomiciliationBancaire
     *
     * @param string $liDomiciliationBancaire
     *
     * @return ViewSalarieInactif
     */
    public function setLiDomiciliationBancaire($liDomiciliationBancaire)
    {
        $this->liDomiciliationBancaire = $liDomiciliationBancaire;

        return $this;
    }

    /**
     * Get liDomiciliationBancaire
     *
     * @return string
     */
    public function getLiDomiciliationBancaire()
    {
        return $this->liDomiciliationBancaire;
    }

    /**
     * Set liVilleEtranger
     *
     * @param string $liVilleEtranger
     *
     * @return ViewSalarieInactif
     */
    public function setLiVilleEtranger($liVilleEtranger)
    {
        $this->liVilleEtranger = $liVilleEtranger;

        return $this;
    }

    /**
     * Get liVilleEtranger
     *
     * @return string
     */
    public function getLiVilleEtranger()
    {
        return $this->liVilleEtranger;
    }

    /**
     * Set liCodepostalEtranger
     *
     * @param string $liCodepostalEtranger
     *
     * @return ViewSalarieInactif
     */
    public function setLiCodepostalEtranger($liCodepostalEtranger)
    {
        $this->liCodepostalEtranger = $liCodepostalEtranger;

        return $this;
    }

    /**
     * Get liCodepostalEtranger
     *
     * @return string
     */
    public function getLiCodepostalEtranger()
    {
        return $this->liCodepostalEtranger;
    }

    /**
     * Set idAgence
     *
     * @param \SalarieBundle\Entity\Param\ParamAgence $idAgence
     *
     * @return ViewSalarieInactif
     */
    public function setIdAgence(\SalarieBundle\Entity\Param\ParamAgence $idAgence = null)
    {
        $this->idAgence = $idAgence;

        return $this;
    }

    /**
     * Get idAgence
     *
     * @return \SalarieBundle\Entity\Param\ParamAgence
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * Set idCivilite
     *
     * @param \SalarieBundle\Entity\Param\ParamCivilite $idCivilite
     *
     * @return ViewSalarieInactif
     */
    public function setIdCivilite(\SalarieBundle\Entity\Param\ParamCivilite $idCivilite = null)
    {
        $this->idCivilite = $idCivilite;

        return $this;
    }

    /**
     * Get idCivilite
     *
     * @return \SalarieBundle\Entity\Param\ParamCivilite
     */
    public function getIdCivilite()
    {
        return $this->idCivilite;
    }

    /**
     * Set idCodepostal
     *
     * @param \SalarieBundle\Entity\Param\ParamAdresseCodepostal $idCodepostal
     *
     * @return ViewSalarieInactif
     */
    public function setIdCodepostal(\SalarieBundle\Entity\Param\ParamAdresseCodepostal $idCodepostal = null)
    {
        $this->idCodepostal = $idCodepostal;

        return $this;
    }

    /**
     * Get idCodepostal
     *
     * @return \SalarieBundle\Entity\Param\ParamAdresseCodepostal
     */
    public function getIdCodepostal()
    {
        return $this->idCodepostal;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ViewSalarieInactif
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idPays
     *
     * @param \SalarieBundle\Entity\Param\ParamAdressePays $idPays
     *
     * @return ViewSalarieInactif
     */
    public function setIdPays(\SalarieBundle\Entity\Param\ParamAdressePays $idPays = null)
    {
        $this->idPays = $idPays;

        return $this;
    }

    /**
     * Get idPays
     *
     * @return \SalarieBundle\Entity\Param\ParamAdressePays
     */
    public function getIdPays()
    {
        return $this->idPays;
    }

    /**
     * Set idSituationFamille
     *
     * @param \SalarieBundle\Entity\Param\ParamSituationFamille $idSituationFamille
     *
     * @return ViewSalarieInactif
     */
    public function setIdSituationFamille(\SalarieBundle\Entity\Param\ParamSituationFamille $idSituationFamille = null)
    {
        $this->idSituationFamille = $idSituationFamille;

        return $this;
    }

    /**
     * Get idSituationFamille
     *
     * @return \SalarieBundle\Entity\Param\ParamSituationFamille
     */
    public function getIdSituationFamille()
    {
        return $this->idSituationFamille;
    }

    /**
     * Set idTypePiece
     *
     * @param \SalarieBundle\Entity\Param\ParamTypePieceIdentite $idTypePiece
     *
     * @return ViewSalarieInactif
     */
    public function setIdTypePiece(\SalarieBundle\Entity\Param\ParamTypePieceIdentite $idTypePiece = null)
    {
        $this->idTypePiece = $idTypePiece;

        return $this;
    }

    /**
     * Get idTypePiece
     *
     * @return \SalarieBundle\Entity\Param\ParamTypePieceIdentite
     */
    public function getIdTypePiece()
    {
        return $this->idTypePiece;
    }

    /**
     * Set idVille
     *
     * @param \SalarieBundle\Entity\Param\ParamAdresseVille $idVille
     *
     * @return ViewSalarieInactif
     */
    public function setIdVille(\SalarieBundle\Entity\Param\ParamAdresseVille $idVille = null)
    {
        $this->idVille = $idVille;

        return $this;
    }

    /**
     * Get idVille
     *
     * @return \SalarieBundle\Entity\Param\ParamAdresseVille
     */
    public function getIdVille()
    {
        return $this->idVille;
    }

    /**
     * Set idPaysNaissance
     *
     * @param \SalarieBundle\Entity\Param\ParamAdressePays $idPaysNaissance
     *
     * @return ViewSalarieInactif
     */
    public function setIdPaysNaissance(\SalarieBundle\Entity\Param\ParamAdressePays $idPaysNaissance = null)
    {
        $this->idPaysNaissance = $idPaysNaissance;

        return $this;
    }

    /**
     * Get idPaysNaissance
     *
     * @return \SalarieBundle\Entity\Param\ParamAdressePays
     */
    public function getIdPaysNaissance()
    {
        return $this->idPaysNaissance;
    }

    /**
     * Set idPaysNationalite
     *
     * @param \SalarieBundle\Entity\Param\ParamAdressePays $idPaysNationalite
     *
     * @return ViewSalarieInactif
     */
    public function setIdPaysNationalite(\SalarieBundle\Entity\Param\ParamAdressePays $idPaysNationalite = null)
    {
        $this->idPaysNationalite = $idPaysNationalite;

        return $this;
    }

    /**
     * Get idPaysNationalite
     *
     * @return \SalarieBundle\Entity\Param\ParamAdressePays
     */
    public function getIdPaysNationalite()
    {
        return $this->idPaysNationalite;
    }

    /**
     * Set idBddPlannet
     *
     * @param \SalarieBundle\Entity\Param\ParamBddPlannet $idBddPlannet
     *
     * @return ViewSalarieInactif
     */
    public function setIdBddPlannet(\SalarieBundle\Entity\Param\ParamBddPlannet $idBddPlannet = null)
    {
        $this->idBddPlannet = $idBddPlannet;

        return $this;
    }

    /**
     * Get idBddPlannet
     *
     * @return \SalarieBundle\Entity\Param\ParamBddPlannet
     */
    public function getIdBddPlannet()
    {
        return $this->idBddPlannet;
    }
}
