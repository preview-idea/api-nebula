<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/10/2018
 * Time: 14:34
 */

namespace SalarieBundle\Entity\Views\Salarie;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_salarie_derniercontrat")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Salarie\ViewDernierContratObjSalarieRepository")
 */
class ViewDernierContratObjSalarie
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_usage", type="string", nullable=false)
     */
    private $liNomUsage;

    /**
     * @var string
     *
     * @ORM\Column(name="li_ville", type="string", nullable=false)
     */
    private $liVille;

    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", nullable=false)
     */
    private $liAgence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_agence", type="integer", nullable=false)
     */
    private $idAgence;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debutcontrat", type="datetime", nullable=true)
     */
    private $dtDebutcontrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fincontrat", type="datetime", nullable=true)
     */
    private $dtFincontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_typecontrat", type="string", nullable=false)
     */
    private $liTypecontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_voie", type="string", nullable=false)
     */
    private $liNomVoie;

    /**
     * @var string
     *
     * @ORM\Column(name="li_adresse_2", type="string", nullable=false)
     */
    private $liAdresse2;

    /**
     * @var string
     *
     * @ORM\Column(name="li_mail", type="string", length=50, nullable=true)
     */
    private $liMail;

    /**
     * @var string
     *
     * @ORM\Column(name="li_codepostal", type="string", nullable=false)
     */
    private $liCodepostal;

    /**
     * @var string
     *
     * @ORM\Column(name="li_pays", type="string", nullable=false)
     */
    private $liPays;

    /**
     * @var string
     *
     * @ORM\Column(name="li_qualifcontrat", type="string", nullable=false)
     */
    private $liQualifcontrat;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_contrat", type="integer", nullable=false)
     */
    private $idContrat;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_qualif_valideur_batch", type="bit", nullable=false)
     */
    private $isQualifValideurBatch;

    /**
     * @return int
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param int $idMatricule
     * @return ViewDernierContratObjSalarie
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiNomUsage()
    {
        return $this->liNomUsage;
    }

    /**
     * @param string $liNomUsage
     * @return ViewDernierContratObjSalarie
     */
    public function setLiNomUsage($liNomUsage)
    {
        $this->liNomUsage = $liNomUsage;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiVille()
    {
        return $this->liVille;
    }

    /**
     * @param string $liVille
     * @return ViewDernierContratObjSalarie
     */
    public function setLiVille($liVille)
    {
        $this->liVille = $liVille;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * @param string $liAgence
     * @return ViewDernierContratObjSalarie
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * @param int $idAgence
     * @return ViewDernierContratObjSalarie
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtDebutcontrat()
    {
        return $this->dtDebutcontrat;
    }

    /**
     * @param \DateTime $dtDebutcontrat
     * @return ViewDernierContratObjSalarie
     */
    public function setDtDebutcontrat($dtDebutcontrat)
    {
        $this->dtDebutcontrat = $dtDebutcontrat;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtFincontrat()
    {
        return $this->dtFincontrat;
    }

    /**
     * @param \DateTime $dtFincontrat
     * @return ViewDernierContratObjSalarie
     */
    public function setDtFincontrat($dtFincontrat)
    {
        $this->dtFincontrat = $dtFincontrat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiTypecontrat()
    {
        return $this->liTypecontrat;
    }

    /**
     * @param string $liTypecontrat
     * @return ViewDernierContratObjSalarie
     */
    public function setLiTypecontrat($liTypecontrat)
    {
        $this->liTypecontrat = $liTypecontrat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiNomVoie()
    {
        return $this->liNomVoie;
    }

    /**
     * @param string $liNomVoie
     * @return ViewDernierContratObjSalarie
     */
    public function setLiNomVoie($liNomVoie)
    {
        $this->liNomVoie = $liNomVoie;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiAdresse2()
    {
        return $this->liAdresse2;
    }

    /**
     * @param string $liAdresse2
     * @return ViewDernierContratObjSalarie
     */
    public function setLiAdresse2($liAdresse2)
    {
        $this->liAdresse2 = $liAdresse2;
        return $this;
    }

    /**
     * Set liMail
     *
     * @param string $liMail
     *
     * @return ViewDernierContratObjSalarie
     */
    public function setLiMail($liMail)
    {
        $this->liMail = $liMail;

        return $this;
    }

    /**
     * Get liMail
     *
     * @return string
     */
    public function getLiMail()
    {
        return $this->liMail;
    }

    /**
     * @return string
     */
    public function getLiCodepostal()
    {
        return $this->liCodepostal;
    }

    /**
     * @param string $liCodepostal
     * @return ViewDernierContratObjSalarie
     */
    public function setLiCodepostal($liCodepostal)
    {
        $this->liCodepostal = $liCodepostal;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiPays()
    {
        return $this->liPays;
    }

    /**
     * @param string $liPays
     * @return ViewDernierContratObjSalarie
     */
    public function setLiPays($liPays)
    {
        $this->liPays = $liPays;
        return $this;
    }

    /**
     * @return bit
     */
    public function getisQualifValideurBatch()
    {
        return $this->isQualifValideurBatch;
    }

    /**
     * @param bit $isQualifValideurBatch
     * @return ViewDernierContratObjSalarie
     */
    public function setIsQualifValideurBatch($isQualifValideurBatch)
    {
        $this->isQualifValideurBatch = $isQualifValideurBatch;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiQualifcontrat()
    {
        return $this->liQualifcontrat;
    }

    /**
     * @param string $liQualifcontrat
     * @return ViewDernierContratObjSalarie
     */
    public function setLiQualifcontrat($liQualifcontrat)
    {
        $this->liQualifcontrat = $liQualifcontrat;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * @param int $idContrat
     * @return ViewDernierContratObjSalarie
     */
    public function setIdContrat($idContrat)
    {
        $this->idContrat = $idContrat;
        return $this;
    }

}
