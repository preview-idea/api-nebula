<?php
/**
 * Entity for RHPI Logs
 *
 * @author      Théo Gautreau
 * @Date:       20/03/2019
 */

namespace SalarieBundle\Entity\Views\Salarie\Log;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_rhpi_visu_log")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Salarie\Log\ViewLogRhpiObjSalarieRepository")
 */
class ViewLogRhpiObjSalarie
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_usage", type="string", nullable=false)
     */
    private $liNomUsage;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_contrat", type="integer", nullable=false)
     */
    private $idContrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", nullable=false)
     */
    private $liAgence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_agence", type="integer", nullable=false)
     */
    private $idAgence;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_export", type="string", nullable=false)
     */
    private $liTypeExport;

    /**
     * @var string
     *
     * @ORM\Column(name="li_message", type="string", nullable=false)
     */
    private $liMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="li_statut_salarie", type="string", nullable=false)
     */
    private $liStatutSalarie;

    /**
     * @var string
     *
     * @ORM\Column(name="li_statut_rib", type="string", nullable=false)
     */
    private $liStatutRib;

    /**
     * @var string
     *
     * @ORM\Column(name="li_statut_contrat", type="string", nullable=false)
     */
    private $liStatutContrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_statut_prime", type="string", nullable=false)
     */
    private $liStatutPrime;

    /**
     * @var string
     *
     * @ORM\Column(name="li_libreglement", type="string", nullable=false)
     */
    private $liLibreglement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_libcontrat", type="string", nullable=false)
     */
    private $liLibcontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_libprime", type="string", nullable=false)
     */
    private $liLibprime;

    /**
     * @return int
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param int $idMatricule
     * @return ViewLogRhpiObjSalarie
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiNomUsage()
    {
        return $this->liNomUsage;
    }

    /**
     * @param string $liNomUsage
     * @return ViewLogRhpiObjSalarie
     */
    public function setLiNomUsage($liNomUsage)
    {
        $this->liNomUsage = $liNomUsage;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * @param int $idContrat
     * @return ViewLogRhpiObjSalarie
     */
    public function setIdContrat($idContrat)
    {
        $this->idContrat = $idContrat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * @param string $liAgence
     * @return ViewLogRhpiObjSalarie
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * @param int $idAgence
     * @return ViewLogRhpiObjSalarie
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiTypeExport()
    {
        return $this->liTypeExport;
    }

    /**
     * @param string $liTypeExport
     * @return ViewLogRhpiObjSalarie
     */
    public function setLiTypeExport($liTypeExport)
    {
        $this->liTypeExport = $liTypeExport;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiMessage()
    {
        return $this->liMessage;
    }

    /**
     * @param string $liMessage
     * @return ViewLogRhpiObjSalarie
     */
    public function setLiMessage($liMessage)
    {
        $this->liMessage = $liMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiStatutSalarie()
    {
        return $this->liStatutSalarie;
    }

    /**
     * @param string $liStatutSalarie
     * @return ViewLogRhpiObjSalarie
     */
    public function setLiStatutSalarie($liStatutSalarie)
    {
        $this->liStatutSalarie = $liStatutSalarie;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiStatutRib()
    {
        return $this->liStatutRib;
    }

    /**
     * @param string $liStatutRib
     * @return ViewLogRhpiObjSalarie
     */
    public function setLiStatutRib($liStatutRib)
    {
        $this->liStatutRib = $liStatutRib;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiStatutContrat()
    {
        return $this->liStatutContrat;
    }

    /**
     * @param string $liStatutContrat
     * @return ViewLogRhpiObjSalarie
     */
    public function setLiStatutContrat($liStatutContrat)
    {
        $this->liStatutContrat = $liStatutContrat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiStatutPrime()
    {
        return $this->liStatutPrime;
    }

    /**
     * @param string $liStatutPrime
     * @return ViewLogRhpiObjSalarie
     */
    public function setLiStatutPrime($liStatutPrime)
    {
        $this->liStatutPrime = $liStatutPrime;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiLibreglement()
    {
        return $this->liLibreglement;
    }

    /**
     * @param string $liLibreglement
     * @return ViewLogRhpiObjSalarie
     */
    public function setLiLibreglement($liLibreglement)
    {
        $this->liLibreglement = $liLibreglement;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiLibcontrat()
    {
        return $this->liLibcontrat;
    }

    /**
     * @param string $liLibcontrat
     * @return ViewLogRhpiObjSalarie
     */
    public function setLiLibcontrat($liLibcontrat)
    {
        $this->liLibcontrat = $liLibcontrat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiLibprime()
    {
        return $this->liLibprime;
    }

    /**
     * @param string $liLibprime
     * @return ViewLogRhpiObjSalarie
     */
    public function setLiLibprime($liLibprime)
    {
        $this->liLibprime = $liLibprime;
        return $this;
    }

}