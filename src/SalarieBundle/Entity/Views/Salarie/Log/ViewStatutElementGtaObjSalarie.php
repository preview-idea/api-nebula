<?php
/**
 * Entity for GTA status Logs
 *
 * @author      Théo Gautreau
 * @Date:       01/04/2019
 */

namespace SalarieBundle\Entity\Views\Salarie\Log;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_gta_element_statut")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Salarie\Log\ViewStatutElementGtaObjSalarieRepository")
 */
class ViewStatutElementGtaObjSalarie
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_element", type="string", nullable=false)
     */
    private $liElement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_statut", type="string", nullable=false)
     */
    private $liStatut;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type", type="string", nullable=false)
     */
    private $liType;

    /**
     * @var string
     *
     * @ORM\Column(name="li_logs", type="string", nullable=false)
     */
    private $liLogs;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_flag", type="integer", nullable=false)
     */
    private $nbFlag;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_order", type="integer", nullable=false)
     */
    private $nbOrder;

    /**
     * @return int
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param int $idMatricule
     * @return ViewStatutElementGtaObjSalarie
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiElement()
    {
        return $this->liElement;
    }

    /**
     * @param string $liElement
     * @return ViewStatutElementGtaObjSalarie
     */
    public function setLiElement($liElement)
    {
        $this->liElement = $liElement;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiStatut()
    {
        return $this->liStatut;
    }

    /**
     * @param string $liStatut
     * @return ViewStatutElementGtaObjSalarie
     */
    public function setLiStatut($liStatut)
    {
        $this->liStatut = $liStatut;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiType()
    {
        return $this->liType;
    }

    /**
     * @param string $liType
     * @return ViewStatutElementGtaObjSalarie
     */
    public function setLiType($liType)
    {
        $this->liType = $liType;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiLogs()
    {
        return $this->liLogs;
    }

    /**
     * @param string $liLogs
     * @return ViewStatutElementGtaObjSalarie
     */
    public function setLiLogs($liLogs)
    {
        $this->liLogs = $liLogs;
        return $this;
    }

    /**
     * @return int
     */
    public function getNbFlag()
    {
        return $this->nbFlag;
    }

    /**
     * @param int $nbFlag
     * @return ViewStatutElementGtaObjSalarie
     */
    public function setNbFlag($nbFlag)
    {
        $this->nbFlag = $nbFlag;
        return $this;
    }

    /**
     * @return int
     */
    public function getNbOrder()
    {
        return $this->nbOrder;
    }

    /**
     * @param int $nbOrder
     * @return ViewStatutElementGtaObjSalarie
     */
    public function setNbOrder($nbOrder)
    {
        $this->nbOrder = $nbOrder;
        return $this;
    }

}