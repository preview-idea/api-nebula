<?php
/**
 * Entity for RHPI Logs
 *
 * @author      Théo Gautreau
 * @Date:       20/03/2019
 */

namespace SalarieBundle\Entity\Views\Salarie\Log;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_group_interface_log")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Salarie\Log\ViewGroupInterfaceLogObjSalarieRepository")
 */
class ViewGroupInterfaceLogObjSalarie
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_usage", type="string", nullable=false)
     */
    private $liNomUsage;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_contrat", type="integer", nullable=true)
     */
    private $idContrat;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_avenant", type="integer", nullable=true)
     */
    private $idAvenant;

    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", nullable=false)
     */
    private $liAgence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_agence", type="integer", nullable=false)
     */
    private $idAgence;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_export", type="string", nullable=false)
     */
    private $liTypeExport;

    /**
     * @var string
     *
     * @ORM\Column(name="li_message", type="string", nullable=false)
     */
    private $liMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="li_statut", type="string", nullable=false)
     */
    private $liStatut;

    /**
     * @var string
     *
     * @ORM\Column(name="li_lib_element", type="string", nullable=false)
     */
    private $liLibElement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_interface", type="string", nullable=false)
     */
    private $liInterface;

    /**
     * @var string
     *
     * @ORM\Column(name="li_element", type="string", nullable=false)
     */
    private $liElement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_situation", type="string", nullable=false)
     */
    private $liTypeSituation;

    /**
     * @return int
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param int $idMatricule
     * @return ViewGroupInterfaceLogObjSalarie
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiNomUsage()
    {
        return $this->liNomUsage;
    }

    /**
     * @param string $liNomUsage
     * @return ViewGroupInterfaceLogObjSalarie
     */
    public function setLiNomUsage($liNomUsage)
    {
        $this->liNomUsage = $liNomUsage;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * @param int $idContrat
     * @return ViewGroupInterfaceLogObjSalarie
     */
    public function setIdContrat($idContrat)
    {
        $this->idContrat = $idContrat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * @param string $liAgence
     * @return ViewGroupInterfaceLogObjSalarie
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * @param int $idAgence
     * @return ViewGroupInterfaceLogObjSalarie
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiTypeExport()
    {
        return $this->liTypeExport;
    }

    /**
     * @param string $liTypeExport
     * @return ViewGroupInterfaceLogObjSalarie
     */
    public function setLiTypeExport($liTypeExport)
    {
        $this->liTypeExport = $liTypeExport;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiMessage()
    {
        return $this->liMessage;
    }

    /**
     * @param string $liMessage
     * @return ViewGroupInterfaceLogObjSalarie
     */
    public function setLiMessage($liMessage)
    {
        $this->liMessage = $liMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiStatut()
    {
        return $this->liStatut;
    }

    /**
     * @param string $liStatut
     * @return ViewGroupInterfaceLogObjSalarie
     */
    public function setLiStatut($liStatut)
    {
        $this->liStatut = $liStatut;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiLibElement()
    {
        return $this->liLibElement;
    }

    /**
     * @param string $liLibElement
     * @return ViewGroupInterfaceLogObjSalarie
     */
    public function setLiLibElement($liLibElement)
    {
        $this->liLibElement = $liLibElement;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiInterface()
    {
        return $this->liInterface;
    }

    /**
     * @param string $liInterface
     * @return ViewGroupInterfaceLogObjSalarie
     */
    public function setLiInterface($liInterface)
    {
        $this->liInterface = $liInterface;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiElement()
    {
        return $this->liElement;
    }

    /**
     * @param string $liElement
     * @return ViewGroupInterfaceLogObjSalarie
     */
    public function setLiElement($liElement)
    {
        $this->liElement = $liElement;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAvenant()
    {
        return $this->idAvenant;
    }

    /**
     * @param int $idAvenant
     * @return ViewGroupInterfaceLogObjSalarie
     */
    public function setIdAvenant($idAvenant)
    {
        $this->idAvenant = $idAvenant;
        return $this;
    }


    /**
     * Set liTypeSituation
     *
     * @param string $liTypeSituation
     *
     * @return ViewGroupInterfaceLogObjSalarie
     */
    public function setLiTypeSituation($liTypeSituation)
    {
        $this->liTypeSituation = $liTypeSituation;

        return $this;
    }

    /**
     * Get liTypeSituation
     *
     * @return string
     */
    public function getLiTypeSituation()
    {
        return $this->liTypeSituation;
    }
}
