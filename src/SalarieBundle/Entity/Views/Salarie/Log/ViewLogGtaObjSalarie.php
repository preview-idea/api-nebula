<?php
/**
 * Entity for GTA Logs
 *
 * @author      Théo Gautreau
 * @Date:       20/03/2019
 */

namespace SalarieBundle\Entity\Views\Salarie\Log;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="v_comete_visu_log")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Views\Salarie\Log\ViewLogGtaObjSalarieRepository")
 */
class ViewLogGtaObjSalarie
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_matricule", type="integer", nullable=false)
     */
    private $idMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_usage", type="string", nullable=false)
     */
    private $liNomUsage;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_contrat", type="integer", nullable=true)
     */
    private $idContrat;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_avenant", type="integer", nullable=true)
     */
    private $idAvenant;

    /**
     * @var string
     *
     * @ORM\Column(name="li_agence", type="string", nullable=false)
     */
    private $liAgence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_agence", type="integer", nullable=false)
     */
    private $idAgence;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_export", type="string", nullable=false)
     */
    private $liTypeExport;

    /**
     * @var string
     *
     * @ORM\Column(name="li_message", type="string", nullable=false)
     */
    private $liMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="li_statut_salarie", type="string", nullable=false)
     */
    private $liStatutSalarie;

    /**
     * @var string
     *
     * @ORM\Column(name="li_statut_contrat", type="string", nullable=false)
     */
    private $liStatutContrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_statut_avenant", type="string", nullable=false)
     */
    private $liStatutAvenant;

    /**
     * @var string
     *
     * @ORM\Column(name="li_libcontrat", type="string", nullable=false)
     */
    private $liLibcontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_libavenant", type="string", nullable=false)
     */
    private $liLibavenant;

    /**
     * @return int
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param int $idMatricule
     * @return ViewLogGtaObjSalarie
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiNomUsage()
    {
        return $this->liNomUsage;
    }

    /**
     * @param string $liNomUsage
     * @return ViewLogGtaObjSalarie
     */
    public function setLiNomUsage($liNomUsage)
    {
        $this->liNomUsage = $liNomUsage;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * @param int $idContrat
     * @return ViewLogGtaObjSalarie
     */
    public function setIdContrat($idContrat)
    {
        $this->idContrat = $idContrat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiAgence()
    {
        return $this->liAgence;
    }

    /**
     * @param string $liAgence
     * @return ViewLogGtaObjSalarie
     */
    public function setLiAgence($liAgence)
    {
        $this->liAgence = $liAgence;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * @param int $idAgence
     * @return ViewLogGtaObjSalarie
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiTypeExport()
    {
        return $this->liTypeExport;
    }

    /**
     * @param string $liTypeExport
     * @return ViewLogGtaObjSalarie
     */
    public function setLiTypeExport($liTypeExport)
    {
        $this->liTypeExport = $liTypeExport;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiMessage()
    {
        return $this->liMessage;
    }

    /**
     * @param string $liMessage
     * @return ViewLogGtaObjSalarie
     */
    public function setLiMessage($liMessage)
    {
        $this->liMessage = $liMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiStatutSalarie()
    {
        return $this->liStatutSalarie;
    }

    /**
     * @param string $liStatutSalarie
     * @return ViewLogGtaObjSalarie
     */
    public function setLiStatutSalarie($liStatutSalarie)
    {
        $this->liStatutSalarie = $liStatutSalarie;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiStatutContrat()
    {
        return $this->liStatutContrat;
    }

    /**
     * @param string $liStatutContrat
     * @return ViewLogGtaObjSalarie
     */
    public function setLiStatutContrat($liStatutContrat)
    {
        $this->liStatutContrat = $liStatutContrat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiStatutAvenant()
    {
        return $this->liStatutAvenant;
    }

    /**
     * @param string $liStatutAvenant
     * @return ViewLogGtaObjSalarie
     */
    public function setLiStatutAvenant($liStatutAvenant)
    {
        $this->liStatutAvenant = $liStatutAvenant;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiLibcontrat()
    {
        return $this->liLibcontrat;
    }

    /**
     * @param string $liLibcontrat
     * @return ViewLogGtaObjSalarie
     */
    public function setLiLibcontrat($liLibcontrat)
    {
        $this->liLibcontrat = $liLibcontrat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiLibavenant()
    {
        return $this->liLibavenant;
    }

    /**
     * @param string $liLibavenant
     * @return ViewLogGtaObjSalarie
     */
    public function setLiLibavenant($liLibavenant)
    {
        $this->liLibavenant = $liLibavenant;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAvenant()
    {
        return $this->idAvenant;
    }

    /**
     * @param int $idAvenant
     * @return ViewLogGtaObjSalarie
     */
    public function setIdAvenant($idAvenant)
    {
        $this->idAvenant = $idAvenant;
        return $this;
    }

}