<?php

namespace SalarieBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ObjSalarieProtection
 *
 * @ORM\Table(name="obj_salarie_protection", indexes={@ORM\Index(name="IDX_EBFFA77B38645A38", columns={"id_mandat"}), @ORM\Index(name="IDX_EBFFA77B53B78B7C", columns={"id_org_syndicale"}), @ORM\Index(name="IDX_EBFFA77B189FBDFD", columns={"id_perimetre_electoral"}), @ORM\Index(name="IDX_EBFFA77B43B83A18", columns={"id_type_protection"}), @ORM\Index(name="IDX_EBFFA77BCBC3B464", columns={"id_matricule_maj"}), @ORM\Index(name="IDX_EBFFA77B928760BB", columns={"id_matricule"})})
 * @ORM\Entity
 */
class ObjSalarieProtection
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_protection", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="obj_salarie_protection_id_protection_seq", allocationSize=1, initialValue=1)
     */
    private $idProtection;

    /**
     * @var string
     *
     * @ORM\Column(name="li_commentaire", type="string", length=100, nullable=true)
     */
    private $liCommentaire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_debut", type="date", nullable=false)
     */
    private $dtDateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_fin_theorique", type="date", nullable=false)
     */
    private $dtDateFinTheorique;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_date_fin_reelle", type="date", nullable=true)
     */
    private $dtDateFinReelle;

    /**
     * @var \SalarieBundle\Entity\Param\ParamOrgSyndicale
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamOrgSyndicale")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_org_syndicale", referencedColumnName="id_org_syndicale")
     * })
     */
    private $idOrgSyndicale;

    /**
     * @var \SalarieBundle\Entity\Param\ParamPerimetreElectoral
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamPerimetreElectoral")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_perimetre_electoral", referencedColumnName="id_perimetre_electoral")
     * })
     */
    private $idPerimetreElectoral;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeProtection
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeProtection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_protection", referencedColumnName="id_type_protection")
     * })
     */
    private $idTypeProtection;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\ObjSalarie
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\ObjSalarie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule", referencedColumnName="id_matricule")
     * })
     */
    private $idMatricule;

    /**
     * One Mandat has One protection.
     * @ORM\OneToMany(targetEntity="SalarieBundle\Entity\ObjSalarieMandat", mappedBy="idProtection", cascade={"persist", "remove"})
     */
    private $mandats;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mandats = new ArrayCollection();
    }

    /**
     * Add mandat
     *
     * @param \SalarieBundle\Entity\ObjSalarieMandat $mandat
     *
     * @return ObjSalarieProtection
     */
    public function addMandat(\SalarieBundle\Entity\ObjSalarieMandat $mandat)
    {
        $this->mandats[] = $mandat;
        $mandat->setIdProtection($this);
        $mandat->setIdMatriculeMaj($this->getIdMatriculeMaj());

        return $this;
    }

    /**
     * Remove mandats
     *
     * @param \SalarieBundle\Entity\ObjSalarieMandat $mandat
     */
    public function removeMandat(\SalarieBundle\Entity\ObjSalarieMandat $mandat)
    {
        $this->mandats->removeElement($mandat);
    }

    /**
     * Get mandats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMandats()
    {
        return $this->mandats;
    }

    /**
     * Get idProtection
     *
     * @return integer
     */
    public function getIdProtection()
    {
        return $this->idProtection;
    }

    /**
     * Set liCommentaire
     *
     * @param string $liCommentaire
     *
     * @return ObjSalarieProtection
     */
    public function setLiCommentaire($liCommentaire)
    {
        $this->liCommentaire = $liCommentaire;

        return $this;
    }

    /**
     * Get liCommentaire
     *
     * @return string
     */
    public function getLiCommentaire()
    {
        return $this->liCommentaire;
    }

    /**
     * Set dtDateDebut
     *
     * @param \DateTime $dtDateDebut
     *
     * @return ObjSalarieProtection
     */
    public function setDtDateDebut($dtDateDebut)
    {
        $this->dtDateDebut = $dtDateDebut;

        return $this;
    }

    /**
     * Get dtDateDebut
     *
     * @return \DateTime
     */
    public function getDtDateDebut()
    {
        return $this->dtDateDebut;
    }

    /**
     * Set dtDateFinTheorique
     *
     * @param \DateTime $dtDateFinTheorique
     *
     * @return ObjSalarieProtection
     */
    public function setDtDateFinTheorique($dtDateFinTheorique)
    {
        $this->dtDateFinTheorique = $dtDateFinTheorique;

        return $this;
    }

    /**
     * Get dtDateFinTheorique
     *
     * @return \DateTime
     */
    public function getDtDateFinTheorique()
    {
        return $this->dtDateFinTheorique;
    }

    /**
     * Set dtDateFinReelle
     *
     * @param \DateTime $dtDateFinReelle
     *
     * @return ObjSalarieProtection
     */
    public function setDtDateFinReelle($dtDateFinReelle)
    {
        $this->dtDateFinReelle = $dtDateFinReelle;

        return $this;
    }

    /**
     * Get dtDateFinReelle
     *
     * @return \DateTime
     */
    public function getDtDateFinReelle()
    {
        return $this->dtDateFinReelle;
    }

    /**
     * Set idOrgSyndicale
     *
     * @param \SalarieBundle\Entity\Param\ParamOrgSyndicale $idOrgSyndicale
     *
     * @return ObjSalarieProtection
     */
    public function setIdOrgSyndicale(\SalarieBundle\Entity\Param\ParamOrgSyndicale $idOrgSyndicale = null)
    {
        $this->idOrgSyndicale = $idOrgSyndicale;

        return $this;
    }

    /**
     * Get idOrgSyndicale
     *
     * @return \SalarieBundle\Entity\Param\ParamOrgSyndicale
     */
    public function getIdOrgSyndicale()
    {
        return $this->idOrgSyndicale;
    }

    /**
     * Set idPerimetreElectoral
     *
     * @param \SalarieBundle\Entity\Param\ParamPerimetreElectoral $idPerimetreElectoral
     *
     * @return ObjSalarieProtection
     */
    public function setIdPerimetreElectoral(\SalarieBundle\Entity\Param\ParamPerimetreElectoral $idPerimetreElectoral = null)
    {
        $this->idPerimetreElectoral = $idPerimetreElectoral;

        return $this;
    }

    /**
     * Get idPerimetreElectoral
     *
     * @return \SalarieBundle\Entity\Param\ParamPerimetreElectoral
     */
    public function getIdPerimetreElectoral()
    {
        return $this->idPerimetreElectoral;
    }

    /**
     * Set idTypeProtection
     *
     * @param \SalarieBundle\Entity\Param\ParamTypeProtection $idTypeProtection
     *
     * @return ObjSalarieProtection
     */
    public function setIdTypeProtection(\SalarieBundle\Entity\Param\ParamTypeProtection $idTypeProtection = null)
    {
        $this->idTypeProtection = $idTypeProtection;

        return $this;
    }

    /**
     * Get idTypeProtection
     *
     * @return \SalarieBundle\Entity\Param\ParamTypeProtection
     */
    public function getIdTypeProtection()
    {
        return $this->idTypeProtection;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ObjSalarieProtection
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idMatricule
     *
     * @param \SalarieBundle\Entity\ObjSalarie $idMatricule
     *
     * @return ObjSalarieProtection
     */
    public function setIdMatricule(\SalarieBundle\Entity\ObjSalarie $idMatricule = null)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return \SalarieBundle\Entity\ObjSalarie
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }
}
