<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratClassification
 *
 * @ORM\Table(name="contrat_classification", indexes={@ORM\Index(name="IDX_268F4D4C2B4AB12A", columns={"id_qualifcontrat"}), @ORM\Index(name="IDX_268F4D4C379FB7D0", columns={"id_coeffcontrat"}), @ORM\Index(name="IDX_268F4D4CBEA930E3", columns={"id_contrat"}), @ORM\Index(name="IDX_268F4D4CCBC3B464", columns={"id_matricule_maj"}), @ORM\Index(name="IDX_268F4D4CFCAB81F8", columns={"id_categorieemploye"})})
 * @ORM\Entity
 */
class ContratClassification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_classification", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_classification_id_ligne_classification_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneClassification;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var \SalarieBundle\Entity\Param\ParamQualifcontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamQualifcontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_qualifcontrat", referencedColumnName="id_qualifcontrat")
     * })
     */
    private $idQualifcontrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCoeffcontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCoeffcontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_coeffcontrat", referencedColumnName="id_coeffcontrat")
     * })
     */
    private $idCoeffcontrat;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCategorieemploye
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCategorieemploye")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_categorieemploye", referencedColumnName="id_categorieemploye")
     * })
     */
    private $idCategorieemploye;

    /**
     * Get idLigneClassification
     *
     * @return integer
     */
    public function getIdLigneClassification()
    {
        return $this->idLigneClassification;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratClassification
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return ContratClassification
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set idQualifcontrat
     *
     * @param \SalarieBundle\Entity\Param\ParamQualifcontrat $idQualifcontrat
     *
     * @return ContratClassification
     */
    public function setIdQualifcontrat(\SalarieBundle\Entity\Param\ParamQualifcontrat $idQualifcontrat = null)
    {
        $this->idQualifcontrat = $idQualifcontrat;

        return $this;
    }

    /**
     * Get idQualifcontrat
     *
     * @return \SalarieBundle\Entity\Param\ParamQualifcontrat
     */
    public function getIdQualifcontrat()
    {
        return $this->idQualifcontrat;
    }

    /**
     * Set idCoeffcontrat
     *
     * @param \SalarieBundle\Entity\Param\ParamCoeffcontrat $idCoeffcontrat
     *
     * @return ContratClassification
     */
    public function setIdCoeffcontrat(\SalarieBundle\Entity\Param\ParamCoeffcontrat $idCoeffcontrat = null)
    {
        $this->idCoeffcontrat = $idCoeffcontrat;

        return $this;
    }

    /**
     * Get idCoeffcontrat
     *
     * @return \SalarieBundle\Entity\Param\ParamCoeffcontrat
     */
    public function getIdCoeffcontrat()
    {
        return $this->idCoeffcontrat;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratClassification
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ContratClassification
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idCategorieemploye
     *
     * @param \SalarieBundle\Entity\Param\ParamCategorieemploye $idCategorieemploye
     *
     * @return ContratClassification
     */
    public function setIdCategorieemploye(\SalarieBundle\Entity\Param\ParamCategorieemploye $idCategorieemploye = null)
    {
        $this->idCategorieemploye = $idCategorieemploye;

        return $this;
    }

    /**
     * Get idCategorieemploye
     *
     * @return \SalarieBundle\Entity\Param\ParamCategorieemploye
     */
    public function getIdCategorieemploye()
    {
        return $this->idCategorieemploye;
    }
}
