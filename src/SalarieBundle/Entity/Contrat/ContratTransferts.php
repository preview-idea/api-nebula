<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratTransferts
 *
 * @ORM\Table(name="contrat_transferts", uniqueConstraints={@ORM\UniqueConstraint(name="contrat_transferts_id_matricule_id_contrat_dt_application_t_key", columns={"id_matricule", "id_contrat", "dt_application_transfert"})}, indexes={@ORM\Index(name="IDX_FA951EC897EE50AD", columns={"id_type_transfert"}), @ORM\Index(name="IDX_FA951EC8928760BB", columns={"id_matricule"}), @ORM\Index(name="IDX_FA951EC8BEA930E3", columns={"id_contrat"}), @ORM\Index(name="IDX_FA951EC842F62F44", columns={"id_agence"}), @ORM\Index(name="IDX_FA951EC8CBC3B464", columns={"id_matricule_maj"})})
 * @ORM\Entity
 */
class ContratTransferts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_transfert", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_transferts_id_ligne_transfert_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneTransfert;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_transfert", type="integer", nullable=false)
     */
    private $idTransfert;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_application_transfert", type="datetime", nullable=false)
     */
    private $dtApplicationTransfert;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_suppression_objet", type="datetime", nullable=true)
     */
    private $dtSuppressionObjet;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeTransfert
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeTransfert")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_transfert", referencedColumnName="id_type_transfert")
     * })
     */
    private $idTypeTransfert;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule", referencedColumnName="id_matricule")
     * })
     */
    private $idMatricule;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamAgence
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamAgence")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_agence", referencedColumnName="id_agence")
     * })
     */
    private $idAgence;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * Get idLigneTransfert
     *
     * @return integer
     */
    public function getIdLigneTransfert()
    {
        return $this->idLigneTransfert;
    }

    /**
     * Set idTransfert
     *
     * @param integer $idTransfert
     *
     * @return ContratTransferts
     */
    public function setIdTransfert($idTransfert)
    {
        $this->idTransfert = $idTransfert;

        return $this;
    }

    /**
     * Get idTransfert
     *
     * @return integer
     */
    public function getIdTransfert()
    {
        return $this->idTransfert;
    }

    /**
     * Set dtApplicationTransfert
     *
     * @param \DateTime $dtApplicationTransfert
     *
     * @return ContratTransferts
     */
    public function setDtApplicationTransfert($dtApplicationTransfert)
    {
        $this->dtApplicationTransfert = $dtApplicationTransfert;

        return $this;
    }

    /**
     * Get dtApplicationTransfert
     *
     * @return \DateTime
     */
    public function getDtApplicationTransfert()
    {
        return $this->dtApplicationTransfert;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return ContratTransferts
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratTransferts
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtSuppressionObjet
     *
     * @param \DateTime $dtSuppressionObjet
     *
     * @return ContratTransferts
     */
    public function setDtSuppressionObjet($dtSuppressionObjet)
    {
        $this->dtSuppressionObjet = $dtSuppressionObjet;

        return $this;
    }

    /**
     * Get dtSuppressionObjet
     *
     * @return \DateTime
     */
    public function getDtSuppressionObjet()
    {
        return $this->dtSuppressionObjet;
    }

    /**
     * Set idTypeTransfert
     *
     * @param \SalarieBundle\Entity\Param\ParamTypeTransfert $idTypeTransfert
     *
     * @return ContratTransferts
     */
    public function setIdTypeTransfert(\SalarieBundle\Entity\Param\ParamTypeTransfert $idTypeTransfert = null)
    {
        $this->idTypeTransfert = $idTypeTransfert;

        return $this;
    }

    /**
     * Get idTypeTransfert
     *
     * @return \SalarieBundle\Entity\Param\ParamTypeTransfert
     */
    public function getIdTypeTransfert()
    {
        return $this->idTypeTransfert;
    }

    /**
     * Set idMatricule
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatricule
     *
     * @return ContratTransferts
     */
    public function setIdMatricule(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatricule = null)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratTransferts
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set idAgence
     *
     * @param \SalarieBundle\Entity\Param\ParamAgence $idAgence
     *
     * @return ContratTransferts
     */
    public function setIdAgence(\SalarieBundle\Entity\Param\ParamAgence $idAgence = null)
    {
        $this->idAgence = $idAgence;

        return $this;
    }

    /**
     * Get idAgence
     *
     * @return \SalarieBundle\Entity\Param\ParamAgence
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ContratTransferts
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }
}
