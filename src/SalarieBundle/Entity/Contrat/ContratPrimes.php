<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratPrimes
 *
 * @ORM\Table(name="contrat_primes", indexes={@ORM\Index(name="IDX_9CA865BF3F9D78A0", columns={"id_primecontrat"}), @ORM\Index(name="IDX_9CA865BFBEA930E3", columns={"id_contrat"}), @ORM\Index(name="IDX_9CA865BFCBC3B464", columns={"id_matricule_maj"}), @ORM\Index(name="IDX_9CA865BF15EF522E", columns={"id_type_tauxprime"})})
 * @ORM\Entity
 */
class ContratPrimes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_primes", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_primes_id_ligne_primes_seq", allocationSize=1, initialValue=1)
     */
    private $idLignePrimes;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_montant_prime", type="decimal", precision=6, scale=4, nullable=false)
     */
    private $nbMontantPrime;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_avecprime", type="bit", nullable=true)
     */
    private $isAvecprime;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_primes", type="integer", nullable=false)
     */
    private $idPrimes;

    /**
     * @var \SalarieBundle\Entity\Param\ParamPrimescontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamPrimescontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_primecontrat", referencedColumnName="id_primecontrat")
     * })
     */
    private $idPrimecontrat;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeTauxprime
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeTauxprime")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_tauxprime", referencedColumnName="id_type_tauxprime")
     * })
     */
    private $idTypeTauxprime;

    /**
     * Get idLignePrimes
     *
     * @return integer
     */
    public function getIdLignePrimes()
    {
        return $this->idLignePrimes;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratPrimes
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return ContratPrimes
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set nbMontantPrime
     *
     * @param string $nbMontantPrime
     *
     * @return ContratPrimes
     */
    public function setNbMontantPrime($nbMontantPrime)
    {
        $this->nbMontantPrime = $nbMontantPrime;

        return $this;
    }

    /**
     * Get nbMontantPrime
     *
     * @return string
     */
    public function getNbMontantPrime()
    {
        return $this->nbMontantPrime;
    }

    /**
     * Set isAvecprime
     *
     * @param bit $isAvecprime
     *
     * @return ContratPrimes
     */
    public function setIsAvecprime($isAvecprime)
    {
        $this->isAvecprime = $isAvecprime;

        return $this;
    }

    /**
     * Get isAvecprime
     *
     * @return bit
     */
    public function getIsAvecprime()
    {
        return $this->isAvecprime;
    }

    /**
     * Set idPrimes
     *
     * @param integer $idPrimes
     *
     * @return ContratPrimes
     */
    public function setIdPrimes($idPrimes)
    {
        $this->idPrimes = $idPrimes;

        return $this;
    }

    /**
     * Get idPrimes
     *
     * @return integer
     */
    public function getIdPrimes()
    {
        return $this->idPrimes;
    }

    /**
     * Set idPrimecontrat
     *
     * @param \SalarieBundle\Entity\Param\ParamPrimescontrat $idPrimecontrat
     *
     * @return ContratPrimes
     */
    public function setIdPrimecontrat(\SalarieBundle\Entity\Param\ParamPrimescontrat $idPrimecontrat = null)
    {
        $this->idPrimecontrat = $idPrimecontrat;

        return $this;
    }

    /**
     * Get idPrimecontrat
     *
     * @return \SalarieBundle\Entity\Param\ParamPrimescontrat
     */
    public function getIdPrimecontrat()
    {
        return $this->idPrimecontrat;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratPrimes
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ContratPrimes
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idTypeTauxprime
     *
     * @param \SalarieBundle\Entity\Param\ParamTypeTauxprime $idTypeTauxprime
     *
     * @return ContratPrimes
     */
    public function setIdTypeTauxprime(\SalarieBundle\Entity\Param\ParamTypeTauxprime $idTypeTauxprime = null)
    {
        $this->idTypeTauxprime = $idTypeTauxprime;

        return $this;
    }

    /**
     * Get idTypeTauxprime
     *
     * @return \SalarieBundle\Entity\Param\ParamTypeTauxprime
     */
    public function getIdTypeTauxprime()
    {
        return $this->idTypeTauxprime;
    }
}
