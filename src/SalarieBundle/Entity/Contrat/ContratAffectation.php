<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratAffectation
 *
 * @ORM\Table(name="contrat_affectation", indexes={@ORM\Index(name="IDX_2E00EFF6911DCB20", columns={"id_positionnementposte"}), @ORM\Index(name="IDX_2E00EFF6C7F894CD", columns={"id_societe"}), @ORM\Index(name="IDX_2E00EFF642F62F44", columns={"id_agence"}), @ORM\Index(name="IDX_2E00EFF64B7CAAD2", columns={"id_conventioncollective"}), @ORM\Index(name="IDX_2E00EFF6CBC3B464", columns={"id_matricule_maj"}), @ORM\Index(name="IDX_2E00EFF6BEA930E3", columns={"id_contrat"}), @ORM\Index(name="IDX_2E00EFF69ED58849", columns={"id_etablissement"})})
 * @ORM\Entity
 */
class ContratAffectation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_affectation", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_affectation_id_ligne_affectation_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneAffectation;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_annexe8", type="bit", nullable=false)
     */
    private $isAnnexe8;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_manager", type="bit", nullable=false)
     */
    private $isManager;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_affectation", type="datetime", nullable=true)
     */
    private $dtDebutAffectation;

    /**
     * @var string
     *
     * @ORM\Column(name="li_bu", type="string", length=3, nullable=true)
     */
    private $liBu;

    /**
     * @var string
     *
     * @ORM\Column(name="li_activite", type="string", length=3, nullable=true)
     */
    private $liActivite;

    /**
     * @var string
     *
     * @ORM\Column(name="li_chantier", type="string", length=9, nullable=true)
     */
    private $liChantier;

    /**
     * @var \SalarieBundle\Entity\Param\ParamPositionnementposte
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamPositionnementposte")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_positionnementposte", referencedColumnName="id_positionnementposte")
     * })
     */
    private $idPositionnementposte;

    /**
     * @var \SalarieBundle\Entity\Param\ParamSociete
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamSociete")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_societe", referencedColumnName="id_societe")
     * })
     */
    private $idSociete;

    /**
     * @var \SalarieBundle\Entity\Param\ParamAgence
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamAgence")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_agence", referencedColumnName="id_agence")
     * })
     */
    private $idAgence;

    /**
     * @var \SalarieBundle\Entity\Param\ParamConventioncollective
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamConventioncollective")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_conventioncollective", referencedColumnName="id_conventioncollective")
     * })
     */
    private $idConventioncollective;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamEtablissement
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamEtablissement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_etablissement", referencedColumnName="id_etablissement")
     * })
     */
    private $idEtablissement;



    /**
     * Get idLigneAffectation
     *
     * @return integer
     */
    public function getIdLigneAffectation()
    {
        return $this->idLigneAffectation;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratAffectation
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return ContratAffectation
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set isAnnexe8
     *
     * @param bit $isAnnexe8
     *
     * @return ContratAffectation
     */
    public function setIsAnnexe8($isAnnexe8)
    {
        $this->isAnnexe8 = $isAnnexe8;

        return $this;
    }

    /**
     * Get isAnnexe8
     *
     * @return bit
     */
    public function getIsAnnexe8()
    {
        return $this->isAnnexe8;
    }

    /**
     * Set isManager
     *
     * @param bit $isManager
     *
     * @return ContratAffectation
     */
    public function setIsManager($isManager)
    {
        $this->isManager = $isManager;

        return $this;
    }

    /**
     * Get isManager
     *
     * @return bit
     */
    public function getIsManager()
    {
        return $this->isManager;
    }

    /**
     * Set dtDebutAffectation
     *
     * @param \DateTime $dtDebutAffectation
     *
     * @return ContratAffectation
     */
    public function setDtDebutAffectation($dtDebutAffectation)
    {
        $this->dtDebutAffectation = $dtDebutAffectation;

        return $this;
    }

    /**
     * Get dtDebutAffectation
     *
     * @return \DateTime
     */
    public function getDtDebutAffectation()
    {
        return $this->dtDebutAffectation;
    }

    /**
     * Set liBu
     *
     * @param string $liBu
     *
     * @return ContratAffectation
     */
    public function setLiBu($liBu)
    {
        $this->liBu = $liBu;

        return $this;
    }

    /**
     * Get liBu
     *
     * @return string
     */
    public function getLiBu()
    {
        return $this->liBu;
    }

    /**
     * Set liActivite
     *
     * @param string $liActivite
     *
     * @return ContratAffectation
     */
    public function setLiActivite($liActivite)
    {
        $this->liActivite = $liActivite;

        return $this;
    }

    /**
     * Get liActivite
     *
     * @return string
     */
    public function getLiActivite()
    {
        return $this->liActivite;
    }

    /**
     * Set liChantier
     *
     * @param string $liChantier
     *
     * @return ContratAffectation
     */
    public function setLiChantier($liChantier)
    {
        $this->liChantier = $liChantier;

        return $this;
    }

    /**
     * Get liChantier
     *
     * @return string
     */
    public function getLiChantier()
    {
        return $this->liChantier;
    }

    /**
     * Set idPositionnementposte
     *
     * @param \SalarieBundle\Entity\Param\ParamPositionnementposte $idPositionnementposte
     *
     * @return ContratAffectation
     */
    public function setIdPositionnementposte(\SalarieBundle\Entity\Param\ParamPositionnementposte $idPositionnementposte = null)
    {
        $this->idPositionnementposte = $idPositionnementposte;

        return $this;
    }

    /**
     * Get idPositionnementposte
     *
     * @return \SalarieBundle\Entity\Param\ParamPositionnementposte
     */
    public function getIdPositionnementposte()
    {
        return $this->idPositionnementposte;
    }

    /**
     * Set idSociete
     *
     * @param \SalarieBundle\Entity\Param\ParamSociete $idSociete
     *
     * @return ContratAffectation
     */
    public function setIdSociete(\SalarieBundle\Entity\Param\ParamSociete $idSociete = null)
    {
        $this->idSociete = $idSociete;

        return $this;
    }

    /**
     * Get idSociete
     *
     * @return \SalarieBundle\Entity\Param\ParamSociete
     */
    public function getIdSociete()
    {
        return $this->idSociete;
    }

    /**
     * Set idAgence
     *
     * @param \SalarieBundle\Entity\Param\ParamAgence $idAgence
     *
     * @return ContratAffectation
     */
    public function setIdAgence(\SalarieBundle\Entity\Param\ParamAgence $idAgence = null)
    {
        $this->idAgence = $idAgence;

        return $this;
    }

    /**
     * Get idAgence
     *
     * @return \SalarieBundle\Entity\Param\ParamAgence
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * Set idConventioncollective
     *
     * @param \SalarieBundle\Entity\Param\ParamConventioncollective $idConventioncollective
     *
     * @return ContratAffectation
     */
    public function setIdConventioncollective(\SalarieBundle\Entity\Param\ParamConventioncollective $idConventioncollective = null)
    {
        $this->idConventioncollective = $idConventioncollective;

        return $this;
    }

    /**
     * Get idConventioncollective
     *
     * @return \SalarieBundle\Entity\Param\ParamConventioncollective
     */
    public function getIdConventioncollective()
    {
        return $this->idConventioncollective;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ContratAffectation
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratAffectation
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set idEtablissement
     *
     * @param \SalarieBundle\Entity\Param\ParamEtablissement $idEtablissement
     *
     * @return ContratAffectation
     */
    public function setIdEtablissement(\SalarieBundle\Entity\Param\ParamEtablissement $idEtablissement = null)
    {
        $this->idEtablissement = $idEtablissement;

        return $this;
    }

    /**
     * Get idEtablissement
     *
     * @return \SalarieBundle\Entity\Param\ParamEtablissement
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }
}
