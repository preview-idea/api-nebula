<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratMotifdates
 *
 * @ORM\Table(name="contrat_motifdates", indexes={@ORM\Index(name="idx_image_contrat_motifdates", columns={"id_contrat", "dt_maj"}), @ORM\Index(name="IDX_7CB018B0CBC3B464", columns={"id_matricule_maj"}), @ORM\Index(name="IDX_7CB018B039236703", columns={"id_motiffincontrat"}), @ORM\Index(name="IDX_7CB018B013230DA2", columns={"id_naturecontrat"}), @ORM\Index(name="IDX_7CB018B0A2C2D8DF", columns={"id_typecontrat"}), @ORM\Index(name="IDX_7CB018B0BEA930E3", columns={"id_contrat"})})
 * @ORM\Entity
 */
class ContratMotifdates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_motifdates", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_motifdates_id_ligne_motifdates_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneMotifdates;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debutcontrat", type="datetime", nullable=false)
     */
    private $dtDebutcontrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fincontrat_prevue", type="datetime", nullable=true)
     */
    private $dtFincontratPrevue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fincontrat", type="datetime", nullable=true)
     */
    private $dtFincontrat;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_salarie_sorti", type="bit", nullable=false)
     */
    private $isSalarieSorti;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\Param\ParamMotiffincontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamMotiffincontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_motiffincontrat", referencedColumnName="id_motiffincontrat")
     * })
     */
    private $idMotiffincontrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamNaturecontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamNaturecontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_naturecontrat", referencedColumnName="id_naturecontrat")
     * })
     */
    private $idNaturecontrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypecontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypecontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_typecontrat", referencedColumnName="id_typecontrat")
     * })
     */
    private $idTypecontrat;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * Get idLigneMotifdates
     *
     * @return integer
     */
    public function getIdLigneMotifdates()
    {
        return $this->idLigneMotifdates;
    }

    /**
     * Set dtDebutcontrat
     *
     * @param \DateTime $dtDebutcontrat
     *
     * @return ContratMotifdates
     */
    public function setDtDebutcontrat($dtDebutcontrat)
    {
        $this->dtDebutcontrat = $dtDebutcontrat;

        return $this;
    }

    /**
     * Get dtDebutcontrat
     *
     * @return \DateTime
     */
    public function getDtDebutcontrat()
    {
        return $this->dtDebutcontrat;
    }

    /**
     * Set dtFincontratPrevue
     *
     * @param \DateTime $dtFincontratPrevue
     *
     * @return ContratMotifdates
     */
    public function setDtFincontratPrevue($dtFincontratPrevue)
    {
        $this->dtFincontratPrevue = $dtFincontratPrevue;

        return $this;
    }

    /**
     * Get dtFincontratPrevue
     *
     * @return \DateTime
     */
    public function getDtFincontratPrevue()
    {
        return $this->dtFincontratPrevue;
    }

    /**
     * Set dtFincontrat
     *
     * @param \DateTime $dtFincontrat
     *
     * @return ContratMotifdates
     */
    public function setDtFincontrat($dtFincontrat)
    {
        $this->dtFincontrat = $dtFincontrat;

        return $this;
    }

    /**
     * Get dtFincontrat
     *
     * @return \DateTime
     */
    public function getDtFincontrat()
    {
        return $this->dtFincontrat;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratMotifdates
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return ContratMotifdates
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set isSalarieSorti
     *
     * @param bit $isSalarieSorti
     *
     * @return ContratMotifdates
     */
    public function setIsSalarieSorti($isSalarieSorti)
    {
        $this->isSalarieSorti = $isSalarieSorti;

        return $this;
    }

    /**
     * Get isSalarieSorti
     *
     * @return bit
     */
    public function getIsSalarieSorti()
    {
        return $this->isSalarieSorti;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ContratMotifdates
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idMotiffincontrat
     *
     * @param \SalarieBundle\Entity\Param\ParamMotiffincontrat $idMotiffincontrat
     *
     * @return ContratMotifdates
     */
    public function setIdMotiffincontrat(\SalarieBundle\Entity\Param\ParamMotiffincontrat $idMotiffincontrat = null)
    {
        $this->idMotiffincontrat = $idMotiffincontrat;

        return $this;
    }

    /**
     * Get idMotiffincontrat
     *
     * @return \SalarieBundle\Entity\Param\ParamMotiffincontrat
     */
    public function getIdMotiffincontrat()
    {
        return $this->idMotiffincontrat;
    }

    /**
     * Set idNaturecontrat
     *
     * @param \SalarieBundle\Entity\Param\ParamNaturecontrat $idNaturecontrat
     *
     * @return ContratMotifdates
     */
    public function setIdNaturecontrat(\SalarieBundle\Entity\Param\ParamNaturecontrat $idNaturecontrat = null)
    {
        $this->idNaturecontrat = $idNaturecontrat;

        return $this;
    }

    /**
     * Get idNaturecontrat
     *
     * @return \SalarieBundle\Entity\Param\ParamNaturecontrat
     */
    public function getIdNaturecontrat()
    {
        return $this->idNaturecontrat;
    }

    /**
     * Set idTypecontrat
     *
     * @param \SalarieBundle\Entity\Param\ParamTypecontrat $idTypecontrat
     *
     * @return ContratMotifdates
     */
    public function setIdTypecontrat(\SalarieBundle\Entity\Param\ParamTypecontrat $idTypecontrat = null)
    {
        $this->idTypecontrat = $idTypecontrat;

        return $this;
    }

    /**
     * Get idTypecontrat
     *
     * @return \SalarieBundle\Entity\Param\ParamTypecontrat
     */
    public function getIdTypecontrat()
    {
        return $this->idTypecontrat;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratMotifdates
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }
}
