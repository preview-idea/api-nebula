<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratRepartitionhoraire
 *
 * @ORM\Table(name="contrat_repartitionhoraire", indexes={@ORM\Index(name="IDX_4E5D000CBEA930E3", columns={"id_contrat"}), @ORM\Index(name="IDX_4E5D000CCBC3B464", columns={"id_matricule_maj"})})
 * @ORM\Entity
 */
class ContratRepartitionhoraire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_repartitionhoraire", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_repartitionhoraire_id_ligne_repartitionhoraire_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneRepartitionhoraire;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_semaine", type="smallint", nullable=false)
     */
    private $nbSemaine;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_lundi", type="smallint", nullable=true)
     */
    private $nbLundi;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_mardi", type="smallint", nullable=true)
     */
    private $nbMardi;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_mercredi", type="smallint", nullable=true)
     */
    private $nbMercredi;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_jeudi", type="smallint", nullable=true)
     */
    private $nbJeudi;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_vendredi", type="smallint", nullable=true)
     */
    private $nbVendredi;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_samedi", type="smallint", nullable=true)
     */
    private $nbSamedi;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_dimanche", type="smallint", nullable=true)
     */
    private $nbDimanche;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_total_heures", type="smallint", nullable=false)
     */
    private $nbTotalHeures;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_repartitionhoraire", type="integer", nullable=false)
     */
    private $idRepartitionhoraire;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * Get idLigneRepartitionhoraire
     *
     * @return integer
     */
    public function getIdLigneRepartitionhoraire()
    {
        return $this->idLigneRepartitionhoraire;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratRepartitionhoraire
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return ContratRepartitionhoraire
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set nbSemaine
     *
     * @param integer $nbSemaine
     *
     * @return ContratRepartitionhoraire
     */
    public function setNbSemaine($nbSemaine)
    {
        $this->nbSemaine = $nbSemaine;

        return $this;
    }

    /**
     * Get nbSemaine
     *
     * @return integer
     */
    public function getNbSemaine()
    {
        return $this->nbSemaine;
    }

    /**
     * Set nbLundi
     *
     * @param integer $nbLundi
     *
     * @return ContratRepartitionhoraire
     */
    public function setNbLundi($nbLundi)
    {
        $this->nbLundi = $nbLundi;

        return $this;
    }

    /**
     * Get nbLundi
     *
     * @return integer
     */
    public function getNbLundi()
    {
        return $this->nbLundi;
    }

    /**
     * Set nbMardi
     *
     * @param integer $nbMardi
     *
     * @return ContratRepartitionhoraire
     */
    public function setNbMardi($nbMardi)
    {
        $this->nbMardi = $nbMardi;

        return $this;
    }

    /**
     * Get nbMardi
     *
     * @return integer
     */
    public function getNbMardi()
    {
        return $this->nbMardi;
    }

    /**
     * Set nbMercredi
     *
     * @param integer $nbMercredi
     *
     * @return ContratRepartitionhoraire
     */
    public function setNbMercredi($nbMercredi)
    {
        $this->nbMercredi = $nbMercredi;

        return $this;
    }

    /**
     * Get nbMercredi
     *
     * @return integer
     */
    public function getNbMercredi()
    {
        return $this->nbMercredi;
    }

    /**
     * Set nbJeudi
     *
     * @param integer $nbJeudi
     *
     * @return ContratRepartitionhoraire
     */
    public function setNbJeudi($nbJeudi)
    {
        $this->nbJeudi = $nbJeudi;

        return $this;
    }

    /**
     * Get nbJeudi
     *
     * @return integer
     */
    public function getNbJeudi()
    {
        return $this->nbJeudi;
    }

    /**
     * Set nbVendredi
     *
     * @param integer $nbVendredi
     *
     * @return ContratRepartitionhoraire
     */
    public function setNbVendredi($nbVendredi)
    {
        $this->nbVendredi = $nbVendredi;

        return $this;
    }

    /**
     * Get nbVendredi
     *
     * @return integer
     */
    public function getNbVendredi()
    {
        return $this->nbVendredi;
    }

    /**
     * Set nbSamedi
     *
     * @param integer $nbSamedi
     *
     * @return ContratRepartitionhoraire
     */
    public function setNbSamedi($nbSamedi)
    {
        $this->nbSamedi = $nbSamedi;

        return $this;
    }

    /**
     * Get nbSamedi
     *
     * @return integer
     */
    public function getNbSamedi()
    {
        return $this->nbSamedi;
    }

    /**
     * Set nbDimanche
     *
     * @param integer $nbDimanche
     *
     * @return ContratRepartitionhoraire
     */
    public function setNbDimanche($nbDimanche)
    {
        $this->nbDimanche = $nbDimanche;

        return $this;
    }

    /**
     * Get nbDimanche
     *
     * @return integer
     */
    public function getNbDimanche()
    {
        return $this->nbDimanche;
    }

    /**
     * Set nbTotalHeures
     *
     * @param integer $nbTotalHeures
     *
     * @return ContratRepartitionhoraire
     */
    public function setNbTotalHeures($nbTotalHeures)
    {
        $this->nbTotalHeures = $nbTotalHeures;

        return $this;
    }

    /**
     * Get nbTotalHeures
     *
     * @return integer
     */
    public function getNbTotalHeures()
    {
        return $this->nbTotalHeures;
    }

    /**
     * Set idRepartitionhoraire
     *
     * @param integer $idRepartitionhoraire
     *
     * @return ContratRepartitionhoraire
     */
    public function setIdRepartitionhoraire($idRepartitionhoraire)
    {
        $this->idRepartitionhoraire = $idRepartitionhoraire;

        return $this;
    }

    /**
     * Get idRepartitionhoraire
     *
     * @return integer
     */
    public function getIdRepartitionhoraire()
    {
        return $this->idRepartitionhoraire;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratRepartitionhoraire
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ContratRepartitionhoraire
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }
}
