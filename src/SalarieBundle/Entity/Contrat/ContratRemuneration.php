<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratRemuneration
 *
 * @ORM\Table(name="contrat_remuneration", indexes={@ORM\Index(name="idx_image_contrat_remuneration", columns={"id_contrat", "dt_maj"}), @ORM\Index(name="IDX_52B73C42BEA930E3", columns={"id_contrat"}), @ORM\Index(name="IDX_52B73C42CBC3B464", columns={"id_matricule_maj"}), @ORM\Index(name="IDX_52B73C425E247FF5", columns={"id_temps_travail"})})
 * @ORM\Entity
 */
class ContratRemuneration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_remuneration", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_remuneration_id_ligne_remuneration_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneRemuneration;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_horaire_mensuel", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $nbHoraireMensuel;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_horaire_annuel", type="decimal", precision=7, scale=2, nullable=false)
     */
    private $nbHoraireAnnuel;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_salaire_base", type="decimal", precision=9, scale=2, nullable=false)
     */
    private $nbSalaireBase;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_taux_horaire", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $nbTauxHoraire;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_jour_forfait", type="smallint", nullable=true)
     */
    private $nbJourForfait;

    /**
     * @var string
     *
     * @ORM\Column(name="li_salaire_base", type="string", length=80, nullable=true)
     */
    private $liSalaireBase;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTempsTravail
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTempsTravail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_temps_travail", referencedColumnName="id_temps_travail")
     * })
     */
    private $idTempsTravail;

    /**
     * Get idLigneRemuneration
     *
     * @return integer
     */
    public function getIdLigneRemuneration()
    {
        return $this->idLigneRemuneration;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratRemuneration
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return ContratRemuneration
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set nbHoraireMensuel
     *
     * @param string $nbHoraireMensuel
     *
     * @return ContratRemuneration
     */
    public function setNbHoraireMensuel($nbHoraireMensuel)
    {
        $this->nbHoraireMensuel = $nbHoraireMensuel;

        return $this;
    }

    /**
     * Get nbHoraireMensuel
     *
     * @return string
     */
    public function getNbHoraireMensuel()
    {
        return $this->nbHoraireMensuel;
    }

    /**
     * Set nbHoraireAnnuel
     *
     * @param string $nbHoraireAnnuel
     *
     * @return ContratRemuneration
     */
    public function setNbHoraireAnnuel($nbHoraireAnnuel)
    {
        $this->nbHoraireAnnuel = $nbHoraireAnnuel;

        return $this;
    }

    /**
     * Get nbHoraireAnnuel
     *
     * @return string
     */
    public function getNbHoraireAnnuel()
    {
        return $this->nbHoraireAnnuel;
    }

    /**
     * Set nbSalaireBase
     *
     * @param string $nbSalaireBase
     *
     * @return ContratRemuneration
     */
    public function setNbSalaireBase($nbSalaireBase)
    {
        $this->nbSalaireBase = $nbSalaireBase;

        return $this;
    }

    /**
     * Get nbSalaireBase
     *
     * @return string
     */
    public function getNbSalaireBase()
    {
        return $this->nbSalaireBase;
    }

    /**
     * Set nbTauxHoraire
     *
     * @param string $nbTauxHoraire
     *
     * @return ContratRemuneration
     */
    public function setNbTauxHoraire($nbTauxHoraire)
    {
        $this->nbTauxHoraire = $nbTauxHoraire;

        return $this;
    }

    /**
     * Get nbTauxHoraire
     *
     * @return string
     */
    public function getNbTauxHoraire()
    {
        return $this->nbTauxHoraire;
    }

    /**
     * Set nbJourForfait
     *
     * @param integer $nbJourForfait
     *
     * @return ContratRemuneration
     */
    public function setNbJourForfait($nbJourForfait)
    {
        $this->nbJourForfait = $nbJourForfait;

        return $this;
    }

    /**
     * Get nbJourForfait
     *
     * @return integer
     */
    public function getNbJourForfait()
    {
        return $this->nbJourForfait;
    }

    /**
     * Set liSalaireBase
     *
     * @param string $liSalaireBase
     *
     * @return ContratRemuneration
     */
    public function setLiSalaireBase($liSalaireBase)
    {
        $this->liSalaireBase = $liSalaireBase;

        return $this;
    }

    /**
     * Get liSalaireBase
     *
     * @return string
     */
    public function getLiSalaireBase()
    {
        return $this->liSalaireBase;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratRemuneration
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ContratRemuneration
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idTempsTravail
     *
     * @param \SalarieBundle\Entity\Param\ParamTempsTravail $idTempsTravail
     *
     * @return ContratRemuneration
     */
    public function setIdTempsTravail(\SalarieBundle\Entity\Param\ParamTempsTravail $idTempsTravail = null)
    {
        $this->idTempsTravail = $idTempsTravail;

        return $this;
    }

    /**
     * Get idTempsTravail
     *
     * @return \SalarieBundle\Entity\Param\ParamTempsTravail
     */
    public function getIdTempsTravail()
    {
        return $this->idTempsTravail;
    }
}
