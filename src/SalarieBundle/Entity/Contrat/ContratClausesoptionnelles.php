<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratClausesoptionnelles
 *
 * @ORM\Table(name="contrat_clausesoptionnelles", indexes={@ORM\Index(name="IDX_5FF4271960A7F9AE", columns={"id_clausescontrat"}), @ORM\Index(name="IDX_5FF42719BEA930E3", columns={"id_contrat"}), @ORM\Index(name="IDX_5FF42719CBC3B464", columns={"id_matricule_maj"})})
 * @ORM\Entity
 */
class ContratClausesoptionnelles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_clausesoptionnelles", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_clausesoptionnelles_id_ligne_clausesoptionnelles_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneClausesoptionnelles;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_duree_clause_non_concurrence", type="smallint", nullable=true)
     */
    private $nbDureeClauseNonConcurrence;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_pourcent_clause_non_concurrence", type="decimal", precision=4, scale=1, nullable=true)
     */
    private $nbPourcentClauseNonConcurrence;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_clausesoptionnelles", type="integer", nullable=false)
     */
    private $idClausesoptionnelles;

    /**
     * @var string
     *
     * @ORM\Column(name="li_lieu_dedit_formation", type="string", length=100, nullable=true)
     */
    private $liLieuDeditFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="li_organisme_dedit_formation", type="string", length=100, nullable=true)
     */
    private $liOrganismeDeditFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_cout_dedit_formation", type="decimal", precision=9, scale=2, nullable=true)
     */
    private $nbCoutDeditFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="li_cout_dedit_formation", type="string", length=100, nullable=true)
     */
    private $liCoutDeditFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="li_programme_dedit_formation", type="string", length=60, nullable=true)
     */
    private $liProgrammeDeditFormation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_dedit_formation", type="datetime", nullable=true)
     */
    private $dtDebutDeditFormation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_dedit_formation", type="datetime", nullable=true)
     */
    private $dtFinDeditFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="li_diplome_dedit_formation", type="string", length=30, nullable=true)
     */
    private $liDiplomeDeditFormation;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamClausescontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamClausescontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_clausescontrat", referencedColumnName="id_clausescontrat")
     * })
     */
    private $idClausescontrat;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\Param\ParamQualifcontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamQualifcontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_qualif_dedit_formation", referencedColumnName="id_qualifcontrat")
     * })
     */
    private $idQualifDeditFormation;

    /**
     * Get idLigneClausesoptionnelles
     *
     * @return integer
     */
    public function getIdLigneClausesoptionnelles()
    {
        return $this->idLigneClausesoptionnelles;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratClausesoptionnelles
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return ContratClausesoptionnelles
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set nbDureeClauseNonConcurrence
     *
     * @param integer $nbDureeClauseNonConcurrence
     *
     * @return ContratClausesoptionnelles
     */
    public function setNbDureeClauseNonConcurrence($nbDureeClauseNonConcurrence)
    {
        $this->nbDureeClauseNonConcurrence = $nbDureeClauseNonConcurrence;

        return $this;
    }

    /**
     * Get nbDureeClauseNonConcurrence
     *
     * @return integer
     */
    public function getNbDureeClauseNonConcurrence()
    {
        return $this->nbDureeClauseNonConcurrence;
    }

    /**
     * Set nbPourcentClauseNonConcurrence
     *
     * @param string $nbPourcentClauseNonConcurrence
     *
     * @return ContratClausesoptionnelles
     */
    public function setNbPourcentClauseNonConcurrence($nbPourcentClauseNonConcurrence)
    {
        $this->nbPourcentClauseNonConcurrence = $nbPourcentClauseNonConcurrence;

        return $this;
    }

    /**
     * Get nbPourcentClauseNonConcurrence
     *
     * @return string
     */
    public function getNbPourcentClauseNonConcurrence()
    {
        return $this->nbPourcentClauseNonConcurrence;
    }

    /**
     * Set idClausesoptionnelles
     *
     * @param integer $idClausesoptionnelles
     *
     * @return ContratClausesoptionnelles
     */
    public function setIdClausesoptionnelles($idClausesoptionnelles)
    {
        $this->idClausesoptionnelles = $idClausesoptionnelles;

        return $this;
    }

    /**
     * Get idClausesoptionnelles
     *
     * @return integer
     */
    public function getIdClausesoptionnelles()
    {
        return $this->idClausesoptionnelles;
    }

    /**
     * Set liLieuDeditFormation
     *
     * @param string $liLieuDeditFormation
     *
     * @return ContratClausesoptionnelles
     */
    public function setLiLieuDeditFormation($liLieuDeditFormation)
    {
        $this->liLieuDeditFormation = $liLieuDeditFormation;

        return $this;
    }

    /**
     * Get liLieuDeditFormation
     *
     * @return string
     */
    public function getLiLieuDeditFormation()
    {
        return $this->liLieuDeditFormation;
    }

    /**
     * Set liOrganismeDeditFormation
     *
     * @param string $liOrganismeDeditFormation
     *
     * @return ContratClausesoptionnelles
     */
    public function setLiOrganismeDeditFormation($liOrganismeDeditFormation)
    {
        $this->liOrganismeDeditFormation = $liOrganismeDeditFormation;

        return $this;
    }

    /**
     * Get liOrganismeDeditFormation
     *
     * @return string
     */
    public function getLiOrganismeDeditFormation()
    {
        return $this->liOrganismeDeditFormation;
    }

    /**
     * Set nbCoutDeditFormation
     *
     * @param string $nbCoutDeditFormation
     *
     * @return ContratClausesoptionnelles
     */
    public function setNbCoutDeditFormation($nbCoutDeditFormation)
    {
        $this->nbCoutDeditFormation = $nbCoutDeditFormation;

        return $this;
    }

    /**
     * Get nbCoutDeditFormation
     *
     * @return string
     */
    public function getNbCoutDeditFormation()
    {
        return $this->nbCoutDeditFormation;
    }

    /**
     * Set liCoutDeditFormation
     *
     * @param string $liCoutDeditFormation
     *
     * @return ContratClausesoptionnelles
     */
    public function setLiCoutDeditFormation($liCoutDeditFormation)
    {
        $this->liCoutDeditFormation = $liCoutDeditFormation;

        return $this;
    }

    /**
     * Get liCoutDeditFormation
     *
     * @return string
     */
    public function getLiCoutDeditFormation()
    {
        return $this->liCoutDeditFormation;
    }

    /**
     * Set liProgrammeDeditFormation
     *
     * @param string $liProgrammeDeditFormation
     *
     * @return ContratClausesoptionnelles
     */
    public function setLiProgrammeDeditFormation($liProgrammeDeditFormation)
    {
        $this->liProgrammeDeditFormation = $liProgrammeDeditFormation;

        return $this;
    }

    /**
     * Get liProgrammeDeditFormation
     *
     * @return string
     */
    public function getLiProgrammeDeditFormation()
    {
        return $this->liProgrammeDeditFormation;
    }

    /**
     * Set dtDebutDeditFormation
     *
     * @param \DateTime $dtDebutDeditFormation
     *
     * @return ContratClausesoptionnelles
     */
    public function setDtDebutDeditFormation($dtDebutDeditFormation)
    {
        $this->dtDebutDeditFormation = $dtDebutDeditFormation;

        return $this;
    }

    /**
     * Get dtDebutDeditFormation
     *
     * @return \DateTime
     */
    public function getDtDebutDeditFormation()
    {
        return $this->dtDebutDeditFormation;
    }

    /**
     * Set dtFinDeditFormation
     *
     * @param \DateTime $dtFinDeditFormation
     *
     * @return ContratClausesoptionnelles
     */
    public function setDtFinDeditFormation($dtFinDeditFormation)
    {
        $this->dtFinDeditFormation = $dtFinDeditFormation;

        return $this;
    }

    /**
     * Get dtFinDeditFormation
     *
     * @return \DateTime
     */
    public function getDtFinDeditFormation()
    {
        return $this->dtFinDeditFormation;
    }

    /**
     * Set liDiplomeDeditFormation
     *
     * @param string $liDiplomeDeditFormation
     *
     * @return ContratClausesoptionnelles
     */
    public function setLiDiplomeDeditFormation($liDiplomeDeditFormation)
    {
        $this->liDiplomeDeditFormation = $liDiplomeDeditFormation;

        return $this;
    }

    /**
     * Get liDiplomeDeditFormation
     *
     * @return string
     */
    public function getLiDiplomeDeditFormation()
    {
        return $this->liDiplomeDeditFormation;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratClausesoptionnelles
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set idClausescontrat
     *
     * @param \SalarieBundle\Entity\Param\ParamClausescontrat $idClausescontrat
     *
     * @return ContratClausesoptionnelles
     */
    public function setIdClausescontrat(\SalarieBundle\Entity\Param\ParamClausescontrat $idClausescontrat = null)
    {
        $this->idClausescontrat = $idClausescontrat;

        return $this;
    }

    /**
     * Get idClausescontrat
     *
     * @return \SalarieBundle\Entity\Param\ParamClausescontrat
     */
    public function getIdClausescontrat()
    {
        return $this->idClausescontrat;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ContratClausesoptionnelles
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idQualifDeditFormation
     *
     * @param \SalarieBundle\Entity\Param\ParamQualifcontrat $idQualifDeditFormation
     *
     * @return ContratClausesoptionnelles
     */
    public function setIdQualifDeditFormation(\SalarieBundle\Entity\Param\ParamQualifcontrat $idQualifDeditFormation = null)
    {
        $this->idQualifDeditFormation = $idQualifDeditFormation;

        return $this;
    }

    /**
     * Get idQualifDeditFormation
     *
     * @return \SalarieBundle\Entity\Param\ParamQualifcontrat
     */
    public function getIdQualifDeditFormation()
    {
        return $this->idQualifDeditFormation;
    }
}
