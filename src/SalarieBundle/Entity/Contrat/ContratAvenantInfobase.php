<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratAvenantInfobase
 *
 * @ORM\Table(name="contrat_avenant_infobase", indexes={@ORM\Index(name="IDX_8A7013AFBEA930E3", columns={"id_contrat"})})
 * @ORM\Entity
 */
class ContratAvenantInfobase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_avenant", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_avenant_infobase_id_avenant_seq", allocationSize=1, initialValue=1)
     */
    private $idAvenant;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_creation", type="datetime", nullable=false)
     */
    private $dtCreation;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_typeavenant", type="smallint", nullable=false)
     */
    private $idTypeavenant;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_ordre_avenant", type="smallint", nullable=false)
     */
    private $idOrdreAvenant;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * Get idAvenant
     *
     * @return integer
     */
    public function getIdAvenant()
    {
        return $this->idAvenant;
    }

    /**
     * Set dtCreation
     *
     * @param \DateTime $dtCreation
     *
     * @return ContratAvenantInfobase
     */
    public function setDtCreation($dtCreation)
    {
        $this->dtCreation = $dtCreation;

        return $this;
    }

    /**
     * Get dtCreation
     *
     * @return \DateTime
     */
    public function getDtCreation()
    {
        return $this->dtCreation;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratAvenantInfobase
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set idTypeavenant
     *
     * @param integer $idTypeavenant
     *
     * @return ContratAvenantInfobase
     */
    public function setIdTypeavenant($idTypeavenant)
    {
        $this->idTypeavenant = $idTypeavenant;

        return $this;
    }

    /**
     * Get idTypeavenant
     *
     * @return integer
     */
    public function getIdTypeavenant()
    {
        return $this->idTypeavenant;
    }

    /**
     * Set idOrdreAvenant
     *
     * @param integer $idOrdreAvenant
     *
     * @return ContratAvenantInfobase
     */
    public function setIdOrdreAvenant($idOrdreAvenant)
    {
        $this->idOrdreAvenant = $idOrdreAvenant;

        return $this;
    }

    /**
     * Get idOrdreAvenant
     *
     * @return integer
     */
    public function getIdOrdreAvenant()
    {
        return $this->idOrdreAvenant;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratAvenantInfobase
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }
}
