<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratAnalytique
 *
 * @ORM\Table(name="contrat_analytique", indexes={@ORM\Index(name="IDX_F89D965DBEA930E3", columns={"id_contrat"}), @ORM\Index(name="IDX_F89D965DF600676E", columns={"id_bu"}), @ORM\Index(name="IDX_F89D965D2955449B", columns={"id_region"}), @ORM\Index(name="IDX_F89D965D42F62F44", columns={"id_agence"}), @ORM\Index(name="IDX_F89D965DE8AEB980", columns={"id_activite"}), @ORM\Index(name="IDX_F89D965D7797B861", columns={"id_metier"}), @ORM\Index(name="IDX_F89D965DCBC3B464", columns={"id_matricule_maj"})})
 * @ORM\Entity
 */
class ContratAnalytique
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_contrat_analytique", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_analytique_id_ligne_contrat_analytique_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneContratAnalytique;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_contrat_analytique", type="integer", nullable=false)
     */
    private $idContratAnalytique;
    /**
     * @var integer
     *
     * @ORM\Column(name="nb_pourcent", type="integer", nullable=false)
     */
    private $nbPourcent;

    /**
     * @var string
     *
     * @ORM\Column(name="li_chan", type="string", length=9, nullable=true)
     */
    private $liChan;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamBu
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamBu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_bu", referencedColumnName="id_bu")
     * })
     */
    private $idBu;

    /**
     * @var \SalarieBundle\Entity\Param\ParamRegion
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_region", referencedColumnName="id_region")
     * })
     */
    private $idRegion;

    /**
     * @var \SalarieBundle\Entity\Param\ParamAgence
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamAgence")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_agence", referencedColumnName="id_agence")
     * })
     */
    private $idAgence;

    /**
     * @var \SalarieBundle\Entity\Param\ParamActivite
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamActivite")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_activite", referencedColumnName="id_activite")
     * })
     */
    private $idActivite;

    /**
     * @var \SalarieBundle\Entity\Param\ParamMetier
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamMetier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_metier", referencedColumnName="id_metier")
     * })
     */
    private $idMetier;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;


    /**
     * Get idLigneContratAnalytique
     *
     * @return integer
     */
    public function getIdLigneContratAnalytique()
    {
        return $this->idLigneContratAnalytique;
    }

    /**
     * Set IdContratAnalytique
     *
     * @param integer $idContratAnalytique
     *
     * @return ContratAnalytique
     */
    public function setIdContratAnalytique($idContratAnalytique)
    {
        $this->idContratAnalytique = $idContratAnalytique;

        return $this;
    }

    /**
     * Get idContratAnalytique
     *
     * @return integer
     */
    public function getIdContratAnalytique()
    {
        return $this->idContratAnalytique;
    }

    /**
     * Set nbPourcent
     *
     * @param integer $nbPourcent
     *
     * @return ContratAnalytique
     */
    public function setNbPourcent($nbPourcent)
    {
        $this->nbPourcent = $nbPourcent;

        return $this;
    }

    /**
     * Get nbPourcent
     *
     * @return integer
     */
    public function getNbPourcent()
    {
        return $this->nbPourcent;
    }

    /**
     * Set liChan
     *
     * @param string $liChan
     *
     * @return ContratAnalytique
     */
    public function setLiChan($liChan)
    {
        $this->liChan = $liChan;

        return $this;
    }

    /**
     * Get liChan
     *
     * @return string
     */
    public function getLiChan()
    {
        return $this->liChan;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratAnalytique
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return ContratAnalytique
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratAnalytique
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set idBu
     *
     * @param \SalarieBundle\Entity\Param\ParamBu $idBu
     *
     * @return ContratAnalytique
     */
    public function setIdBu(\SalarieBundle\Entity\Param\ParamBu $idBu = null)
    {
        $this->idBu = $idBu;

        return $this;
    }

    /**
     * Get idBu
     *
     * @return \SalarieBundle\Entity\Param\ParamBu
     */
    public function getIdBu()
    {
        return $this->idBu;
    }

    /**
     * Set idRegion
     *
     * @param \SalarieBundle\Entity\Param\ParamRegion $idRegion
     *
     * @return ContratAnalytique
     */
    public function setIdRegion(\SalarieBundle\Entity\Param\ParamRegion $idRegion = null)
    {
        $this->idRegion = $idRegion;

        return $this;
    }

    /**
     * Get idRegion
     *
     * @return \SalarieBundle\Entity\Param\ParamRegion
     */
    public function getIdRegion()
    {
        return $this->idRegion;
    }

    /**
     * Set idAgence
     *
     * @param \SalarieBundle\Entity\Param\ParamAgence $idAgence
     *
     * @return ContratAnalytique
     */
    public function setIdAgence(\SalarieBundle\Entity\Param\ParamAgence $idAgence = null)
    {
        $this->idAgence = $idAgence;

        return $this;
    }

    /**
     * Get idAgence
     *
     * @return \SalarieBundle\Entity\Param\ParamAgence
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * Set idActivite
     *
     * @param \SalarieBundle\Entity\Param\ParamActivite $idActivite
     *
     * @return ContratAnalytique
     */
    public function setIdActivite(\SalarieBundle\Entity\Param\ParamActivite $idActivite = null)
    {
        $this->idActivite = $idActivite;

        return $this;
    }

    /**
     * Get idActivite
     *
     * @return \SalarieBundle\Entity\Param\ParamActivite
     */
    public function getIdActivite()
    {
        return $this->idActivite;
    }

    /**
     * Set idMetier
     *
     * @param \SalarieBundle\Entity\Param\ParamMetier $idMetier
     *
     * @return ContratAnalytique
     */
    public function setIdMetier(\SalarieBundle\Entity\Param\ParamMetier $idMetier = null)
    {
        $this->idMetier = $idMetier;

        return $this;
    }

    /**
     * Get idMetier
     *
     * @return \SalarieBundle\Entity\Param\ParamMetier
     */
    public function getIdMetier()
    {
        return $this->idMetier;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ContratAnalytique
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }
}
