<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratAnciennete
 *
 * @ORM\Table(name="contrat_anciennete", indexes={@ORM\Index(name="IDX_2927F110BEA930E3", columns={"id_contrat"}), @ORM\Index(name="IDX_2927F110CBC3B464", columns={"id_matricule_maj"})})
 * @ORM\Entity
 */
class ContratAnciennete
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_anciennete", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_anciennete_id_ligne_anciennete_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneAnciennete;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_entree_groupe", type="datetime", nullable=false)
     */
    private $dtEntreeGroupe;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_entree_societe", type="datetime", nullable=false)
     */
    private $dtEntreeSociete;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_anciennete_branche", type="datetime", nullable=false)
     */
    private $dtAncienneteBranche;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_anciennete_csp", type="datetime", nullable=false)
     */
    private $dtAncienneteCsp;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;



    /**
     * Get idLigneAnciennete
     *
     * @return integer
     */
    public function getIdLigneAnciennete()
    {
        return $this->idLigneAnciennete;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratAnciennete
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return ContratAnciennete
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set dtEntreeGroupe
     *
     * @param \DateTime $dtEntreeGroupe
     *
     * @return ContratAnciennete
     */
    public function setDtEntreeGroupe($dtEntreeGroupe)
    {
        $this->dtEntreeGroupe = $dtEntreeGroupe;

        return $this;
    }

    /**
     * Get dtEntreeGroupe
     *
     * @return \DateTime
     */
    public function getDtEntreeGroupe()
    {
        return $this->dtEntreeGroupe;
    }

    /**
     * Set dtEntreeSociete
     *
     * @param \DateTime $dtEntreeSociete
     *
     * @return ContratAnciennete
     */
    public function setDtEntreeSociete($dtEntreeSociete)
    {
        $this->dtEntreeSociete = $dtEntreeSociete;

        return $this;
    }

    /**
     * Get dtEntreeSociete
     *
     * @return \DateTime
     */
    public function getDtEntreeSociete()
    {
        return $this->dtEntreeSociete;
    }

    /**
     * Set dtAncienneteBranche
     *
     * @param \DateTime $dtAncienneteBranche
     *
     * @return ContratAnciennete
     */
    public function setDtAncienneteBranche($dtAncienneteBranche)
    {
        $this->dtAncienneteBranche = $dtAncienneteBranche;

        return $this;
    }

    /**
     * Get dtAncienneteBranche
     *
     * @return \DateTime
     */
    public function getDtAncienneteBranche()
    {
        return $this->dtAncienneteBranche;
    }

    /**
     * Set dtAncienneteCsp
     *
     * @param \DateTime $dtAncienneteCsp
     *
     * @return ContratAnciennete
     */
    public function setDtAncienneteCsp($dtAncienneteCsp)
    {
        $this->dtAncienneteCsp = $dtAncienneteCsp;

        return $this;
    }

    /**
     * Get dtAncienneteCsp
     *
     * @return \DateTime
     */
    public function getDtAncienneteCsp()
    {
        return $this->dtAncienneteCsp;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratAnciennete
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ContratAnciennete
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }
}
