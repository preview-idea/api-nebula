<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratSituations
 *
 * @ORM\Table(name="contrat_situations", indexes={@ORM\Index(name="idx_image_contrat_situations", columns={"id_contrat", "id_type_situation"}), @ORM\Index(name="IDX_DD653C54658D66DC", columns={"id_type_situation"}), @ORM\Index(name="IDX_DD653C54BEA930E3", columns={"id_contrat"})})
 * @ORM\Entity
 */
class ContratSituations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_situations_id_ligne_seq", allocationSize=1, initialValue=1)
     */
    private $idLigne;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_situation", type="date", nullable=false)
     */
    private $dtSituation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_modification", type="datetime", nullable=false)
     */
    private $dtModification;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_statut", type="smallint", nullable=false)
     */
    private $nbStatut;

    /**
     * @var \SalarieBundle\Entity\Param\ParamSituations
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamSituations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_situation", referencedColumnName="id_type_situation")
     * })
     */
    private $idTypeSituation;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var \SalarieBundle\Entity\Avenant\AvenantInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Avenant\AvenantInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_avenant", referencedColumnName="id_contrat")
     * })
     */
    private $idAvenant;

    /**
     * Get idLigne
     *
     * @return integer
     */
    public function getIdLigne()
    {
        return $this->idLigne;
    }

    /**
     * Set dtSituation
     *
     * @param \DateTime $dtSituation
     *
     * @return ContratSituations
     */
    public function setDtSituation($dtSituation)
    {
        $this->dtSituation = $dtSituation;

        return $this;
    }

    /**
     * Get dtSituation
     *
     * @return \DateTime
     */
    public function getDtSituation()
    {
        return $this->dtSituation;
    }

    /**
     * Set dtModification
     *
     * @param \DateTime $dtModification
     *
     * @return ContratSituations
     */
    public function setDtModification($dtModification)
    {
        $this->dtModification = $dtModification;

        return $this;
    }

    /**
     * Get dtModification
     *
     * @return \DateTime
     */
    public function getDtModification()
    {
        return $this->dtModification;
    }

    /**
     * Set nbStatut
     *
     * @param integer $nbStatut
     *
     * @return ContratSituations
     */
    public function setNbStatut($nbStatut)
    {
        $this->nbStatut = $nbStatut;

        return $this;
    }

    /**
     * Get nbStatut
     *
     * @return integer
     */
    public function getNbStatut()
    {
        return $this->nbStatut;
    }

    /**
     * Set idTypeSituation
     *
     * @param \SalarieBundle\Entity\Param\ParamSituations $idTypeSituation
     *
     * @return ContratSituations
     */
    public function setIdTypeSituation(\SalarieBundle\Entity\Param\ParamSituations $idTypeSituation = null)
    {
        $this->idTypeSituation = $idTypeSituation;

        return $this;
    }

    /**
     * Get idTypeSituation
     *
     * @return \SalarieBundle\Entity\Param\ParamSituations
     */
    public function getIdTypeSituation()
    {
        return $this->idTypeSituation;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratSituations
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set idAvenant
     *
     * @param \SalarieBundle\Entity\Avenant\AvenantInfobase $idAvenant
     *
     * @return ContratSituations
     */
    public function setIdAvenant(\SalarieBundle\Entity\Avenant\AvenantInfobase $idAvenant = null)
    {
        $this->idAvenant = $idAvenant;

        return $this;
    }

    /**
     * Get idAvenant
     *
     * @return \SalarieBundle\Entity\Avenant\AvenantInfobase
     */
    public function getIdAvenant()
    {
        return $this->idAvenant;
    }
}
