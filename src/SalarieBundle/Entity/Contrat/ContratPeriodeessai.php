<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratPeriodeessai
 *
 * @ORM\Table(name="contrat_periodeessai", indexes={@ORM\Index(name="IDX_2FD2F02612782BFD", columns={"id_type_periodeessai"}), @ORM\Index(name="IDX_2FD2F026BEA930E3", columns={"id_contrat"}), @ORM\Index(name="IDX_2FD2F026CBC3B464", columns={"id_matricule_maj"})})
 * @ORM\Entity
 */
class ContratPeriodeessai
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_periodeessai", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_periodeessai_id_ligne_periodeessai_seq", allocationSize=1, initialValue=1)
     */
    private $idLignePeriodeessai;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_mois_renouvellement", type="smallint", nullable=true)
     */
    private $nbMoisRenouvellement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_initiale", type="datetime", nullable=true)
     */
    private $dtFinInitiale;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_renouvellement", type="datetime", nullable=true)
     */
    private $dtFinRenouvellement;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_duree_initiale", type="integer", nullable=true)
     */
    private $nbDureeInitiale;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypePeriodeessai
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypePeriodeessai")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_periodeessai", referencedColumnName="id_type_periodeessai")
     * })
     */
    private $idTypePeriodeessai;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * Get idLignePeriodeessai
     *
     * @return integer
     */
    public function getIdLignePeriodeessai()
    {
        return $this->idLignePeriodeessai;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratPeriodeessai
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return ContratPeriodeessai
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set nbMoisRenouvellement
     *
     * @param integer $nbMoisRenouvellement
     *
     * @return ContratPeriodeessai
     */
    public function setNbMoisRenouvellement($nbMoisRenouvellement)
    {
        $this->nbMoisRenouvellement = $nbMoisRenouvellement;

        return $this;
    }

    /**
     * Get nbMoisRenouvellement
     *
     * @return integer
     */
    public function getNbMoisRenouvellement()
    {
        return $this->nbMoisRenouvellement;
    }

    /**
     * Set dtFinInitiale
     *
     * @param \DateTime $dtFinInitiale
     *
     * @return ContratPeriodeessai
     */
    public function setDtFinInitiale($dtFinInitiale)
    {
        $this->dtFinInitiale = $dtFinInitiale;

        return $this;
    }

    /**
     * Get dtFinInitiale
     *
     * @return \DateTime
     */
    public function getDtFinInitiale()
    {
        return $this->dtFinInitiale;
    }

    /**
     * Set dtFinRenouvellement
     *
     * @param \DateTime $dtFinRenouvellement
     *
     * @return ContratPeriodeessai
     */
    public function setDtFinRenouvellement($dtFinRenouvellement)
    {
        $this->dtFinRenouvellement = $dtFinRenouvellement;

        return $this;
    }

    /**
     * Get dtFinRenouvellement
     *
     * @return \DateTime
     */
    public function getDtFinRenouvellement()
    {
        return $this->dtFinRenouvellement;
    }

    /**
     * Set nbDureeInitiale
     *
     * @param integer $nbDureeInitiale
     *
     * @return ContratPeriodeessai
     */
    public function setNbDureeInitiale($nbDureeInitiale)
    {
        $this->nbDureeInitiale = $nbDureeInitiale;

        return $this;
    }

    /**
     * Get nbDureeInitiale
     *
     * @return integer
     */
    public function getNbDureeInitiale()
    {
        return $this->nbDureeInitiale;
    }

    /**
     * Set idTypePeriodeessai
     *
     * @param \SalarieBundle\Entity\Param\ParamTypePeriodeessai $idTypePeriodeessai
     *
     * @return ContratPeriodeessai
     */
    public function setIdTypePeriodeessai(\SalarieBundle\Entity\Param\ParamTypePeriodeessai $idTypePeriodeessai = null)
    {
        $this->idTypePeriodeessai = $idTypePeriodeessai;

        return $this;
    }

    /**
     * Get idTypePeriodeessai
     *
     * @return \SalarieBundle\Entity\Param\ParamTypePeriodeessai
     */
    public function getIdTypePeriodeessai()
    {
        return $this->idTypePeriodeessai;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratPeriodeessai
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ContratPeriodeessai
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }
}
