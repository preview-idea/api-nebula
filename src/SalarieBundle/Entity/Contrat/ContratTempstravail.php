<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratTempstravail
 *
 * @ORM\Table(name="contrat_tempstravail", indexes={@ORM\Index(name="idx_image_contrat_tempstravail", columns={"id_contrat", "dt_maj"}), @ORM\Index(name="IDX_CD4430F4CBC3B464", columns={"id_matricule_maj"}), @ORM\Index(name="IDX_CD4430F4794A3719", columns={"id_fraisdeplacement"}), @ORM\Index(name="IDX_CD4430F464E1201D", columns={"id_decompte_temps_travail"}), @ORM\Index(name="IDX_CD4430F46AF3940B", columns={"id_cycle_temps_partiel"}), @ORM\Index(name="IDX_CD4430F4BEA930E3", columns={"id_contrat"})})
 * @ORM\Entity
 */
class ContratTempstravail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_tempstravail", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_tempstravail_id_ligne_tempstravail_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneTempstravail;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_ticket_resto", type="bit", nullable=false)
     */
    private $isTicketResto;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\Param\ParamFraisdeplacement
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamFraisdeplacement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_fraisdeplacement", referencedColumnName="id_fraisdeplacement")
     * })
     */
    private $idFraisdeplacement;

    /**
     * @var \SalarieBundle\Entity\Param\ParamDecompteTempsTravail
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamDecompteTempsTravail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_decompte_temps_travail", referencedColumnName="id_decompte_temps_travail")
     * })
     */
    private $idDecompteTempsTravail;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCycleTempsPartiel
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCycleTempsPartiel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cycle_temps_partiel", referencedColumnName="id_cycle_temps_partiel")
     * })
     */
    private $idCycleTempsPartiel;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * Get idLigneTempstravail
     *
     * @return integer
     */
    public function getIdLigneTempstravail()
    {
        return $this->idLigneTempstravail;
    }

    /**
     * Set isTicketResto
     *
     * @param bit $isTicketResto
     *
     * @return ContratTempstravail
     */
    public function setIsTicketResto($isTicketResto)
    {
        $this->isTicketResto = $isTicketResto;

        return $this;
    }

    /**
     * Get isTicketResto
     *
     * @return bit
     */
    public function getIsTicketResto()
    {
        return $this->isTicketResto;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratTempstravail
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return ContratTempstravail
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ContratTempstravail
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idFraisdeplacement
     *
     * @param \SalarieBundle\Entity\Param\ParamFraisdeplacement $idFraisdeplacement
     *
     * @return ContratTempstravail
     */
    public function setIdFraisdeplacement(\SalarieBundle\Entity\Param\ParamFraisdeplacement $idFraisdeplacement = null)
    {
        $this->idFraisdeplacement = $idFraisdeplacement;

        return $this;
    }

    /**
     * Get idFraisdeplacement
     *
     * @return \SalarieBundle\Entity\Param\ParamFraisdeplacement
     */
    public function getIdFraisdeplacement()
    {
        return $this->idFraisdeplacement;
    }

    /**
     * Set idDecompteTempsTravail
     *
     * @param \SalarieBundle\Entity\Param\ParamDecompteTempsTravail $idDecompteTempsTravail
     *
     * @return ContratTempstravail
     */
    public function setIdDecompteTempsTravail(\SalarieBundle\Entity\Param\ParamDecompteTempsTravail $idDecompteTempsTravail = null)
    {
        $this->idDecompteTempsTravail = $idDecompteTempsTravail;

        return $this;
    }

    /**
     * Get idDecompteTempsTravail
     *
     * @return \SalarieBundle\Entity\Param\ParamDecompteTempsTravail
     */
    public function getIdDecompteTempsTravail()
    {
        return $this->idDecompteTempsTravail;
    }

    /**
     * Set idCycleTempsPartiel
     *
     * @param \SalarieBundle\Entity\Param\ParamCycleTempsPartiel $idCycleTempsPartiel
     *
     * @return ContratTempstravail
     */
    public function setIdCycleTempsPartiel(\SalarieBundle\Entity\Param\ParamCycleTempsPartiel $idCycleTempsPartiel = null)
    {
        $this->idCycleTempsPartiel = $idCycleTempsPartiel;

        return $this;
    }

    /**
     * Get idCycleTempsPartiel
     *
     * @return \SalarieBundle\Entity\Param\ParamCycleTempsPartiel
     */
    public function getIdCycleTempsPartiel()
    {
        return $this->idCycleTempsPartiel;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratTempstravail
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }
}
