<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratValidation
 *
 * @ORM\Table(name="contrat_validation", indexes={@ORM\Index(name="IDX_AB9AAE05BEA930E3", columns={"id_contrat"}), @ORM\Index(name="IDX_AB9AAE05CBC3B464", columns={"id_matricule_maj"}), @ORM\Index(name="IDX_AB9AAE0581AE0B45", columns={"id_signataire"})})
 * @ORM\Entity
 */
class ContratValidation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_validation", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_validation_id_ligne_validation_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneValidation;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_signature", type="datetime", nullable=false)
     */
    private $dtSignature;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_signataire", referencedColumnName="id_matricule")
     * })
     */
    private $idSignataire;


    /**
     * Get idLigneValidation
     *
     * @return integer
     */
    public function getIdLigneValidation()
    {
        return $this->idLigneValidation;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratValidation
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return ContratValidation
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set dtSignature
     *
     * @param \DateTime $dtSignature
     *
     * @return ContratValidation
     */
    public function setDtSignature($dtSignature)
    {
        $this->dtSignature = $dtSignature;

        return $this;
    }

    /**
     * Get dtSignature
     *
     * @return \DateTime
     */
    public function getDtSignature()
    {
        return $this->dtSignature;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratValidation
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ContratValidation
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idSignataire
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idSignataire
     *
     * @return ContratValidation
     */
    public function setIdSignataire(\SalarieBundle\Entity\Salarie\SalarieInfobase $idSignataire = null)
    {
        $this->idSignataire = $idSignataire;

        return $this;
    }

    /**
     * Get idSignataire
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdSignataire()
    {
        return $this->idSignataire;
    }
}
