<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratInfobase
 *
 * @ORM\Table(name="contrat_infobase", indexes={@ORM\Index(name="idx_image_contrat_infobase", columns={"id_contrat", "id_matricule", "is_actif", "is_avenant"}), @ORM\Index(name="IDX_ECFF64BDC6AA661", columns={"id_matricule_suppr"}), @ORM\Index(name="IDX_ECFF64BD56C4C4C9", columns={"id_motif_suppression_objet"}), @ORM\Index(name="IDX_ECFF64BDFCB19C26", columns={"id_mutuelle_synchro_bd5"}), @ORM\Index(name="IDX_ECFF64BD928760BB", columns={"id_matricule"}), @ORM\Index(name="IDX_ECFF64BDFB4E06AA", columns={"id_banque_emetteur"})})
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Contrat\ContratInfobaseRepository")
 */
class ContratInfobase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_contrat", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_infobase_id_contrat_seq", allocationSize=1, initialValue=1)
     */
    private $idContrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_creation", type="datetime", nullable=false)
     */
    private $dtCreation;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_suppression_objet", type="datetime", nullable=true)
     */
    private $dtSuppressionObjet;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_avenant", type="bit", nullable=false)
     */
    private $isAvenant = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="id_contrat_saphir", type="string", length=9, nullable=true)
     */
    private $idContratSaphir;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_modification", type="datetime", nullable=true)
     */
    private $dtModification;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_modifie", type="smallint", nullable=false)
     */
    private $nbModifie;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_exporte", type="smallint", nullable=false)
     */
    private $nbExporte;

    /**
     * @var string
     *
     * @ORM\Column(name="li_suppression_objet", type="string", length=2, nullable=true)
     */
    private $liSuppressionObjet;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_suppr", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeSuppr;

    /**
     * @var \SalarieBundle\Entity\Param\ParamMotifSuppressionObjet
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamMotifSuppressionObjet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_motif_suppression_objet", referencedColumnName="id_motif_suppression_objet")
     * })
     */
    private $idMotifSuppressionObjet;


    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule", referencedColumnName="id_matricule")
     * })
     */
    private $idMatricule;

    /**
     * @var \SalarieBundle\Entity\Param\ParamBanque
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamBanque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_banque_emetteur", referencedColumnName="id_banque")
     * })
     */
    private $idBanqueEmetteur;

    /**
     * Get idContrat
     *
     * @return integer
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set dtCreation
     *
     * @param \DateTime $dtCreation
     *
     * @return ContratInfobase
     */
    public function setDtCreation($dtCreation)
    {
        $this->dtCreation = $dtCreation;

        return $this;
    }

    /**
     * Get dtCreation
     *
     * @return \DateTime
     */
    public function getDtCreation()
    {
        return $this->dtCreation;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratInfobase
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtSuppressionObjet
     *
     * @param \DateTime $dtSuppressionObjet
     *
     * @return ContratInfobase
     */
    public function setDtSuppressionObjet($dtSuppressionObjet)
    {
        $this->dtSuppressionObjet = $dtSuppressionObjet;

        return $this;
    }

    /**
     * Get dtSuppressionObjet
     *
     * @return \DateTime
     */
    public function getDtSuppressionObjet()
    {
        return $this->dtSuppressionObjet;
    }

    /**
     * Set isAvenant
     *
     * @param bit $isAvenant
     *
     * @return ContratInfobase
     */
    public function setIsAvenant($isAvenant)
    {
        $this->isAvenant = $isAvenant;

        return $this;
    }

    /**
     * Get isAvenant
     *
     * @return bit
     */
    public function getIsAvenant()
    {
        return $this->isAvenant;
    }

    /**
     * Set idContratSaphir
     *
     * @param string $idContratSaphir
     *
     * @return ContratInfobase
     */
    public function setIdContratSaphir($idContratSaphir)
    {
        $this->idContratSaphir = $idContratSaphir;

        return $this;
    }

    /**
     * Get idContratSaphir
     *
     * @return string
     */
    public function getIdContratSaphir()
    {
        return $this->idContratSaphir;
    }

    /**
     * Set dtModification
     *
     * @param \DateTime $dtModification
     *
     * @return ContratInfobase
     */
    public function setDtModification($dtModification)
    {
        $this->dtModification = $dtModification;

        return $this;
    }

    /**
     * Get dtModification
     *
     * @return \DateTime
     */
    public function getDtModification()
    {
        return $this->dtModification;
    }

    /**
     * Set nbModifie
     *
     * @param integer $nbModifie
     *
     * @return ContratInfobase
     */
    public function setNbModifie($nbModifie)
    {
        $this->nbModifie = $nbModifie;

        return $this;
    }

    /**
     * Get nbModifie
     *
     * @return integer
     */
    public function getNbModifie()
    {
        return $this->nbModifie;
    }

    /**
     * Set nbExporte
     *
     * @param integer $nbExporte
     *
     * @return ContratInfobase
     */
    public function setNbExporte($nbExporte)
    {
        $this->nbExporte = $nbExporte;

        return $this;
    }

    /**
     * Get nbExporte
     *
     * @return integer
     */
    public function getNbExporte()
    {
        return $this->nbExporte;
    }

    /**
     * Set liSuppressionObjet
     *
     * @param string $liSuppressionObjet
     *
     * @return ContratInfobase
     */
    public function setLiSuppressionObjet($liSuppressionObjet)
    {
        $this->liSuppressionObjet = $liSuppressionObjet;

        return $this;
    }

    /**
     * Get liSuppressionObjet
     *
     * @return string
     */
    public function getLiSuppressionObjet()
    {
        return $this->liSuppressionObjet;
    }

    /**
     * Set idMatriculeSuppr
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeSuppr
     *
     * @return ContratInfobase
     */
    public function setIdMatriculeSuppr(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeSuppr = null)
    {
        $this->idMatriculeSuppr = $idMatriculeSuppr;

        return $this;
    }

    /**
     * Get idMatriculeSuppr
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeSuppr()
    {
        return $this->idMatriculeSuppr;
    }

    /**
     * Set idMotifSuppressionObjet
     *
     * @param \SalarieBundle\Entity\Param\ParamMotifSuppressionObjet $idMotifSuppressionObjet
     *
     * @return ContratInfobase
     */
    public function setIdMotifSuppressionObjet(\SalarieBundle\Entity\Param\ParamMotifSuppressionObjet $idMotifSuppressionObjet = null)
    {
        $this->idMotifSuppressionObjet = $idMotifSuppressionObjet;

        return $this;
    }

    /**
     * Get idMotifSuppressionObjet
     *
     * @return \SalarieBundle\Entity\Param\ParamMotifSuppressionObjet
     */
    public function getIdMotifSuppressionObjet()
    {
        return $this->idMotifSuppressionObjet;
    }

    /**
     * Set idMatricule
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatricule
     *
     * @return ContratInfobase
     */
    public function setIdMatricule(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatricule = null)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set idBanqueEmetteur
     *
     * @param \SalarieBundle\Entity\Param\ParamBanque $idBanqueEmetteur
     *
     * @return ContratInfobase
     */
    public function setIdBanqueEmetteur(\SalarieBundle\Entity\Param\ParamBanque $idBanqueEmetteur = null)
    {
        $this->idBanqueEmetteur = $idBanqueEmetteur;

        return $this;
    }

    /**
     * Get idBanqueEmetteur
     *
     * @return \SalarieBundle\Entity\Param\ParamBanque
     */
    public function getIdBanqueEmetteur()
    {
        return $this->idBanqueEmetteur;
    }
}
