<?php

namespace SalarieBundle\Entity\Contrat;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratClausesadditionnelles
 *
 * @ORM\Table(name="contrat_clausesadditionnelles", indexes={@ORM\Index(name="IDX_4240B12353345E1E", columns={"id_annees_etude"}), @ORM\Index(name="IDX_4240B123BEA930E3", columns={"id_contrat"}), @ORM\Index(name="IDX_4240B123CBC3B464", columns={"id_matricule_maj"}), @ORM\Index(name="IDX_4240B1232BDAA9C6", columns={"id_qualif_ancien_employeur"}), @ORM\Index(name="IDX_4240B123D259D582", columns={"id_categorieemploye_ancien_employeur"}), @ORM\Index(name="IDX_4240B123AF37AEB1", columns={"id_coeffcontrat_ancien_employeur"}), @ORM\Index(name="IDX_4240B123B1BB0CEC", columns={"id_matricule_remplacement"}), @ORM\Index(name="IDX_4240B123B9B85A93", columns={"id_matricule_ancien_titulaire"}), @ORM\Index(name="IDX_4240B123A7FC728C", columns={"id_coeff_apres_pe"}), @ORM\Index(name="IDX_4240B1236583ECED", columns={"id_motif_tempspartiel"}), @ORM\Index(name="IDX_4240B123E0AC57C9", columns={"id_type_diplome"})})
 * @ORM\Entity
 */
class ContratClausesadditionnelles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_clausesadditionnelles", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_clausesadditionnelles_id_ligne_clausesadditionnelles_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneClausesadditionnelles;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var string
     *
     * @ORM\Column(name="li_societe_ancien_employeur", type="string", length=20, nullable=true)
     */
    private $liSocieteAncienEmployeur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_embauche_ancien_employeur", type="date", nullable=true)
     */
    private $dtEmbaucheAncienEmployeur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_signature_contrat_ancien_employeur", type="date", nullable=true)
     */
    private $dtSignatureContratAncienEmployeur;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_site_repris", type="string", length=30, nullable=true)
     */
    private $liNomSiteRepris;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_salarie_protege", type="bit", nullable=true)
     */
    private $isSalarieProtege;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_heures", type="decimal", precision=4, scale=1, nullable=true)
     */
    private $nbHeures;

    /**
     * @var string
     *
     * @ORM\Column(name="li_intitule_diplome", type="string", length=60, nullable=true)
     */
    private $liIntituleDiplome;

    /**
     * @var string
     *
     * @ORM\Column(name="li_motif_personne_remplacement", type="string", length=100, nullable=true)
     */
    private $liMotifPersonneRemplacement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_motif_surcroit", type="string", length=100, nullable=true)
     */
    private $liMotifSurcroit;

    /**
     * @var string
     *
     * @ORM\Column(name="li_motif_contrat_saisonnier", type="string", length=100, nullable=true)
     */
    private $liMotifContratSaisonnier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_embauche_remplacant_cdi", type="date", nullable=true)
     */
    private $dtEmbaucheRemplacantCdi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_courrier_demande_tp", type="date", nullable=true)
     */
    private $dtCourrierDemandeTp;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_clausesadditionnelles", type="integer", nullable=false)
     */
    private $idClausesadditionnelles;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nivechpos_ancien_employeur", type="string", length=6, nullable=true)
     */
    private $liNivechposAncienEmployeur;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCycleFormation
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCycleFormation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_annees_etude", referencedColumnName="id_cycle_formation")
     * })
     */
    private $idAnneesEtude;

    /**
     * @var \SalarieBundle\Entity\Contrat\ContratInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Contrat\ContratInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat")
     * })
     */
    private $idContrat;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\Param\ParamQualifcontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamQualifcontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_qualif_ancien_employeur", referencedColumnName="id_qualifcontrat")
     * })
     */
    private $idQualifAncienEmployeur;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCategorieemploye
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCategorieemploye")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_categorieemploye_ancien_employeur", referencedColumnName="id_categorieemploye")
     * })
     */
    private $idCategorieemployeAncienEmployeur;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCoeffcontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCoeffcontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_coeffcontrat_ancien_employeur", referencedColumnName="id_coeffcontrat")
     * })
     */
    private $idCoeffcontratAncienEmployeur;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_remplacement", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeRemplacement;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_ancien_titulaire", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeAncienTitulaire;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCoeffcontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCoeffcontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_coeff_apres_pe", referencedColumnName="id_coeffcontrat")
     * })
     */
    private $idCoeffApresPe;

    /**
     * @var \SalarieBundle\Entity\Param\ParamMotifTempspartiel
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamMotifTempspartiel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_motif_tempspartiel", referencedColumnName="id_motif_tempspartiel")
     * })
     */
    private $idMotifTempspartiel;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeDiplome
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeDiplome")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_diplome", referencedColumnName="id_type_diplome")
     * })
     */
    private $idTypeDiplome;

    /**
     * Get idLigneClausesadditionnelles
     *
     * @return integer
     */
    public function getIdLigneClausesadditionnelles()
    {
        return $this->idLigneClausesadditionnelles;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return ContratClausesadditionnelles
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return ContratClausesadditionnelles
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set liSocieteAncienEmployeur
     *
     * @param string $liSocieteAncienEmployeur
     *
     * @return ContratClausesadditionnelles
     */
    public function setLiSocieteAncienEmployeur($liSocieteAncienEmployeur)
    {
        $this->liSocieteAncienEmployeur = $liSocieteAncienEmployeur;

        return $this;
    }

    /**
     * Get liSocieteAncienEmployeur
     *
     * @return string
     */
    public function getLiSocieteAncienEmployeur()
    {
        return $this->liSocieteAncienEmployeur;
    }

    /**
     * Set dtEmbaucheAncienEmployeur
     *
     * @param \DateTime $dtEmbaucheAncienEmployeur
     *
     * @return ContratClausesadditionnelles
     */
    public function setDtEmbaucheAncienEmployeur($dtEmbaucheAncienEmployeur)
    {
        $this->dtEmbaucheAncienEmployeur = $dtEmbaucheAncienEmployeur;

        return $this;
    }

    /**
     * Get dtEmbaucheAncienEmployeur
     *
     * @return \DateTime
     */
    public function getDtEmbaucheAncienEmployeur()
    {
        return $this->dtEmbaucheAncienEmployeur;
    }

    /**
     * Set dtSignatureContratAncienEmployeur
     *
     * @param \DateTime $dtSignatureContratAncienEmployeur
     *
     * @return ContratClausesadditionnelles
     */
    public function setDtSignatureContratAncienEmployeur($dtSignatureContratAncienEmployeur)
    {
        $this->dtSignatureContratAncienEmployeur = $dtSignatureContratAncienEmployeur;

        return $this;
    }

    /**
     * Get dtSignatureContratAncienEmployeur
     *
     * @return \DateTime
     */
    public function getDtSignatureContratAncienEmployeur()
    {
        return $this->dtSignatureContratAncienEmployeur;
    }

    /**
     * Set liNomSiteRepris
     *
     * @param string $liNomSiteRepris
     *
     * @return ContratClausesadditionnelles
     */
    public function setLiNomSiteRepris($liNomSiteRepris)
    {
        $this->liNomSiteRepris = $liNomSiteRepris;

        return $this;
    }

    /**
     * Get liNomSiteRepris
     *
     * @return string
     */
    public function getLiNomSiteRepris()
    {
        return $this->liNomSiteRepris;
    }

    /**
     * Set isSalarieProtege
     *
     * @param bit $isSalarieProtege
     *
     * @return ContratClausesadditionnelles
     */
    public function setIsSalarieProtege($isSalarieProtege)
    {
        $this->isSalarieProtege = $isSalarieProtege;

        return $this;
    }

    /**
     * Get isSalarieProtege
     *
     * @return bit
     */
    public function getIsSalarieProtege()
    {
        return $this->isSalarieProtege;
    }

    /**
     * Set nbHeures
     *
     * @param string $nbHeures
     *
     * @return ContratClausesadditionnelles
     */
    public function setNbHeures($nbHeures)
    {
        $this->nbHeures = $nbHeures;

        return $this;
    }

    /**
     * Get nbHeures
     *
     * @return string
     */
    public function getNbHeures()
    {
        return $this->nbHeures;
    }

    /**
     * Set liIntituleDiplome
     *
     * @param string $liIntituleDiplome
     *
     * @return ContratClausesadditionnelles
     */
    public function setLiIntituleDiplome($liIntituleDiplome)
    {
        $this->liIntituleDiplome = $liIntituleDiplome;

        return $this;
    }

    /**
     * Get liIntituleDiplome
     *
     * @return string
     */
    public function getLiIntituleDiplome()
    {
        return $this->liIntituleDiplome;
    }

    /**
     * Set liMotifPersonneRemplacement
     *
     * @param string $liMotifPersonneRemplacement
     *
     * @return ContratClausesadditionnelles
     */
    public function setLiMotifPersonneRemplacement($liMotifPersonneRemplacement)
    {
        $this->liMotifPersonneRemplacement = $liMotifPersonneRemplacement;

        return $this;
    }

    /**
     * Get liMotifPersonneRemplacement
     *
     * @return string
     */
    public function getLiMotifPersonneRemplacement()
    {
        return $this->liMotifPersonneRemplacement;
    }

    /**
     * Set liMotifSurcroit
     *
     * @param string $liMotifSurcroit
     *
     * @return ContratClausesadditionnelles
     */
    public function setLiMotifSurcroit($liMotifSurcroit)
    {
        $this->liMotifSurcroit = $liMotifSurcroit;

        return $this;
    }

    /**
     * Get liMotifSurcroit
     *
     * @return string
     */
    public function getLiMotifSurcroit()
    {
        return $this->liMotifSurcroit;
    }

    /**
     * Set liMotifContratSaisonnier
     *
     * @param string $liMotifContratSaisonnier
     *
     * @return ContratClausesadditionnelles
     */
    public function setLiMotifContratSaisonnier($liMotifContratSaisonnier)
    {
        $this->liMotifContratSaisonnier = $liMotifContratSaisonnier;

        return $this;
    }

    /**
     * Get liMotifContratSaisonnier
     *
     * @return string
     */
    public function getLiMotifContratSaisonnier()
    {
        return $this->liMotifContratSaisonnier;
    }

    /**
     * Set dtEmbaucheRemplacantCdi
     *
     * @param \DateTime $dtEmbaucheRemplacantCdi
     *
     * @return ContratClausesadditionnelles
     */
    public function setDtEmbaucheRemplacantCdi($dtEmbaucheRemplacantCdi)
    {
        $this->dtEmbaucheRemplacantCdi = $dtEmbaucheRemplacantCdi;

        return $this;
    }

    /**
     * Get dtEmbaucheRemplacantCdi
     *
     * @return \DateTime
     */
    public function getDtEmbaucheRemplacantCdi()
    {
        return $this->dtEmbaucheRemplacantCdi;
    }

    /**
     * Set dtCourrierDemandeTp
     *
     * @param \DateTime $dtCourrierDemandeTp
     *
     * @return ContratClausesadditionnelles
     */
    public function setDtCourrierDemandeTp($dtCourrierDemandeTp)
    {
        $this->dtCourrierDemandeTp = $dtCourrierDemandeTp;

        return $this;
    }

    /**
     * Get dtCourrierDemandeTp
     *
     * @return \DateTime
     */
    public function getDtCourrierDemandeTp()
    {
        return $this->dtCourrierDemandeTp;
    }

    /**
     * Set idClausesadditionnelles
     *
     * @param integer $idClausesadditionnelles
     *
     * @return ContratClausesadditionnelles
     */
    public function setIdClausesadditionnelles($idClausesadditionnelles)
    {
        $this->idClausesadditionnelles = $idClausesadditionnelles;

        return $this;
    }

    /**
     * Get idClausesadditionnelles
     *
     * @return integer
     */
    public function getIdClausesadditionnelles()
    {
        return $this->idClausesadditionnelles;
    }

    /**
     * Set idAnneesEtude
     *
     * @param \SalarieBundle\Entity\Param\ParamCycleFormation $idAnneesEtude
     *
     * @return ContratClausesadditionnelles
     */
    public function setIdAnneesEtude(\SalarieBundle\Entity\Param\ParamCycleFormation $idAnneesEtude = null)
    {
        $this->idAnneesEtude = $idAnneesEtude;

        return $this;
    }

    /**
     * Get idAnneesEtude
     *
     * @return \SalarieBundle\Entity\Param\ParamCycleFormation
     */
    public function getIdAnneesEtude()
    {
        return $this->idAnneesEtude;
    }

    /**
     * Set idContrat
     *
     * @param \SalarieBundle\Entity\Contrat\ContratInfobase $idContrat
     *
     * @return ContratClausesadditionnelles
     */
    public function setIdContrat(\SalarieBundle\Entity\Contrat\ContratInfobase $idContrat = null)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat
     *
     * @return \SalarieBundle\Entity\Contrat\ContratInfobase
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ContratClausesadditionnelles
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idQualifAncienEmployeur
     *
     * @param \SalarieBundle\Entity\Param\ParamQualifcontrat $idQualifAncienEmployeur
     *
     * @return ContratClausesadditionnelles
     */
    public function setIdQualifAncienEmployeur(\SalarieBundle\Entity\Param\ParamQualifcontrat $idQualifAncienEmployeur = null)
    {
        $this->idQualifAncienEmployeur = $idQualifAncienEmployeur;

        return $this;
    }

    /**
     * Get idQualifAncienEmployeur
     *
     * @return \SalarieBundle\Entity\Param\ParamQualifcontrat
     */
    public function getIdQualifAncienEmployeur()
    {
        return $this->idQualifAncienEmployeur;
    }

    /**
     * Set idCategorieemployeAncienEmployeur
     *
     * @param \SalarieBundle\Entity\Param\ParamCategorieemploye $idCategorieemployeAncienEmployeur
     *
     * @return ContratClausesadditionnelles
     */
    public function setIdCategorieemployeAncienEmployeur(\SalarieBundle\Entity\Param\ParamCategorieemploye $idCategorieemployeAncienEmployeur = null)
    {
        $this->idCategorieemployeAncienEmployeur = $idCategorieemployeAncienEmployeur;

        return $this;
    }

    /**
     * Get idCategorieemployeAncienEmployeur
     *
     * @return \SalarieBundle\Entity\Param\ParamCategorieemploye
     */
    public function getIdCategorieemployeAncienEmployeur()
    {
        return $this->idCategorieemployeAncienEmployeur;
    }

    /**
     * Set idCoeffcontratAncienEmployeur
     *
     * @param \SalarieBundle\Entity\Param\ParamCoeffcontrat $idCoeffcontratAncienEmployeur
     *
     * @return ContratClausesadditionnelles
     */
    public function setIdCoeffcontratAncienEmployeur(\SalarieBundle\Entity\Param\ParamCoeffcontrat $idCoeffcontratAncienEmployeur = null)
    {
        $this->idCoeffcontratAncienEmployeur = $idCoeffcontratAncienEmployeur;

        return $this;
    }

    /**
     * Get idCoeffcontratAncienEmployeur
     *
     * @return \SalarieBundle\Entity\Param\ParamCoeffcontrat
     */
    public function getIdCoeffcontratAncienEmployeur()
    {
        return $this->idCoeffcontratAncienEmployeur;
    }

    /**
     * Set idMatriculeRemplacement
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeRemplacement
     *
     * @return ContratClausesadditionnelles
     */
    public function setIdMatriculeRemplacement(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeRemplacement = null)
    {
        $this->idMatriculeRemplacement = $idMatriculeRemplacement;

        return $this;
    }

    /**
     * Get idMatriculeRemplacement
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeRemplacement()
    {
        return $this->idMatriculeRemplacement;
    }

    /**
     * Set idMatriculeAncienTitulaire
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeAncienTitulaire
     *
     * @return ContratClausesadditionnelles
     */
    public function setIdMatriculeAncienTitulaire(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeAncienTitulaire = null)
    {
        $this->idMatriculeAncienTitulaire = $idMatriculeAncienTitulaire;

        return $this;
    }

    /**
     * Get idMatriculeAncienTitulaire
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeAncienTitulaire()
    {
        return $this->idMatriculeAncienTitulaire;
    }

    /**
     * Set idCoeffApresPe
     *
     * @param \SalarieBundle\Entity\Param\ParamCoeffcontrat $idCoeffApresPe
     *
     * @return ContratClausesadditionnelles
     */
    public function setIdCoeffApresPe(\SalarieBundle\Entity\Param\ParamCoeffcontrat $idCoeffApresPe = null)
    {
        $this->idCoeffApresPe = $idCoeffApresPe;

        return $this;
    }

    /**
     * Get idCoeffApresPe
     *
     * @return \SalarieBundle\Entity\Param\ParamCoeffcontrat
     */
    public function getIdCoeffApresPe()
    {
        return $this->idCoeffApresPe;
    }

    /**
     * Set idMotifTempspartiel
     *
     * @param \SalarieBundle\Entity\Param\ParamMotifTempspartiel $idMotifTempspartiel
     *
     * @return ContratClausesadditionnelles
     */
    public function setidMotifTempspartiel(\SalarieBundle\Entity\Param\ParamMotifTempspartiel $idMotifTempspartiel = null)
    {
        $this->idMotifTempspartiel = $idMotifTempspartiel;

        return $this;
    }

    /**
     * Get idMotifTempspartiel
     *
     * @return \SalarieBundle\Entity\Param\ParamMotifTempspartiel
     */
    public function getidMotifTempspartiel()
    {
        return $this->idMotifTempspartiel;
    }

    /**
     * Set idTypeDiplome
     *
     * @param \SalarieBundle\Entity\Param\ParamTypeDiplome $idTypeDiplome
     *
     * @return ContratClausesadditionnelles
     */
    public function setIdTypeDiplome(\SalarieBundle\Entity\Param\ParamTypeDiplome $idTypeDiplome = null)
    {
        $this->idTypeDiplome = $idTypeDiplome;

        return $this;
    }

    /**
     * Get idTypeDiplome
     *
     * @return \SalarieBundle\Entity\Param\ParamTypeDiplome
     */
    public function getIdTypeDiplome()
    {
        return $this->idTypeDiplome;
    }

    /**
     * Set liNivechposAncienEmployeur
     *
     * @param string $liNivechposAncienEmployeur
     *
     * @return ContratClausesadditionnelles
     */
    public function setLiNivechposAncienEmployeur($liNivechposAncienEmployeur)
    {
        $this->liNivechposAncienEmployeur = $liNivechposAncienEmployeur;

        return $this;
    }

    /**
     * Get liNivechposAncienEmployeur
     *
     * @return string
     */
    public function getLiNivechposAncienEmployeur()
    {
        return $this->liNivechposAncienEmployeur;
    }
}
