<?php

namespace SalarieBundle\Entity\Param;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParamActivite
 *
 * @ORM\Table(name="param_activite")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Param\ParamActiviteRepository")
 */
class ParamActivite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_activite", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="param_activite_id_activite_seq", allocationSize=1, initialValue=1)
     */
    private $idActivite;

    /**
     * @var string
     *
     * @ORM\Column(name="li_activite", type="string", nullable=false)
     */
    private $liActivite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_actif", type="date", nullable=false)
     */
    private $dtDebutActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_actif", type="date", nullable=true)
     */
    private $dtFinActif;

    /**
     * @var string
     *
     * @ORM\Column(name="id_activite_ax", type="string", length=3, nullable=false)
     */
    private $idActiviteAx;

    /**
     * Get idActivite
     *
     * @return integer
     */
    public function getIdActivite()
    {
        return $this->idActivite;
    }

    /**
     * Set liActivite
     *
     * @param string $liActivite
     *
     * @return ParamActivite
     */
    public function setLiActivite($liActivite)
    {
        $this->liActivite = $liActivite;

        return $this;
    }

    /**
     * Get liActivite
     *
     * @return string
     */
    public function getLiActivite()
    {
        return $this->liActivite;
    }

    /**
     * Set dtDebutActif
     *
     * @param \DateTime $dtDebutActif
     *
     * @return ParamActivite
     */
    public function setDtDebutActif($dtDebutActif)
    {
        $this->dtDebutActif = $dtDebutActif;

        return $this;
    }

    /**
     * Get dtDebutActif
     *
     * @return \DateTime
     */
    public function getDtDebutActif()
    {
        return $this->dtDebutActif;
    }

    /**
     * Set dtFinActif
     *
     * @param \DateTime $dtFinActif
     *
     * @return ParamActivite
     */
    public function setDtFinActif($dtFinActif)
    {
        $this->dtFinActif = $dtFinActif;

        return $this;
    }

    /**
     * Get dtFinActif
     *
     * @return \DateTime
     */
    public function getDtFinActif()
    {
        return $this->dtFinActif;
    }

    /**
     * Set idActiviteAx
     *
     * @param string $idActiviteAx
     *
     * @return ParamActivite
     */
    public function setIdActiviteAx($idActiviteAx)
    {
        $this->idActiviteAx = $idActiviteAx;

        return $this;
    }

    /**
     * Get idActiviteAx
     *
     * @return string
     */
    public function getIdActiviteAx()
    {
        return $this->idActiviteAx;
    }
}
