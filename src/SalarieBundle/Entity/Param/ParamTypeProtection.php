<?php

namespace SalarieBundle\Entity\Param;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParamTypeProtection
 *
 * @ORM\Table(name="param_type_protection")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Param\ParamTypeProtectionRepository")
 */
class ParamTypeProtection
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_type_protection", type="smallint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="param_type_protection_id_type_protection_seq", allocationSize=1, initialValue=1)
     */
    private $idTypeProtection;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_protection", type="string", length=30, nullable=false)
     */
    private $liTypeProtection;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_perimetre_electoral", type="bit", nullable=false)
     */
    private $isPerimetreElectoral;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_org_syndicale", type="bit", nullable=false)
     */
    private $isOrgSyndicale;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_sans_etiquette", type="bit", nullable=false)
     */
    private $isSansEtiquette;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_order", type="integer", nullable=true)
     */
    private $nbOrder;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_actif", type="date", nullable=false)
     */
    private $dtDebutActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_actif", type="date", nullable=true)
     */
    private $dtFinActif;

    /**
     * Get idTypeProtection
     *
     * @return integer
     */
    public function getIdTypeProtection()
    {
        return $this->idTypeProtection;
    }

    /**
     * Set liTypeProtection
     *
     * @param string $liTypeProtection
     *
     * @return ParamTypeProtection
     */
    public function setLiTypeProtection($liTypeProtection)
    {
        $this->liTypeProtection = $liTypeProtection;

        return $this;
    }

    /**
     * Get liTypeProtection
     *
     * @return string
     */
    public function getLiTypeProtection()
    {
        return $this->liTypeProtection;
    }

    /**
     * Set isPerimetreElectoral
     *
     * @param bit $isPerimetreElectoral
     *
     * @return ParamTypeProtection
     */
    public function setIsPerimetreElectoral($isPerimetreElectoral)
    {
        $this->isPerimetreElectoral = $isPerimetreElectoral;

        return $this;
    }

    /**
     * Get isPerimetreElectoral
     *
     * @return bit
     */
    public function getIsPerimetreElectoral()
    {
        return $this->isPerimetreElectoral;
    }

    /**
     * Set isOrgSyndicale
     *
     * @param bit $isOrgSyndicale
     *
     * @return ParamTypeProtection
     */
    public function setIsOrgSyndicale($isOrgSyndicale)
    {
        $this->isOrgSyndicale = $isOrgSyndicale;

        return $this;
    }

    /**
     * Get isOrgSyndicale
     *
     * @return bit
     */
    public function getIsOrgSyndicale()
    {
        return $this->isOrgSyndicale;
    }

    /**
     * Set isSansEtiquette
     *
     * @param bit $isSansEtiquette
     *
     * @return ParamTypeProtection
     */
    public function setIsSansEtiquette($isSansEtiquette)
    {
        $this->isSansEtiquette = $isSansEtiquette;

        return $this;
    }

    /**
     * Get isSansEtiquette
     *
     * @return bit
     */
    public function getIsSansEtiquette()
    {
        return $this->isSansEtiquette;
    }

    /**
     * Set nbOrder
     *
     * @param integer $nbOrder
     *
     * @return ParamTypeProtection
     */
    public function setNbOrder($nbOrder)
    {
        $this->nbOrder = $nbOrder;

        return $this;
    }

    /**
     * Get nbOrder
     *
     * @return integer
     */
    public function getNbOrder()
    {
        return $this->nbOrder;
    }

    /**
     * Set dtDebutActif
     *
     * @param \DateTime $dtDebutActif
     *
     * @return ParamTypeProtection
     */
    public function setDtDebutActif($dtDebutActif)
    {
        $this->dtDebutActif = $dtDebutActif;

        return $this;
    }

    /**
     * Get dtDebutActif
     *
     * @return \DateTime
     */
    public function getDtDebutActif()
    {
        return $this->dtDebutActif;
    }

    /**
     * Set dtFinActif
     *
     * @param \DateTime $dtFinActif
     *
     * @return ParamTypeProtection
     */
    public function setDtFinActif($dtFinActif)
    {
        $this->dtFinActif = $dtFinActif;

        return $this;
    }

    /**
     * Get dtFinActif
     *
     * @return \DateTime
     */
    public function getDtFinActif()
    {
        return $this->dtFinActif;
    }
}
