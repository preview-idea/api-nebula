<?php

namespace SalarieBundle\Entity\Param;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParamTypeTransfert
 *
 * @ORM\Table(name="param_type_transfert")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Param\ParamTypeTransfertRepository")
 */
class ParamTypeTransfert
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_type_transfert", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="param_type_transfert_id_type_transfert_seq", allocationSize=1, initialValue=1)
     */
    private $idTypeTransfert;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_transfert", type="string", length=30, nullable=false)
     */
    private $liTypeTransfert;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_actif", type="date", nullable=false)
     */
    private $dtDebutActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_actif", type="date", nullable=true)
     */
    private $dtFinActif;

    /**
     * Get idTypeTransfert
     *
     * @return integer
     */
    public function getIdTypeTransfert()
    {
        return $this->idTypeTransfert;
    }

    /**
     * Set liTypeTransfert
     *
     * @param string $liTypeTransfert
     *
     * @return ParamTypeTransfert
     */
    public function setLiTypeTransfert($liTypeTransfert)
    {
        $this->liTypeTransfert = $liTypeTransfert;

        return $this;
    }

    /**
     * Get liTypeTransfert
     *
     * @return string
     */
    public function getLiTypeTransfert()
    {
        return $this->liTypeTransfert;
    }

    /**
     * Set dtDebutActif
     *
     * @param \DateTime $dtDebutActif
     *
     * @return ParamTypeTransfert
     */
    public function setDtDebutActif($dtDebutActif)
    {
        $this->dtDebutActif = $dtDebutActif;

        return $this;
    }

    /**
     * Get dtDebutActif
     *
     * @return \DateTime
     */
    public function getDtDebutActif()
    {
        return $this->dtDebutActif;
    }

    /**
     * Set dtFinActif
     *
     * @param \DateTime $dtFinActif
     *
     * @return ParamTypeTransfert
     */
    public function setDtFinActif($dtFinActif)
    {
        $this->dtFinActif = $dtFinActif;

        return $this;
    }

    /**
     * Get dtFinActif
     *
     * @return \DateTime
     */
    public function getDtFinActif()
    {
        return $this->dtFinActif;
    }
}
