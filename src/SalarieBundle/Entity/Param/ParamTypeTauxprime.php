<?php

namespace SalarieBundle\Entity\Param;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParamTypeTauxprime
 *
 * @ORM\Table(name="param_type_tauxprime")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Param\ParamTypeTauxPrimeRepository")
 */
class ParamTypeTauxprime
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_type_tauxprime", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="param_type_tauxprime_seq", allocationSize=1, initialValue=1)
     */
    private $idTypeTauxprime;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_tauxprime", type="string", length=12, nullable=false)
     */
    private $liTypeTauxprime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_actif", type="date", nullable=false)
     */
    private $dtDebutActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_actif", type="date", nullable=true)
     */
    private $dtFinActif;

    /**
     * Get idTypeTauxprime
     *
     * @return integer
     */
    public function getIdTypeTauxprime()
    {
        return $this->idTypeTauxprime;
    }

    /**
     * Set liTypeTauxprime
     *
     * @param string $liTypeTauxprime
     *
     * @return ParamTypeTauxprime
     */
    public function setLiTypeTauxprime($liTypeTauxprime)
    {
        $this->liTypeTauxprime = $liTypeTauxprime;

        return $this;
    }

    /**
     * Get liTypeTauxprime
     *
     * @return string
     */
    public function getLiTypeTauxprime()
    {
        return $this->liTypeTauxprime;
    }

    /**
     * Set dtDebutActif
     *
     * @param \DateTime $dtDebutActif
     *
     * @return ParamTypeTauxprime
     */
    public function setDtDebutActif($dtDebutActif)
    {
        $this->dtDebutActif = $dtDebutActif;

        return $this;
    }

    /**
     * Get dtDebutActif
     *
     * @return \DateTime
     */
    public function getDtDebutActif()
    {
        return $this->dtDebutActif;
    }

    /**
     * Set dtFinActif
     *
     * @param \DateTime $dtFinActif
     *
     * @return ParamTypeTauxprime
     */
    public function setDtFinActif($dtFinActif)
    {
        $this->dtFinActif = $dtFinActif;

        return $this;
    }

    /**
     * Get dtFinActif
     *
     * @return \DateTime
     */
    public function getDtFinActif()
    {
        return $this->dtFinActif;
    }
}
