<?php

namespace SalarieBundle\Entity\Param;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParamTypePriseCharge
 *
 * @ORM\Table(name="param_type_prise_charge")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Param\ParamTypePriseChargeRepository")
 */
class ParamTypePriseCharge
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_type_prise_charge", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="param_type_prise_charge_id_type_prise_charge_seq", allocationSize=1, initialValue=1)
     */
    private $idTypePriseCharge;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_prise_charge", type="string", length=30, nullable=false)
     */
    private $liTypePriseCharge;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_debut_actif", type="date", nullable=false)
     */
    private $dtDebutActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fin_actif", type="date", nullable=true)
     */
    private $dtFinActif;

    /**
     * Get idTypePriseCharge
     *
     * @return integer
     */
    public function getIdTypePriseCharge()
    {
        return $this->idTypePriseCharge;
    }

    /**
     * Set liTypePriseCharge
     *
     * @param string $liTypePriseCharge
     *
     * @return ParamTypePriseCharge
     */
    public function setLiTypePriseCharge($liTypePriseCharge)
    {
        $this->liTypePriseCharge = $liTypePriseCharge;

        return $this;
    }

    /**
     * Get liTypePriseCharge
     *
     * @return string
     */
    public function getLiTypePriseCharge()
    {
        return $this->liTypePriseCharge;
    }

    /**
     * Set dtDebutActif
     *
     * @param \DateTime $dtDebutActif
     *
     * @return ParamTypePriseCharge
     */
    public function setDtDebutActif($dtDebutActif)
    {
        $this->dtDebutActif = $dtDebutActif;

        return $this;
    }

    /**
     * Get dtDebutActif
     *
     * @return \DateTime
     */
    public function getDtDebutActif()
    {
        return $this->dtDebutActif;
    }

    /**
     * Set dtFinActif
     *
     * @param \DateTime $dtFinActif
     *
     * @return ParamTypePriseCharge
     */
    public function setDtFinActif($dtFinActif)
    {
        $this->dtFinActif = $dtFinActif;

        return $this;
    }

    /**
     * Get dtFinActif
     *
     * @return \DateTime
     */
    public function getDtFinActif()
    {
        return $this->dtFinActif;
    }
}
