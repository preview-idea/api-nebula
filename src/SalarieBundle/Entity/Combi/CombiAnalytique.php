<?php

namespace SalarieBundle\Entity\Combi;

use Doctrine\ORM\Mapping as ORM;

/**
 * CombiAnalytique
 *
 * @ORM\Table(name="combi_analytique", uniqueConstraints={@ORM\UniqueConstraint(name="combi_analytique_ukey", columns={"id_positionnementposte", "id_bu", "id_region", "id_agence", "id_activite"})}, indexes={@ORM\Index(name="IDX_47901002911DCB20", columns={"id_positionnementposte"}), @ORM\Index(name="IDX_47901002F600676E", columns={"id_bu"}), @ORM\Index(name="IDX_479010022955449B", columns={"id_region"}), @ORM\Index(name="IDX_4790100242F62F44", columns={"id_agence"}), @ORM\Index(name="IDX_47901002E8AEB980", columns={"id_activite"}), @ORM\Index(name="IDX_479010027797B861", columns={"id_metier"})})
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Combi\CombiAnalytiqueRepository")
 */
class CombiAnalytique
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_analytique", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="combi_analytique_id_ligne_analytique_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneAnalytique;

    /**
     * @var \SalarieBundle\Entity\Param\ParamPositionnementposte
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamPositionnementposte")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_positionnementposte", referencedColumnName="id_positionnementposte")
     * })
     */
    private $idPositionnementposte;

    /**
     * @var \SalarieBundle\Entity\Param\ParamBu
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamBu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_bu", referencedColumnName="id_bu")
     * })
     */
    private $idBu;

    /**
     * @var \SalarieBundle\Entity\Param\ParamRegion
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_region", referencedColumnName="id_region")
     * })
     */
    private $idRegion;

    /**
     * @var \SalarieBundle\Entity\Param\ParamAgence
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamAgence")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_agence", referencedColumnName="id_agence")
     * })
     */
    private $idAgence;

    /**
     * @var \SalarieBundle\Entity\Param\ParamActivite
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamActivite")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_activite", referencedColumnName="id_activite")
     * })
     */
    private $idActivite;

    /**
     * @var \SalarieBundle\Entity\Param\ParamMetier
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamMetier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_metier", referencedColumnName="id_metier")
     * })
     */
    private $idMetier;

    /**
     * @return int
     */
    public function getIdLigneAnalytique()
    {
        return $this->idLigneAnalytique;
    }

    /**
     * @param int $idLigneAnalytique
     * @return CombiAnalytique
     */
    public function setIdLigneAnalytique($idLigneAnalytique)
    {
        $this->idLigneAnalytique = $idLigneAnalytique;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamPositionnementposte
     */
    public function getIdPositionnementposte()
    {
        return $this->idPositionnementposte;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamPositionnementposte $idPositionnementposte
     * @return CombiAnalytique
     */
    public function setIdPositionnementposte($idPositionnementposte)
    {
        $this->idPositionnementposte = $idPositionnementposte;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamBu
     */
    public function getIdBu()
    {
        return $this->idBu;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamBu $idBu
     * @return CombiAnalytique
     */
    public function setIdBu($idBu)
    {
        $this->idBu = $idBu;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamRegion
     */
    public function getIdRegion()
    {
        return $this->idRegion;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamRegion $idRegion
     * @return CombiAnalytique
     */
    public function setIdRegion($idRegion)
    {
        $this->idRegion = $idRegion;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamAgence
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamAgence $idAgence
     * @return CombiAnalytique
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamActivite
     */
    public function getIdActivite()
    {
        return $this->idActivite;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamActivite $idActivite
     * @return CombiAnalytique
     */
    public function setIdActivite($idActivite)
    {
        $this->idActivite = $idActivite;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamMetier
     */
    public function getIdMetier()
    {
        return $this->idMetier;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamMetier $idMetier
     * @return CombiAnalytique
     */
    public function setIdMetier($idMetier)
    {
        $this->idMetier = $idMetier;
        return $this;
    }

}

