<?php

namespace SalarieBundle\Entity\Combi;

use Doctrine\ORM\Mapping as ORM;

/**
 * CombiCometeTempsTravail
 *
 * @ORM\Table(name="combi_comete_temps_travail", uniqueConstraints={@ORM\UniqueConstraint(name="combi_comete_temps_travail_id_temps_travail_id_decompte_tem_key", columns={"id_temps_travail", "id_decompte_temps_travail", "id_societe"})}, indexes={@ORM\Index(name="IDX_3747864E5E247FF5", columns={"id_temps_travail"}), @ORM\Index(name="IDX_3747864E64E1201D", columns={"id_decompte_temps_travail"}), @ORM\Index(name="IDX_3747864EC7F894CD", columns={"id_societe"})})
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Combi\CombiCometeTempsTravailRepository")
 */
class CombiCometeTempsTravail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_comete_temps_travail", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="combi_comete_temps_travail_id_ligne_comete_temps_travail_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneCometeTempsTravail;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_categorie_comete", type="bigint", nullable=false)
     */
    private $idCategorieComete;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cycle_comete", type="integer", nullable=false)
     */
    private $idCycleComete;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTempsTravail
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTempsTravail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_temps_travail", referencedColumnName="id_temps_travail")
     * })
     */
    private $idTempsTravail;

    /**
     * @var \SalarieBundle\Entity\Param\ParamDecompteTempsTravail
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamDecompteTempsTravail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_decompte_temps_travail", referencedColumnName="id_decompte_temps_travail")
     * })
     */
    private $idDecompteTempsTravail;

    /**
     * @var \SalarieBundle\Entity\Param\ParamSociete
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamSociete")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_societe", referencedColumnName="id_societe")
     * })
     */
    private $idSociete;

    /**
     * @return int
     */
    public function getIdLigneCometeTempsTravail()
    {
        return $this->idLigneCometeTempsTravail;
    }

    /**
     * @param int $idLigneCometeTempsTravail
     * @return CombiCometeTempsTravail
     */
    public function setIdLigneCometeTempsTravail($idLigneCometeTempsTravail)
    {
        $this->idLigneCometeTempsTravail = $idLigneCometeTempsTravail;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdCategorieComete()
    {
        return $this->idCategorieComete;
    }

    /**
     * @param int $idCategorieComete
     * @return CombiCometeTempsTravail
     */
    public function setIdCategorieComete($idCategorieComete)
    {
        $this->idCategorieComete = $idCategorieComete;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdCycleComete()
    {
        return $this->idCycleComete;
    }

    /**
     * @param int $idCycleComete
     * @return CombiCometeTempsTravail
     */
    public function setIdCycleComete($idCycleComete)
    {
        $this->idCycleComete = $idCycleComete;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamTempsTravail
     */
    public function getIdTempsTravail()
    {
        return $this->idTempsTravail;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamTempsTravail $idTempsTravail
     * @return CombiCometeTempsTravail
     */
    public function setIdTempsTravail($idTempsTravail)
    {
        $this->idTempsTravail = $idTempsTravail;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamDecompteTempsTravail
     */
    public function getIdDecompteTempsTravail()
    {
        return $this->idDecompteTempsTravail;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamDecompteTempsTravail $idDecompteTempsTravail
     * @return CombiCometeTempsTravail
     */
    public function setIdDecompteTempsTravail($idDecompteTempsTravail)
    {
        $this->idDecompteTempsTravail = $idDecompteTempsTravail;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamSociete
     */
    public function getIdSociete()
    {
        return $this->idSociete;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamSociete $idSociete
     * @return CombiCometeTempsTravail
     */
    public function setIdSociete($idSociete)
    {
        $this->idSociete = $idSociete;
        return $this;
    }

}

