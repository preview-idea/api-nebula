<?php

namespace SalarieBundle\Entity\Combi;

use Doctrine\ORM\Mapping as ORM;

/**
 * CombiRelationDocumentData
 *
 * @ORM\Table(name="combi_relation_document_data", uniqueConstraints={@ORM\UniqueConstraint(name="combi_relation_document_data_id_matricule_id_ty_doc_id_objet_da", columns={"id_matricule", "id_type_doc", "id_objet_data"})}, indexes={@ORM\Index(name="IDX_B1DAB2D4D48DF710", columns={"id_type_doc"}), @ORM\Index(name="IDX_B1DAB2D4928760BB", columns={"id_matricule"})})
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Combi\CombiRelationDocumentDataRepository")
 */
class CombiRelationDocumentData
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_relation_document_data", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="relation_document_data_id_ligne_relation_document_data_seq", allocationSize=1, initialValue=1)
     */
    private $idRelationDocumentData;

    /**
     * @var string
     *
     * @ORM\Column(name="li_chemin_stock", type="string", length=150, nullable=true)
     */
    private $liCheminStock;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeDoc
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeDoc")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_doc", referencedColumnName="id_type_doc")
     * })
     */
    private $idTypeDoc;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule", referencedColumnName="id_matricule")
     * })
     */
    private $idMatricule;

    /**
     * Get idRelationDocumentData
     *
     * @return integer
     */
    public function getIdRelationDocumentData()
    {
        return $this->idRelationDocumentData;
    }

    /**
     * Set liCheminStock
     *
     * @param string $liCheminStock
     *
     * @return CombiRelationDocumentData
     */
    public function setLiCheminStock($liCheminStock)
    {
        $this->liCheminStock = $liCheminStock;

        return $this;
    }

    /**
     * Get liCheminStock
     *
     * @return string
     */
    public function getLiCheminStock()
    {
        return $this->liCheminStock;
    }

    /**
     * Set idTypeDoc
     *
     * @param \SalarieBundle\Entity\Param\ParamTypeDoc $idTypeDoc
     *
     * @return CombiRelationDocumentData
     */
    public function setIdTypeDoc(\SalarieBundle\Entity\Param\ParamTypeDoc $idTypeDoc = null)
    {
        $this->idTypeDoc = $idTypeDoc;

        return $this;
    }

    /**
     * Get idTypeDoc
     *
     * @return \SalarieBundle\Entity\Param\ParamTypeDoc
     */
    public function getIdTypeDoc()
    {
        return $this->idTypeDoc;
    }

    /**
     * Set idMatricule
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatricule
     *
     * @return CombiRelationDocumentData
     */
    public function setIdMatricule(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatricule = null)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }
}
