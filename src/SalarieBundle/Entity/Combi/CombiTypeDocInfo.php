<?php

namespace SalarieBundle\Entity\Combi;

use Doctrine\ORM\Mapping as ORM;

/**
 * CombiTypeDocInfo
 *
 * @ORM\Table(name="combi_type_doc_info", indexes={@ORM\Index(name="IDX_C9172F22E1447018", columns={"id_type_doc_info"}), @ORM\Index(name="IDX_C9172F22D48DF710", columns={"id_type_doc"})})
 * @ORM\Entity
 */
class CombiTypeDocInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_combi_param_type_doc_info", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="combi_type_doc_info_id_combi_param_type_doc_info_seq", allocationSize=1, initialValue=1)
     */
    private $idCombiParamTypeDocInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="li_type_doc", type="string", length=30, nullable=true)
     */
    private $liTypeDoc;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeDocInfo
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeDocInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_doc_info", referencedColumnName="id_type_doc_info")
     * })
     */
    private $idTypeDocInfo;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeDoc
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeDoc")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_doc", referencedColumnName="id_type_doc")
     * })
     */
    private $idTypeDoc;

    /**
     * Get idCombiParamTypeDocInfo
     *
     * @return integer
     */
    public function getIdCombiParamTypeDocInfo()
    {
        return $this->idCombiParamTypeDocInfo;
    }

    /**
     * Set liTypeDoc
     *
     * @param string $liTypeDoc
     *
     * @return CombiTypeDocInfo
     */
    public function setLiTypeDoc($liTypeDoc)
    {
        $this->liTypeDoc = $liTypeDoc;

        return $this;
    }

    /**
     * Get liTypeDoc
     *
     * @return string
     */
    public function getLiTypeDoc()
    {
        return $this->liTypeDoc;
    }

    /**
     * Set idTypeDocInfo
     *
     * @param \SalarieBundle\Entity\Param\ParamTypeDocInfo $idTypeDocInfo
     *
     * @return CombiTypeDocInfo
     */
    public function setIdTypeDocInfo(\SalarieBundle\Entity\Param\ParamTypeDocInfo $idTypeDocInfo = null)
    {
        $this->idTypeDocInfo = $idTypeDocInfo;

        return $this;
    }

    /**
     * Get idTypeDocInfo
     *
     * @return \SalarieBundle\Entity\Param\ParamTypeDocInfo
     */
    public function getIdTypeDocInfo()
    {
        return $this->idTypeDocInfo;
    }

    /**
     * Set idTypeDoc
     *
     * @param \SalarieBundle\Entity\Param\ParamTypeDoc $idTypeDoc
     *
     * @return CombiTypeDocInfo
     */
    public function setIdTypeDoc(\SalarieBundle\Entity\Param\ParamTypeDoc $idTypeDoc = null)
    {
        $this->idTypeDoc = $idTypeDoc;

        return $this;
    }

    /**
     * Get idTypeDoc
     *
     * @return \SalarieBundle\Entity\Param\ParamTypeDoc
     */
    public function getIdTypeDoc()
    {
        return $this->idTypeDoc;
    }
}
