<?php

namespace SalarieBundle\Entity\Combi;

use Doctrine\ORM\Mapping as ORM;

/**
 * CombiCategorieCoeff
 *
 * @ORM\Table(name="combi_categorie_coeff", uniqueConstraints={@ORM\UniqueConstraint(name="combi_categorie_coeff_id_conventioncollective_id_coeffcontr_key", columns={"id_conventioncollective", "id_coeffcontrat", "id_categorieemploye"})}, indexes={@ORM\Index(name="IDX_60C48845649BCBD2", columns={"id_categorieemploye"}), @ORM\Index(name="IDX_60C48845DE5F92AB", columns={"id_coeffcontrat_apres_pre"}), @ORM\Index(name="IDX_60C48845379FB7D0", columns={"id_coeffcontrat"}), @ORM\Index(name="IDX_60C488454B7CAAD2", columns={"id_conventioncollective"})})
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Combi\CombiCategorieCoeffRepository")
 */
class CombiCategorieCoeff
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_categorie_coeff", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="combi_categorie_coeff_id_ligne_categorie_coeff_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneCategorieCoeff;

    /**
     * @var string
     *
     * @ORM\Column(name="li_niveau", type="string", length=4, nullable=true)
     */
    private $liNiveau;

    /**
     * @var string
     *
     * @ORM\Column(name="li_echelon", type="string", length=2, nullable=true)
     */
    private $liEchelon;

    /**
     * @var string
     *
     * @ORM\Column(name="li_position", type="string", length=5, nullable=true)
     */
    private $liPosition;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_taux_horaire", type="decimal", precision=10, scale=4, nullable=true)
     */
    private $nbTauxHoraire;

    /**
     * @var string
     *
     * @ORM\Column(name="li_niveau_rhpi", type="string", length=4, nullable=true)
     */
    private $liNiveauRhpi;

    /**
     * @var string
     *
     * @ORM\Column(name="li_position_rhpi", type="string", length=4, nullable=true)
     */
    private $liPositionRhpi;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_salaire_mensuel", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $nbSalaireMensuel;

    /**
     * @var string
     *
     * @ORM\Column(name="li_code_comete", type="string", length=5, nullable=true)
     */
    private $liCodeComete;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCategorieemploye
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCategorieemploye")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_categorieemploye", referencedColumnName="id_categorieemploye")
     * })
     */
    private $idCategorieemploye;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCoeffcontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCoeffcontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_coeffcontrat_apres_pre", referencedColumnName="id_coeffcontrat")
     * })
     */
    private $idCoeffcontratApresPre;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCoeffcontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCoeffcontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_coeffcontrat", referencedColumnName="id_coeffcontrat")
     * })
     */
    private $idCoeffcontrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamConventioncollective
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamConventioncollective")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_conventioncollective", referencedColumnName="id_conventioncollective")
     * })
     */
    private $idConventioncollective;

    /**
     * @return int
     */
    public function getIdLigneCategorieCoeff()
    {
        return $this->idLigneCategorieCoeff;
    }

    /**
     * @param int $idLigneCategorieCoeff
     * @return CombiCategorieCoeff
     */
    public function setIdLigneCategorieCoeff($idLigneCategorieCoeff)
    {
        $this->idLigneCategorieCoeff = $idLigneCategorieCoeff;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiNiveau()
    {
        return $this->liNiveau;
    }

    /**
     * @param string $liNiveau
     * @return CombiCategorieCoeff
     */
    public function setLiNiveau($liNiveau)
    {
        $this->liNiveau = $liNiveau;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiEchelon()
    {
        return $this->liEchelon;
    }

    /**
     * @param string $liEchelon
     * @return CombiCategorieCoeff
     */
    public function setLiEchelon($liEchelon)
    {
        $this->liEchelon = $liEchelon;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiPosition()
    {
        return $this->liPosition;
    }

    /**
     * @param string $liPosition
     * @return CombiCategorieCoeff
     */
    public function setLiPosition($liPosition)
    {
        $this->liPosition = $liPosition;
        return $this;
    }

    /**
     * @return string
     */
    public function getNbTauxHoraire()
    {
        return $this->nbTauxHoraire;
    }

    /**
     * @param string $nbTauxHoraire
     * @return CombiCategorieCoeff
     */
    public function setNbTauxHoraire($nbTauxHoraire)
    {
        $this->nbTauxHoraire = $nbTauxHoraire;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiNiveauRhpi()
    {
        return $this->liNiveauRhpi;
    }

    /**
     * @param string $liNiveauRhpi
     * @return CombiCategorieCoeff
     */
    public function setLiNiveauRhpi($liNiveauRhpi)
    {
        $this->liNiveauRhpi = $liNiveauRhpi;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiPositionRhpi()
    {
        return $this->liPositionRhpi;
    }

    /**
     * @param string $liPositionRhpi
     * @return CombiCategorieCoeff
     */
    public function setLiPositionRhpi($liPositionRhpi)
    {
        $this->liPositionRhpi = $liPositionRhpi;
        return $this;
    }

    /**
     * @return string
     */
    public function getNbSalaireMensuel()
    {
        return $this->nbSalaireMensuel;
    }

    /**
     * @param string $nbSalaireMensuel
     * @return CombiCategorieCoeff
     */
    public function setNbSalaireMensuel($nbSalaireMensuel)
    {
        $this->nbSalaireMensuel = $nbSalaireMensuel;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiCodeComete()
    {
        return $this->liCodeComete;
    }

    /**
     * @param string $liCodeComete
     * @return CombiCategorieCoeff
     */
    public function setLiCodeComete($liCodeComete)
    {
        $this->liCodeComete = $liCodeComete;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamCategorieemploye
     */
    public function getIdCategorieemploye()
    {
        return $this->idCategorieemploye;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamCategorieemploye $idCategorieemploye
     * @return CombiCategorieCoeff
     */
    public function setIdCategorieemploye($idCategorieemploye)
    {
        $this->idCategorieemploye = $idCategorieemploye;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamCoeffcontrat
     */
    public function getIdCoeffcontratApresPre()
    {
        return $this->idCoeffcontratApresPre;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamCoeffcontrat $idCoeffcontratApresPre
     * @return CombiCategorieCoeff
     */
    public function setIdCoeffcontratApresPre($idCoeffcontratApresPre)
    {
        $this->idCoeffcontratApresPre = $idCoeffcontratApresPre;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamCoeffcontrat
     */
    public function getIdCoeffcontrat()
    {
        return $this->idCoeffcontrat;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamCoeffcontrat $idCoeffcontrat
     * @return CombiCategorieCoeff
     */
    public function setIdCoeffcontrat($idCoeffcontrat)
    {
        $this->idCoeffcontrat = $idCoeffcontrat;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamConventioncollective
     */
    public function getIdConventioncollective()
    {
        return $this->idConventioncollective;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamConventioncollective $idConventioncollective
     * @return CombiCategorieCoeff
     */
    public function setIdConventioncollective($idConventioncollective)
    {
        $this->idConventioncollective = $idConventioncollective;
        return $this;
    }

}

