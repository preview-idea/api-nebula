<?php

namespace SalarieBundle\Entity\Combi;

use Doctrine\ORM\Mapping as ORM;

/**
 * CombiSocieteEtablissementAgence
 *
 * @ORM\Table(name="combi_societe_etablissement_agence", uniqueConstraints={@ORM\UniqueConstraint(name="combi_societe_etablissement_a_id_agence_id_societe_id_etabl_key", columns={"id_agence", "id_societe", "id_etablissement"})}, indexes={@ORM\Index(name="IDX_D69C3138C7F894CD", columns={"id_societe"}), @ORM\Index(name="IDX_D69C313842F62F44", columns={"id_agence"}), @ORM\Index(name="IDX_D69C31389ED58849", columns={"id_etablissement"})})
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Combi\CombiSocieteEtablissementAgenceRepository")
 */
class CombiSocieteEtablissementAgence
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_combi_sea", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="combi_societe_etablissement_agence_id_ligne_combi_sea_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneCombiSea;

    /**
     * @var \SalarieBundle\Entity\Param\ParamSociete
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamSociete")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_societe", referencedColumnName="id_societe")
     * })
     */
    private $idSociete;

    /**
     * @var \SalarieBundle\Entity\Param\ParamAgence
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamAgence")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_agence", referencedColumnName="id_agence")
     * })
     */
    private $idAgence;

    /**
     * @var \SalarieBundle\Entity\Param\ParamEtablissement
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamEtablissement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_etablissement", referencedColumnName="id_etablissement")
     * })
     */
    private $idEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="li_departement_mobilite", type="string", length=65, nullable=true)
     */
    private $liDepartementMobilite;

    /**
     * @return int
     */
    public function getIdLigneCombiSea()
    {
        return $this->idLigneCombiSea;
    }

    /**
     * @param int $idLigneCombiSea
     * @return CombiSocieteEtablissementAgence
     */
    public function setIdLigneCombiSea($idLigneCombiSea)
    {
        $this->idLigneCombiSea = $idLigneCombiSea;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamSociete
     */
    public function getIdSociete()
    {
        return $this->idSociete;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamSociete $idSociete
     * @return CombiSocieteEtablissementAgence
     */
    public function setIdSociete($idSociete)
    {
        $this->idSociete = $idSociete;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamAgence
     */
    public function getIdAgence()
    {
        return $this->idAgence;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamAgence $idAgence
     * @return CombiSocieteEtablissementAgence
     */
    public function setIdAgence($idAgence)
    {
        $this->idAgence = $idAgence;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamEtablissement
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamEtablissement $idEtablissement
     * @return CombiSocieteEtablissementAgence
     */
    public function setIdEtablissement($idEtablissement)
    {
        $this->idEtablissement = $idEtablissement;
        return $this;
    }

    /**
     * @return string
     */
    public function getLiDepartementMobilite()
    {
        return $this->liDepartementMobilite;
    }

    /**
     * @param string $liDepartementMobilite
     */
    public function setLiDepartementMobilite($liDepartementMobilite)
    {
        $this->liDepartementMobilite = $liDepartementMobilite;
    }
}

