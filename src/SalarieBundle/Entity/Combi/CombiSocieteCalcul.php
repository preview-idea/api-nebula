<?php

namespace SalarieBundle\Entity\Combi;

use Doctrine\ORM\Mapping as ORM;

/**
 * CombiSocieteCalcul
 *
 * @ORM\Table(name="combi_societe_calcul", indexes={@ORM\Index(name="IDX_312915FB85172A22", columns={"id_param_societe"}), @ORM\Index(name="IDX_312915FBBBDD0ECE", columns={"id_type_calcul_ep"})})
 * @ORM\Entity
 */
class CombiSocieteCalcul
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_combi_societe_calcul", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="combi_societe_calcul_id_combi_societe_calcul", allocationSize=1, initialValue=1)
     */
    private $idCombiSocieteCalcul;

    /**
     * @var \SalarieBundle\Entity\Param\ParamSociete
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamSociete")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_param_societe", referencedColumnName="id_societe")
     * })
     */
    private $idParamSociete;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeCalculEp
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeCalculEp")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_calcul_ep", referencedColumnName="id_type_calcul_ep")
     * })
     */
    private $idTypeCalculEp;



    /**
     * Get idCombiSocieteCalcul
     *
     * @return integer
     */
    public function getIdCombiSocieteCalcul()
    {
        return $this->idCombiSocieteCalcul;
    }

    /**
     * Set idParamSociete
     *
     * @param \SalarieBundle\Entity\Param\ParamSociete $idParamSociete
     *
     * @return CombiSocieteCalcul
     */
    public function setIdParamSociete(\SalarieBundle\Entity\Param\ParamSociete $idParamSociete = null)
    {
        $this->idParamSociete = $idParamSociete;

        return $this;
    }

    /**
     * Get idParamSociete
     *
     * @return \SalarieBundle\Entity\Param\ParamSociete
     */
    public function getIdParamSociete()
    {
        return $this->idParamSociete;
    }

    /**
     * Set idTypeCalculEp
     *
     * @param \SalarieBundle\Entity\Param\ParamTypeCalculEp $idTypeCalculEp
     *
     * @return CombiSocieteCalcul
     */
    public function setIdTypeCalculEp(\SalarieBundle\Entity\Param\ParamTypeCalculEp $idTypeCalculEp = null)
    {
        $this->idTypeCalculEp = $idTypeCalculEp;

        return $this;
    }

    /**
     * Get idTypeCalculEp
     *
     * @return \SalarieBundle\Entity\Param\ParamTypeCalculEp
     */
    public function getIdTypeCalculEp()
    {
        return $this->idTypeCalculEp;
    }
}
