<?php

namespace SalarieBundle\Entity\Combi;

use Doctrine\ORM\Mapping as ORM;

/**
 * CombiNaturecontratTypeavenant
 *
 * @ORM\Table(name="combi_naturecontrat_typeavenant", uniqueConstraints={@ORM\UniqueConstraint(name="combi_naturecontrat_typeavena_id_naturecontrat_id_typeavena_key", columns={"id_naturecontrat", "id_typeavenant", "id_positionnementposte"})}, indexes={@ORM\Index(name="IDX_968B293013230DA2", columns={"id_naturecontrat"}), @ORM\Index(name="IDX_968B2930C0081DA9", columns={"id_typeavenant"}), @ORM\Index(name="IDX_968B2930911DCB20", columns={"id_positionnementposte"})})
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Combi\CombiNaturecontratTypeavenantRepository")
 */
class CombiNaturecontratTypeavenant
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_naturecontrat_typeavenant", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="combi_naturecontrat_typeavena_id_ligne_naturecontrat_typeav_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneNaturecontratTypeavenant;

    /**
     * @var \SalarieBundle\Entity\Param\ParamNaturecontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamNaturecontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_naturecontrat", referencedColumnName="id_naturecontrat")
     * })
     */
    private $idNaturecontrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypeavenant
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypeavenant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_typeavenant", referencedColumnName="id_typeavenant")
     * })
     */
    private $idTypeavenant;

    /**
     * @var \SalarieBundle\Entity\Param\ParamPositionnementposte
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamPositionnementposte")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_positionnementposte", referencedColumnName="id_positionnementposte")
     * })
     */
    private $idPositionnementposte;

    /**
     * @return int
     */
    public function getIdLigneNaturecontratTypeavenant()
    {
        return $this->idLigneNaturecontratTypeavenant;
    }

    /**
     * @param int $idLigneNaturecontratTypeavenant
     * @return CombiNaturecontratTypeavenant
     */
    public function setIdLigneNaturecontratTypeavenant($idLigneNaturecontratTypeavenant)
    {
        $this->idLigneNaturecontratTypeavenant = $idLigneNaturecontratTypeavenant;
        return $this;
    }

    /**
     * @return Param\ParamNaturecontrat
     */
    public function getIdNaturecontrat()
    {
        return $this->idNaturecontrat;
    }

    /**
     * @param Param\ParamNaturecontrat $idNaturecontrat
     * @return CombiNaturecontratTypeavenant
     */
    public function setIdNaturecontrat($idNaturecontrat)
    {
        $this->idNaturecontrat = $idNaturecontrat;
        return $this;
    }

    /**
     * @return Param\ParamTypeavenant
     */
    public function getIdTypeavenant()
    {
        return $this->idTypeavenant;
    }

    /**
     * @param Param\ParamTypeavenant $idTypeavenant
     * @return CombiNaturecontratTypeavenant
     */
    public function setIdTypeavenant($idTypeavenant)
    {
        $this->idTypeavenant = $idTypeavenant;
        return $this;
    }

    /**
     * @return Param\ParamPositionnementposte
     */
    public function getIdPositionnementposte()
    {
        return $this->idPositionnementposte;
    }

    /**
     * @param Param\ParamPositionnementposte $idPositionnementposte
     * @return CombiNaturecontratTypeavenant
     */
    public function setIdPositionnementposte($idPositionnementposte)
    {
        $this->idPositionnementposte = $idPositionnementposte;
        return $this;
    }

}

