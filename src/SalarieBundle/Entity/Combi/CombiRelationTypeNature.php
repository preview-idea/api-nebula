<?php

namespace SalarieBundle\Entity\Combi;

use Doctrine\ORM\Mapping as ORM;

/**
 * CombiRelationTypeNature
 *
 * @ORM\Table(name="combi_relation_type_nature", uniqueConstraints={@ORM\UniqueConstraint(name="combi_relation_type_nature_id_typecontrat_id_naturecontrat_key", columns={"id_typecontrat", "id_naturecontrat"})}, indexes={@ORM\Index(name="IDX_1180D2D513230DA2", columns={"id_naturecontrat"}), @ORM\Index(name="IDX_1180D2D5A2C2D8DF", columns={"id_typecontrat"})})
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Combi\CombiRelationTypeNatureRepository")
 */
class CombiRelationTypeNature
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_relation_type_nature", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contrat_relation_type_nature_id_ligne_relation_type_nature_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneRelationTypeNature;

    /**
     * @var \SalarieBundle\Entity\Param\ParamNaturecontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamNaturecontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_naturecontrat", referencedColumnName="id_naturecontrat")
     * })
     */
    private $idNaturecontrat;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypecontrat
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypecontrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_typecontrat", referencedColumnName="id_typecontrat")
     * })
     */
    private $idTypecontrat;

    /**
     * @return int
     */
    public function getIdLigneRelationTypeNature()
    {
        return $this->idLigneRelationTypeNature;
    }

    /**
     * @param int $idLigneRelationTypeNature
     * @return CombiRelationTypeNature
     */
    public function setIdLigneRelationTypeNature($idLigneRelationTypeNature)
    {
        $this->idLigneRelationTypeNature = $idLigneRelationTypeNature;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamNaturecontrat
     */
    public function getIdNaturecontrat()
    {
        return $this->idNaturecontrat;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamNaturecontrat $idNaturecontrat
     * @return CombiRelationTypeNature
     */
    public function setIdNaturecontrat($idNaturecontrat)
    {
        $this->idNaturecontrat = $idNaturecontrat;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamTypecontrat
     */
    public function getIdTypecontrat()
    {
        return $this->idTypecontrat;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamTypecontrat $idTypecontrat
     * @return CombiRelationTypeNature
     */
    public function setIdTypecontrat($idTypecontrat)
    {
        $this->idTypecontrat = $idTypecontrat;
        return $this;
    }

}

