<?php

namespace SalarieBundle\Entity\Combi;

use Doctrine\ORM\Mapping as ORM;

/**
 * CombiPerimetreElectoralCse
 *
 * @ORM\Table(name="combi_perimetre_electoral_cse", uniqueConstraints={@ORM\UniqueConstraint(name="combi_perimetre_electoral_cse_id_perimetre_id_cse_key", columns={"id_perimetre_electoral", "id_cse"})}, indexes={@ORM\Index(name="IDX_688FB84112BEFBD3", columns={"id_cse"}), @ORM\Index(name="IDX_688FB841189FBDFD", columns={"id_perimetre_electoral"})})
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Combi\CombiPerimetreElectoralCseRepository")
 */
class CombiPerimetreElectoralCse
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_perimetre_electoral_cse", type="smallint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="combi_perimetre_electoral_cse_id_ligne_perimetre_electoral_cse_seq", allocationSize=1, initialValue=1)
     */
    private $idLignePerimetreElectoralCse;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCse
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCse")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cse", referencedColumnName="id_cse")
     * })
     */
    private $idCse;

    /**
     * @var \SalarieBundle\Entity\Param\ParamPerimetreElectoral
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamPerimetreElectoral")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_perimetre_electoral", referencedColumnName="id_perimetre_electoral")
     * })
     */
    private $idPerimetreElectoral;

    /**
     * @return int
     */
    public function getIdLignePerimetreElectoralCse()
    {
        return $this->idLignePerimetreElectoralCse;
    }

    /**
     * @param int $idLignePerimetreElectoralCse
     * @return CombiPerimetreElectoralCse
     */
    public function setIdLignePerimetreElectoralCse($idLignePerimetreElectoralCse)
    {
        $this->idLignePerimetreElectoralCse = $idLignePerimetreElectoralCse;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamCse
     */
    public function getIdCse()
    {
        return $this->idCse;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamCse $idCse
     * @return CombiPerimetreElectoralCse
     */
    public function setIdCse($idCse)
    {
        $this->idCse = $idCse;
        return $this;
    }

    /**
     * @return \SalarieBundle\Entity\Param\ParamPerimetreElectoral
     */
    public function getIdPerimetreElectoral()
    {
        return $this->idPerimetreElectoral;
    }

    /**
     * @param \SalarieBundle\Entity\Param\ParamPerimetreElectoral $idPerimetreElectoral
     * @return CombiPerimetreElectoralCse
     */
    public function setIdPerimetreElectoral($idPerimetreElectoral)
    {
        $this->idPerimetreElectoral = $idPerimetreElectoral;
        return $this;
    }

}

