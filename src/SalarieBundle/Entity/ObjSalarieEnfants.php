<?php

namespace SalarieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * ObjSalarieEnfants
 *
 * @ORM\Table(name="obj_salarie_enfants", indexes={@ORM\Index(name="IDX_99F358BB47CCA38E", columns={"id_sexe"}), @ORM\Index(name="IDX_99F358BBCBC3B464", columns={"id_matricule_maj"}), @ORM\Index(name="IDX_99F358BB928760BB", columns={"id_matricule"}), @ORM\Index(name="IDX_99F358BB7C97F743", columns={"id_civilite"}), @ORM\Index(name="IDX_99F358BBC2469C07", columns={"id_type_prise_charge"})})
 * @ORM\Entity
 */
class ObjSalarieEnfants
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_enfant", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="obj_salarie_enfants_id_enfant_seq", allocationSize=1, initialValue=1)
     */
    private $idEnfant;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_deces", type="date", nullable=true)
     */
    private $dtDeces;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_naissance", type="date", nullable=true)
     */
    private $dtNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="li_lieu_naissance", type="string", length=50, nullable=true)
     */
    private $liLieuNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_enfant", type="string", length=30, nullable=true)
     */
    private $liNomEnfant;

    /**
     * @var string
     *
     * @ORM\Column(name="li_prenom_enfant", type="string", length=20, nullable=true)
     */
    private $liPrenomEnfant;

    /**
     * @var \SalarieBundle\Entity\Param\ParamSexe
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamSexe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_sexe", referencedColumnName="id_sexe")
     * })
     */
    private $idSexe;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\ObjSalarie
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\ObjSalarie", inversedBy="enfants")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule", referencedColumnName="id_matricule")
     * })
     * @Serializer\Exclude()
     */
    private $idMatricule;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCivilite
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCivilite")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_civilite", referencedColumnName="id_civilite")
     * })
     */
    private $idCivilite;

    /**
     * @var \SalarieBundle\Entity\Param\ParamTypePriseCharge
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamTypePriseCharge")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_prise_charge", referencedColumnName="id_type_prise_charge")
     * })
     */
    private $idTypePriseCharge;

    /**
     * Get idEnfant
     *
     * @return integer
     */
    public function getIdEnfant()
    {
        return $this->idEnfant;
    }

    /**
     * Set dtDeces
     *
     * @param \DateTime $dtDeces
     *
     * @return ObjSalarieEnfants
     */
    public function setDtDeces($dtDeces)
    {
        $this->dtDeces = $dtDeces;

        return $this;
    }

    /**
     * Get dtDeces
     *
     * @return \DateTime
     */
    public function getDtDeces()
    {
        return $this->dtDeces;
    }

    /**
     * Set dtNaissance
     *
     * @param \DateTime $dtNaissance
     *
     * @return ObjSalarieEnfants
     */
    public function setDtNaissance($dtNaissance)
    {
        $this->dtNaissance = $dtNaissance;

        return $this;
    }

    /**
     * Get dtNaissance
     *
     * @return \DateTime
     */
    public function getDtNaissance()
    {
        return $this->dtNaissance;
    }

    /**
     * Set liLieuNaissance
     *
     * @param string $liLieuNaissance
     *
     * @return ObjSalarieEnfants
     */
    public function setLiLieuNaissance($liLieuNaissance)
    {
        $this->liLieuNaissance = $liLieuNaissance;

        return $this;
    }

    /**
     * Get liLieuNaissance
     *
     * @return string
     */
    public function getLiLieuNaissance()
    {
        return $this->liLieuNaissance;
    }

    /**
     * Set liNomEnfant
     *
     * @param string $liNomEnfant
     *
     * @return ObjSalarieEnfants
     */
    public function setLiNomEnfant($liNomEnfant)
    {
        $this->liNomEnfant = $liNomEnfant;

        return $this;
    }

    /**
     * Get liNomEnfant
     *
     * @return string
     */
    public function getLiNomEnfant()
    {
        return $this->liNomEnfant;
    }

    /**
     * Set liPrenomEnfant
     *
     * @param string $liPrenomEnfant
     *
     * @return ObjSalarieEnfants
     */
    public function setLiPrenomEnfant($liPrenomEnfant)
    {
        $this->liPrenomEnfant = $liPrenomEnfant;

        return $this;
    }

    /**
     * Get liPrenomEnfant
     *
     * @return string
     */
    public function getLiPrenomEnfant()
    {
        return $this->liPrenomEnfant;
    }

    /**
     * Set idSexe
     *
     * @param \SalarieBundle\Entity\Param\ParamSexe $idSexe
     *
     * @return ObjSalarieEnfants
     */
    public function setIdSexe(\SalarieBundle\Entity\Param\ParamSexe $idSexe = null)
    {
        $this->idSexe = $idSexe;

        return $this;
    }

    /**
     * Get idSexe
     *
     * @return \SalarieBundle\Entity\Param\ParamSexe
     */
    public function getIdSexe()
    {
        return $this->idSexe;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ObjSalarieEnfants
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idMatricule
     *
     * @param \SalarieBundle\Entity\ObjSalarie $idMatricule
     *
     * @return ObjSalarieEnfants
     */
    public function setIdMatricule(\SalarieBundle\Entity\ObjSalarie $idMatricule = null)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return \SalarieBundle\Entity\ObjSalarie
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set idCivilite
     *
     * @param \SalarieBundle\Entity\Param\ParamCivilite $idCivilite
     *
     * @return ObjSalarieEnfants
     */
    public function setIdCivilite(\SalarieBundle\Entity\Param\ParamCivilite $idCivilite = null)
    {
        $this->idCivilite = $idCivilite;

        return $this;
    }

    /**
     * Get idCivilite
     *
     * @return \SalarieBundle\Entity\Param\ParamCivilite
     */
    public function getIdCivilite()
    {
        return $this->idCivilite;
    }

    /**
     * Set idTypePriseCharge
     *
     * @param \SalarieBundle\Entity\Param\ParamTypePriseCharge $idTypePriseCharge
     *
     * @return ObjSalarieEnfants
     */
    public function setIdTypePriseCharge(\SalarieBundle\Entity\Param\ParamTypePriseCharge $idTypePriseCharge = null)
    {
        $this->idTypePriseCharge = $idTypePriseCharge;

        return $this;
    }

    /**
     * Get idTypePriseCharge
     *
     * @return \SalarieBundle\Entity\Param\ParamTypePriseCharge
     */
    public function getIdTypePriseCharge()
    {
        return $this->idTypePriseCharge;
    }
}
