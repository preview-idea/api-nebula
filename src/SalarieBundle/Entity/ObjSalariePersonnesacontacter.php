<?php

namespace SalarieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * ObjSalariePersonnesacontacter
 *
 * @ORM\Table(name="obj_salarie_personnesacontacter", indexes={@ORM\Index(name="IDX_BCBE21E2CBC3B464", columns={"id_matricule_maj"}), @ORM\Index(name="IDX_BCBE21E2928760BB", columns={"id_matricule"}), @ORM\Index(name="IDX_BCBE21E27C97F743", columns={"id_civilite"})})
 * @ORM\Entity
 */
class ObjSalariePersonnesacontacter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_personne_contacter", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="obj_salarie_pacontacter_id_pacontacter_seq", allocationSize=1, initialValue=1)
     */
    private $idPersonneContacter;

    /**
     * @var string
     *
     * @ORM\Column(name="li_telephone_1", type="string", length=15, nullable=false)
     */
    private $liTelephone1;

    /**
     * @var string
     *
     * @ORM\Column(name="li_telephone_2", type="string", length=15, nullable=true)
     */
    private $liTelephone2;

    /**
     * @var string
     *
     * @ORM\Column(name="li_relation_salarie_pac", type="string", length=20, nullable=false)
     */
    private $liRelationSalariePac;

    /**
     * @var string
     *
     * @ORM\Column(name="li_nom_personne_contacter", type="string", length=30, nullable=false)
     */
    private $liNomPersonneContacter;

    /**
     * @var string
     *
     * @ORM\Column(name="li_prenom_personne_contacter", type="string", length=20, nullable=false)
     */
    private $liPrenomPersonneContacter;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * @var \SalarieBundle\Entity\ObjSalarie
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\ObjSalarie", inversedBy="personnes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule", referencedColumnName="id_matricule")
     * })
     * @Serializer\Exclude()
     */
    private $idMatricule;

    /**
     * @var \SalarieBundle\Entity\Param\ParamCivilite
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Param\ParamCivilite")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_civilite", referencedColumnName="id_civilite")
     * })
     */
    private $idCivilite;

    /**
     * Get idPersonneContacter
     *
     * @return integer
     */
    public function getIdPersonneContacter()
    {
        return $this->idPersonneContacter;
    }

    /**
     * Set liTelephone1
     *
     * @param string $liTelephone1
     *
     * @return ObjSalariePersonnesacontacter
     */
    public function setLiTelephone1($liTelephone1)
    {
        $this->liTelephone1 = $liTelephone1;

        return $this;
    }

    /**
     * Get liTelephone1
     *
     * @return string
     */
    public function getLiTelephone1()
    {
        return $this->liTelephone1;
    }

    /**
     * Set liTelephone2
     *
     * @param string $liTelephone2
     *
     * @return ObjSalariePersonnesacontacter
     */
    public function setLiTelephone2($liTelephone2)
    {
        $this->liTelephone2 = $liTelephone2;

        return $this;
    }

    /**
     * Get liTelephone2
     *
     * @return string
     */
    public function getLiTelephone2()
    {
        return $this->liTelephone2;
    }

    /**
     * Set liRelationSalariePac
     *
     * @param string $liRelationSalariePac
     *
     * @return ObjSalariePersonnesacontacter
     */
    public function setLiRelationSalariePac($liRelationSalariePac)
    {
        $this->liRelationSalariePac = $liRelationSalariePac;

        return $this;
    }

    /**
     * Get liRelationSalariePac
     *
     * @return string
     */
    public function getLiRelationSalariePac()
    {
        return $this->liRelationSalariePac;
    }

    /**
     * Set liNomPersonneContacter
     *
     * @param string $liNomPersonneContacter
     *
     * @return ObjSalariePersonnesacontacter
     */
    public function setLiNomPersonneContacter($liNomPersonneContacter)
    {
        $this->liNomPersonneContacter = $liNomPersonneContacter;

        return $this;
    }

    /**
     * Get liNomPersonneContacter
     *
     * @return string
     */
    public function getLiNomPersonneContacter()
    {
        return $this->liNomPersonneContacter;
    }

    /**
     * Set liPrenomPersonneContacter
     *
     * @param string $liPrenomPersonneContacter
     *
     * @return ObjSalariePersonnesacontacter
     */
    public function setLiPrenomPersonneContacter($liPrenomPersonneContacter)
    {
        $this->liPrenomPersonneContacter = $liPrenomPersonneContacter;

        return $this;
    }

    /**
     * Get liPrenomPersonneContacter
     *
     * @return string
     */
    public function getLiPrenomPersonneContacter()
    {
        return $this->liPrenomPersonneContacter;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return ObjSalariePersonnesacontacter
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }

    /**
     * Set idMatricule
     *
     * @param \SalarieBundle\Entity\ObjSalarie $idMatricule
     *
     * @return ObjSalariePersonnesacontacter
     */
    public function setIdMatricule(\SalarieBundle\Entity\ObjSalarie $idMatricule = null)
    {
        $this->idMatricule = $idMatricule;

        return $this;
    }

    /**
     * Get idMatricule
     *
     * @return \SalarieBundle\Entity\ObjSalarie
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * Set idCivilite
     *
     * @param \SalarieBundle\Entity\Param\ParamCivilite $idCivilite
     *
     * @return ObjSalariePersonnesacontacter
     */
    public function setIdCivilite(\SalarieBundle\Entity\Param\ParamCivilite $idCivilite = null)
    {
        $this->idCivilite = $idCivilite;

        return $this;
    }

    /**
     * Get idCivilite
     *
     * @return \SalarieBundle\Entity\Param\ParamCivilite
     */
    public function getIdCivilite()
    {
        return $this->idCivilite;
    }
}
