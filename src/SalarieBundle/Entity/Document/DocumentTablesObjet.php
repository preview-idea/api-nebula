<?php

namespace SalarieBundle\Entity\Document;

use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentTablesObjet
 *
 * @ORM\Table(name="document_tables_objet", uniqueConstraints={@ORM\UniqueConstraint(name="document_tables_objet_id_table_objet_id_type_document_key", columns={"id_table_objet", "id_type_document"}), @ORM\UniqueConstraint(name="document_tables_objet_id_ordre_jointure_id_type_document_key", columns={"id_ordre_jointure", "id_type_document"})})
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Document\DocumentTablesObjetRepository")
 */
class DocumentTablesObjet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_document_tables_objet", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="document_tables_objet_id_ligne_document_tables_objet_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneDocumentTablesObjet;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_table_objet", type="integer", nullable=false)
     */
    private $idTableObjet;

    /**
     * @var string
     *
     * @ORM\Column(name="li_table_objet", type="string", length=40, nullable=false)
     */
    private $liTableObjet;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_in_variables", type="bit", nullable=false)
     */
    private $isInVariables;

    /**
     * @var string
     *
     * @ORM\Column(name="li_table_objet_description", type="string", length=50, nullable=true)
     */
    private $liTableObjetDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_ordre_jointure", type="smallint", nullable=true)
     */
    private $idOrdreJointure = '-1';

    /**
     * @var string
     *
     * @ORM\Column(name="li_colonne_jointure", type="string", length=20, nullable=true)
     */
    private $liColonneJointure;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_type_document", type="smallint", nullable=false)
     */
    private $idTypeDocument = '1';

    /**
     * Get idLigneDocumentTablesObjet
     *
     * @return integer
     */
    public function getIdLigneDocumentTablesObjet()
    {
        return $this->idLigneDocumentTablesObjet;
    }

    /**
     * Set idTableObjet
     *
     * @param integer $idTableObjet
     *
     * @return DocumentTablesObjet
     */
    public function setIdTableObjet($idTableObjet)
    {
        $this->idTableObjet = $idTableObjet;

        return $this;
    }

    /**
     * Get idTableObjet
     *
     * @return integer
     */
    public function getIdTableObjet()
    {
        return $this->idTableObjet;
    }

    /**
     * Set liTableObjet
     *
     * @param string $liTableObjet
     *
     * @return DocumentTablesObjet
     */
    public function setLiTableObjet($liTableObjet)
    {
        $this->liTableObjet = $liTableObjet;

        return $this;
    }

    /**
     * Get liTableObjet
     *
     * @return string
     */
    public function getLiTableObjet()
    {
        return $this->liTableObjet;
    }

    /**
     * Set isInVariables
     *
     * @param bit $isInVariables
     *
     * @return DocumentTablesObjet
     */
    public function setIsInVariables($isInVariables)
    {
        $this->isInVariables = $isInVariables;

        return $this;
    }

    /**
     * Get isInVariables
     *
     * @return bit
     */
    public function getIsInVariables()
    {
        return $this->isInVariables;
    }

    /**
     * Set liTableObjetDescription
     *
     * @param string $liTableObjetDescription
     *
     * @return DocumentTablesObjet
     */
    public function setLiTableObjetDescription($liTableObjetDescription)
    {
        $this->liTableObjetDescription = $liTableObjetDescription;

        return $this;
    }

    /**
     * Get liTableObjetDescription
     *
     * @return string
     */
    public function getLiTableObjetDescription()
    {
        return $this->liTableObjetDescription;
    }

    /**
     * Set idOrdreJointure
     *
     * @param integer $idOrdreJointure
     *
     * @return DocumentTablesObjet
     */
    public function setIdOrdreJointure($idOrdreJointure)
    {
        $this->idOrdreJointure = $idOrdreJointure;

        return $this;
    }

    /**
     * Get idOrdreJointure
     *
     * @return integer
     */
    public function getIdOrdreJointure()
    {
        return $this->idOrdreJointure;
    }

    /**
     * Set liColonneJointure
     *
     * @param string $liColonneJointure
     *
     * @return DocumentTablesObjet
     */
    public function setLiColonneJointure($liColonneJointure)
    {
        $this->liColonneJointure = $liColonneJointure;

        return $this;
    }

    /**
     * Get liColonneJointure
     *
     * @return string
     */
    public function getLiColonneJointure()
    {
        return $this->liColonneJointure;
    }

    /**
     * Set idTypeDocument
     *
     * @param integer $idTypeDocument
     *
     * @return DocumentTablesObjet
     */
    public function setIdTypeDocument($idTypeDocument)
    {
        $this->idTypeDocument = $idTypeDocument;

        return $this;
    }

    /**
     * Get idTypeDocument
     *
     * @return integer
     */
    public function getIdTypeDocument()
    {
        return $this->idTypeDocument;
    }
}
