<?php

namespace SalarieBundle\Entity\Document;

use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentTablesParam
 *
 * @ORM\Table(name="document_tables_param")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Document\DocumentTablesParamRepository")
 */
class DocumentTablesParam
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_table_param", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idTableParam;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_type_document", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idTypeDocument = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="li_table_param", type="string", length=40, nullable=false)
     */
    private $liTableParam;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_in_gencontrat", type="bit", nullable=false)
     */
    private $isInGencontrat;

    /**
     * @var string
     *
     * @ORM\Column(name="li_colonne_id", type="string", length=40, nullable=true)
     */
    private $liColonneId;

    /**
     * @var string
     *
     * @ORM\Column(name="li_table_param_description", type="string", length=30, nullable=true)
     */
    private $liTableParamDescription;

    /**
     * Set idTableParam
     *
     * @param integer $idTableParam
     *
     * @return DocumentTablesParam
     */
    public function setIdTableParam($idTableParam)
    {
        $this->idTableParam = $idTableParam;

        return $this;
    }

    /**
     * Get idTableParam
     *
     * @return integer
     */
    public function getIdTableParam()
    {
        return $this->idTableParam;
    }

    /**
     * Set idTypeDocument
     *
     * @param integer $idTypeDocument
     *
     * @return DocumentTablesParam
     */
    public function setIdTypeDocument($idTypeDocument)
    {
        $this->idTypeDocument = $idTypeDocument;

        return $this;
    }

    /**
     * Get idTypeDocument
     *
     * @return integer
     */
    public function getIdTypeDocument()
    {
        return $this->idTypeDocument;
    }

    /**
     * Set liTableParam
     *
     * @param string $liTableParam
     *
     * @return DocumentTablesParam
     */
    public function setLiTableParam($liTableParam)
    {
        $this->liTableParam = $liTableParam;

        return $this;
    }

    /**
     * Get liTableParam
     *
     * @return string
     */
    public function getLiTableParam()
    {
        return $this->liTableParam;
    }

    /**
     * Set isInGencontrat
     *
     * @param bit $isInGencontrat
     *
     * @return DocumentTablesParam
     */
    public function setIsInGencontrat($isInGencontrat)
    {
        $this->isInGencontrat = $isInGencontrat;

        return $this;
    }

    /**
     * Get isInGencontrat
     *
     * @return bit
     */
    public function getIsInGencontrat()
    {
        return $this->isInGencontrat;
    }

    /**
     * Set liColonneId
     *
     * @param string $liColonneId
     *
     * @return DocumentTablesParam
     */
    public function setLiColonneId($liColonneId)
    {
        $this->liColonneId = $liColonneId;

        return $this;
    }

    /**
     * Get liColonneId
     *
     * @return string
     */
    public function getLiColonneId()
    {
        return $this->liColonneId;
    }

    /**
     * Set liTableParamDescription
     *
     * @param string $liTableParamDescription
     *
     * @return DocumentTablesParam
     */
    public function setLiTableParamDescription($liTableParamDescription)
    {
        $this->liTableParamDescription = $liTableParamDescription;

        return $this;
    }

    /**
     * Get liTableParamDescription
     *
     * @return string
     */
    public function getLiTableParamDescription()
    {
        return $this->liTableParamDescription;
    }
}
