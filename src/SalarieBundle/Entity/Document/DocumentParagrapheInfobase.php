<?php

namespace SalarieBundle\Entity\Document;

use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentParagrapheInfobase
 *
 * @ORM\Table(name="document_paragraphe_elements_tables_param", uniqueConstraints={@ORM\UniqueConstraint(name="document_paragraphe_elements__id_paragraphe_id_element_para_key", columns={"id_paragraphe", "id_element_param", "id_table_param", "id_type_document"})}, indexes={@ORM\Index(name="IDX_E18A4743E9912961ABBE7CEF", columns={"id_paragraphe", "id_type_document"}), @ORM\Index(name="IDX_E18A4743DEF0D975B0147C06ABBE7CEF", columns={"id_element_param", "id_table_param", "id_type_document"})})
 * @ORM\Entity
 */
class DocumentParagrapheInfobase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_paragraphe", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idParagraphe;

    /**
     * @var string
     *
     * @ORM\Column(name="li_paragraphe", type="string", length=60, nullable=false)
     */
    private $liParagraphe;

    /**
     * @var string
     *
     * @ORM\Column(name="li_contenu", type="text", nullable=false)
     */
    private $liContenu;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_optionnel", type="bit", nullable=false)
     */
    private $isOptionnel;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var string
     *
     * @ORM\Column(name="li_commentaire_maj", type="string", length=200, nullable=false)
     */
    private $liCommentaireMaj;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_ordre_paragraphe", type="smallint", nullable=true)
     */
    private $nbOrdreParagraphe;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_paragraphe_prec", type="smallint", nullable=true)
     */
    private $idParagraphePrec;

    /**
     * @var \SalarieBundle\Entity\Document\DocumentGrpParagrapheInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Document\DocumentGrpParagrapheInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_grp_paragraphe", referencedColumnName="id_grp_paragraphe"),
     *   @ORM\JoinColumn(name="id_type_document", referencedColumnName="id_type_document")
     * })
     */
    private $idGrpParagraphe;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * Set idParagraphe
     *
     * @param integer $idParagraphe
     *
     * @return DocumentParagrapheInfobase
     */
    public function setIdParagraphe($idParagraphe)
    {
        $this->idParagraphe = $idParagraphe;

        return $this;
    }

    /**
     * Get idParagraphe
     *
     * @return integer
     */
    public function getIdParagraphe()
    {
        return $this->idParagraphe;
    }

    /**
     * Set liParagraphe
     *
     * @param string $liParagraphe
     *
     * @return DocumentParagrapheInfobase
     */
    public function setLiParagraphe($liParagraphe)
    {
        $this->liParagraphe = $liParagraphe;

        return $this;
    }

    /**
     * Get liParagraphe
     *
     * @return string
     */
    public function getLiParagraphe()
    {
        return $this->liParagraphe;
    }

    /**
     * Set liContenu
     *
     * @param string $liContenu
     *
     * @return DocumentParagrapheInfobase
     */
    public function setLiContenu($liContenu)
    {
        $this->liContenu = $liContenu;

        return $this;
    }

    /**
     * Get liContenu
     *
     * @return string
     */
    public function getLiContenu()
    {
        return $this->liContenu;
    }

    /**
     * Set isOptionnel
     *
     * @param bit $isOptionnel
     *
     * @return DocumentParagrapheInfobase
     */
    public function setIsOptionnel($isOptionnel)
    {
        $this->isOptionnel = $isOptionnel;

        return $this;
    }

    /**
     * Get isOptionnel
     *
     * @return bit
     */
    public function getIsOptionnel()
    {
        return $this->isOptionnel;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return DocumentParagrapheInfobase
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return DocumentParagrapheInfobase
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set liCommentaireMaj
     *
     * @param string $liCommentaireMaj
     *
     * @return DocumentParagrapheInfobase
     */
    public function setLiCommentaireMaj($liCommentaireMaj)
    {
        $this->liCommentaireMaj = $liCommentaireMaj;

        return $this;
    }

    /**
     * Get liCommentaireMaj
     *
     * @return string
     */
    public function getLiCommentaireMaj()
    {
        return $this->liCommentaireMaj;
    }

    /**
     * Set nbOrdreParagraphe
     *
     * @param integer $nbOrdreParagraphe
     *
     * @return DocumentParagrapheInfobase
     */
    public function setNbOrdreParagraphe($nbOrdreParagraphe)
    {
        $this->nbOrdreParagraphe = $nbOrdreParagraphe;

        return $this;
    }

    /**
     * Get nbOrdreParagraphe
     *
     * @return integer
     */
    public function getNbOrdreParagraphe()
    {
        return $this->nbOrdreParagraphe;
    }

    /**
     * Set idParagraphePrec
     *
     * @param integer $idParagraphePrec
     *
     * @return DocumentParagrapheInfobase
     */
    public function setIdParagraphePrec($idParagraphePrec)
    {
        $this->idParagraphePrec = $idParagraphePrec;

        return $this;
    }

    /**
     * Get idParagraphePrec
     *
     * @return integer
     */
    public function getIdParagraphePrec()
    {
        return $this->idParagraphePrec;
    }

    /**
     * Set idGrpParagraphe
     *
     * @param \SalarieBundle\Entity\Document\DocumentGrpParagrapheInfobase $idGrpParagraphe
     *
     * @return DocumentParagrapheInfobase
     */
    public function setIdGrpParagraphe(\SalarieBundle\Entity\Document\DocumentGrpParagrapheInfobase $idGrpParagraphe = null)
    {
        $this->idGrpParagraphe = $idGrpParagraphe;

        return $this;
    }

    /**
     * Get idGrpParagraphe
     *
     * @return \SalarieBundle\Entity\Document\DocumentGrpParagrapheInfobase
     */
    public function getIdGrpParagraphe()
    {
        return $this->idGrpParagraphe;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return DocumentParagrapheInfobase
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }
}
