<?php

namespace SalarieBundle\Entity\Document;

use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentElementsParam
 *
 * @ORM\Table(name="document_elements_param")
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Document\DocumentElementsParamRepository")
 */
class DocumentElementsParam
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_table_param", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idTableParam;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_element_param", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idElementParam;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_type_document", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idTypeDocument = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="li_table_param", type="string", length=40, nullable=false)
     */
    private $liTableParam;

    /**
     * @var string
     *
     * @ORM\Column(name="li_element_param", type="string", length=100, nullable=false)
     */
    private $liElementParam;

    /**
     * Set idTableParam
     *
     * @param integer $idTableParam
     *
     * @return DocumentElementsParam
     */
    public function setIdTableParam($idTableParam)
    {
        $this->idTableParam = $idTableParam;

        return $this;
    }

    /**
     * Get idTableParam
     *
     * @return integer
     */
    public function getIdTableParam()
    {
        return $this->idTableParam;
    }

    /**
     * Set idElementParam
     *
     * @param integer $idElementParam
     *
     * @return DocumentElementsParam
     */
    public function setIdElementParam($idElementParam)
    {
        $this->idElementParam = $idElementParam;

        return $this;
    }

    /**
     * Get idElementParam
     *
     * @return integer
     */
    public function getIdElementParam()
    {
        return $this->idElementParam;
    }

    /**
     * Set idTypeDocument
     *
     * @param integer $idTypeDocument
     *
     * @return DocumentElementsParam
     */
    public function setIdTypeDocument($idTypeDocument)
    {
        $this->idTypeDocument = $idTypeDocument;

        return $this;
    }

    /**
     * Get idTypeDocument
     *
     * @return integer
     */
    public function getIdTypeDocument()
    {
        return $this->idTypeDocument;
    }

    /**
     * Set liTableParam
     *
     * @param string $liTableParam
     *
     * @return DocumentElementsParam
     */
    public function setLiTableParam($liTableParam)
    {
        $this->liTableParam = $liTableParam;

        return $this;
    }

    /**
     * Get liTableParam
     *
     * @return string
     */
    public function getLiTableParam()
    {
        return $this->liTableParam;
    }

    /**
     * Set liElementParam
     *
     * @param string $liElementParam
     *
     * @return DocumentElementsParam
     */
    public function setLiElementParam($liElementParam)
    {
        $this->liElementParam = $liElementParam;

        return $this;
    }

    /**
     * Get liElementParam
     *
     * @return string
     */
    public function getLiElementParam()
    {
        return $this->liElementParam;
    }
}
