<?php

namespace SalarieBundle\Entity\Document;

use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentGrpParagrapheInfobase
 *
 * @ORM\Table(name="document_grp_paragraphe_infobase", uniqueConstraints={@ORM\UniqueConstraint(name="document_grp_paragraphe_infob_nb_ordre_grp_paragraphe_id_ty_key", columns={"nb_ordre_grp_paragraphe", "id_type_document"}), @ORM\UniqueConstraint(name="document_grp_paragraphe_infob_id_grp_paragraphe_prec_id_typ_key", columns={"id_grp_paragraphe_prec", "id_type_document"})}, indexes={@ORM\Index(name="fki_gencontrat_grp_paragraphe_infobase_id_matricule_maj_fkey", columns={"id_matricule_maj"})})
 * @ORM\Entity
 */
class DocumentGrpParagrapheInfobase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_grp_paragraphe", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idGrpParagraphe;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_type_document", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idTypeDocument = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="li_grp_paragraphe", type="string", length=80, nullable=false)
     */
    private $liGrpParagraphe;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_actif", type="bit", nullable=false)
     */
    private $isActif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_maj", type="datetime", nullable=false)
     */
    private $dtMaj;

    /**
     * @var string
     *
     * @ORM\Column(name="li_commentaire_maj", type="string", length=200, nullable=false)
     */
    private $liCommentaireMaj;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_ordre_grp_paragraphe", type="smallint", nullable=true)
     */
    private $nbOrdreGrpParagraphe;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_grp_paragraphe_prec", type="smallint", nullable=true)
     */
    private $idGrpParagraphePrec;

    /**
     * @var \SalarieBundle\Entity\Salarie\SalarieInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Salarie\SalarieInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_matricule_maj", referencedColumnName="id_matricule")
     * })
     */
    private $idMatriculeMaj;

    /**
     * Set idGrpParagraphe
     *
     * @param integer $idGrpParagraphe
     *
     * @return DocumentGrpParagrapheInfobase
     */
    public function setIdGrpParagraphe($idGrpParagraphe)
    {
        $this->idGrpParagraphe = $idGrpParagraphe;

        return $this;
    }

    /**
     * Get idGrpParagraphe
     *
     * @return integer
     */
    public function getIdGrpParagraphe()
    {
        return $this->idGrpParagraphe;
    }

    /**
     * Set idTypeDocument
     *
     * @param integer $idTypeDocument
     *
     * @return DocumentGrpParagrapheInfobase
     */
    public function setIdTypeDocument($idTypeDocument)
    {
        $this->idTypeDocument = $idTypeDocument;

        return $this;
    }

    /**
     * Get idTypeDocument
     *
     * @return integer
     */
    public function getIdTypeDocument()
    {
        return $this->idTypeDocument;
    }

    /**
     * Set liGrpParagraphe
     *
     * @param string $liGrpParagraphe
     *
     * @return DocumentGrpParagrapheInfobase
     */
    public function setLiGrpParagraphe($liGrpParagraphe)
    {
        $this->liGrpParagraphe = $liGrpParagraphe;

        return $this;
    }

    /**
     * Get liGrpParagraphe
     *
     * @return string
     */
    public function getLiGrpParagraphe()
    {
        return $this->liGrpParagraphe;
    }

    /**
     * Set isActif
     *
     * @param bit $isActif
     *
     * @return DocumentGrpParagrapheInfobase
     */
    public function setIsActif($isActif)
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return bit
     */
    public function getIsActif()
    {
        return $this->isActif;
    }

    /**
     * Set dtMaj
     *
     * @param \DateTime $dtMaj
     *
     * @return DocumentGrpParagrapheInfobase
     */
    public function setDtMaj($dtMaj)
    {
        $this->dtMaj = $dtMaj;

        return $this;
    }

    /**
     * Get dtMaj
     *
     * @return \DateTime
     */
    public function getDtMaj()
    {
        return $this->dtMaj;
    }

    /**
     * Set liCommentaireMaj
     *
     * @param string $liCommentaireMaj
     *
     * @return DocumentGrpParagrapheInfobase
     */
    public function setLiCommentaireMaj($liCommentaireMaj)
    {
        $this->liCommentaireMaj = $liCommentaireMaj;

        return $this;
    }

    /**
     * Get liCommentaireMaj
     *
     * @return string
     */
    public function getLiCommentaireMaj()
    {
        return $this->liCommentaireMaj;
    }

    /**
     * Set nbOrdreGrpParagraphe
     *
     * @param integer $nbOrdreGrpParagraphe
     *
     * @return DocumentGrpParagrapheInfobase
     */
    public function setNbOrdreGrpParagraphe($nbOrdreGrpParagraphe)
    {
        $this->nbOrdreGrpParagraphe = $nbOrdreGrpParagraphe;

        return $this;
    }

    /**
     * Get nbOrdreGrpParagraphe
     *
     * @return integer
     */
    public function getNbOrdreGrpParagraphe()
    {
        return $this->nbOrdreGrpParagraphe;
    }

    /**
     * Set idGrpParagraphePrec
     *
     * @param integer $idGrpParagraphePrec
     *
     * @return DocumentGrpParagrapheInfobase
     */
    public function setIdGrpParagraphePrec($idGrpParagraphePrec)
    {
        $this->idGrpParagraphePrec = $idGrpParagraphePrec;

        return $this;
    }

    /**
     * Get idGrpParagraphePrec
     *
     * @return integer
     */
    public function getIdGrpParagraphePrec()
    {
        return $this->idGrpParagraphePrec;
    }

    /**
     * Set idMatriculeMaj
     *
     * @param \SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj
     *
     * @return DocumentGrpParagrapheInfobase
     */
    public function setIdMatriculeMaj(\SalarieBundle\Entity\Salarie\SalarieInfobase $idMatriculeMaj = null)
    {
        $this->idMatriculeMaj = $idMatriculeMaj;

        return $this;
    }

    /**
     * Get idMatriculeMaj
     *
     * @return \SalarieBundle\Entity\Salarie\SalarieInfobase
     */
    public function getIdMatriculeMaj()
    {
        return $this->idMatriculeMaj;
    }
}
