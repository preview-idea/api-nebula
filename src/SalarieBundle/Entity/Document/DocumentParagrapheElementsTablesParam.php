<?php

namespace SalarieBundle\Entity\Document;

use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentParagrapheElementsTablesParam
 *
 * @ORM\Table(name="document_paragraphe_elements_tables_param", uniqueConstraints={@ORM\UniqueConstraint(name="document_paragraphe_elements__id_paragraphe_id_element_para_key", columns={"id_paragraphe", "id_element_param", "id_table_param", "id_type_document"})}, indexes={@ORM\Index(name="IDX_E18A4743E9912961ABBE7CEF", columns={"id_paragraphe", "id_type_document"}), @ORM\Index(name="IDX_E18A4743DEF0D975B0147C06ABBE7CEF", columns={"id_element_param", "id_table_param", "id_type_document"})})
 * @ORM\Entity
 */
class DocumentParagrapheElementsTablesParam
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_ligne_paragraphe_elements", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="document_paragraphe_elements_tables_param_id_ligne_paragraphe_elements_seq", allocationSize=1, initialValue=1)
     */
    private $idLigneParagrapheElements;

    /**
     * @var \SalarieBundle\Entity\Document\DocumentParagrapheInfobase
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Document\DocumentParagrapheInfobase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_paragraphe", referencedColumnName="id_paragraphe"),
     *   @ORM\JoinColumn(name="id_type_document", referencedColumnName="id_type_document")
     * })
     */
    private $idParagraphe;

    /**
     * @var \SalarieBundle\Entity\Document\DocumentElementsParam
     *
     * @ORM\ManyToOne(targetEntity="SalarieBundle\Entity\Document\DocumentElementsParam")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_element_param", referencedColumnName="id_element_param"),
     *   @ORM\JoinColumn(name="id_table_param", referencedColumnName="id_table_param"),
     *   @ORM\JoinColumn(name="id_type_document", referencedColumnName="id_type_document")
     * })
     */
    private $idElementParam;

    /**
     * Get idLigneParagrapheElements
     *
     * @return integer
     */
    public function getIdLigneParagrapheElements()
    {
        return $this->idLigneParagrapheElements;
    }

    /**
     * Set idParagraphe
     *
     * @param \SalarieBundle\Entity\Document\DocumentParagrapheInfobase $idParagraphe
     *
     * @return DocumentParagrapheElementsTablesParam
     */
    public function setIdParagraphe(\SalarieBundle\Entity\Document\DocumentParagrapheInfobase $idParagraphe = null)
    {
        $this->idParagraphe = $idParagraphe;

        return $this;
    }

    /**
     * Get idParagraphe
     *
     * @return \SalarieBundle\Entity\Document\DocumentParagrapheInfobase
     */
    public function getIdParagraphe()
    {
        return $this->idParagraphe;
    }

    /**
     * Set idElementParam
     *
     * @param \SalarieBundle\Entity\Document\DocumentElementsParam $idElementParam
     *
     * @return DocumentParagrapheElementsTablesParam
     */
    public function setIdElementParam(\SalarieBundle\Entity\Document\DocumentElementsParam $idElementParam = null)
    {
        $this->idElementParam = $idElementParam;

        return $this;
    }

    /**
     * Get idElementParam
     *
     * @return \SalarieBundle\Entity\Document\DocumentElementsParam
     */
    public function getIdElementParam()
    {
        return $this->idElementParam;
    }
}
