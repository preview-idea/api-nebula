<?php

namespace SalarieBundle\Entity\Document;

use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentElementsObjet
 *
 * @ORM\Table(name="document_elements_objet", uniqueConstraints={@ORM\UniqueConstraint(name="document_elements_objet_id_table_objet_id_colonne_objet_id__key", columns={"id_table_objet", "id_colonne_objet", "id_type_document"})}, indexes={@ORM\Index(name="IDX_37B20FD41B91E2AE", columns={"id_ligne_document_tables_objet"})})
 * @ORM\Entity(repositoryClass="SalarieBundle\Repository\Document\DocumentElementsObjetRepository")
 */
class DocumentElementsObjet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_colonne_objet", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idColonneObjet;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_table_objet", type="integer", nullable=false)
     */
    private $idTableObjet;

    /**
     * @var string
     *
     * @ORM\Column(name="li_table_objet", type="string", length=40, nullable=false)
     */
    private $liTableObjet;

    /**
     * @var string
     *
     * @ORM\Column(name="li_colonne_objet", type="string", length=40, nullable=false)
     */
    private $liColonneObjet;

    /**
     * @var string
     *
     * @ORM\Column(name="li_colonne_objet_description", type="string", length=60, nullable=true)
     */
    private $liColonneObjetDescription;

    /**
     * @var bit
     *
     * @ORM\Column(name="is_visible_utilisateur", type="bit", nullable=true)
     */
    private $isVisibleUtilisateur;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_type_document", type="smallint", nullable=false)
     */
    private $idTypeDocument = '1';

    /**
     * @var \SalarieBundle\Entity\Document\DocumentTablesObjet
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="SalarieBundle\Entity\Document\DocumentTablesObjet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ligne_document_tables_objet", referencedColumnName="id_ligne_document_tables_objet")
     * })
     */
    private $idLigneDocumentTablesObjet;

    /**
     * Set idColonneObjet
     *
     * @param integer $idColonneObjet
     *
     * @return DocumentElementsObjet
     */
    public function setIdColonneObjet($idColonneObjet)
    {
        $this->idColonneObjet = $idColonneObjet;

        return $this;
    }

    /**
     * Get idColonneObjet
     *
     * @return integer
     */
    public function getIdColonneObjet()
    {
        return $this->idColonneObjet;
    }

    /**
     * Set idTableObjet
     *
     * @param integer $idTableObjet
     *
     * @return DocumentElementsObjet
     */
    public function setIdTableObjet($idTableObjet)
    {
        $this->idTableObjet = $idTableObjet;

        return $this;
    }

    /**
     * Get idTableObjet
     *
     * @return integer
     */
    public function getIdTableObjet()
    {
        return $this->idTableObjet;
    }

    /**
     * Set liTableObjet
     *
     * @param string $liTableObjet
     *
     * @return DocumentElementsObjet
     */
    public function setLiTableObjet($liTableObjet)
    {
        $this->liTableObjet = $liTableObjet;

        return $this;
    }

    /**
     * Get liTableObjet
     *
     * @return string
     */
    public function getLiTableObjet()
    {
        return $this->liTableObjet;
    }

    /**
     * Set liColonneObjet
     *
     * @param string $liColonneObjet
     *
     * @return DocumentElementsObjet
     */
    public function setLiColonneObjet($liColonneObjet)
    {
        $this->liColonneObjet = $liColonneObjet;

        return $this;
    }

    /**
     * Get liColonneObjet
     *
     * @return string
     */
    public function getLiColonneObjet()
    {
        return $this->liColonneObjet;
    }

    /**
     * Set liColonneObjetDescription
     *
     * @param string $liColonneObjetDescription
     *
     * @return DocumentElementsObjet
     */
    public function setLiColonneObjetDescription($liColonneObjetDescription)
    {
        $this->liColonneObjetDescription = $liColonneObjetDescription;

        return $this;
    }

    /**
     * Get liColonneObjetDescription
     *
     * @return string
     */
    public function getLiColonneObjetDescription()
    {
        return $this->liColonneObjetDescription;
    }

    /**
     * Set isVisibleUtilisateur
     *
     * @param bit $isVisibleUtilisateur
     *
     * @return DocumentElementsObjet
     */
    public function setIsVisibleUtilisateur($isVisibleUtilisateur)
    {
        $this->isVisibleUtilisateur = $isVisibleUtilisateur;

        return $this;
    }

    /**
     * Get isVisibleUtilisateur
     *
     * @return bit
     */
    public function getIsVisibleUtilisateur()
    {
        return $this->isVisibleUtilisateur;
    }

    /**
     * Set idTypeDocument
     *
     * @param integer $idTypeDocument
     *
     * @return DocumentElementsObjet
     */
    public function setIdTypeDocument($idTypeDocument)
    {
        $this->idTypeDocument = $idTypeDocument;

        return $this;
    }

    /**
     * Get idTypeDocument
     *
     * @return integer
     */
    public function getIdTypeDocument()
    {
        return $this->idTypeDocument;
    }

    /**
     * Set idLigneDocumentTablesObjet
     *
     * @param \SalarieBundle\Entity\Document\DocumentTablesObjet $idLigneDocumentTablesObjet
     *
     * @return DocumentElementsObjet
     */
    public function setIdLigneDocumentTablesObjet(\SalarieBundle\Entity\Document\DocumentTablesObjet $idLigneDocumentTablesObjet)
    {
        $this->idLigneDocumentTablesObjet = $idLigneDocumentTablesObjet;

        return $this;
    }

    /**
     * Get idLigneDocumentTablesObjet
     *
     * @return \SalarieBundle\Entity\Document\DocumentTablesObjet
     */
    public function getIdLigneDocumentTablesObjet()
    {
        return $this->idLigneDocumentTablesObjet;
    }
}
