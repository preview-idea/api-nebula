<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 18/01/2018
 * Time: 14:31
 */

namespace SalarieBundle\Controller;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use SalarieBundle\Entity\ObjSalarie;
use SalarieBundle\Entity\Salarie\SalarieInfobase;
use SalarieBundle\Entity\Views\Salarie\ViewDernierContratObjSalarie;
use SalarieBundle\Entity\Views\Salarie\ViewDerniereIdentiteObjSalarie;
use SalarieBundle\Form\ObjSalarieType;
use Core\Tools\GenerateForm\GenerateForm;
use SalarieBundle\Repository\ObjSalarieRepository;
use SalarieBundle\Repository\Views\Salarie\ViewDernierContratObjSalarieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SalarieBundle\Repository\Salarie\SalarieInfobaseRepository;

class SalarieController extends Controller
{

    /** @var ObjSalarieRepository $objSalarieRepo */
    protected $objSalarieRepo;

    /** @var ViewDernierContratObjSalarieRepository $viewSalarieRepo */
    protected $viewSalarieRepo;

    /** @var \Doctrine\Common\Persistence\ObjectManager $manager */
    protected $manager;

    /** @var SalarieInfobaseRepository $salarieInfobaseRepository */
    protected $salarieInfobaseRepository;

    /** @var ViewDerniereIdentiteObjSalarie $viewDerniereIdentiteRepo */
    protected $viewDerniereIdentiteRepo;

    /**
     * SalarieController constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        if (!$this->manager instanceof EntityManager) :
            $this->manager = $managerRegistry->getManager('salarie');
            $this->objSalarieRepo =  $this->manager->getRepository(ObjSalarie::class);
            $this->viewSalarieRepo =  $this->manager->getRepository(ViewDernierContratObjSalarie::class);
            $this->viewDerniereIdentiteRepo =  $this->manager->getRepository(ViewDerniereIdentiteObjSalarie::class);
            $this->salarieInfobaseRepository =  $this->manager->getRepository(SalarieInfobase::class);

        endif;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/salaries")
     * @return array
     */
    public function salarieListAction()
    {
        return $this->viewSalarieRepo->viewListDernierContrat();

    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/salaries")
     * @param Request $request
     * @return object|View
     */
    public function salarieNewAction(Request $request)
    {
        $salarie = new ObjSalarie();

        $form = $this->createForm(ObjSalarieType::class, $salarie);
        $form->submit($request->request->all());

        if ($form->isValid()) :
            $this->manager->persist($salarie);
            $this->manager->flush();

            return $salarie;
        endif;

        return $form;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/salaries/{idMatricule}", requirements={"idMatricule"="\d+"})
     * @param Request $request
     * @return View|ObjSalarie
     */
    public function salarieOneAction(Request $request)
    {
        /** @var ObjSalarie $salarie */
        $salarie = $this->objSalarieRepo->find(
            $request->get('idMatricule')
        );

        if (!$salarie) :
            return $this->salarieNotFound();
        endif;

        return $salarie;
    }

    /**
     * @Rest\View()
     * @Rest\Put("/salaries/{idMatricule}", requirements={"idMatricule"="\d+"})
     * @param Request $request
     * @param bool $clearMissing
     * @return View|object
     */
    public function salarieUpdateAction(Request $request, $clearMissing = true)
    {
        $salarie = $this->salarieOneAction($request);

        if ($salarie instanceof View) :
            return $salarie;
        endif;

        $form = $this->createForm(ObjSalarieType::class, $salarie);
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) :
            $this->manager->persist($salarie);
            $this->manager->flush();

            return $salarie;
        endif;

        return $form;
    }

    /**
     * @Rest\View()
     * @Rest\Patch("/salaries/{idMatricule}", requirements={"idMatricule"="\d+"})
     * @param Request $request
     * @return View|object
     */
    public function salarieUpdatePatchAction(Request $request)
    {
        return $this->salarieUpdateAction($request, false);
    }

    /**
     * @Rest\View()
     * @Rest\Delete("/salaries/{idMatricule}", requirements={"idMatricule"="\d+"})
     * @param Request $request
     * @return View|array
     */
    public function salarieRemoveAction(Request $request)
    {
        $salarie = $this->objSalarieRepo->find(
            $request->get('idMatricule')
        );

        /** @var ObjSalarie $salarie */
        if(!$salarie) :
            return $this->salarieNotFound();

        endif;

        $this->manager->remove($salarie);
        $this->manager->flush();

        return View::create(
            ['message' => 'Suppression effectuée'],
            Response::HTTP_OK
        );

    }

    /**
     * Fonction déplacée dans SalarieController pour utilisation dans Contrat\ExtraController
     *
     * @Rest\View()
     * @Rest\Post("/salaries/inactif/{idMatricule}", requirements={"idMatricule"="\d+"})
     * @param Request $request
     * @return View| ObjSalarie
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneInactifAction(Request $request)
    {
        $salarie = $this->objSalarieRepo->viewGetOneInactif(
            $request->get('idMatricule')
        );

        if (!$salarie) :
            return $this->salarieNotFound();

        endif;

        return $salarie;

    }

    /**
     * @param Request $request
     * @param string $index
     * @return View|mixed
     */
    protected function verifiyFilterParam(Request $request, $index = 'params')
    {
        $filter = $request->query->get('filter');

        if (!isset($filter[$index])) :
            view::create( ['message' => 'Désolé il manque le filtre'],Response::HTTP_BAD_REQUEST);
        endif;

        return $filter;

    }

    /**
     * @return View
     */
    protected function salarieNotFound()
    {
        return View::create(
            ['message' => 'Salarie introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }

}