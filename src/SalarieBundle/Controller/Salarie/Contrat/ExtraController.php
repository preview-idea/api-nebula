<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/09/2018
 * Time: 16:39
 */

namespace SalarieBundle\Controller\Salarie\Contrat;

use Core\Tools\Batch\CoreBatches;
use Core\Tools\GenerateForm\GenerateForm;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use SalarieBundle\Controller\Salarie\ContratController;
use SalarieBundle\Entity\Avenant\AvenantInfobase;
use SalarieBundle\Entity\ObjContrat;
use SalarieBundle\Entity\ObjContratTransferts;
use SalarieBundle\Form\ObjContratTransfertsType;
use SalarieBundle\Repository\Avenant\AvenantInfobaseRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ExtraController extends ContratController
{

    /**
     * @Rest\View()
     * @Rest\Get("/contrat/form")
     * @return GenerateForm[]
     */
    public function getContratFormAction()
    {
        $this->form->setObject([
            'obj_contrat',
            'obj_contrat_clausesadditionnelles',
            'obj_contrat_clausesoptionnelles',
            'obj_contrat_repartitionhoraire',
            'obj_contrat_primes'
        ]);

        return $this->form->generateForm($this->manager);
    }

    /**
     * @Rest\View()
     * @Rest\Get("/contrats/validate/{idMatriculeMaj}", requirements={"idMatriculeMaj"="\d+"})
     * @param Request $request
     * @return ObjContrat[]
     */
    public function contratListValidAction(Request $request)
    {
        $contrat = $this->objContratRepo->getListOnValidation(
            $request->get('idMatriculeMaj')
        );

        return $contrat;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/contrats/{idContrat}/transfert", requirements={"idMatricule"="\d+"})
     * @param Request $request
     * @return object
     */
    public function contratTransfertAction(Request $request)
    {
        $contrat = $this->contratOneAction($request);

        if ($contrat instanceof View) :
            return $contrat;

        endif;

        $transfert = new ObjContratTransferts();

        $form = $this->createForm(ObjContratTransfertsType::class, $transfert);
        $form->submit($request->request->all());

        if ($form->isValid()) :
            $this->manager->persist($transfert);
            $this->manager->flush();

            return $transfert;

        endif;

        return $form;

    }

    /**
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/salaries/{idMatricule}/contrats/current", requirements={"idMatricule"="\d+"})
     * @param Request $request
     * @return object|array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function contratCurrentAction(Request $request)
    {
        $salarie = $this->salarieOneAction($request);

        if ($salarie instanceof View) :
            return $salarie;

        endif;

        $contrat = $this->objContratRepo->getCurrentContratsByIdMatricule(
            $request->get('idMatricule')
        );

        if (!$contrat) :
            return $this->contratNotFound();

        endif;

        return $contrat;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/contrats/{idContrat}/ismajbyavenant", requirements={"idContrat"="\d+"})
     * @param Request $request
     * @return View
     */
    public function documentIsMajByAvenantAction(Request $request)
    {
        /** @var AvenantInfobaseRepository $avenantInfobaseRepository */
        $avenantInfobaseRepository = $this->manager->getRepository(AvenantInfobase::class);

        $nbLine = $avenantInfobaseRepository->countLineMajByAvenant(
            $request->get('idContrat')
        );

        return View::create(
            ['isMajByAvenant' => $nbLine ? true : false],
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\View()
     * @Rest\Get("/salaries/{idMatricule}/contrats/{idContrat}/histo", requirements={"idMatricule"="\d+", "idContrat"="\d+"})
     * @param Request $request
     * @return array|View
     */
    public function histoContratAction(Request $request)
    {
        $contrat = $this->contratOneAction($request);

        if ($contrat instanceof View) :
            return $contrat;
        endif;

        return $this->contratInfobaseRepository->getDateCreationAndMaj($contrat->getIdContrat(), false);
    }

    /**
     * @Rest\View()
     * @Rest\Get("/salaries/contrats/ncloture")
     * @return array|View
     */
    public function getNonClotureContratAction()
    {
        $contratNonCloture = $this->viewNonClotureMosaicCometeObjContratRepo->getNonClotureContrat();

        if(!$contratNonCloture)
            return $this->contratListNotFound();

        return $contratNonCloture;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/salaries/contrats/nbncloture")
     * @return array|View
     */
    public function getNbNonClotureContratAction()
    {
        $nbContrat = $this->viewNonClotureMosaicCometeObjContratRepo->getNbNonClotureContrat();

        if (!isset($nbContrat['message']))
            return View::create([
                'nbContratNonCloture' => $nbContrat],
                Response::HTTP_OK
            );

        return $nbContrat;
    }

    /**
     * Get inactive contrat
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/salaries/{idMatricule}/contrats/{idContrat}/inactif", requirements={"idMatricule"="\d+", "idContrat"="\d+"})
     * @param Request $request
     * @return object
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function contratInactifOneAction(Request $request)
    {

        $salarie = $this->salarieOneAction($request);

        if ($salarie instanceof View) :

            // Test aussi si le salarie est inactif
            $salarie = $this->getOneInactifAction($request);
            if ($salarie instanceof View) :
                return $salarie;
            endif;

        endif;

        $contrat = $this->viewContratDwhRepo->findOneBy([
            'idMatricule' => $request->get('idMatricule'),
            'idContrat' => $request->get('idContrat')
        ]);

        if (!$contrat) :
            return $this->contratNotFound();
        endif;

        return $contrat;

    }

    /**
     * @Rest\View()
     * @Rest\Post("/salaries/contrats/cloture")
     * @param Request $request
     * @return array|View
     */
    public function getClotureContratByDateAction(Request $request)
    {
        $month = $request->get('idMonth');
        $year = $request->get('idYear');
        $contratCloture = $this->viewClotureObjContratRepo->getListContratClotureByDate($month,$year);

        if(!$contratCloture)
            return $this->contratListNotFound();

        return $contratCloture;
    }

    /**
     * @return View
     */
    protected function contratListNotFound()
    {
        return View::create(
            ['message' => 'Aucun Contrat trouvé'],
            Response::HTTP_NOT_FOUND
        );
    }

}