<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/09/2018
 * Time: 16:39
 */

namespace SalarieBundle\Controller\Salarie\Contrat\Avenant;

use Core\Tools\GenerateForm\GenerateForm;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use SalarieBundle\Controller\Salarie\Contrat\AvenantController;
use SalarieBundle\Entity\ObjAvenant;
use Symfony\Component\HttpFoundation\Request;

class ExtraController extends AvenantController
{

    /**
     * @Rest\View()
     * @Rest\Get("/avenants/validate/{idMatriculeMaj}", requirements={"idMatriculeMaj"="\d+"})
     * @param Request $request
     * @return array
     */
    public function avenantListValidAction(Request $request)
    {

        $avenant = $this->objAvenantRepo->getListOnValidation(
            $request->get('idMatriculeMaj')
        );

        return $avenant;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/avenant/form")
     * @return GenerateForm
     */
    public function getAvenantFormAction()
    {
        $this->form->setObject([
            'obj_avenant'
        ]);

        $fiche_avenant = $this->form->generateForm($this->manager);
        return $fiche_avenant;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/salaries/{idMatricule}/contrats/{idContrat}/avenants/{idAvenant}/histo", requirements={"idMatricule"="\d+", "idContrat"="\d+"})
     * @param Request $request
     * @return array|View
     */
    public function histoAvenantAction(Request $request)
    {
        $avenant = $this->avenantOneAction($request);

        if ($avenant instanceof View) :
            return $avenant;
        endif;

        return $this->contratInfobaseRepository->getDateCreationAndMaj($avenant->getIdContrat(), true);
    }

}