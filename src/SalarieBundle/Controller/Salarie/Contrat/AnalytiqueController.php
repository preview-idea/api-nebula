<?php
/**
 * Created by PhpStorm.
 * User: hbernier
 * Date: 24/05/2018
 * Time: 11:14
 */

namespace SalarieBundle\Controller\Salarie\Contrat;

use Doctrine\Common\Persistence\ManagerRegistry;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use SalarieBundle\Controller\Salarie\ContratController;
use SalarieBundle\Entity\ObjContrat;
use SalarieBundle\Entity\ObjContratAnalytique;
use SalarieBundle\Form\ObjContratAnalytiqueType;
use SalarieBundle\Repository\ObjContratAnalytiqueRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AnalytiqueController extends ContratController
{
    /** @var ObjContratAnalytiqueRepository $objAnalytiqueRepo  */
    protected $objAnalytiqueRepo;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry);
        $this->objAnalytiqueRepo = $this->manager->getRepository(ObjContratAnalytique::class);
    }

    /**
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     * @Rest\Post("/analytiques")
     * @param Request $request
     * @return array
     */
    public function contratWithAnalytiqueListAction(Request $request)
    {

        $filter = [
            'dtDebut' => $request->get('dtDebut'),
            'dtFin' => $request->get('dtFin')
        ];

        $analytiques = $this->objAnalytiqueRepo->getAllByDatesContrat($filter);
        return $analytiques;

    }

    /**
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/salaries/contrats/{idContrat}/analytiques", requirements={"idContrat"="\d+"})
     * @param Request $request
     * @return array|object
     */
    public function analytiqueListAction(Request $request)
    {
        $contrat = $this->objContratRepo->find($request->get('idContrat'));

        if($contrat instanceof View) :
            return $contrat;
        endif;

        $analytiques = $this->objAnalytiqueRepo->findBy(
            ['idContrat' => $contrat->getIdContrat()],
            ['nbPourcent' => 'DESC']
        );

        return $analytiques;

    }

    /**
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/salaries/contrats/{idContrat}/analytiques/{idContratAnalytique}", requirements={"idContrat"="\d+", "idContratAnalytique"="\d+"})
     * @param Request $request
     * @return object
     */
    public function analytiqueOneAction(Request $request)
    {

        $contrat = $this->objContratRepo->find($request->get('idContrat'));

        if ($contrat instanceof View) :
            return $contrat;
        endif;

        $analytique = $this->objAnalytiqueRepo->findOneBy([
            'idContrat' => $contrat->getIdContrat(),
            'idContratAnalytique' => $request->get('idContratAnalytique')
        ]);

        if(!$analytique) :
            return $this->analytiqueNotFound();
        endif;

        return $analytique;

    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/salaries/contrats/{idContrat}/analytiques", requirements={"idContrat"="\d+"})
     * @param Request $request
     * @return object
     */
    public function analytiqueNewAction(Request $request)
    {
        /** @var ObjContrat $contrat */
        $contrat = $this->objContratRepo->find($request->get('idContrat'));

        if (!$contrat) :
            return $this->contratNotFound();
        endif;

        $analytique = new ObjContratAnalytique();
        $analytique->setIdContrat($contrat);

        $form = $this->createForm(ObjContratAnalytiqueType::class, $analytique);
        $form->submit($request->request->all());

        if ($form->isValid()) :
            $this->manager->persist($analytique);
            $this->manager->flush();

            return $analytique;
        endif;

        return $form;

    }

    /**
     * @Rest\View()
     * @Rest\Patch("/salaries/contrats/{idContrat}/analytiques/{idContratAnalytique}", requirements={"idContrat"="\d+", "idContratAnalytique"="\d+"})
     * @param Request $request
     * @param bool $clearMissing
     * @return object
     */
    public function analytiqueUpdateAction(Request $request, $clearMissing = true)
    {
        /** @var ObjContrat $contrat */
        $contrat = $this->objContratRepo->find($request->get('idContrat'));

        if (!$contrat) :
            return $this->contratNotFound();
        endif;

        $analytique = $this->objAnalytiqueRepo->findOneBy([
            'idContrat' => $contrat->getIdContrat(),
            'idContratAnalytique' => $request->get('idContratAnalytique')
        ]);

        if(!$analytique) :
            return $this->analytiqueNotFound();
        endif;

        $form = $this->createForm(ObjContratAnalytiqueType::class, $analytique);
        $form->submit($request->request->all(), $clearMissing);

        if($form->isValid()) :
            $this->manager->persist($analytique);
            $this->manager->flush();

            return $analytique;
        endif;

        return $form;

    }

    /**
     * @Rest\View()
     * @Rest\Patch("/salaries/contrats/{idContrat}/analytiques/{idContratAnalytique}", requirements={"idContrat"="\d+", "idContratAnalytique"="\d+"})
     * @param Request $request
     * @return object
     */
    public function analytiqueUpdatePatchAction(Request $request)
    {
        return $this->analytiqueUpdateAction($request);
    }

    /**
     * @Rest\View()
     * @Rest\Delete("/salaries/contrats/analytiques/{idContratAnalytique}", requirements={"idContratAnalytique"="\d+"})
     * @param Request $request
     * @return object
     */
    public function analytiqueRemoveAction(Request $request)
    {
        $analytique = $this->objAnalytiqueRepo->findOneBy([
            'idContratAnalytique' => $request->get('idContratAnalytique')
        ]);

        if (!$analytique) :
            return $this->analytiqueNotFound();
        endif;

        $this->manager->remove($analytique);
        $this->manager->flush();

        return View::create(
            ['message' => 'Suppression effectuée'],
            Response::HTTP_OK
        );

    }

    /**
     * @return View
     */
    private function analytiqueNotFound()
    {
        return View::create([
            'message' => 'Analytique introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }

}