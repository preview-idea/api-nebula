<?php
/**
 * Created by PhpStorm.
 * User: dev1
 * Date: 03/04/2018
 * Time: 12:13
 */

namespace SalarieBundle\Controller\Salarie\Contrat;

use Doctrine\Common\Persistence\ManagerRegistry;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use SalarieBundle\Controller\Salarie\ContratController;
use SalarieBundle\Entity\ObjAvenant;
use SalarieBundle\Entity\ObjContrat;
use SalarieBundle\Entity\Views\Avenant\ViewHistoriqueObjAvenant;
use SalarieBundle\Form\ObjAvenantType;
use SalarieBundle\Repository\ObjAvenantRepository;
use SalarieBundle\Repository\Views\Avenant\ViewHistoriqueObjAvenantRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AvenantController extends ContratController
{

    /** @var ObjAvenantRepository $objAvenantRepo */
    protected $objAvenantRepo;

    /** @var ViewHistoriqueObjAvenantRepository $viewAvenantRepo  */
    protected $viewAvenantRepo;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry);
        $this->objAvenantRepo = $this->manager->getRepository(ObjAvenant::class);
        $this->viewAvenantRepo = $this->manager->getRepository(ViewHistoriqueObjAvenant::class);
    }

    /**
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/salaries/{idMatricule}/contrats/{idContrat}/avenants",
     *     requirements={"idMatricule"="\d+", "idContrat"="\d+"})
     * @param Request $request
     * @return View|array
     */
    public function avenantListAction(Request $request)
    {

        $contrat = $this->contratOneAction($request);

        if ($contrat instanceof View) :
            return $contrat;
        endif;

        $avenant = $this->viewAvenantRepo->viewListAvenantByidContrat($contrat->getIdContrat());

        return $avenant;

    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/salaries/{idMatricule}/contrats/{idContrat}/avenants",
     *     requirements={"idMatricule"="\d+","idContrat"="\d+"})
     * @param Request $request
     * @return object
     */
    public function avenantNewAction(Request $request)
    {
        /** @var ObjContrat $contrat */
        $contrat = $this->contratOneAction($request);

        if ($contrat instanceof View) :
            return $contrat;
        endif;

        $avenant = new ObjAvenant();
        $avenant->setIdContratOrigine($contrat);

        $form = $this->createForm(ObjAvenantType::class, $avenant);
        $form->submit($request->request->all());

        if ($form->isValid()) :
            $this->manager->persist($avenant);
            $this->manager->flush();
            $this->manager->clear();

            $newAvenant = $this->objAvenantRepo->findOneBy(
                ['idLigneAvenant' => $avenant->getIdLigneAvenant()]
            );

            return $newAvenant;
        endif;

        return $form;

    }

    /**
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/salaries/{idMatricule}/contrats/{idContrat}/avenants/{idAvenant}",
     *     requirements={"idMatricule"="\d+", "idContrat"="\d+", "idAvenant"="\d+"})
     * @param Request $request
     * @return object
     */
    public function avenantOneAction(Request $request)
    {
        $contrat = $this->contratOneAction($request);

        if ($contrat instanceof View) :
            return $contrat;
        endif;

        $avenant = $this->objAvenantRepo->findOneBy(
            ['idContrat' => $request->get('idAvenant')]
        );

        if (!$avenant) :
            return $this->avenantNotFound();

        endif;

        return $avenant;

    }

    /**
     * @Rest\View()
     * @Rest\Delete("/salaries/{idMatricule}/contrats/{idContrat}/avenants/{idAvenant}",
     *     requirements={"idMatricule"="\d+", "idContrat"="\d+", "idAvenant"="\d+"})
     * @param Request $request
     * @return object
     */
    public function avenantRemoveAction(Request $request)
    {
        $contrat = $this->contratOneAction($request);

        if ($contrat instanceof View) :
            return $contrat;
        endif;

        $avenant = $this->objAvenantRepo->findOneBy([
            'idContratOrigine' => $contrat->getIdContrat(),
            'idContrat' => $request->get('idAvenant')
        ]);

        if(!$avenant) :
            return $this->avenantNotFound();
        endif;

        $this->manager->remove($avenant);
        $this->manager->flush();

        return View::create(
            ['message' => 'Suppression effectuée'],
            Response::HTTP_OK
        );

    }

    /**
     * @Rest\View()
     * @Rest\Put("/salaries/{idMatricule}/contrats/{idContrat}/avenants/{idAvenant}",
     *     requirements={"idMatricule"="\d+", "idContrat"="\d+", "idAvenant"="\d+"})
     * @param Request $request
     * @param bool $clearMissing
     * @return object
     */
    public function avenantUpdateAction(Request $request, $clearMissing = true)
    {

        $contrat = $this->contratOneAction($request);

        if ($contrat instanceof View) :
            return $contrat;
        endif;

        $avenant = $this->objAvenantRepo->findOneBy([
            'idContratOrigine' => $contrat->getIdContrat(),
            'idContrat' => $request->get('idAvenant')
        ]);

        if(!$avenant) :
            return $this->avenantNotFound();
        endif;

        $form = $this->createForm(ObjAvenantType::class, $avenant);
        $form->submit($request->request->all(), $clearMissing);

        if($form->isValid()) :
            $this->manager->persist($avenant);
            $this->manager->flush();

            return $avenant;
        endif;

        return $form;
    }

    /**
     * @Rest\View()
     * @Rest\Patch("/salaries/{idMatricule}/contrats/{idContrat}/avenants/{idAvenant}",
     *     requirements={"idMatricule"="\d+", "idContrat"="\d+", "idAvenant"="\d+"})
     * @param Request $request
     * @return object
     */
    public function avenantUpdatePatchAction(Request $request)
    {
        return $this->avenantUpdateAction($request, false);
    }

    /**
     * @return View
     */
    private function avenantNotFound()
    {
        return View::create([
            'message' => 'Avenant introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }


}