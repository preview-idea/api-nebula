<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 09/02/2018
 * Time: 18:33
 */

namespace SalarieBundle\Controller\Salarie;

use Doctrine\Common\Persistence\ManagerRegistry;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use SalarieBundle\Controller\SalarieController;
use SalarieBundle\Entity\Contrat\ContratInfobase;
use SalarieBundle\Entity\ObjContrat;
use SalarieBundle\Entity\Views\Contrat\ViewClotureObjContrat;
use SalarieBundle\Entity\Views\Contrat\ViewContratDwh;
use SalarieBundle\Entity\Views\Contrat\ViewHistoriqueObjContrat;
use SalarieBundle\Entity\Views\Contrat\ViewNonClotureMosaicCometeObjContrat;
use SalarieBundle\Form\ObjContratType;
use SalarieBundle\Repository\ObjContratRepository;
use SalarieBundle\Repository\Views\Contrat\ViewClotureObjContratRepository;
use SalarieBundle\Repository\Views\Contrat\ViewContratDwhRepository;
use SalarieBundle\Repository\Views\Contrat\ViewHistoriqueObjContratRepository;
use SalarieBundle\Repository\Views\Contrat\ViewNonClotureMosaicCometeObjContratRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SalarieBundle\Repository\Contrat\ContratInfobaseRepository;

class ContratController extends SalarieController
{
    /**
     * @var ViewHistoriqueObjContratRepository $viewContratRepo
     */
    protected $viewContratRepo;

    /**
     * @var ObjContratRepository $objContratRepo
     */
    protected $objContratRepo;

    /** @var ContratInfobaseRepository $contratInfobaseRepository */
    protected $contratInfobaseRepository;

    /**
     * @var ViewNonClotureMosaicCometeObjContratRepository $viewNonClotureMosaicCometeObjContratRepo
     */
    protected $viewNonClotureMosaicCometeObjContratRepo;

    /**
     * @var ViewContratDwhRepository $viewContratDwhRepo
     */
    protected $viewContratDwhRepo;

    /**
     * @var ViewClotureObjContratRepository $viewClotureObjContratRepo
     */
    protected $viewClotureObjContratRepo;


    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry);
        $this->objContratRepo = $this->manager->getRepository(ObjContrat::class);
        $this->viewContratRepo = $this->manager->getRepository(ViewHistoriqueObjContrat::class);
        $this->contratInfobaseRepository =  $this->manager->getRepository(ContratInfobase::class);
        $this->viewNonClotureMosaicCometeObjContratRepo = $this->manager->getRepository(ViewNonClotureMosaicCometeObjContrat::class);
        $this->viewClotureObjContratRepo = $this->manager->getRepository(ViewClotureObjContrat::class);
        $this->viewContratDwhRepo = $this->manager->getRepository(ViewContratDwh::class);
    }

    /**
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/salaries/{idMatricule}/contrats", requirements={"idMatricule"="\d+"})
     * @param Request $request
     * @return object|array
     */
    public function contratListAction(Request $request)
    {

        $contrat = $this->viewContratRepo->viewListContratByIdMatricule(
            $request->get('idMatricule')
        );

        return $contrat;

    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/salaries/{idMatricule}/contrats", requirements={"idMatricule"="\d+"})
     * @param Request $request
     * @return object
     */
    public function contratNewAction(Request $request)
    {
        $salarie = $this->salarieOneAction($request);

        if ($salarie instanceof View) :
            return $salarie;
        endif;

        $contrat = new ObjContrat();
        $contrat->setIdMatricule($salarie);

        $form = $this->createForm(ObjContratType::class, $contrat);
        $form->submit($request->request->all());

        if ($form->isValid()) :
            $this->manager->persist($contrat);
            $this->manager->flush();

            return $contrat;
        endif;

        return $form;

    }

    /**
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/salaries/{idMatricule}/contrats/{idContrat}", requirements={"idMatricule"="\d+", "idContrat"="\d+"})
     * @param Request $request
     * @return object
     */
    public function contratOneAction(Request $request)
    {

        $salarie = $this->salarieOneAction($request);

        if ($salarie instanceof View) :
            return $salarie;
        endif;

        $contrat = $this->objContratRepo->findOneBy([
            'idMatricule' => $request->get('idMatricule'),
            'idContrat' => $request->get('idContrat')
        ]);

        if (!$contrat) :
            return $this->contratNotFound();
        endif;

        return $contrat;

    }

    /**
     * @Rest\View()
     * @Rest\Put("/salaries/{idMatricule}/contrats/{idContrat}", requirements={"idMatricule"="\d+", "idContrat"="\d+"})
     * @param Request $request
     * @param bool $clearMissing
     * @return object
     */
    public function contratUpdateAction(Request $request, $clearMissing = true)
    {

        $salarie = $this->salarieOneAction($request);

        if ($salarie instanceof View) :
            return $salarie;
        endif;

        $contrat = $this->objContratRepo->findOneBy([
                'idMatricule' => $request->get('idMatricule'),
                'idContrat' => $request->get('idContrat')
            ]);

        if (!$contrat) :
            return $this->contratNotFound();
        endif;

        $form = $this->createForm(ObjContratType::class, $contrat);
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) :
            $this->manager->persist($contrat);
            $this->manager->flush();

            return $contrat;
        endif;

        return $form;

    }

    /**
     * @Rest\View()
     * @Rest\Patch("/salaries/{idMatricule}/contrats/{idContrat}", requirements={"idMatricule"="\d+", "idContrat"="\d+"})
     * @param Request $request
     * @return object
     */
    public function contratUpdatePatchAction(Request $request)
    {
        return $this->contratUpdateAction($request, false);
    }

    /**
     * @Rest\View()
     * @Rest\Delete("/salaries/{idMatricule}/contrats/{idContrat}", requirements={"idMatricule"="\d+", "idContrat"="\d+"})
     * @param Request $request
     * @return object
     */
    public function contratRemoveAction(Request $request)
    {
        $contrat = $this->contratOneAction($request);

        if (!$contrat) :
            return $this->contratNotFound();
        endif;

        $this->manager->remove($contrat);
        $this->manager->flush();

        return View::create(
            ['message' => 'Suppression effectuée'],
            Response::HTTP_OK
        );

    }

    /**
     * @return View
     */
    protected function contratNotFound()
    {
        return View::create(
            ['message' => 'Contrat introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }
}