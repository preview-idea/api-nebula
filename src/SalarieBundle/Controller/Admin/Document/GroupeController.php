<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 04/06/2018
 * Time: 12:40
 */

namespace SalarieBundle\Controller\Admin\Document;


use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use SalarieBundle\Controller\Admin\DocumentController;
use SalarieBundle\Entity\ObjDocumentGrpParagraphe;
use SalarieBundle\Form\ObjDocumentGrpParagrapheType;
use SalarieBundle\Repository\ObjDocumentGrpParagrapheRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GroupeController extends DocumentController
{

    /**
     * @Rest\View()
     * @Rest\Get("/admin/document/{idTypeDocument}/groupes", requirements={"idTypeDocument"="\d+"})
     * @param Request $request
     * @return array
     */
    public function groupeListAction(Request $request)
    {
        /** @var ObjDocumentGrpParagrapheRepository $grpParagrapheRepository */
        $grpParagrapheRepository = $this->getDoctrine()->getManager('salarie')
            ->getRepository(ObjDocumentGrpParagraphe::class);

        $groupes = $grpParagrapheRepository->getList(
            $request->get('idTypeDocument')
        );

        return $groupes;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/document/{idTypeDocument}/groupes/{idGrpParagraphe}",
     *     requirements={"idTypeDocument"="\d+", "idGrpParagraphe"="\d+"})
     * @param Request $request
     * @return View|ObjDocumentGrpParagraphe
     */
    public function groupeOneAction(Request $request)
    {

        $groupe = $this->grpParagrapheRepo->findOneBy([
            'idGrpParagraphe' => $request->get('idGrpParagraphe'),
            'idTypeDocument'  => $request->get('idTypeDocument')
        ]);

        if (!$groupe) :
            return $this->groupeNotFound();
        endif;

        return $groupe;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/admin/document/{idTypeDocument}/groupes", requirements={"idTypeDocument"="\d+"})
     * @param Request $request
     * @return object
     */
    public function groupeNewAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager('salarie');

        $groupe = new ObjDocumentGrpParagraphe();

        $form = $this->createForm(ObjDocumentGrpParagrapheType::class, $groupe);
        $form->submit($request->request->all());

        if ($form->isValid()) :
            $groupe->setIdGrpParagraphe($this->getGrpSequence());
            $em->persist($groupe);
            $em->flush();

            return $groupe;

        endif;

        return $form;

    }

    /**
     * @Rest\View()
     * @Rest\Patch("/admin/document/{idTypeDocument}/groupes/{idGrpParagraphe}",
     *     requirements={"idGrpParagraphe"="\d+", "idTypeDocument"="\d+"})
     * @param Request $request
     * @return View|object
     */
    public function groupeUpdateAction(Request $request)
    {

        $groupe = $this->groupeOneAction($request);

        if ($groupe instanceof View) :
            return $groupe;

        endif;

        $em = $this->getDoctrine()->getManager('salarie');

        $form = $this->createForm(ObjDocumentGrpParagrapheType::class, $groupe);
        $form->submit($request->request->all(), false);

        if ($form->isValid()) :
            $em->persist($groupe);
            $em->flush();

            return $groupe;

        endif;

        return $form;

    }

    /**
     * @Rest\View()
     * @Rest\Delete("/admin/document/{idTypeDocument}/groupes/{idGrpParagraphe}",
     *     requirements={"idGrpParagraphe"="\d+", "idTypeDocument"="\d+"})
     * @param Request $request
     * @return object
     */
    public function groupeRemoveAction(Request $request)
    {
        $groupe = $this->groupeOneAction($request);

        if ($groupe instanceof View) :
            return $groupe;

        endif;

        $em = $this->getDoctrine()->getManager('salarie');
        $em->remove($groupe);
        $em->flush();

        return View::create([
            'message' => 'Suppression effectuée' ]
        );

    }

    /**
     * GET SEQUENCE BEFORE INSERT ITEM.
     * On est obligé de récupéré la séquence dans une premier temps, car l'ORM ne supporte pas
     * l'ajout de la séquence dans une key composite.
     * @return View|string
     */
    private function getGrpSequence()
    {
        $em = $this->getDoctrine()->getManager('salarie');

        /** @var ObjDocumentGrpParagrapheRepository $grpParagrapheRepository */
        $grpParagrapheRepository = $em->getRepository(ObjDocumentGrpParagraphe::class);

        $sequence = $grpParagrapheRepository->getSequenceGrp();

        if (!$sequence) :
            return View::create([
                'message' => 'Sequence not found.'],
                Response::HTTP_NOT_FOUND
            );
        endif;

        return $sequence;
    }

    /**
     * @return View
     */
    private function groupeNotFound()
    {
        return View::create([
            'message' => 'Groupe introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }
}