<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 04/06/2018
 * Time: 12:40
 */

namespace SalarieBundle\Controller\Admin\Document;


use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use SalarieBundle\Entity\ObjDocumentParagraphe;
use SalarieBundle\Form\ObjDocumentParagrapheType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ParagrapheController extends GroupeController
{

    /**
     * @Rest\View()
     * @Rest\Get("/admin/document/{idTypeDocument}/paragraphes", requirements={"idTypeDocument"="\d+"})
     * @param Request $request
     * @return ObjDocumentParagraphe[]
     */
    public function paragrapheListAction(Request $request)
    {
        return $this->docParagraphRepo->getList($request->get('idTypeDocument'));
    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/document/{idTypeDocument}/groupes/{idGrpParagraphe}/paragraphes/{idParagraphe}",
     *     requirements={"idTypeDocument"="\d+", "idGrpParagraphe"="\d+", "idParagraphe"="\d+"})
     * @param Request $request
     * @return View|ObjDocumentParagraphe|\Symfony\Component\Form\FormInterface
     */
    public function paragrapheOneAction(Request $request)
    {

        /** @var ObjDocumentParagraphe $paragraph */
        $paragraph = $this->docParagraphRepo->findOneBy([
            'idTypeDocument' => $request->get('idTypeDocument'),
            'idParagraphe' => $request->get('idParagraphe'),
        ]);

        if (!$paragraph) :
            return $this->paragrapheNotFound();
        endif;

        return $paragraph;

    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/admin/document/{idTypeDocument}/groupes/{idGrpParagraphe}/paragraphes",
     *     requirements={"idTypeDocument"="\d+", "idGrpParagraphe"="\d+"})
     * @param Request $request
     * @return object
     * @throws \Doctrine\ORM\ORMException
     */
    public function paragrapheNewAction(Request $request)
    {

        $groupe = $this->groupeOneAction($request);

        if ($groupe instanceof View) :
            return $groupe;
        endif;

        $paragraphe = new ObjDocumentParagraphe();
        $paragraphe->setIdGrpParagraphe($groupe);

        $form = $this->createForm(ObjDocumentParagrapheType::class, $paragraphe);
        $form->submit($request->request->all(), false);

        if ($form->isValid()) :
            $paragraphe->setIdParagraphe($this->getPrgSequence());
            $this->manager->persist($paragraphe);
            $this->manager->flush();

            return $paragraphe;

        endif;

        return $form;

    }

    /**
     * @Rest\View()
     * @Rest\Patch("/admin/document/{idTypeDocument}/groupes/{idGrpParagraphe}/paragraphes/{idParagraphe}",
     *     requirements={"idTypeDocument"="\d+", "idParagraphe"="\d+", "idGrpParagraphe"="\d+"})
     * @param Request $request
     * @return View|object
     * @throws \Doctrine\ORM\ORMException
     */
    public function paragrapheUpdateAction(Request $request)
    {

        $paragraph = $this->paragrapheOneAction($request);

        if ($paragraph instanceof View) :
            return $paragraph;
        endif;

        $form = $this->createForm(ObjDocumentParagrapheType::class, $paragraph);
        $form->submit($request->request->all(), false);

        if ($form->isValid()) :
            $this->manager->persist($paragraph);
            $this->manager->flush();

            return $paragraph;

        endif;

        return $form;

    }

    /**
     * @Rest\View()
     * @Rest\Delete("/admin/document/{idTypeDocument}/groupes/{idGrpParagraphe}/paragraphes/{idParagraphe}",
     *     requirements={"idTypeDocument"="\d+", "idGrpParagraphe"="\d+", "idParagraphe"="\d+"})
     * @param Request $request
     * @return object
     * @throws \Doctrine\ORM\ORMException
     */
    public function paragrapheRemoveAction(Request $request)
    {

        $paragraph = $this->paragrapheOneAction($request);

        if ($paragraph instanceof View) :
            return $paragraph;
        endif;

        $this->manager->remove($paragraph);
        $this->manager->flush();

        return View::create([
            'message' => 'Suppression effectuée' ]
        );

    }

    /**
     * GET SEQUENCE BEFORE INSERT ITEM.
     * On est obligé de récupéré la séquence dans une premier temps, car l'ORM ne supporte pas
     * l'ajout de la séquence dans une key composite.
     * @return View|string
     */
    private function getPrgSequence()
    {
        $sequence = $this->docParagraphRepo->getSequencePrg();

        if (!$sequence) :
            return View::create([
                'message' => 'Sequence not found.'],
                Response::HTTP_NOT_FOUND
            );
        endif;

        return $sequence;
    }
    
    /**
     * @return View
     */
    private function paragrapheNotFound()
    {
        return View::create([
            'message' => 'Paragraphe introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }
}