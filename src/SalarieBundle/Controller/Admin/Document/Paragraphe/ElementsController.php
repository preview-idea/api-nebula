<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 13/06/2018
 * Time: 15:57
 */

namespace SalarieBundle\Controller\Admin\Document\Paragraphe;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use SalarieBundle\Controller\Admin\Document\ParagrapheController;
use SalarieBundle\Entity\ObjDocumentParagraphe;
use SalarieBundle\Entity\ObjDocumentParagrapheElements;
use SalarieBundle\Entity\Param\ParamTypeDocumentGenere;
use SalarieBundle\Repository\ObjDocumentParagrapheElementsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ElementsController extends ParagrapheController
{

    /**
     * @Rest\View()
     * @Rest\Get("/admin/document/{idTypeDocument}/groupes/{idGrpParagraphe}/paragraphes/{idParagraphe}/elements", requirements={"idParagraphe"="\d+", "idTypeDocument"="\d+"})
     * @param Request $request
     * @return array | View
     */
    public function elementListAction(Request $request)
    {

        $paragraph = $this->paragrapheOneAction($request);

        if ($paragraph instanceof View) :
            return $paragraph;
        endif;

        $em = $this->getDoctrine()->getManager('salarie');

        /** @var ObjDocumentParagrapheElementsRepository $paragrapheElementsRepository */
        $paragrapheElementsRepository = $em->getRepository(ObjDocumentParagrapheElements::class);

        $elements = $paragrapheElementsRepository->findby([
                'idParagraphe' => $paragraph
            ]);

        return $elements;

    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/admin/document/{idTypeDocument}/groupes/{idGrpParagraphe}/paragraphes/{idParagraphe}/elements",
     *     requirements={"idTypeDocument"="\d+","idParagraphe"="\d+", "idGrpParagraphe"="\d+"})
     * @param Request $request
     * @return object
     */
    public function elementNewAction(Request $request)
    {

        $paragraph = $this->paragrapheOneAction($request);

        if ($paragraph instanceof View) :
            return $paragraph;
        endif;

        $elements = $this->createElements($request, $paragraph);
        return $elements;
    }


    /**
     * @param Request $request
     * @param ObjDocumentParagraphe $paragraphe
     * @return mixed|\Symfony\Component\Form\FormInterface
     */
    public function createElements(Request $request, ObjDocumentParagraphe $paragraphe)
    {

        $em = $this->getDoctrine()->getManager('salarie');

        /** @var  ObjDocumentParagrapheElementsRepository $elementRepository */
        $elementRepository = $em->getRepository(ObjDocumentParagrapheElements::class);
        $this->clearElements($paragraphe);

        $elements = $request->get('elements');
        $arrObjElement = [];

        if (!$elements) :
            return $elements;

        endif;

        foreach ($elements as $element) :
            $element['idParagraphe'] = (int)$paragraphe->getIdParagraphe();
            $elementRepository->createElementByParagraph($element);
            $arrObjElement[] = $element;

        endforeach;

        return $arrObjElement;

    }

    /**
     * @param ObjDocumentParagraphe $paragraphe
     */
    public function clearElements(ObjDocumentParagraphe $paragraphe)
    {

        $em = $this->getDoctrine()->getManager('salarie');

        /** @var ObjDocumentParagrapheElementsRepository $paragrapheElementsRepository */
        $paragrapheElementsRepository = $em->getRepository(ObjDocumentParagrapheElements::class);

        $elements = $paragrapheElementsRepository->findby([
            'idParagraphe' => $paragraphe
        ]);

        foreach ($elements as $element) :
            $em->remove($element);
            $em->flush();

        endforeach;;

    }

    public function getTypeDocument($type)
    {
        $em = $this->getDoctrine()->getManager('salarie');

        $typeDocument = $em->getRepository(ParamTypeDocumentGenere::class)
            ->find($type);

        return $typeDocument;

    }


}