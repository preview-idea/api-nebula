<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 04/06/2018
 * Time: 12:40
 */

namespace SalarieBundle\Controller\Admin;

use Doctrine\Common\Persistence\ManagerRegistry;
use SalarieBundle\Controller\AdminController;
use SalarieBundle\Repository\Param\ParamAdministrationRepository;

class TablesController extends AdminController
{

    /** @var ParamAdministrationRepository $objParamAdminTables */
    protected $objParamAdminTables;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry);
        $this->objParamAdminTables =  $this->manager->getRepository('SalarieBundle:Param\\ParamAdministrationTables');

    }

    /**
     * @param $listParamAdmin
     * @param string $type
     * @return array
     */
    protected function createFormatForTables($listParamAdmin, $type = 'params')
    {

        $list = [];
        foreach ($listParamAdmin as $tableParam) :

            if(!array_key_exists($tableParam['idTable'], $list)) :
                $list[$tableParam['idTable']] = array_slice($tableParam, 0, 3, true);

            endif;

            $list[$tableParam['idTable']]['columns'][] = array_slice($tableParam, 3, 10, true);

        endforeach;

        //order on column liTableDescription
        $arrLiTableDescription = array_column($list, 'liTableDescription');
        array_multisort($arrLiTableDescription, SORT_ASC, $list);

        return [ $type => array_values($list) ];

    }

}