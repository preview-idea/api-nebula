<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 04/06/2018
 * Time: 12:40
 */

namespace SalarieBundle\Controller\Admin\Tables;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use SalarieBundle\Controller\Admin\TablesController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TablesParamController extends TablesController
{

    /**
     * @Rest\View()
     * @Rest\Get("/admin/tables/params")
     * @return object|array
     */
    public function getListTableParamAction()
    {
        $param = $this->createFormatForTables(
            $this->objParamAdminTables->getTablesNotEmptyDescriptionAndUiElements()
        );

        return $param;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/tables/params/{liTableParam}")
     * @param Request $request
     * @return View|array
     */
    public function getTableParamAction(Request $request)
    {
        $param = $this->manager->getRepository(sprintf(
            'SalarieBundle:Param\%s',
            $this->underScoreToCamel($request->get('liTableParam'), true)
        ));

        if (!$param) :
            return $this->tableParamNotFound();
        endif;

        $param = (method_exists($param, 'getAllWithoutJoin')) ? $param->getAllWithoutJoin() : $param->findAll();

        return $param;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/tables/params/{liTableParam}/{idElementParam}", requirements={"idElementParam"="\d+"})
     * @param Request $request
     * @return object
     */
    public function getOneElementTableParamAction(Request $request)
    {

        $param = $this->manager->getRepository(sprintf(
            'SalarieBundle:Param\%s',
            $this->underScoreToCamel($request->get('liTableParam'), true)
        ))->find($request->get('idElementParam'));

        return $param;

    }

    /**
     * @Rest\View()
     * @Rest\Post("/admin/tables/params/{liTableParam}")
     * @param Request $request
     * @return object
     */
    public function createElementTableParamAction(Request $request)
    {
        $tableParamName = $this->underScoreToCamel($request->get('liTableParam'), true);
        $className = sprintf( '\SalarieBundle\Entity\Param\%s', $tableParamName);

        /** @var Object $tableParam */
        $tableParam = new $className;

        $form = $this->createForm(sprintf('SalarieBundle\Form\Param\%sType', $tableParamName), $tableParam);
        $form->submit($request->request->all());

        try {
            if ($form->isValid()) :
                $this->manager->persist($tableParam);
                $this->manager->flush();

                return $tableParam;
            endif;

            return $form;

        } catch (\Exception $e) {
            return $this->traitMessageError($e->getMessage());
        }

    }

    /**
     * @Rest\View()
     * @Rest\Put("/admin/tables/params/{liTableParam}/{idElementParam}", requirements={"idElementParam"="\d+"})
     * @param Request $request
     * @return object
     */
    public function updateElementTableParamAction(Request $request)
    {
        $tableParam = $this->getOneElementTableParamAction($request);
        $tableParamName = $this->underScoreToCamel($request->get('liTableParam'), true);

        $form = $this->createForm(sprintf('SalarieBundle\Form\Param\%sType', $tableParamName), $tableParam);
        $form->submit($request->request->all());

        try {
            if ($form->isValid()) :
                $this->manager->persist($tableParam);
                $this->manager->flush();

                return $tableParam;
            endif;

            return $form;

        } catch (\Exception $e) {
            return $this->traitMessageError($e->getMessage());
        }

    }

    /**
     * @Rest\View()
     * @Rest\Delete("/admin/tables/params/{liTableParam}/{idElementParam}", requirements={"idElementParam"="\d+"})
     * @param Request $request
     * @return View
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeElementTableParamAction(Request $request)
    {
        $tableParam = $this->getOneElementTableParamAction($request);

        $this->manager->remove($tableParam);
        $this->manager->flush();

        return View::create(
            ['message' => 'Suppression effectuée avec succès'],
            Response::HTTP_OK
        );


    }

    /**
     * @return View
     */
    protected function tableParamNotFound()
    {
        return View::create([
            'message' => 'Table param introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }


}