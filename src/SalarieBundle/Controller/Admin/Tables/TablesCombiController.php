<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 04/06/2018
 * Time: 12:40
 */

namespace SalarieBundle\Controller\Admin\Tables;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use SalarieBundle\Controller\Admin\TablesController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TablesCombiController extends TablesController
{

    /**
     * @Rest\View()
     * @Rest\Get("/admin/tables/combi")
     */
    public function getListTableCombiAction()
    {

    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/tables/combi/{liTableCombi}")
     * @param Request $request
     * @return View|array
     */
    public function getTableCombiAction(Request $request)
    {
        $combiRepo = $this->manager->getRepository(sprintf(
            'SalarieBundle:Combi\%s',
            $this->underScoreToCamel($request->get('liTableCombi'), true)
        ));

        if (!$combiRepo) :
            return $this->tableCombiNotFound();
        endif;

        $combi = (method_exists($combiRepo, 'getAllWithoutJoin')) ? $combiRepo->getAllWithoutJoin() : $combiRepo->findAll();

        return $combi;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/tables/combi/{liTableCombi}/{idElementCombi}", requirements={"idElementCombi"="\d+"})
     * @param Request $request
     * @return object
     */
    public function getOneElementTableCombiAction(Request $request)
    {
        $combiElement = $this->manager->getRepository(sprintf(
            'SalarieBundle:Combi\%s',
            $this->underScoreToCamel($request->get('liTableCombi'), true)
        ))->find($request->get('idElementCombi'));

        if (!$combiElement) :
            return $this->tableCombiElementNotFound();
        endif;

        return $combiElement;

    }

    /**
     * @Rest\View()
     * @Rest\Post("/admin/tables/combi/{liTableCombi}")
     * @param Request $request
     * @return object
     */
    public function createElementTableCombiAction(Request $request)
    {
        $tableCombiName = $this->underScoreToCamel($request->get('liTableCombi'), true);
        $className = sprintf( '\SalarieBundle\Entity\Combi\%s', $tableCombiName);

        /** @var Object $tableCombi */
        $tableCombi = new $className;

        $form = $this->createForm(sprintf('SalarieBundle\Form\Combi\%sType', $tableCombiName), $tableCombi);
        $form->submit($request->request->all());

        try {
            if ($form->isValid()) :
                $this->manager->persist($tableCombi);
                $this->manager->flush();

                return $tableCombi;
            endif;

            return $form;

        } catch (\Exception $e) {
            return $this->traitMessageError($e->getMessage());
        }

    }

    /**
     * @Rest\View()
     * @Rest\Put("/admin/tables/combi/{liTableCombi}/{idElementCombi}", requirements={"idElementCombi"="\d+"})
     * @param Request $request
     * @return object
     */
    public function updateElementTableCombiAction(Request $request)
    {
        $tableCombi = $this->getOneElementTableCombiAction($request);

        if ($tableCombi instanceof View) :
            return $tableCombi;
        endif;

        $tableCombimName = $this->underScoreToCamel($request->get('liTableCombi'), true);

        $form = $this->createForm(sprintf('SalarieBundle\Form\Combi\%sType', $tableCombimName), $tableCombi);
        $form->submit($request->request->all());

        try {
            if ($form->isValid()) :
                $this->manager->persist($tableCombi);
                $this->manager->flush();

                return $tableCombi;
            endif;

            return $form;

        } catch (\Exception $e) {
            return $this->traitMessageError($e->getMessage());
        }

    }

    /**
     * @Rest\View()
     * @Rest\Delete("/admin/tables/params/{liTableCombi}/{idElementCombi}", requirements={"idElementCombi"="\d+"})
     * @param Request $request
     * @return View
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeElementTableCombiAction(Request $request)
    {
        $tableCombi = $this->getOneElementTableCombiAction($request);

        if ($tableCombi instanceof View) :
            return $tableCombi;
        endif;

        $this->manager->remove($tableCombi);
        $this->manager->flush();

        return View::create(
            ['message' => 'Suppression effectuée avec succès'],
            Response::HTTP_OK
        );

    }

    /**
     * @return View
     */
    protected function tableCombiNotFound()
    {
        return View::create([
            'message' => 'Table combi introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }

    /**
     * @return View
     */
    protected function tableCombiElementNotFound()
    {
        return View::create([
            'message' => 'Element combi introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }

}