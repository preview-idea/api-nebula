<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 04/06/2018
 * Time: 12:40
 */

namespace SalarieBundle\Controller\Admin;

use Core\Tools\GenerateForm\GenerateForm;
use Doctrine\Common\Persistence\ManagerRegistry;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use SalarieBundle\Controller\AdminController;
use SalarieBundle\Entity\Document\DocumentElementsParam;
use SalarieBundle\Entity\Document\DocumentTablesObjet;
use SalarieBundle\Entity\Document\DocumentTablesParam;
use SalarieBundle\Entity\ObjDocumentGenere;
use SalarieBundle\Entity\ForeignTables\ExtObjDocumentGenere;
use SalarieBundle\Entity\ObjDocumentGrpParagraphe;
use SalarieBundle\Entity\ObjDocumentParagraphe;
use SalarieBundle\Repository\Document\DocumentElementsObjetRepository;
use SalarieBundle\Repository\Document\DocumentElementsParamRepository;
use SalarieBundle\Repository\Document\DocumentTablesObjetRepository;
use SalarieBundle\Repository\Document\DocumentTablesParamRepository;
use SalarieBundle\Repository\ForeignTables\ExtObjDocumentGenereRepository;
use SalarieBundle\Repository\ObjDocumentGenereRepository;
use SalarieBundle\Entity\Document\DocumentElementsObjet;
use SalarieBundle\Repository\ObjDocumentGrpParagrapheRepository;
use SalarieBundle\Repository\ObjDocumentParagrapheRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DocumentController extends AdminController
{

    /** @var ObjDocumentGrpParagrapheRepository $grpParagrapheRepo */
    protected $grpParagrapheRepo;

    /** @var ObjDocumentParagrapheRepository $docParagraphRepo */
    protected $docParagraphRepo;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry);

        $this->docParagraphRepo = $this->manager->getRepository(ObjDocumentParagraphe::class);
        $this->grpParagrapheRepo = $this->manager->getRepository(ObjDocumentGrpParagraphe::class);

    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/document/{idTypeDocument}/objets", requirements={"idTypeDocument"="\d+"})
     * @param Request $request
     * @return object[]
     */
    public function getListObjAction(Request $request)
    {
        /** @var DocumentTablesObjetRepository $tablesObjRepo */
        $tablesObjRepo = $this->manager->getRepository(DocumentTablesObjet::class);

        $objets = $tablesObjRepo->getListTablesObj(
            $request->get('idTypeDocument')
        );

        return $objets;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/document/{idTypeDocument}/objets/{idTablesObj}/elements", requirements={"idTablesObj"="\d+", "idTypeDocument"="\d+"})
     * @param Request $request
     * @return object[]
     */
    public function getVariablesAction(Request $request)
    {

        /** @var DocumentElementsObjetRepository $elementObjRepo */
        $elementObjRepo = $this->manager->getRepository(DocumentElementsObjet::class);

        $variables = $elementObjRepo->getAllByObjet(
            $request->get('idTablesObj'),
            $request->get('idTypeDocument')
        );

        return $variables;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/document/{idTypeDocument}/params", requirements={"idTypeDocument"="\d+"})
     * @param Request $request
     * @return object[]
     */
    public function getListParamAction(Request $request)
    {

        /** @var DocumentTablesParamRepository $tablesParamRepo */
        $tablesParamRepo = $this->manager->getRepository(DocumentTablesParam::class);

        $objets = $tablesParamRepo->getListTablesParam(
            $request->get('idTypeDocument')
        );

        return $objets;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/document/{idTypeDocument}/params/{idTablesParam}/elements", requirements={"idTablesParam"="\d+","idTypeDocument"="\d+"})
     * @param Request $request
     * @return object[]
     */
    public function getElementsParamAction(Request $request)
    {
        /** @var DocumentElementsParamRepository $elementParamRepo */
        $elementParamRepo = $this->manager->getRepository(DocumentElementsParam::class);

        $elements = $elementParamRepo->getAllElementByParam(
            $request->get('idTablesParam'),
            $request->get('idTypeDocument')
        );

        return $elements;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/document/{idTypeDocument}/generate/{idDocument}", requirements={"idTypeDocument"="\d+", "idDocument"="\d+"})
     * @param Request $request
     * @return ObjDocumentGenere[]
     */
    public function documentGenerateAction(Request $request)
    {
        /** @var ObjDocumentGenereRepository $objGenereRepo */
        $objGenereRepo = $this->manager->getRepository(ObjDocumentGenere::class);

        $document = $objGenereRepo->procGenerateDocument(
            $request->get('idDocument'),
            $request->get('idTypeDocument')
        );

        if (key_exists('p_get_document', $document)) :
            $content = $objGenereRepo->getContentByTypeDocument(
                $request->get('idDocument'),
                $request->get('idTypeDocument')
            );

            return $content;
        endif;

        return $document;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/admin/document/{idTypeDocument}/content/{idDocument}", requirements={"idTypeDocument"="\d+", "idDocument"="\d+"})
     * @param Request $request
     * @return View | ExtObjDocumentGenere[]
     */
    public function documentOneAction(Request $request)
    {
        /** @var ExtObjDocumentGenereRepository $extObjGenereRepo */
        $extObjGenereRepo = $this->manager->getRepository(ExtObjDocumentGenere::class);

        $content = $extObjGenereRepo->getContentByTypeDocument(
            $request->get('idDocument'),
            $request->get('idTypeDocument')
        );

        if(!$content) :
            return $this->contentNotFound();
        endif;

        return $content;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/document/form")
     * @return GenerateForm[]
     */
    public function documentFormAction()
    {
        $form = new GenerateForm();
        $form->setObject([
            'obj_document_paragraphe',
            'obj_document_grp_paragraphe',
            'obj_document_paragraphe_elements'
        ]);

        return $form->generateForm($this->manager);
    }

    /**
     * @return View
     */
    protected function contentNotFound()
    {
        return View::create(
            ['message' => 'Contenu introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }
}