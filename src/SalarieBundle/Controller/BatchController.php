<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 18/01/2018
 * Time: 14:31
 */

namespace SalarieBundle\Controller;

use Core\Tools\Batch\CoreBatches;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BatchController extends Controller
{

    /**
     * @Rest\View()
     * @Rest\Get("/batches")
     * @param CoreBatches $batches
     * @return mixed
     */
    public function batchListAction(CoreBatches $batches)
    {
        return $batches->getList();
    }

    /**
     * @Rest\View()
     * @Rest\Get("/batches/current")
     * @param CoreBatches $batches
     * @return mixed
     */
    public function batchCurrentListAction(CoreBatches $batches)
    {
        return $batches->getCurrentList();
    }

    /**
     * @Rest\View()
     * @Rest\Get("/batches/contrat/{idContrat}/current", requirements={"{idContrat}"="\d+"})
     * @param Request $request
     * @param CoreBatches $batches
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function batchCurrentContratAction(Request $request, CoreBatches $batches)
    {
        $batch = $batches->getContratDemandeEnCoursOuEnAttente($request->get('idContrat'));

        if (!$batch) :
            return $this->batchNotFound();
        endif;

        return $batch;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/batches/salarie/{idMatricule}/current", requirements={"{idMatricule}"="\d+"})
     * @param Request $request
     * @param CoreBatches $batches
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function batchCurrentMatriculeAction(Request $request, CoreBatches $batches)
    {
        $batch = $batches->getMatriculeDemandeEnCoursOuEnAttente($request->get('idMatricule'));

        if (!$batch) :
            return $this->batchNotFound();
        endif;

        return $batch;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/batches/nbcurrent")
     * @param CoreBatches $batches
     * @return mixed
     */
    public function batchNbCurrentAction(CoreBatches $batches)
    {
        $nbCurrent = $batches->getNbCurrent();

        if (!isset($nbContrat['message']))
            return View::create([
                'nbCurrentBatch' => $nbCurrent],
                Response::HTTP_OK
            );

        return $nbCurrent;
    }

    /**
     * @Rest\View()
     * @Rest\Post("/batches")
     * @param Request $request
     * @param CoreBatches $batches
     * @return object
     */
    public function batchNewAction(Request $request, CoreBatches $batches)
    {
        return $batches->create($request);
    }

    /**
     * @Rest\View()
     * @Rest\Patch("/batches/{idTraitement}", requirements={"{idTraitement}"="\d+"})
     * @param Request $request
     * @param CoreBatches $batches
     * @return object
     */
    public function batchPatchAction(Request $request, CoreBatches $batches)
    {
        return $batches->update($request);
    }

    /**
     * @Rest\View()
     * @Rest\Get("/batches/{idTraitement}", requirements={"{idTraitement}"="\d+"})
     * @param Request $request
     * @param CoreBatches $batches
     * @return mixed
     */
    public function batchOneAction(Request $request, CoreBatches $batches)
    {
        return $batches->getOne($request->get('idTraitement'));
    }

    /**
     * @return View
     */
    protected function batchNotFound()
    {
        return View::create(
            ['message' => 'Batch introuvable'],
            Response::HTTP_NOT_FOUND
        );
    }

}