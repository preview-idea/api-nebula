<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 23/02/2018
 * Time: 11:05
 */

namespace SalarieBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CombiController extends Controller
{

    /**
     * @Rest\View()
     * @Rest\Post("/combi/{combi}")
     * @param Request $request
     * @return mixed
     */
    public function salarieCombiAction(Request $request)
    {

        $combi = $request->get('combi');
        $filter = $request->get('filter');

        if (!$combi) :
            return  View::create(
                ["message" => "Combi introuvable"],
                Response::HTTP_NOT_FOUND
            );

        endif;

        $em = $this->getDoctrine()->getManager('salarie');

        if (preg_match('/^view_/', $combi)) :

            $repo = $em->getRepository(sprintf(
                'SalarieBundle:Views\%s',
                $this->underScoreToCamel($combi, true)
            ));

            $data = $repo->getViewCombi($filter);

        else :

            $repo = $em->getRepository(sprintf(
                'SalarieBundle:Combi\Combi%s',
                $this->underScoreToCamel($combi, true)
            ));

            $data = $repo->getCombi($filter);

        endif;

        return $data;

    }

    private function underScoreToCamel($string, $capitalizeFirst = false)
    {
        $str = str_replace('_', '', ucwords($string, '_'));

        if (!$capitalizeFirst) :
            $str[0] = strtolower($str[0]);

        endif;

        return $str;
    }
}