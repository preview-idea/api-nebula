<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 13/06/2018
 * Time: 13:08
 */

namespace SalarieBundle\Controller;

use Core\Tools\GenerateForm\GenerateForm;
use Core\Tools\MessageDB\MessageDB;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    /** @var EntityManager $manager */
    protected $manager;

    /** @var GenerateForm $form */
    protected $form;

    /**
     * AdminController constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        if (!$this->manager instanceof EntityManager) :
            $this->manager = $managerRegistry->getManager('salarie');

        endif;

        // Form Generate
        if (!$this->form instanceof GenerateForm) :
            $this->form = new GenerateForm();

        endif;

    }

    /**
     * @param $string
     * @param bool $capitalizeFirst
     * @return mixed
     */
    protected function underScoreToCamel($string, $capitalizeFirst = false)
    {
        $str = str_replace('_', '', ucwords($string, '_'));

        if (!$capitalizeFirst) :
            $str[0] = strtolower($str[0]);

        endif;

        return $str;
    }

    /**
     * @param $message
     * @return View
     */
    protected function traitMessageError($message)
    {

        /** @var MessageDB $msg_db */
        $msg_db = $this->get('exception_message_db');
        $msg_db->traitMessageDB($message);

        return View::create(
            ['bdd_msg' => $msg_db->getMsgError() . $msg_db->getMsgDetail()],
            RESPONSE::HTTP_OK
        );
    }

}