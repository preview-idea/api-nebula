<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 23/02/2018
 * Time: 11:05
 */

namespace SalarieBundle\Controller;

use Core\Entity\Utilisateurs;
use Core\Repository\UtilisateursRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ParamsController extends Controller
{

    /**
     * @Rest\View()
     * @Rest\Post("/params/{param}")
     * @param Request $request
     * @return mixed
     */
    public function salarieParamsAction(Request $request)
    {

        $param = $request->get('param');
        $filter = $request->get('filter');

        if (!$param) :
            return  View::create(
                ["message" => "Paramètre introuvable"],
                Response::HTTP_NOT_FOUND
        );

        endif;

        $em = $this->getDoctrine()->getManager('salarie');

        if (preg_match('/^view_/', $param)) :

            $repo = $em->getRepository(sprintf(
                'SalarieBundle:Views\%s',
                $this->underScoreToCamel($param, true)
            ));

            return $repo->getViewParams($filter);

        endif;

        $repo = $em->getRepository(sprintf(
            'SalarieBundle:Param\Param%s',
            $this->underScoreToCamel($param, true)
        ));

        $result = $repo->getParams($filter);

        if (!preg_match('/ParamPositionnementposte$/', $repo->getClassName())) :
            return $result;
        endif;

        // Verify if user is structure and has access to view 'Structure' in the list of positionnementposte
        /** @var Utilisateurs $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        /** @var UtilisateursRepository $objUserRepo */
        $objUserRepo = $this->getDoctrine()->getManager()
            ->getRepository('CoreNebula:Utilisateurs');

        $structure = $objUserRepo->getDroitUtilisateurStructure($user->getIdUtilisateur());
        $result = key_exists('isQualifcontratStructure', $structure) && (int)$structure['isDroitVoirStructure'] !== 1 ?
            array_splice($result, array_search('Structure', $result), -1) : $result;

        return $result;

    }

    private function underScoreToCamel($string, $capitalizeFirst = false)
    {
        $str = str_replace('_', '', ucwords($string, '_'));

        if (!$capitalizeFirst) :
            $str[0] = strtolower($str[0]);

        endif;

        return $str;
    }

}