<?php

namespace SalarieBundle\Repository\Param;

use Doctrine\ORM\EntityRepository;

class ParamActiviteRepository extends EntityRepository
{
    public function getParams($filter)
    {
        $query = $this->createQueryBuilder("a")
            ->select("
                a.idActivite,
                a.liActivite
            ")
            ->where("a.liActivite LIKE :filter")
            ->andWhere("a.dtDebutActif IS NULL  OR a.dtDebutActif <= CURRENT_DATE()")
            ->andWhere("a.dtFinActif IS NULL OR a.dtFinActif >= CURRENT_DATE()")
            ->setParameter('filter', '%'.$filter.'%')
            ->orderBy('a.liActivite', 'ASC')
            ->getQuery();

        return $query->getResult();

    }

    public function getAllWithoutJoin()
    {
        $query = $this->createQueryBuilder("a")
            ->select("
                a.idActivite,
                a.liActivite,
                a.idActiviteAx,
                a.dtDebutActif,
                a.dtFinActif,
                CASE
                    WHEN a.dtFinActif IS NULL OR a.dtFinActif >= CURRENT_DATE()
                    THEN 'Actif' ELSE 'Inactif'
                END as liEtat
            ")
            ->orderBy('a.idActivite', 'ASC')
            ->getQuery();

        return $query->getResult();

    }
}

