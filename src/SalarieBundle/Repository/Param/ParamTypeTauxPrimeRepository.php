<?php

namespace SalarieBundle\Repository\Param;

use Doctrine\ORM\EntityRepository;

class ParamTypeTauxPrimeRepository extends EntityRepository
{
    public function getParams($filter)
    {
        $query = $this->createQueryBuilder("t")
            ->select("
                t.idTypeTauxprime,
                t.liTypeTauxprime
            ")
            ->where("t.liTypeTauxprime LIKE :filter")
            ->andWhere("t.dtDebutActif IS NULL  OR t.dtDebutActif <= CURRENT_DATE()")
            ->andWhere("t.dtFinActif IS NULL OR t.dtFinActif >= CURRENT_DATE()")
            ->orderBy('t.liTypeTauxprime', 'ASC')
            ->setParameter('filter', '%'.$filter.'%')
            ->getQuery();

        return $query->getResult();

    }

    public function getAllWithoutJoin()
    {
        $query = $this->createQueryBuilder("s")
            ->select("
                s.idTypeTauxprime, 
                s.liTypeTauxprime,
                s.dtDebutActif,
                s.dtFinActif,
                CASE
                    WHEN s.dtFinActif IS NULL OR s.dtFinActif >= CURRENT_DATE()
                    THEN 'Actif' ELSE 'Inactif'
                END as liEtat
            ")
            ->orderBy('s.idTypeTauxprime', 'ASC')
            ->getQuery();

        return $query->getResult();

    }
}
