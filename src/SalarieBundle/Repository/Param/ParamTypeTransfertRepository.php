<?php

namespace SalarieBundle\Repository\Param;

use Doctrine\ORM\EntityRepository;

class ParamTypeTransfertRepository extends EntityRepository
{
    public function getParams($filter)
    {
        $query = $this->createQueryBuilder("t")
            ->select("
                t.idTypeTransfert,
                t.liTypeTransfert
            ")
            ->where("t.liTypeTransfert LIKE :filter")
            ->andWhere("t.dtDebutActif IS NULL  OR t.dtDebutActif <= CURRENT_DATE()")
            ->andWhere("t.dtFinActif IS NULL OR t.dtFinActif >= CURRENT_DATE()")
            ->orderBy('t.liTypeTransfert', 'ASC')
            ->setParameter('filter', '%'.$filter.'%')
            ->getQuery();

        return $query->getResult();

    }

    public function getAllWithoutJoin()
    {
        $query = $this->createQueryBuilder("s")
            ->select("
                s.idTypeTransfert, 
                s.liTypeTransfert,
                s.dtDebutActif,
                s.dtFinActif,
                CASE
                    WHEN s.dtFinActif IS NULL OR s.dtFinActif >= CURRENT_DATE()
                    THEN 'Actif' ELSE 'Inactif'
                END as liEtat
            ")
            ->orderBy('s.idTypeTransfert', 'ASC')
            ->getQuery();

        return $query->getResult();

    }
}
