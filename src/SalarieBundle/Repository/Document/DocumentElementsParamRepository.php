<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 13/06/2018
 * Time: 16:07
 */

namespace SalarieBundle\Repository\Document;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class DocumentElementsParamRepository extends EntityRepository
{

    public function getAllElementByParam($tableParam, $typeDocument) {

        $query = $this->createQueryBuilder("e")
            ->select("
                e.idElementParam,
                e.liElementParam
            ")
            ->where('e.idTableParam = :tableParam AND e.idTypeDocument = :type')
            ->setParameter('type', $typeDocument, Types::INTEGER)
            ->setParameter('tableParam', $tableParam, Types::INTEGER)
            ->orderBy('e.liElementParam', 'ASC')
            ->getQuery();

        return $query->getResult();

    }
}