<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 13/06/2018
 * Time: 16:07
 */

namespace SalarieBundle\Repository\Document;

use Doctrine\ORM\EntityRepository;

class DocumentElementsObjetRepository extends EntityRepository
{

    public function getAllByObjet($tableObj, $document) {

        $query = $this->createQueryBuilder("e")
            ->select("
                e.liColonneObjet,
                e.liColonneObjetDescription
            ")
            ->where('e.idTableObjet = :tableObj AND e.idTypeDocument = :type')
            ->setParameter('tableObj', $tableObj)
            ->setParameter('type', $document)
            ->orderBy('e.idColonneObjet', 'ASC')
            ->getQuery();

        return $query->getResult();

    }
}