<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 13/06/2018
 * Time: 16:07
 */

namespace SalarieBundle\Repository\Document;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

/**
 * Class DocumentTablesObjetRepository
 * @package SalarieBundle\Repository\Document
 */
class DocumentTablesObjetRepository extends EntityRepository
{

    public function getListTablesObj($idTypeDocument) {

        $query = $this->createQueryBuilder("t")
            ->select("
                t.idTableObjet,
                t.liTableObjetDescription
            ")
            ->where('t.idTypeDocument = :type')
            ->setParameter('type', $idTypeDocument, Types::INTEGER)
            ->orderBy('t.liTableObjet', 'ASC')
            ->getQuery();

        return $query->getResult();

    }
}