<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 13/06/2018
 * Time: 16:07
 */

namespace SalarieBundle\Repository\Document;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class DocumentTablesParamRepository extends EntityRepository
{

    public function getListTablesParam($idTypeDocument) {

        $query = $this->createQueryBuilder("p")
            ->select("
                p.idTableParam,
                p.liTableParamDescription
            ")
            ->where('p.isInGencontrat = :bit AND p.liTableParamDescription IS NOT NULL AND p.idTypeDocument = :type')
            ->setParameter('type', $idTypeDocument, Types::INTEGER)
            ->setParameter('bit', 1)
            ->orderBy('p.liTableParamDescription', 'ASC')
            ->getQuery();

        return $query->getResult();

    }
}