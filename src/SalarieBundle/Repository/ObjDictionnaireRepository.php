<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 23/01/2018
 * Time: 17:59
 */

namespace SalarieBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ObjDictionnaireRepository extends EntityRepository
{
    public function getAllElements(array $objects)
    {

        $query = $this->createQueryBuilder("d")
            ->select("
                d.isChampNull,
                d.liInfobulleUi,
                d.liTableRef,
                d.liTailleChamp,
                d.liBlockForm,
                d.liTypeElementUi,
                d.liDescriptionChamp,
                d.liNomChamp
            ")
            ->where("d.liTypeElementUi IS NOT NULL AND d.liObjet IN (:objects)")
            ->setParameter('objects', $objects)
            ->orderBy('d.idOrdreElementUi', 'ASC')
            ->getQuery();

        return $query->getResult();

    }

    public function getAllTablesParam($filter)
    {
        $query = $this->createQueryBuilder("p")
            ->select("
                p.liTableRef,
                p.liDescriptionChamp
            ")
            ->where("p.liTableRef IS NOT NULL")
            ->andWhere("p.liObjet LIKE :filter")
            ->setParameter('filter', '%'.$filter.'%')
            ->orderBy('p.liDescriptionChamp', 'ASC')
            ->getQuery();

        return $query->getResult();
    }
}
