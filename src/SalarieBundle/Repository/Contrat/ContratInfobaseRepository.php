<?php

namespace SalarieBundle\Repository\Contrat;

use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\Types\Types;

class ContratInfobaseRepository extends EntityRepository
{
    public function getDateCreationAndMaj($idContrat, $isAvenant)
    {
        $isAvenant ? $isAvenant = '1' : $isAvenant = '0';

        $query = $this->createQueryBuilder("s")
            ->select("
                s.dtCreation,
                s.dtModification
            ")
            ->andWhere('s.idContrat = :idContrat')
            ->andWhere('s.isActif = :isActif')
            ->andWhere('s.isAvenant = :isAvenant')
            ->setParameter('idContrat', $idContrat,Types::INTEGER)
            ->setParameter('isActif', '1')
            ->setParameter('isAvenant', $isAvenant)
            ->getQuery();

        return $query->getOneOrNullResult();

    }
}
