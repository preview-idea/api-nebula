<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 23/01/2018
 * Time: 17:59
 */

namespace SalarieBundle\Repository;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class ObjDocumentGenereRepository extends EntityRepository
{
    public function getContentByTypeDocument($idDocument, $idType)
    {

        $query = $this->createQueryBuilder("g")
            ->select('grp.liGrpParagraphe, g.liContenu')
            ->leftJoin('g.idGrpParagraphe', 'grp')
            ->where('g.liContenu IS NOT NULL AND g.idGen = :document AND grp.idTypeDocument = :type')
            ->setParameter('document', $idDocument, Types::INTEGER)
            ->setParameter('type', $idType, Types::INTEGER)
            ->orderBy('g.nbOrdreGrpParagraphe, g.nbOrdreParagraphe', 'ASC')
            ->getQuery();

        return $query->getResult();

    }

    public function procGenerateDocument($idDocument, $idType)
    {
        $connection = $this->_em->getConnection();

        if (!$idDocument) :
            return [
                'message' => 'Désolé il manque l\'id du document',
                'status' => '404'
            ];

        else :

            $sql = 'SELECT  public.p_get_document(:type,:document)' ;

            try {
                $query = $connection->prepare($sql);
                $query->bindValue(':document', $idDocument, \PDO::PARAM_INT);
                $query->bindValue(':type', $idType, \PDO::PARAM_INT);
                $query->execute();
                $result = $query->fetch(\PDO::FETCH_ASSOC);

                return $result;


            } catch (DBALException $e) {
                return ['message' =>  $e->getMessage()];
            }

        endif;
    }

}
