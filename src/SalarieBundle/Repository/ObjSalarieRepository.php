<?php

namespace SalarieBundle\Repository;

use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use SalarieBundle\Entity\Views\Salarie\ViewActifInactifObjSalarie;
use SalarieBundle\Entity\Views\Salarie\ViewDernierContratObjSalarie;
use SalarieBundle\Entity\Views\Salarie\ViewSalarieInactif;
use SalarieBundle\Repository\Views\Salarie\ViewActifInactifObjSalarieRepository;
use SalarieBundle\Repository\Views\Salarie\ViewDernierContratObjSalarieRepository;
use SalarieBundle\Repository\Views\Salarie\ViewSalarieInactifRepository;

class ObjSalarieRepository extends EntityRepository
{

    /**
     * @param $filter
     * @return array
     */
    public function getListActif($filter)
    {
        $query = $this->createQueryBuilder('s')
            ->select("
                s.idMatricule,
                CASE
                    WHEN (s.liNomUsage <> s.liNom) THEN CONCAT(s.liNomUsage,' ',s.liPrenom,' (',s.liNom,')',' - ', s.idMatricule)
                    ELSE CONCAT(s.liNomUsage,' ',s.liPrenom,' - ',s.idMatricule)
                END as liNomComplet
            ")
            ->where("LOWER(CONCAT(s.liNomUsage,' ',s.liPrenom,' ',s.liNom,' ',s.idMatricule)) LIKE :filter")
            ->setParameter('filter', '%'.$filter['params'].'%')
            ->getQuery();

        return $query->getResult();
    }

    /**
     * @return array
     */
    public function getListHorsCee()
    {
        $query = $this->createQueryBuilder('s')
            ->select("
                s.idMatricule,
                CASE
                    WHEN (s.liNomUsage <> s.liNom) THEN CONCAT(s.liNomUsage,' ',s.liPrenom,' (',s.liNom,')')
                    ELSE CONCAT(s.liNomUsage,' ',s.liPrenom)
                END as liNomComplet,
                a.liAgence,
                t.liTypePiece,
                s.liNumeroPiece,
                s.dtDebutValidite,
                s.dtFinValidite,
                CASE
                    WHEN (s.dtFinValidite IS NULL OR s.dtFinValidite >= CURRENT_DATE()) AND s.dtDebutValidite <= CURRENT_DATE()
                    THEN 'Actif' ELSE 'Inactif'
                END as liEtat 
            ")
            ->leftJoin('s.idAgence', 'a')
            ->leftJoin('s.idTypePiece', 't')
            ->leftJoin('s.idPaysNationalite', 'p')
            ->where("p.isCee = :isCee")
            ->setParameter('isCee', 0)
            ->getQuery();

        return $query->getResult();
    }

    /**
     * @param $matricule
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneToInfo($matricule)
    {

        $query = $this->createQueryBuilder("s")
            ->select("
                s.idMatricule,
                s.liPrenom,
                s.liNom,
                s.liMail,
                s.liTelephone1,
                s.liNomUsage,
                s.liVilleNaissance,
                s.liCheminPhoto,
                s.dtNaissance,
                s.idNirDef,
                nat.isCee,
                s.dtDebutValidite,
                s.dtFinValidite
            ")
            ->leftJoin('s.idPaysNationalite', 'nat')
            ->where('s.idMatricule = :matricule')
            ->setParameter('matricule', $matricule, Types::INTEGER)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * @param $filter
     * @return array
     */
    public function viewListAllSalaries($filter)
    {
        /** @var ViewActifInactifObjSalarieRepository $viewSalarieActifInactif */
        $viewSalarieActifInactif = $this->_em->getRepository(ViewActifInactifObjSalarie::class);

        return $viewSalarieActifInactif->viewListSalariesActifInactif($filter);

    }

    /**
     * @param $filter
     * @return array|mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function viewNomComplet($filter)
    {
        /** @var ViewActifInactifObjSalarieRepository $viewSalarieActifInactif */
        $viewSalarieActifInactif = $this->_em->getRepository(ViewActifInactifObjSalarie::class);

        return $viewSalarieActifInactif->viewOneSalarieActifInactif($filter);

    }

    public function getSalarieByNir($idNirDef)
    {

        $connect = $this->_em->getConnection();
        $sql = $connect->createQueryBuilder()
            ->select('
                v_s.id_matricule as "idMatricule",
                v_s.li_prenom as "liPrenom",
                v_s.li_nom as "liNom",
                v_s.id_nir as "idNir",
                v_s.is_nir_prov as "isNirProv",
                v_s.li_chemin_photo as "liCheminPhoto",
                v_s.li_pays as "liPays",
                v_s.li_ville_naissance as "liVilleNaissance",
                v_s.li_etat as "liEtat"
            ')
            ->from('v_salarie_nir_utilise', 'v_s')
            ->where('v_s.id_nir = :idNir')
            ->orderBy('v_s.id_matricule', 'ASC')
            ->getSQL();

        try {
            $query = $connect->prepare($sql);
            $query->bindValue(':idNir', $idNirDef, Types::INTEGER);
            $query->execute();

            return $query->fetchAllAssociative();

        } catch (\Doctrine\DBAL\Exception $e) {
            return ['message' => $e->getMessage() ];

        } catch (Exception $e) {
            return ['message' => $e->getMessage() ];

        }

    }

    public function viewListInactifByFilter($filter)
    {

        /** @var ViewSalarieInactifRepository $viewSalarieInactif */
        $viewSalarieInactif = $this->_em->getRepository(ViewSalarieInactif::class);

        return $viewSalarieInactif->viewListSalariesInactifByFilter($filter);

    }

    /**
     * @param $idMatricule
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function viewGetOneInactif($idMatricule)
    {
        /** @var ViewSalarieInactifRepository $viewSalarieInactif */
        $viewSalarieInactif = $this->_em->getRepository(ViewSalarieInactif::class);

        return $viewSalarieInactif->viewGetOneSalarieInactif($idMatricule);
    }

    public function getNbSalarieActif()
    {

        /** @var ViewDernierContratObjSalarieRepository $viewSalarieDernierContrat */
        $viewSalarieDernierContrat = $this->_em->getRepository(ViewDernierContratObjSalarie::class);

        return count($viewSalarieDernierContrat->viewListDernierContrat());

    }


}