<?php

namespace SalarieBundle\Repository\Combi;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class CombiCategorieCoeffRepository extends EntityRepository
{
    public function getCombi($filter)
    {

        $query = $this->createQueryBuilder("c")
            ->select("
                coe.idCoeffcontrat,
                coe.liCoeffcontrat,
                c.nbTauxHoraire,
                c.nbSalaireMensuel,
                c.liPosition,
                c.liNiveau,
                c.liEchelon
            ")
            ->leftJoin('c.idCoeffcontrat', 'coe')
            ->where("c.idCategorieemploye = :categorie")
            ->andWhere("c.idConventioncollective = :convention")
            ->orderBy('coe.liCoeffcontrat', 'ASC')
            ->setParameter('categorie', $filter['categorie'], Types::INTEGER)
            ->setParameter('convention', $filter['convention'], Types::INTEGER)
            ->getQuery();

        return $query->getResult();

    }

    public function getAllWithoutJoin()
    {
        $query = $this->createQueryBuilder("c")
            ->select("
                c.idLigneCategorieCoeff,
                c.liNiveau,
                c.liEchelon,
                c.liPosition,
                c.nbTauxHoraire,
                c.liNiveauRhpi,
                c.liPositionRhpi,
                c.nbSalaireMensuel,
                c.liCodeComete,
                ca.idCategorieemploye,
                ca.liCategorieemploye,
                co.liCoeffcontrat as liCoeffcontratApresPre,
                cc.idCoeffcontrat,
                cc.liCoeffcontrat,
                cco.idConventioncollective,
                cco.liConventioncollective
            ")
            ->leftJoin("c.idCategorieemploye","ca")
            ->leftJoin("c.idCoeffcontratApresPre","co")
            ->leftJoin("c.idCoeffcontrat","cc")
            ->leftJoin("c.idConventioncollective","cco")
            ->orderBy('c.idLigneCategorieCoeff', 'ASC')
            ->getQuery();

        return $query->getResult();

    }
}
