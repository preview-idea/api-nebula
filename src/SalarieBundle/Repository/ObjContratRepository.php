<?php

namespace SalarieBundle\Repository;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class ObjContratRepository extends EntityRepository
{

    /**
     * @param $idMatricule integer
     * @return array
     */
    public function getCurrentContratsByIdMatricule($idMatricule)
    {
        $query = $this->createQueryBuilder('c')
            ->select("
                c.idContrat,
                t.liTypecontrat,
                q.liQualifcontrat,
                p.idPositionnementposte,
                c.dtDebutcontrat,
                c.dtFincontrat,
                c.dtFincontratPrevue,
                ag.idAgence,
                ag.liAgence,
                c.nbSalaireBase,
                s.idSocieteAx,
                c.dtAncienneteBranche
            ")
            ->leftJoin('c.idQualifcontrat', 'q')
            ->leftJoin('c.idTypecontrat', 't')
            ->leftJoin('c.idPositionnementposte', 'p')
            ->leftJoin('c.idAgence', 'ag')
            ->leftJoin('c.idSociete', 's')
            ->where('c.idMatricule = :matricule AND c.isValid = :bit')
            ->andWhere('c.dtFincontrat IS NULL OR c.dtFincontrat <= CURRENT_DATE()')
            ->setParameter('matricule', $idMatricule)
            ->setParameter('bit', 1)
            ->orderBy('c.dtDebutcontrat', 'DESC')
            ->getQuery();

        return $query->getResult();

    }

    public function getListOnValidation($idMatriculeMaj)
    {
        $query = $this->createQueryBuilder("c")
            ->select("
                c.idContrat,
                s.idMatricule,
                CASE
                    WHEN (s.liNomUsage <> s.liNom) THEN CONCAT(s.liNomUsage,' ',s.liPrenom,' (',s.liNom,')')
                    ELSE CONCAT(s.liNomUsage,' ',s.liPrenom)
                END as liNomComplet,
                t.liTypecontrat,
                n.liNaturecontrat,
                q.liQualifcontrat,
                c.dtDebutcontrat,
                c.dtFincontrat,
                c.dtFincontratPrevue,
                CONCAT(sno.liNom,' ',sno.liPrenom) as liNomSignataire
            ")
            ->leftJoin('c.idQualifcontrat', 'q')
            ->leftJoin('c.idNaturecontrat', 'n')
            ->leftJoin('c.idMatricule', 's')
            ->leftJoin('c.idTypecontrat', 't')
            ->leftJoin('c.idSignataire', 'sign')
            ->leftJoin('SalarieBundle:Salarie\\SalarieNomprenom', 'sno', 'WITH',
                'sno.idMatricule = sign.idMatricule AND sno.isActif = :actif')
            ->where("c.idMatriculeMaj = :idMatriculeMaj AND c.isValid = :bit")
            ->setParameter('idMatriculeMaj', $idMatriculeMaj)
            ->setParameter('actif', 1, Types::INTEGER)
            ->setParameter('bit', 0)
            ->orderBy('c.idContrat', 'ASC')
            ->getQuery();

        return $query->getResult();
    }

    public function majSalaire($dateApplicationConvention)
    {
        $connection = $this->_em->getConnection();

        $sql = 'SELECT public.p_maj_contrat_ecart_convention(:dateApplicationConvention) AS nbMaj';

        try {
            $query = $connection->prepare($sql);
            $query->bindValue(':dateApplicationConvention', $dateApplicationConvention, \PDO::PARAM_STR);
            $query->execute();

            return $query->fetchAssociative();

        } catch (Exception $e) {
            return ['message' =>  $e->getMessage()];
        }

    }
}
