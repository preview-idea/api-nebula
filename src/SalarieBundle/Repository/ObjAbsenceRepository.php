<?php

namespace SalarieBundle\Repository;

use Core\Custom\Types\BitType;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class ObjAbsenceRepository extends EntityRepository
{
    /**
     * @param $idMatricule
     * @return int|mixed|string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCurrentAbsence($idMatricule)
    {
        $query = $this->createQueryBuilder('ab')
            ->select("
                ab.idAbsence,
                typ.idTypeAbsence,
                typ.liTypeAbsence,
                ab.dtAbsence,
                sa.idMatricule,
                ab.isActif
            ")
            ->leftJoin('ab.idTypeAbsence', 'typ')
            ->leftJoin('ab.idMatricule', 'sa')
            ->where('sa.idMatricule = :idMatricule')
            ->andWhere('ab.isActif = :isActif')
            ->setParameter('idMatricule', $idMatricule, Types::INTEGER)
            ->setParameter('isActif', 1, BitType::BIT)
            ->orderBy('ab.dtAbsence', 'DESC')
            ->setMaxResults(1)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

}
