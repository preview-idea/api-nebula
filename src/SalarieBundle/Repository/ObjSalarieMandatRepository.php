<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 14/03/2019
 * Time: 16:22
 */

namespace SalarieBundle\Repository;

use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class ObjSalarieMandatRepository extends EntityRepository
{
    public function updateCoordonneeIrp($idMandat)
    {

        $connect = $this->_em->getConnection();
        $sql = $connect->createQueryBuilder()
            ->select(
                'public.p_maj_coordonnee_irp(:idMandat) as "isOk"'
            );

        try {
            $query = $connect->prepare($sql);
            $query->bindValue(':idMandat', $idMandat, Types::INTEGER);
            $query->execute();

            return  $query->fetchAllAssociative();

        } catch (\Doctrine\DBAL\Exception $e) {
            return ['message' =>  $e->getMessage()];

        } catch (Exception $e) {
            return ['message' =>  $e->getMessage()];
        }

    }
}