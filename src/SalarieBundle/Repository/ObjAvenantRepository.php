<?php

namespace SalarieBundle\Repository;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;

class ObjAvenantRepository extends EntityRepository
{

    public function getListOnValidation($idMatriculeMaj)
    {

        $query = $this->createQueryBuilder("a")
            ->select("
                s.idMatricule,
                c.idContrat,
                a.idContrat as idAvenant,
                CASE
                    WHEN (s.liNomUsage <> s.liNom) THEN CONCAT(s.liNomUsage,' ',s.liPrenom,' (',s.liNom,')')
                    ELSE CONCAT(s.liNomUsage,' ',s.liPrenom)
                END as liNomComplet,
                t.liTypecontrat,
                n.liNaturecontrat,
                q.liQualifcontrat,
                a.dtDebutAvenant,
                a.dtFinAvenant,
                CONCAT(sno.liNom,' ',sno.liPrenom) as liNomSignataire
            ")
            ->leftJoin('a.idQualifcontrat', 'q')
            ->leftJoin('a.idNaturecontrat', 'n')
            ->leftJoin('a.idMatricule', 's')
            ->leftJoin('a.idContratOrigine', 'c')
            ->leftJoin('a.idTypecontrat', 't')
            ->leftJoin('a.idSignataire', 'sign')
            ->leftJoin('SalarieBundle:Salarie\\SalarieNomprenom', 'sno', 'WITH',
                'sno.idMatricule = sign.idMatricule AND sno.isActif = :actif')
            ->where("a.idMatriculeMaj = :idMatriculeMaj AND a.isValid = :bit")
            ->setParameter('idMatriculeMaj', $idMatriculeMaj)
            ->setParameter('actif', 1, Types::INTEGER)
            ->setParameter('bit', 0)
            ->orderBy('a.idLigneAvenant', 'ASC')
            ->getQuery();

        return $query->getResult();
    }

}
