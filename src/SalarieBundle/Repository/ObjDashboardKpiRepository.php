<?php

namespace SalarieBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ObjDashboardKpiRepository extends EntityRepository{


    public function getKpiEntretien()
    {
        $query = $this->createQueryBuilder("a")
            ->select("a"
            )
            ->where('a.liVueTable like :liVueTable')
            ->setParameter('liVueTable', "%Entretien%")
            ->orderBy("a.nbOrdre", "ASC")
            ->getQuery();

        return $query->getResult();
    }

}
