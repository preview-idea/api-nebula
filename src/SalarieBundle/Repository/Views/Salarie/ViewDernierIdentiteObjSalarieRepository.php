<?php

namespace SalarieBundle\Repository\Views\Salarie;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * ViewDernierContratObjSalarieRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ViewDernierIdentiteObjSalarieRepository extends EntityRepository
{
    public function getNbHorsCeeExpire()
    {

        $query = $this->createQueryBuilder("v_s")
            ->select("COUNT(v_s)")
            ->where('v_s.liEtat = :etat')
            ->setParameter(':etat', 'Inactive')
            ->getQuery();

        try {
            return $query->getSingleScalarResult();
        } catch (NoResultException $e) {
            return ['message' => $e->getMessage() ];
        } catch (NonUniqueResultException $e) {
            return ['message' => $e->getMessage() ];
        }

    }

}
