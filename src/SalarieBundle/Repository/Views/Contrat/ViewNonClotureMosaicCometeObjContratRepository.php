<?php

namespace SalarieBundle\Repository\Views\Contrat;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class ViewNonClotureMosaicCometeObjContratRepository extends EntityRepository
{
    public function getNbNonClotureContrat()
    {

        $query = $this->createQueryBuilder('c')
            ->select('COUNT(c)')
            ->join('c.idAgence', 'age')
            ->getQuery();

        try {
            return $query->getSingleScalarResult();

        } catch (NoResultException $e) {
            return ['message' => $e->getMessage() ];

        } catch (NonUniqueResultException $e) {
            return ['message' => $e->getMessage() ];
        }

    }

    public function getNonClotureContrat()
    {

        $query = $this->createQueryBuilder('c')
            ->select('
                c.idContrat,
                c.idMatricule,
                c.liNomComplet,
                c.liTypecontrat,
                age.liAgence,
                c.liQualifcontrat,
                c.dtFincontrat,
                c.isClotureMosaic,
                c.isClotureComete,
                c.isActif,
                c.dtDebutcontrat
            ')
            ->join('c.idAgence', 'age')
            ->orderBy('c.dtFincontrat', 'ASC')
            ->getQuery();

        return $query->getResult();
    }
}
